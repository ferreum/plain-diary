# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.4.0] - 2025-02-02

### Added
- Support positive reminder offsets for reminders after event time
  (`@12:34 +5m`).

### Changed
- Avoid waking up device on day change by extending wakeup window.

### Fixed
- The next-reminder button now shows up if reminder time is within 12 hour
  range, even if event time is outside 12 hour range.
- Issue where quick notes were added to undo history (and potentially
  lost) when using a reminder notification to open the page.
- Improved text update responsiveness when using undo/redo.
- Wrong option description on SDK 34 and above, where notifications can always
  be dismissed.

## [3.3.0] - 2024-12-19

### Changed
- Raised target platform to Android 15 (SDK 35).
  - This required changing the quick note notification to keep it as compact as
    possible. This can also improve usability on the lock screen, because it
    may not need expansion of the notification shade.
- Group reminder notification channels in notification settings.
- Next-reminder button shows exclamation mark, if important reminder is set
  while full-screen reminders are disabled. Click it to enable full-screen
  reminders.
- Hide "Skip reminder" action of upcoming reminder notifications on the lock screen.

### Fixed
- New page time preference sometimes not respected by time toolbar button.

## [3.2.2] - 2024-12-12

### Fixed
- Upcoming important reminders were dismissable on the lock screen if privacy
  settings allow showing notification content. This now requires device unlock.

## [3.2.1] - 2024-12-10

### Fixed
- Long standing issue where editor elements would sometimes freeze.

## [3.2.0] - 2024-11-29

### Changed
- Lots of optimizations for improved responsiveness and reduced battery usage.

### Added
- Option to disable emoji support-library for improved editor performance.
- Option to disable suggestions in editor for improved performance.
- Entry to system notification settings screen to open app settings menu.

### Fixed
- App sometimes getting frozen before important reminder alarm starts, if
  fullscreen notification permission was not granted on Android 15.
- Important reminders not working correctly if app was dismissed from recents.
- Wrong accessibility descriptions on some toolbar buttons.

## [3.1.1] - 2024-09-26

### Changed
- Keep cursor position when appending to file.

### Removed
- Unused `WRITE_CALENDAR` permission.

### Fixed
- Keyboard sometimes not showing when it should.
- Reminder adjustment wrongly inserting additional "-0m" reminder if cursor is
  between spaces.
- Reminder notification not moving cursor to the event, if clicked after adding
  a quick note.

## [3.1.0] - 2024-09-15

### Added
- Set important reminders by marking with `!`. Reminders of the format
  `@12:34!` or `@12:34 -15m!` play the system alarm sound until dismissed and
  wake the device with a fullscreen display.
- Indicate next non-silent reminder in the editor toolbar, up to 12 hours in
  advance.
- Swipe-enabled toolbar button for event reminders.
  See more in [help](app/src/main/assets/help.md#reminder-button).

### Changed
- Time button now adjusts the event time if the cursor is anywhere on the
  event's line of text.
- New shared link/text flow: Instead of selecting page to append to, use normal
  navigation to select wanted page and move cursor to insertion position. Use
  bottom toolbar button to insert shared text on the cursor.
- Shared links are formatted without caption if title is not provided by the
  sharing app.
- Move cursor to event line when clicking reminder notification.
- Auto-cancel reminder notification when clicked.
- Customized time picker style.
- Plain links are matched more strictly to reduce false positives.

### Removed
- Linking to media files. Links did not work, because app loses permission on
  restart without full storage access.

### Fixed
- Hints being shown persistently if they are dismissed during fade-in
  animation.
- Escape (,),<,> in links so they work well in markdown syntax.
- Internal error on first startup before first file is created.
- Snackbar staying visible if interrupted during fade-out animation.
- Internal errors logged on Android SDK 28 and below.

## [3.0.0] - 2024-08-04

### Breaking
- Renamed to PTO - Plaintext Organizer. This version now installs as a
  different app.
- Files now always use the `.txt` extension. This is automatically migrated on
  import from `backup.zip`.
- Removed support for external storage usage for pages directory.
- Removed import from external storage. Use import from `backup.zip` instead.

### Added
- Current day indicator line in search results.
- Quick note activity as app shortcut.
- Autostart notification (if enabled) and load event reminders (if enabled)
  after app update.
- Show prefix in quick note text field.

### Changed
- Replaced all PNG resources with vector drawables.
- Made quick notes screen edge-to-edge.
- Search results avoid cutting off preview close to start or end of line.
- Omit overlapping plain text search results.

### Fixed
- Regex errors on search screen not being displayed.
- Minor issues with undo steps and drag adjustment buttons.
- Issues with simultaneous editor instances for the same page.
- Input errors on search screen not being displayed.
- Quick note activity not saving when switching to launcher or different task.
- Various stability improvements.

## [3.0.0-alpha4] - 2024-07-26

### Added
- Show hints for notes and events in adjacent pages.

### Changed
- Simplified cursor positioning behavior after undo/redo.
- Enabled optimization in the build process.
- Preserved source information and disabled obfuscation in release builds for
  better crash information.

### Fixed
- Exception logger checking wrong preference to enable/disable itself.
- Potential internal wakelock error.

## [3.0.0-alpha3] - 2024-07-22

### Added
- Link notification menu from preferences.
- Option to open configured logfile in preferences context menu.

### Changed
- Preserve time at-prefix for next minute when adjusting time backwards.
- Omit empty line in reminder notifications without text.
- Auto-open keyboard on future pages like on today's page.
- Navigate to today's page on backpress.
- Made snackbars transparent to touch events.
- Restrict pages to years 0 until 9999. Filenames of pages outside this range
  were never well defined.

### Fixed
- Notification processed more reliably during device deep sleep.
- Save state indicator staying pressed when clicked while it changes state.
- Logger failing to append if no other pages exist for the configured month.

## [3.0.0-alpha2] - 2024-07-17

### Added
- Reminder notifications for events formatted like `@12:34 foo bar`.
- Preference to append internal error log to a page.

### Changed
- Reworked light theme.
- Format times as events when inserted with toolbar button, if in future.
- Always insert separating space before inserted time if needed.

## [3.0.0-alpha1] - 2024-07-09

### Removed
- Markdown viewer feature.
- Internet access permission.
- Support for Android SDK 25 and below.

### Added
- Preference to select dark or light mode.

### Fixed
- Quickinput closing itself on configuration change (e.g. screen rotation).

## [2.8.0] - 2024-07-07

### Added
- Copy link to clipboard by long click on toolbar button.

### Changed
- Updated all dependencies and build tools.

### Fixed
- Internal errors observing files on devices below Android SDK 29.
- Theoretical issue that could delay search results.

## [2.7.0] - 2023-06-25

### Changed
- Updated all dependencies.

### Fixed
- Jittering scroll position while typing at the end of files under some
  conditions.

## [2.6.2] - 2021-10-25

### Changed
- Quick input notification uses default background color on SDK 31.

### Fixed
- Notification content not being visible when locked on SDK 31.

### Removed
- Inline input in the quick input notification. Fixes crash on SDK 31 when
  showing expandable notification.

## [2.6.1] - 2021-10-03

### Changed
- Added a workaround for the text cursor sometimes being covered when opening
  the keyboard.

## [2.6.0] - 2021-09-25

This release brings internal storage usage and migration, as well as import
from `backup.zip`. It is strongly recommended to migrate to internal storage.
See the [Readme notice](README.md#privacy) for usage on Android 11 and up.

### Added
- Support for internal storage as folder location.
- Migration feature to import folder to internal storage or between external
  paths.
- Feature for importing files from `backup.zip`.
- Allow copying `backup.zip` to downloads when using internal storage.
- Option to change the title of the quick input notification.
- Option to use invisible status bar icon for quick input notification.

### Changed
- Default folder location is now internal storage if not changed previously,
  and the external "Diary" directory is empty.
- Reordered settings.

### Fixed
- Added workaround for softInput (keyboard) sometimes being in single-line
  state after scrolling across many pages in the view pager.

### Removed
- The file editor activity (separate from the page editor) is not accessible
  for external files any more. It didn't work when started with external files
  adhering to Android's modern file access methods (content URIs).

## [2.5.0] - 2021-08-01

### Changed
- Changed application packagename to `de.ferreum.plaindiary`. Versions after
  this are installed as a different application and need to be configured
  again once.

## [2.4.0] - 2021-08-01

### Added
- Text search in editors and previews.

### Changed
- When sharing links with the app, apply the title of more kinds of links.
- Open soft keyboard in edit mode only for today page.
- Quick input activity closes itself after 30 seconds of inactivity.
- Quick input activity always saves note when closing, if not empty.

### Fixed
- Focus problems when changing pages in edit mode.
- "Today" menu items not updating when the current day changes.

## [2.3.0] - 2021-02-11

### Added
- Click title to bring up the date picker for page selection.
- Option to use ISO date format for page titles, including the day
  of week.
- In datepicker, clicking the already selected date accepts the choice.

### Changed
- Show "Go to date…" only in overflow menu as clicking title now has the same
  function.

### Fixed
- Caching issue in search, now allowing newly created pages to be found.
- Crash in search when a previously searched file was deleted.

## [2.2.1] - 2021-01-14

### Fixed
- Notification never being expandable before changing the preference at least
  once.
- Issues where content isn't synced properly when there are multiple instances
  of the main activity.

## [2.2.0] - 2021-01-12

### Added
- Indicate search mode on keyboard "enter" button and with SnackBar when
  cycling through modes.
- Highlight for most recently clicked search result.
- Dropdown for year/month selection in custom datepicker.
- Option to disable expanded notification.
- Option to enable javascript in markdown view mode.
- Bottom toolbar buttons for creating, editing and viewing links.
- Clear button for search text.

### Changed
- Improved handling of modes in search input.
- Click on time button while a time is selected now brings up a time picker.
- Moved "Add media…" and "Add events" to the new bottom toolbar menu.
- Diary pages are now written atomically.
- Replaced markdownj with commonmark-java as markdown processor.
- "New empty page time" now applies regardless of exiting today's file.
- Group search results by day.

### Removed
- Swipe gestures in datepicker.
- Native datepicker for page selection.

### Fixed
- Fix potentially overwriting existing file after granting file permission.
- Date links using the "date:" scheme accepted any character as a separator.
  Now only `.`, `/` and `-` separators are accepted.
- Scroll position in search results is now more reliable.

## [2.1.0] - 2020-12-23

### Added
- Run a service to ensure content is saved even when the app is dismissed from
  recents.
- Sanitize search input when entering multiple tokens.
- Search through pages using plain, word-bound or regex patterns--use keyboard
  "enter" button to cycle through modes.

### Changed
- Completely reworked the custom datepicker to fix bugs and improve styling and
  responsiveness.
- Better cursor positioning after undo/redo.
- Preserve undo history more reliably (after leaving the app: until a timer
  expires or app is killed).

### Fixed
- Added a workaround to prevent view pager from stopping too early when
  selecting a date with the datepicker (ViewPager2 bug).
- Fix crashes on undo when autocorrect is enabled.

### Removed
- Copying of imported media files; only imported as external links for now.
  Feature could return as an on-demand option when importing a link.

## [2.0.0] - 2020-12-10

### Added
- Continuous saving while editing.
- Undo/Redo functions for editing.
- Bottom toolbar while editing that is easy to reach while typing.
- Drag-enabled buttons on bottom toolbar for quick adjustments (time) or
  repetitions (undo/redo).
- Responsive search through all pages.
  - Allow specifying where to split search terms using keyboard "enter" button.
  - Preserve search state wherever possible, even when going back to the
    editor.
- Show search results in a read-only preview mode.
- Notification for appending a short note to the current day's page, even with
  locked device.
- Option to disable TextClassifier, as it may be annoying depending on the
  system (native Android feature that is enabled for all text input by
  default).
- Option to set a time at which a new day starts. Used for deciding what page
  to open on startup and for appending a note using the notification.
- Use FileObserver to update files when modified externally (bugged on some
  platforms (Samsung…)).
- Internal handling of `diary://date/year/month/day` links.

### Changed
- Modernize most of the codebase.
  - Migrate most of the code to Kotlin.
  - Improve responsiveness using coroutines and removing almost all I/O from
    main thread.
  - Use java8 time API.
  - Completely reimplemented and modularized the main Activity that previously
    contained most of the app's functionality.
- Swipe through pages with an actual view pager.
- Use external browser by default. Loading external pages in markdown view is
  discouraged.
- Replace option to disable markdown with an option to start in edit mode
  (enabled by default).
- Always save as .md files. Still recognize .txt files for compatibility.
- Run backup in a service, show progress and result in notification.
- Move floating edit/markdown button to ActionBar, keeping the editor
  unobstructed. Switching the mode affects all pages.
- When inserting a time, strip leading zero from hour.
- Date link format is now `date:[year]/[month]/[day]` instead of
  `date:[day]/[month]/[year]`.

### Removed
- "Cancel" action from the ActionBar. It was too dangerous to lose all changes
  since start of the edit session.
- Back-history in editor. Rely on view pager instead.
- Swipe gestures in editor/markdown mode. Use the datepicker or swipe toolbars
  to use the view pager instead.
- Calendar event creation.
- `[lat,lon]` Geo coordinates shortcut expansion. Must insert
  `![osm](geo:lat,lon)` manually (this format is inserted when "sharing" a geo
  location from another app).
- Cursor position expansion.
- Index page feature.
- Javascript in markdown view. Might return as an option.
- Option for dark theme. Use system's Light/Dark theme setting instead.
- Quotes/brace content selection on double-tap in editor, because it was
  overriding word-wise selection, didn't handle apostrophes well and could also
  lead to inadvertent deletion of text when intending to replace a word.

### Fixed
- Fix overwriting of selected date's file with the edited page's content when
  using the datepicker after editing a page.
- Flicker on startup caused by unnecessary animation.
