# <img src="app/src/main/res/mipmap-xxhdpi/ic_launcher_round.webp" width="75" height="75"> PTO - Plaintext Organizer

Stay organized with one plaintext file per day.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="Get it on F-Droid" height="85">](https://f-droid.org/packages/de.ferreum.pto/)

To-do list, notes, appointment book, diary, journal, and… alarm clock? All
possible with one plaintext file per day. PTO brings a convenient editor and
important additional functions.

## Features

* Powerful [text editor](app/src/main/assets/help.md#the-editor) with
  undo/redo, autosave, and more.
* Take [quick notes](app/src/main/assets/help.md#quick-notes) using a
  notification, without unlocking your device.
* Reminder notifications for upcoming
  [events](app/src/main/assets/help.md#event-times-and-reminders)
  (`@12:34 foobar`).
* Indicate reminders and notes on adjacent pages with
  [hints](app/src/main/assets/help.md#hints).
* Fast, responsive chronological search through all files.
* Save and open links to other apps and webpages.
* Uses no network connection. All data is local on the device.
* Optional template for new pages.
* Backup/import pages to/from ZIP file.

See the [Help](app/src/main/assets/help.md) file for more.

<img src="./metadata/en-US/images/phoneScreenshots/1.png" width="20%"> <img src="./metadata/en-US/images/phoneScreenshots/2.png" width="20%"> <img src="./metadata/en-US/images/phoneScreenshots/4.png" width="20%">

## Origin

PTO is a (now distant) fork of _billthefarmer_'s
[Diary](https://github.com/billthefarmer/diary) with a major overhaul of the
code using modern Android programming principles. _billthefarmer_'s
[Diary](https://github.com/billthefarmer/diary) itself is a fork of
[Diary](http://git.savannah.gnu.org/cgit/diary.git) on `savannah.gnu.org`.
Before renaming to PTO in version 3.0.0, this app was called "Plain Diary".

See [Help](app/src/main/assets/help.md#origin) for details.

## Privacy

PTO stores files in internal storage, accessible only to the app itself.

Support for using external storage (inherited from origin projects) has been
removed with version 3.0.0. To use files from external storage, they have to be
re-imported from `backup.zip`.

The app has no network access.

---

PTO is licensed under GPLv3.
