## Releasing
1. Increase version in app/build.gradle:
  - version: `<major>.<minor>.<patch>`
    suffix: `-alpha<N>`, `-rc<N>`, or similar.
  - versionName: `"<version>"`
  - versionCode: `major * 1000000 + minor * 1000 + patch + suffix`
    - `suffix` is incremented for every new suffix for a minor version and
      included in following releases until next minor bump.
2. Insert the new version headline and date in `CHANGELOG.md`, below the
  `## [Unreleased]` line:
  - `## [<version>] - <year>-<month>-<day>`
3. Add changelogs in `metadata/*/changelogs`
4. Commit the changes
  - message: `version <version>`
5. Create version tag
  - `git tag v<version>`

## Building
1. Build apk:
  - ./gradlew clean app:assembleRelease
  apk is located at `app/build/outputs/apk/release/app-release.apk`
2. Sign:
  apksigner sign --ks=... --out=signed.apk app/build/outputs/apk/release/app-release-unsigned.apk
