package de.ferreum.pto.backup

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import androidx.preference.PreferenceManager
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ServiceTestRule
import de.ferreum.pto.preferences.FolderLocation
import de.ferreum.pto.preferences.FolderLocation.ExternalStorage
import de.ferreum.pto.preferences.ZipFileLocation
import de.ferreum.pto.util.assertThrows
import de.ferreum.pto.util.file
import de.ferreum.pto.util.writeZip
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.conflate
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.job
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.io.File
import java.io.IOException

class FolderImportServiceTest {

    @get:Rule
    val serviceTestRule = ServiceTestRule()

    @get:Rule
    val temporaryFolder = TemporaryFolder()

    private lateinit var targetContext: Context

    private lateinit var preferences: SharedPreferences

    private lateinit var binder: FolderImportService.LocalBinder

    @Before
    fun setUp() {
        targetContext = InstrumentationRegistry.getInstrumentation().targetContext
        preferences = PreferenceManager.getDefaultSharedPreferences(targetContext)
        preferences.edit().clear().apply()
    }

    @After
    fun tearDown() {
        runBlocking {
            withTimeout(1000) {
                getController().importState.first { it !is ImportState.Active }
            }
        }
        preferences.edit().clear().apply()
    }

    @Test
    fun when_startImport_is_called__then_files_are_copied() = runBlocking {
        val zipFile = temporaryFolder.newFile()
        zipFile.writeZip {
            file("2021/01/02.txt", 0) {
                write("test content".toByteArray())
            }
        }
        val destFolder = temporaryFolder.newFolder()

        getController().startImport(
            folderImportParams(
                ZipFileLocation(Uri.parse("file://" + zipFile.absolutePath)),
                ExternalStorage(destFolder)
            )
        )
        awaitCompletionOrThrow()

        assertEquals("test content", File(destFolder, "2021/01/02.txt").readText())
    }

    @Test
    fun given_import_takes_time__when_starting_import__then_importState_emits_Active() = runBlocking {
        targetContext.withFakeZipImporter { importer ->
            coroutineScope {
                importer.answer = {
                    this@coroutineScope.coroutineContext.job.join()
                    ImportReport(emptyList(), 0, 1)
                }

                getController().startImport(
                    folderImportParams(
                        ZipFileLocation(Uri.parse("content://backup45.zip")),
                        ExternalStorage(File("/folder/two"))
                    )
                )

                assertEquals(
                    ImportState.Active(
                        folderImportParams(
                            ZipFileLocation(Uri.parse("content://backup45.zip")),
                            ExternalStorage(File("/folder/two")),
                        )
                    ),
                    getController().importState.conflate().first()
                )
            }
        }
    }

    @Test
    fun given_import_fails__when_awaitCompletionOrThrow_is_called__then_it_throws_the_exception() = runBlocking {
        targetContext.withFakeZipImporter { importer ->
            val exception = IOException("testing")
            importer.answer = { throw exception }
            getController().startImport(
                folderImportParams(
                    ZipFileLocation(Uri.parse("content://backup45.zip")),
                    FolderLocation.InternalStorage
                )
            )

            assertThrows(IOException::class) {
                awaitCompletionOrThrow()
            }
        }
    }

    @Test
    fun given_import_fails__when_starting_import__then_importState_emits_Failed() = runBlocking {
        targetContext.withFakeZipImporter { importer ->
            val exception = Exception("testing")
            importer.answer = { throw exception }

            getController().startImport(
                folderImportParams(
                    ZipFileLocation(Uri.parse("content://backup45.zip")),
                    ExternalStorage(File("/folder/two"))
                )
            )
            assertThrows(Exception::class) {
                awaitCompletionOrThrow()
            }

            assertEquals(
                ImportState.Failed(exception),
                getController().importState.conflate().first()
            )
        }
    }

    @Test
    fun given_impot_completes__when_starting_impot__then_importState_emits_Complete() = runBlocking {
        targetContext.withFakeZipImporter { importer ->
            importer.answer = { ImportReport(emptyList(), 0, 1) }

            getController().startImport(
                folderImportParams(
                    ZipFileLocation(Uri.parse("content://backup12.zip")),
                    ExternalStorage(File("/folder/two"))
                )
            )
            awaitCompletionOrThrow()

            assertEquals(
                ImportState.Completed(ImportReport(emptyList(), 0, 1)),
                getController().importState.conflate().first()
            )
        }
    }

    @Test
    fun given_import_is_already_running__when_startImport_is_called_a_second_time__then_new_request_is_ignored() = runBlocking {
        val importCalls = mutableListOf<Pair<Uri, File>>()
        targetContext.withFakeZipImporter { importer ->
            coroutineScope {
                importer.answer = {
                    importCalls.add(zipFile to destFolder)
                    this@coroutineScope.coroutineContext.job.join()
                    ImportReport(emptyList(), 0, 1 )
                }

                getController().startImport(
                    folderImportParams(
                        ZipFileLocation(Uri.parse("content://backup12.zip")),
                        ExternalStorage(File("/folder/one")),
                    )
                )
                getController().startImport(
                    folderImportParams(
                        ZipFileLocation(Uri.parse("content://backup34.zip")),
                        ExternalStorage(File("/folder/two")),
                    )
                )
            }
            awaitCompletionOrThrow()
        }

        assertEquals(listOf(Uri.parse("content://backup12.zip") to File("/folder/one")), importCalls)
    }

    @Test
    fun given_previous_import_failed__when_startImport_is_called_a_second_time__then_new_request_is_started() = runBlocking {
        val importCalls = mutableListOf<Pair<Uri, File>>()
        targetContext.withFakeZipImporter { importer ->
            importer.answer = {
                importCalls.add(zipFile to destFolder)
                if (destFolder.name == "one") {
                    throw IOException("testing")
                }
                ImportReport(emptyList(), 0, 1)
            }
            getController().startImport(
                folderImportParams(
                    ZipFileLocation(Uri.parse("content://backup12.zip")),
                    ExternalStorage(File("/folder/one")),
                )
            )
            assertThrows(IOException::class) {
                awaitCompletionOrThrow()
            }

            getController().startImport(
                folderImportParams(
                    ZipFileLocation(Uri.parse("content://backup34.zip")),
                    ExternalStorage(File("/folder/two"))
                )
            )
            awaitCompletionOrThrow()
        }

        assertEquals(listOf(
            Uri.parse("content://backup12.zip") to File("/folder/one"),
            Uri.parse("content://backup34.zip") to File("/folder/two"),
        ), importCalls)
    }

    @Test
    fun when_startImport_is_called__then_zip_import_is_started() = runBlocking {
        targetContext.withFakeZipImporter { importer ->
            getController().startImport(
                folderImportParams(
                    ZipFileLocation(Uri.parse("content://backup34.zip")),
                    ExternalStorage(File("/dest/path")),
                    overwrite = false,
                )
            )
            awaitCompletionOrThrow()

            assertEquals(
                FakeZipImporter.Call(
                    zipFile = Uri.parse("content://backup34.zip"),
                    destFolder = File("/dest/path"),
                    overwrite = false,
                ),
                importer.calls.single()
            )
        }
    }

    @Test
    fun when_startImport_is_called_with_overwrite__then_zip_import_is_started_with_overwrite() = runBlocking {
        targetContext.withFakeZipImporter { importer ->
            getController().startImport(
                folderImportParams(
                    ZipFileLocation(Uri.parse("content://backup45.zip")),
                    ExternalStorage(File("dest/path")),
                    overwrite = true,
                )
            )
            awaitCompletionOrThrow()

            assertTrue(importer.calls.single().overwrite)
        }
    }

    private suspend fun awaitCompletionOrThrow() {
        val result = getController().importState.first { it !is ImportState.Active }
        println(result)
        when (result) {
            is ImportState.Failed -> throw result.throwable
            is ImportState.Active -> throw AssertionError(result)
            is ImportState.Completed -> Unit // success
            ImportState.None -> throw AssertionError("import was not started")
        }
    }

    private fun getController() = bindService().controller

    private fun bindService(): FolderImportService.LocalBinder {
        if (::binder.isInitialized) {
            return binder
        }
        val intent = Intent(targetContext, FolderImportService::class.java)
        binder = serviceTestRule.bindService(intent) as FolderImportService.LocalBinder
        return binder
    }

    private fun folderImportParams(
        zipFileLocation: ZipFileLocation,
        destFolder: FolderLocation,
        overwrite: Boolean = false,
        tryMove: Boolean = true,
    ) = FolderImportParams(
        zipFileLocation = zipFileLocation,
        destFolder = destFolder,
        overwrite = overwrite,
        tryMove = tryMove,
    )

}
