package de.ferreum.pto.backup

import android.app.Activity
import android.app.Instrumentation.ActivityResult
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import de.ferreum.pto.R
import de.ferreum.pto.preferences.PtoFileHelper.Companion.RELPATH_FILES_INTERNAL_PAGES
import de.ferreum.pto.preferences.PtoPreferencesActivity
import de.ferreum.pto.util.scrollToUnconditionally
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.containsString
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.io.File

class ImportZipFragmentTest {

    private lateinit var scenario: ActivityScenario<PtoPreferencesActivity>

    private lateinit var targetContext: Context
    private lateinit var preferences: SharedPreferences

    @Before
    fun setUp() {
        Intents.init()
        targetContext = InstrumentationRegistry.getInstrumentation().targetContext
        preferences = PreferenceManager.getDefaultSharedPreferences(targetContext)
        preferences.edit().clear().commit()
    }

    @After
    fun tearDown() {
        preferences.edit().clear().commit()
        Intents.release()
    }

    @Test
    fun when_started__then_zip_import_screen_is_shown() {
        startZipImportScreen()

        assertZipImportScreen()
    }

    @Test
    fun when_choose_zip_file_is_clicked__then_document_chooser_is_started() {
        startZipImportScreen()
        Intents.intending(hasAction(Intent.ACTION_OPEN_DOCUMENT))
            .respondWith(ActivityResult(Activity.RESULT_CANCELED, null))

        onView(withHint(R.string.import_choose_zip_hint))
            .perform(click())

        Intents.intended(allOf(
            hasAction(Intent.ACTION_OPEN_DOCUMENT),
            hasExtra(Intent.EXTRA_MIME_TYPES, arrayOf("application/zip")),
        ))
    }

    @Test
    fun given_document_chooser_was_opened__when_document_chooser_returns_zip_file__then_zip_file_is_selected() {
        startZipImportScreen()

        selectZipFileUri("content://test/backup12.zip")

        onView(withHint(R.string.import_choose_zip_hint))
            .check(matches(withText(containsString("backup12.zip"))))
    }

    @Test
    fun given_uri_was_selected__when_ok_is_clicked__then_import_is_started() {
        targetContext.withFakeZipImporter { importer ->
            startZipImportScreen()
            selectZipFileUri("content://test/backup23.zip")

            clickOkButton()

            assertEquals(
                FakeZipImporter.Call(
                    zipFile = Uri.parse("content://test/backup23.zip"),
                    destFolder = File(targetContext.filesDir, RELPATH_FILES_INTERNAL_PAGES),
                    overwrite = false,
                ),
                importer.calls.single()
            )
        }
    }

    @Test
    fun given_overwrite_is_checked__when_ok_is_clicked__then_import_is_started_with_overwrite() {
        targetContext.withFakeZipImporter { importer ->
            startZipImportScreen()
            selectZipFileUri("content://test/backup23.zip")
            onView(withText(R.string.import_allow_overwriting_files_checkbox))
                .check(matches(isNotChecked()))
                .perform(click())

            clickOkButton()

            assertTrue(importer.calls.single().overwrite)
        }
    }

    @Test
    fun given_import_succeeds__when_ok_is_clicked__then_success_is_shown() {
        targetContext.withFakeZipImporter { importer ->
            importer.answer = { ImportReport(emptyList(), 0, 12) }
            startZipImportScreen()
            selectZipFileUri("content://test/backup23.zip")

            clickOkButton()

            onView(withText(buildString {
                appendLine(targetContext.getString(R.string.import_complete_message_ok))
                append(targetContext.getString(R.string.import_note_number_imported_files, 12))
            })).check(matches(isDisplayed()))
        }
    }

    @Test
    fun given_import_succeeds__when_ok_is_clicked__then_success_is_shown_with_notes() {
        targetContext.withFakeZipImporter { importer ->
            importer.answer = { ImportReport(listOf("one/1", "two/2"), 6, 12) }
            startZipImportScreen()
            selectZipFileUri("content://test/backup23.zip")

            clickOkButton()

            onView(withText(buildString {
                appendLine(targetContext.getString(R.string.import_complete_message_with_notes))
                appendLine(targetContext.getString(R.string.import_note_number_imported_files, 12))
                appendLine(targetContext.getString(R.string.import_note_number_renamed_files, 6))
                appendLine()
                appendLine(targetContext.getString(R.string.import_note_skipped_files_list, 2))
                appendLine("one/1")
                append("two/2")
            })).check(matches(isDisplayed()))
        }
    }

    @Test
    fun given_import_succeeds__when_ok_is_clicked__then_no_files_migrated_message_is_shown() {
        targetContext.withFakeZipImporter { importer ->
            importer.answer = { ImportReport(listOf("one/1"), 6, 0) }
            startZipImportScreen()
            selectZipFileUri("content://test/backup23.zip")

            clickOkButton()

            onView(withText(buildString {
                appendLine(targetContext.getString(R.string.import_complete_message_no_files))
                appendLine(targetContext.getString(R.string.import_note_number_imported_files, 0))
                appendLine(targetContext.getString(R.string.import_note_number_renamed_files, 6))
                appendLine()
                appendLine(targetContext.getString(R.string.import_note_skipped_files_list, 1))
                append("one/1")
            })).check(matches(isDisplayed()))
        }
    }

    @Test
    fun given_import_screen_is_shown__when_import_fails__then_the_error_is_shown() {
        targetContext.withFakeZipImporter { importer ->
            importer.answer = { throw Throwable("the testing exception") }
            startZipImportScreen()
            selectZipFileUri("content://test/backup45.zip")

            clickOkButton()

            onView(withText(containsString("the testing exception")))
                .check(matches(isDisplayed()))
        }
    }

    @Test
    fun given_import_succeeded__when_ok_is_clicked__then_screen_is_closed() {
        targetContext.withFakeZipImporter { importer ->
            importer.answer = { ImportReport(emptyList(), 0, 0) }
            startZipImportScreen()
            selectZipFileUri("content://test/backup23.zip")
            clickOkButton()

            clickOkButton()

            assertPreferencesScreen()
        }
    }


    private fun clickOkButton() {
        onView(withText(android.R.string.ok))
            .perform(click())
    }

    private fun selectZipFileUri(uri: String) {
        Intents.intending(hasAction(Intent.ACTION_OPEN_DOCUMENT))
            .respondWith(ActivityResult(Activity.RESULT_OK, Intent()
                .setData(Uri.parse(uri))))

        onView(withHint(R.string.import_choose_zip_hint))
            .perform(click())
    }

    private fun startZipImportScreen() {
        scenario = launchActivity()
        onView(isAssignableFrom(RecyclerView::class.java))
            .perform(swipeUp(), swipeUp())
        onView(withText(R.string.import_zip_title))
            .perform(scrollToUnconditionally(), click())
    }

    private fun assertZipImportScreen() {
        onView(withHint(R.string.import_choose_zip_hint))
            .check(matches(isDisplayed()))
    }

    private fun assertPreferencesScreen() {
        onView(isAssignableFrom(RecyclerView::class.java))
            .perform(swipeDown(), swipeDown())
        onView(withText(R.string.pref_newpage_time_title))
            .check(matches(isDisplayed()))
    }

}
