package de.ferreum.pto.backup

import android.app.Activity
import android.app.Instrumentation
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import androidx.core.content.FileProvider
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import de.ferreum.pto.Constants
import de.ferreum.pto.R
import de.ferreum.pto.preferences.PtoFileHelper.Companion.RELPATH_FILES_INTERNAL_PAGES
import de.ferreum.pto.preferences.PtoPreferencesActivity
import de.ferreum.pto.util.file
import de.ferreum.pto.util.scrollToUnconditionally
import de.ferreum.pto.util.writeZip
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.io.File

class ImportZipTest {

    private var cacheTemporaryFolder: TemporaryFolder? = null

    private lateinit var targetContext: Context
    private lateinit var preferences: SharedPreferences

    @Before
    fun setUp() {
        Intents.init()
        targetContext = InstrumentationRegistry.getInstrumentation().targetContext
        preferences = PreferenceManager.getDefaultSharedPreferences(targetContext)
        preferences.edit().clear().commit()

        cacheTemporaryFolder = TemporaryFolder(targetContext.cacheDir).apply { create() }
    }

    @After
    fun tearDown() {
        cacheTemporaryFolder?.delete()
        preferences.edit().clear().commit()
        Intents.release()
    }

    @Test
    fun given_import_zip_is_valid__when_ok_is_clicked__then_files_are_imported() {
        val zipFile = cacheTemporaryFolder!!.newFile()
        zipFile.writeZip {
            file("2021/01/02.txt", 0) {
                write("import zip test file content".toByteArray())
            }
        }
        val uri = FileProvider.getUriForFile(
            targetContext,
            targetContext.packageName + Constants.FILE_PROVIDER_AUTHORITY_SUFFIX,
            zipFile
        )

        startZipImportScreen()
        selectZipFileUri(uri)
        clickOkButton()

        assertEquals("import zip test file content",
            File(targetContext.filesDir, "$RELPATH_FILES_INTERNAL_PAGES/2021/01/02.txt").readText())
    }

    private fun startZipImportScreen() {
        launchActivity<PtoPreferencesActivity>()
        onView(isAssignableFrom(RecyclerView::class.java))
            .perform(swipeUp(), swipeUp())
        onView(withText(R.string.import_zip_title))
            .perform(scrollToUnconditionally(), click())
    }

    private fun selectZipFileUri(uri: Uri) {
        Intents.intending(IntentMatchers.hasAction(Intent.ACTION_OPEN_DOCUMENT))
            .respondWith(Instrumentation.ActivityResult(Activity.RESULT_OK, Intent()
                .setData(uri)))

        onView(withHint(R.string.import_choose_zip_hint))
            .perform(click())
    }

    private fun clickOkButton() {
        onView(withText(android.R.string.ok))
            .perform(click())
    }

}
