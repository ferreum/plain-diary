package de.ferreum.pto.backup

import android.content.Context
import de.ferreum.pto.PtoApplication

internal inline fun Context.withFakeZipImporter(block: (FakeZipImporter) -> Unit) {
    val importer = FakeZipImporter()
    val app = applicationContext as PtoApplication
    val orig = app.zipImporter
    assert(orig is ZipImporterImpl) { "unexpected importer class: $orig" }
    try {
        app.zipImporter = importer
        block(importer)
    } finally {
        app.zipImporter = orig
    }
}
