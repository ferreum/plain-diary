package de.ferreum.pto.backup

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import de.ferreum.pto.preferences.PtoFileHelper.Companion.RELPATH_FILES_INTERNAL_PAGES
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.io.File
import java.util.zip.ZipInputStream

internal class ZipBackupTest {

    private lateinit var targetContext: Context

    private var internalBaseDir: File? = null
    private var cacheTemporaryFolder: TemporaryFolder? = null

    @Before
    fun setUp() {
        targetContext = InstrumentationRegistry.getInstrumentation().targetContext

        internalBaseDir = File(targetContext.filesDir, RELPATH_FILES_INTERNAL_PAGES)

        cacheTemporaryFolder = TemporaryFolder(targetContext.cacheDir).apply { create() }
    }

    @After
    fun tearDown() {
        cacheTemporaryFolder?.delete()
    }

    @Test
    fun backup_writes_zip_file() = runTest {
        val zipFile = cacheTemporaryFolder!!.newFile()
        val baseFolder = cacheTemporaryFolder!!.newFolder()
        File(baseFolder, "dir1").mkdir()
        File(baseFolder, "dir1/file1").writeText("first file")
        File(baseFolder, "dir1/file2").writeText("second file")
        File(baseFolder, "dir2/dir3").mkdirs()
        File(baseFolder, "dir2/dir3/file3").writeText("third file")

        ZipBackup.backup(baseFolder, zipFile) { _, _ -> }

        assertEquals(
            mapOf(
                "dir1/" to null,
                "dir1/file1" to "first file",
                "dir1/file2" to "second file",
                "dir2/" to null,
                "dir2/dir3/" to null,
                "dir2/dir3/file3" to "third file",
            ),
            zipFile.readZipToMap()
        )
    }

    @Test
    fun backup_works_with_many_files() = runTest {
        val zipFile = cacheTemporaryFolder!!.newFile()
        val baseFolder = cacheTemporaryFolder!!.newFolder()
        val expectMap = mutableMapOf<String, String?>()
        repeat(10) { outer ->
            val outerDir = File(baseFolder, outer.toString())
            outerDir.mkdir()
            expectMap["$outer/"] = null
            repeat(12) { inner ->
                val innerDir = File(outerDir, inner.toString())
                innerDir.mkdir()
                expectMap["$outer/$inner/"] = null
                repeat(32) { num ->
                    File(innerDir, "file$num.txt").writeText("dummy file")
                    expectMap["$outer/$inner/file$num.txt"] = "dummy file"
                }
            }
        }

        ZipBackup.backup(baseFolder, zipFile) { _, _ -> }

        assertEquals(
            expectMap,
            zipFile.readZipToMap()
        )
    }

    @Test
    fun backup_works_with_large_file() = runTest {
        val zipFile = cacheTemporaryFolder!!.newFile()
        val baseFolder = cacheTemporaryFolder!!.newFolder()
        File(baseFolder, "dir").mkdir()
        val longText = "Lorem ipsum dolor sit amet".repeat(4096)
        File(baseFolder, "dir/file").writeText(longText)
        assertEquals(26 * 4096, File(baseFolder, "dir/file").length())

        ZipBackup.backup(baseFolder, zipFile) { _, _ -> }

        assertEquals(
            mapOf("dir/" to null, "dir/file" to longText),
            zipFile.readZipToMap()
        )
    }

    @Test
    fun backup_calls_progress_callback() = runTest {
        val zipFile = cacheTemporaryFolder!!.newFile()
        val baseFolder = cacheTemporaryFolder!!.newFolder()
        File(baseFolder, "dir").mkdir()
        repeat(1024) {
            File(baseFolder, "dir/file$it").writeText("dummy file")
        }
        var lastProgress = -1
        var lastMax = -1
        var numCalls = 0

        ZipBackup.backup(baseFolder, zipFile) { progress, max ->
            numCalls++
            lastProgress = progress
            lastMax = max
        }

        assertEquals("lastProgress=$lastProgress lastMax=$lastMax", 20, 20)
        assertTrue("numCalls=$numCalls", numCalls > 2)
        assertTrue("numCalls=$numCalls", numCalls <= 20)
    }

    private fun File.readZipToMap(): Map<String, String?> {
        return inputStream().use { inputStream ->
            ZipInputStream(inputStream).use { zip ->
                val map = mutableMapOf<String, String?>()
                while (true) {
                    val entry = zip.nextEntry ?: break
                    if (entry.isDirectory) {
                        map[entry.name] = null
                    } else {
                        map[entry.name] = zip.readBytes().decodeToString()
                    }
                }
                map
            }
        }
    }
}
