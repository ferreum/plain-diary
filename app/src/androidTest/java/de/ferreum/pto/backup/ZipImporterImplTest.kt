package de.ferreum.pto.backup

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider
import androidx.test.platform.app.InstrumentationRegistry
import de.ferreum.pto.Constants
import de.ferreum.pto.util.directory
import de.ferreum.pto.util.file
import de.ferreum.pto.util.getFileNameContentMap
import de.ferreum.pto.util.writeZip
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.io.File
import java.io.IOException
import java.util.zip.ZipException

class ZipImporterImplTest {

    @get:Rule
    val cacheTemporaryFolder = TemporaryFolder()

    private lateinit var pagesTemporaryFolder: TemporaryFolder

    private lateinit var targetContext: Context

    private lateinit var importer: ZipImporterImpl

    @Before
    fun setUp() {
        targetContext = InstrumentationRegistry.getInstrumentation().targetContext
        val testDir = File(targetContext.filesDir, "test")
        testDir.mkdirs()
        pagesTemporaryFolder = TemporaryFolder(testDir)
        pagesTemporaryFolder.create()
        importer = ZipImporterImpl(
            targetContext.contentResolver,
            Dispatchers.IO,
        )
    }

    @After
    fun tearDown() {
        pagesTemporaryFolder.delete()
    }

    @Test
    fun when_called__then_it_unpacks_data_from_content_URI_stream() = runBlocking {
        val destFolder = pagesTemporaryFolder.newFolder()
        val zipFile = cacheTemporaryFolder.newFile("backup.zip")
        zipFile.writeZip {
            directory("2020/", 0)
            directory("2020/02/", 0)
            file("2020/02/03.txt", 0) {
                write("test file content".toByteArray())
            }
            file("2021/04/05.txt", 0) {
                write("test file content 2".toByteArray())
            }
            directory("2022/", 0)
            file("2022/06/07.txt", 0) {
                write("test file content 3".toByteArray())
            }
            file("2023/10/11.txt", 0) {
                write("test file content 4".toByteArray())
            }
        }
        assertTrue(File(destFolder, "2022/06").mkdirs())
        assertTrue(File(destFolder, "2023/10").mkdirs())

        importer.importZipFile(zipFile.toFileProviderUri(), destFolder)

        assertEquals("test file content",
            File(destFolder, "2020/02/03.txt").readText())
        assertEquals("test file content 2",
            File(destFolder, "2021/04/05.txt").readText())
        assertEquals("test file content 3",
            File(destFolder, "2022/06/07.txt").readText())
        assertEquals("test file content 4",
            File(destFolder, "2023/10/11.txt").readText())
    }

    @Test
    fun given_dest_file_exists__when_called_without_overwrite__then_it_throws_exception() =
        runBlocking {
            val destFolder = pagesTemporaryFolder.newFolder()
            val zipFile = cacheTemporaryFolder.newFile()
            zipFile.writeZip {
                file("2021/01/02.txt", 0) {
                    write("backup test file content".toByteArray())
                }
            }
            File(destFolder, "2021/01/02.txt").apply {
                assertTrue(parentFile!!.mkdirs())
                writeText("original test file content")
            }

            try {
                importer.importZipFile(zipFile.toFileProviderUri(), destFolder, overwrite = false)
            } catch (e: Throwable) {
                if (e !is IOException) {
                    throw AssertionError("unexpected exception", e)
                }
            }

            assertEquals(mapOf("2021/01/02.txt" to "original test file content"),
                destFolder.getFileNameContentMap())
        }

    @Test
    fun given_dest_file_exists__when_called_with_overwrite__then_the_file_is_overwritten() =
        runBlocking {
            val destFolder = pagesTemporaryFolder.newFolder()
            val zipFile = cacheTemporaryFolder.newFile()
            zipFile.writeZip {
                file("2021/01/02.txt", 0) {
                    write("new content".toByteArray())
                }
            }
            File(destFolder, "2021/01/02.txt").apply {
                assertTrue(parentFile!!.mkdirs())
                writeText("original test file content")
            }

            importer.importZipFile(zipFile.toFileProviderUri(), destFolder, overwrite = true)

            assertEquals(mapOf("2021/01/02.txt" to "new content"),
                destFolder.getFileNameContentMap())
        }

    @Test
    fun given_zip_file_contains_unsafe_path__when_called__then_it_omits_and_lists_file() = runBlocking {
        testInvalidPath("../2021/02/03.txt")
        testInvalidPath("2021/02/..")
        testInvalidPath("2021/../05.txt")
        testInvalidPath("../../../2021/02/03.txt")
    }

    @Test
    fun given_zip_file_contains_unexpected_files__when_called__then_it_omits_and_lists_file() = runBlocking {
        val destFolder = pagesTemporaryFolder.newFolder()
        val zipFile = cacheTemporaryFolder.newFile()
        zipFile.writeZip {
            file("2021/02/supplemental.png", 0) {
                write("some additional data".toByteArray())
            }
            file("2021/02/12.txt", 0) {
                write("test content".toByteArray())
            }
        }

        val result = importer.importZipFile(zipFile.toFileProviderUri(), destFolder)

        assertEquals(ImportReport(listOf("2021/02/supplemental.png"), 0, 1), result)
        assertEquals(mapOf("2021/02/12.txt" to "test content"),
            destFolder.getFileNameContentMap())
    }

    @Test
    fun given_zip_file_contains_markdown_files__when_called__then_it_imports_as_txt() = runBlocking {
        val destFolder = pagesTemporaryFolder.newFolder()
        val zipFile = cacheTemporaryFolder.newFile()
        zipFile.writeZip {
            file("2021/01/02.md", 0) {
                write("markdown data".toByteArray())
            }
            file("2021/01/03.txt", 0) {
                write("txt data".toByteArray())
            }
        }

        val result = importer.importZipFile(zipFile.toFileProviderUri(), destFolder)

        assertEquals(ImportReport(emptyList(), 1, 2), result)
        assertEquals(
            mapOf(
                "2021/01/02.txt" to "markdown data",
                "2021/01/03.txt" to "txt data"
            ),
            destFolder.getFileNameContentMap()
        )
    }

    @Test
    fun given_markdown_and_txt_file_exists__then_markdown_file_is_imported_as_txt() = runBlocking {
        val destFolder = pagesTemporaryFolder.newFolder()
        val zipFile = cacheTemporaryFolder.newFile()
        zipFile.writeZip {
            file("2021/01/02.md", 0) {
                write("markdown data".toByteArray())
            }
            file("2021/01/02.txt", 0) {
                write("txt data".toByteArray())
            }
        }

        val result = importer.importZipFile(zipFile.toFileProviderUri(), destFolder)

        assertEquals(ImportReport(listOf("2021/01/02.txt"), 1, 1), result)
        assertEquals(mapOf("2021/01/02.txt" to "markdown data"), destFolder.getFileNameContentMap())
    }

    @Test
    fun given_directory_creation_fails__when_called__then_it_throws_exception() = runBlocking<Unit> {
        val zipFile = cacheTemporaryFolder.newFile()
        zipFile.writeZip {
            file("2021/03/04.txt", 0) {
                write("test file content".toByteArray())
            }
        }

        try {
            importer.importZipFile(zipFile.toFileProviderUri(), File("/var/empty/"))
        } catch (e: Throwable) {
            if (e !is IOException) {
                throw AssertionError("unexpected exception", e)
            }
        }
    }

    private suspend fun testInvalidPath(path: String) {
        val destFolder = pagesTemporaryFolder.newFolder()
        val zipFile = File(cacheTemporaryFolder.root, "backup.zip")
        zipFile.writeZip {
            file(path, 0) {
                write("test file content".toByteArray())
            }
        }

        val result = runCatching {
            importer.importZipFile(zipFile.toFileProviderUri(), destFolder)
        }

        result.fold(
            // on older platforms ZipImporter should filter
            onSuccess = { report ->
                assertEquals(ImportReport(listOf(path), 0, 0), report)
            },
            // on newer platforms ZipInputStream throws on unsafe paths
            onFailure = { exception ->
                if (exception !is ZipException) {
                    throw AssertionError("expected ZipException but got: $exception", exception)
                }
            }
        )
        assertEquals(
            emptyMap<String, String>(),
            pagesTemporaryFolder.root.getFileNameContentMap()
        )
    }

    private fun File.toFileProviderUri(): Uri = FileProvider.getUriForFile(
        targetContext,
        targetContext.packageName + Constants.FILE_PROVIDER_AUTHORITY_SUFFIX,
        this
    )

}
