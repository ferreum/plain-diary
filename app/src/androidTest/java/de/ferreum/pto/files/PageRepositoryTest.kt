package de.ferreum.pto.files

import app.cash.turbine.test
import de.ferreum.pto.Constants
import de.ferreum.pto.files.PageRepository.FileInfo
import de.ferreum.pto.preferences.PtoFileHelper
import de.ferreum.pto.preferences.PtoFileHelper.Companion.RELPATH_FILES_INTERNAL_PAGES
import de.ferreum.pto.preferences.RecordingLogger
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.io.File
import java.time.LocalDate
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.time.ExperimentalTime

@ExperimentalTime
class PageRepositoryTest {

    @get:Rule
    val temporaryFolder = TemporaryFolder()
    private lateinit var filesPath: File

    private lateinit var fileHelper: PtoFileHelper

    private val scope = CoroutineScope(Job())
    private val logger = RecordingLogger()
    private val pageWriteSignal = PageWriteSignal()

    private lateinit var pageRepository: PageRepository

    @Before
    fun setUp() {
        filesPath = temporaryFolder.newFolder()
        fileHelper = PtoFileHelper(filesPath)
    }

    private fun createRepository(date: LocalDate) {
        pageRepository = PageRepository(
            CompletableDeferred(fileHelper),
            EmptyCoroutineContext,
            date,
            pageWriteSignal,
        )
    }

    @After
    fun tearDown() {
        scope.cancel()
        logger.throwIfAny()
    }

    @Test
    fun when_flow_is_used__then_it_emits_event_for_internal_file() = runBlocking {
        createRepository(LocalDate.of(1990, 1, 1))
        val file = File(filesPath, "$RELPATH_FILES_INTERNAL_PAGES/1990/01/01.txt")
        assertTrue(file.parentFile!!.mkdirs())
        file.writeText("test content")

        val result = pageRepository.fileInfoFlow.first()

        assertEquals(FileInfo(
            date = LocalDate.of(1990, 1, 1),
            file = file,
            lastModified = file.lastModified(),
        ), result)
    }

    @Test
    fun given_use_internal_storage_is_enabled__when_saveFile_is_called__then_it_writes_the_internal_file() = runBlocking {
        createRepository(LocalDate.of(1990, 1, 1))

        pageRepository.saveFile("test file content")

        val content = File(filesPath, "$RELPATH_FILES_INTERNAL_PAGES/1990/01/01.txt").readText()
        assertEquals("test file content", content)
    }

    @Test
    fun given_currentDate_is_out_of_range__then_it_throws_IllegalArgumentException() = runBlocking {
        createRepository(Constants.PAGE_DATE_MAX.plusDays(1))

        try {
            pageRepository.fileInfoFlow.first()
            throw AssertionError("no error thrown")
        } catch (e: IllegalArgumentException) {
            // success
        }
    }

    @Test
    fun fileInfoFlow_updates_on_fileWriteSignal() = runBlocking {
        createRepository(LocalDate.of(1990, 1, 1))
        val dayFile = fileHelper.getDayFile(LocalDate.of(1990, 1, 1))

        pageRepository.fileInfoFlow.test {
            assertEquals(0, awaitItem().lastModified)

            assertTrue(dayFile.parentFile!!.mkdirs())
            dayFile.writeText("test")
            pageWriteSignal.notifyPageWrite(PageWriteSignal.PageWrite(LocalDate.of(1990, 1, 1), null))

            assertEquals(dayFile.lastModified(), awaitItem().lastModified)
        }
    }

    @Test
    fun fileInfoFlow_updates_once_on_check_after_writeSignal() = runBlocking {
        createRepository(LocalDate.of(1990, 1, 1))
        val dayFile = fileHelper.getDayFile(LocalDate.of(1990, 1, 1))

        pageRepository.fileInfoFlow.test {
            assertEquals(0, awaitItem().lastModified)

            assertTrue(dayFile.parentFile!!.mkdirs())
            dayFile.writeText("test")
            pageWriteSignal.notifyPageWrite(PageWriteSignal.PageWrite(LocalDate.of(1990, 1, 1), null))
            pageRepository.checkForChangedFile()

            assertEquals(dayFile.lastModified(), awaitItem().lastModified)
            assertEquals(emptyList<Nothing>(), cancelAndConsumeRemainingEvents())
        }
    }

    @Test
    fun fileInfoFlow_updates_once_on_writeSignal_after_check() = runBlocking {
        createRepository(LocalDate.of(1990, 1, 1))
        val dayFile = fileHelper.getDayFile(LocalDate.of(1990, 1, 1))

        pageRepository.fileInfoFlow.test {
            assertEquals(0, awaitItem().lastModified)

            assertTrue(dayFile.parentFile!!.mkdirs())
            dayFile.writeText("test")
            pageRepository.checkForChangedFile()
            pageWriteSignal.notifyPageWrite(PageWriteSignal.PageWrite(LocalDate.of(1990, 1, 1), null))

            assertEquals(dayFile.lastModified(), awaitItem().lastModified)
            assertEquals(emptyList<Nothing>(), cancelAndConsumeRemainingEvents())
        }
    }
}
