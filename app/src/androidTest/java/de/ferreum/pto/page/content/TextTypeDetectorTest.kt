package de.ferreum.pto.page.content

import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import java.time.LocalDate
import java.time.LocalTime

class TextTypeDetectorTest {

    private val detector = TextTypeDetector()

    @Test
    fun classifyAtPosition_detects_plain_link() = runBlocking {
        val text = "test link: https://example.com end"

        val results = listOf(
            detector.classifyAtPosition(text, 10),
            detector.classifyAtPosition(text, 11),
            detector.classifyAtPosition(text, 19),
            detector.classifyAtPosition(text, 30),
            detector.classifyAtPosition(text, 31),
        )

        val link = listOf(TextType.Link(11, 30, "https://example.com", null, "https://example.com"))
        assertEquals(listOf(emptyList(), link, link, link, emptyList()), results)
    }

    @Test
    fun classifyAtPosition_detects_selected_plain_link() = runBlocking {
        val text = "test link: https://example.com end"

        val results = listOf(
            detector.classifyAtPosition(text, 10, 30).first(),
            detector.classifyAtPosition(text, 11, 30).first(),
            detector.classifyAtPosition(text, 12, 29).first(),
            detector.classifyAtPosition(text, 11, 31).first(),
        )

        val selection1 = TextType.Selection(10, 30, " https://example.com")
        val selection2 = TextType.Selection(11, 31, "https://example.com ")
        val link = TextType.Link(11, 30, "https://example.com", null, "https://example.com")
        assertEquals(listOf(selection1, link, link, selection2), results)
    }

    @Test
    fun classifyAtPosition_detects_full_link() = runBlocking {
        val text = "test: [link](https://example.com) (end)"

        val results = listOf(
            detector.classifyAtPosition(text, 5),
            detector.classifyAtPosition(text, 6),
            detector.classifyAtPosition(text, 14),
            detector.classifyAtPosition(text, 33),
            detector.classifyAtPosition(text, 34),
        )

        val link = listOf(TextType.Link(6, 33, "[link](https://example.com)", "link", "https://example.com"))
        assertEquals(listOf(emptyList(), link, link, link, emptyList()), results)
    }

    @Test
    fun classifyAtPosition_detects_selected_full_link() = runBlocking {
        val text = "test: [link](https://example.com) (end)"

        val results = listOf(
            detector.classifyAtPosition(text, 5, 33).first(),
            detector.classifyAtPosition(text, 6, 33).first(),
            detector.classifyAtPosition(text, 7, 32).first(),
            detector.classifyAtPosition(text, 6, 34).first(),
        )

        val selection1 = TextType.Selection(5, 33, " [link](https://example.com)")
        val selection2 = TextType.Selection(6, 34, "[link](https://example.com) ")
        val link = TextType.Link(6, 33, "[link](https://example.com)", "link", "https://example.com")
        assertEquals(listOf(selection1, link, link, selection2), results)
    }

    @Test
    fun classifyAtPosition_detects_date() = runBlocking {
        val text = "time: 2020-01-12 [link on 2020-02-20](https://example.com)"

        val date0 = TextType.Date(6, 16, "2020-01-12", LocalDate.of(2020, 1, 12))
        val date1 = TextType.Date(26, 36, "2020-02-20", LocalDate.of(2020, 2, 20))
        val link = TextType.Link(17, 58, "[link on 2020-02-20](https://example.com)", "link on 2020-02-20", "https://example.com")

        assertEquals(emptyList<TextType>(), detector.classifyAtPosition(text, 5))
        assertEquals(listOf(date0), detector.classifyAtPosition(text, 6))
        assertEquals(listOf(date0), detector.classifyAtPosition(text, 16))
        assertEquals(listOf(link), detector.classifyAtPosition(text, 17))
        assertEquals(listOf(date0, TextType.Selection(6, 16, "2020-01-12")), detector.classifyAtPosition(text, 6, 16))
        assertEquals(listOf(date1, link), detector.classifyAtPosition(text, 30))
        assertEquals(listOf(link, TextType.Selection(25, 36, " 2020-02-20")), detector.classifyAtPosition(text, 25, 36))
        assertEquals(listOf(link, TextType.Selection(26, 37, "2020-02-20]")), detector.classifyAtPosition(text, 26, 37))
    }

    @Test
    fun classifyAtPosition_detects_time() = runBlocking {
        val text = "time: 13:36 [link at 12:23](https://example.com)"

        val time0 = TextType.Time(6, 11, "13:36", LocalTime.of(13, 36))
        val time1 = TextType.Time(21, 26, "12:23", LocalTime.of(12, 23))
        val link = TextType.Link(12, 48, "[link at 12:23](https://example.com)", "link at 12:23", "https://example.com")

        assertEquals(emptyList<TextType>(), detector.classifyAtPosition(text, 5))
        assertEquals(listOf(time0), detector.classifyAtPosition(text, 6))
        assertEquals(listOf(time0), detector.classifyAtPosition(text, 11))
        assertEquals(listOf(link), detector.classifyAtPosition(text, 12))
        assertEquals(listOf(time0, TextType.Selection(6, 11, "13:36")), detector.classifyAtPosition(text, 6, 11))
        assertEquals(listOf(time1, link), detector.classifyAtPosition(text, 24))
        assertEquals(listOf(link, TextType.Selection(20, 26, " 12:23")), detector.classifyAtPosition(text, 20, 26))
        assertEquals(listOf(link, TextType.Selection(21, 27, "12:23]")), detector.classifyAtPosition(text, 21, 27))
    }

    @Test
    fun classifyAtPosition_detects_time_with_surrounding_chars() = runBlocking {
        val text = ".12:34 _13:36_"

        val time0 = TextType.Time(1, 6, "12:34", LocalTime.of(12, 34))
        val time1 = TextType.Time(8, 13, "13:36", LocalTime.of(13, 36))

        assertEquals(emptyList<TextType>(), detector.classifyAtPosition(text, 0))
        assertEquals(listOf(time0), detector.classifyAtPosition(text, 4))
        assertEquals(emptyList<TextType>(), detector.classifyAtPosition(text, 7))
        assertEquals(listOf(time1), detector.classifyAtPosition(text, 11))
        assertEquals(emptyList<TextType>(), detector.classifyAtPosition(text, 14))
    }

    @Test
    fun classifyAtPosition_detects_event() = runBlocking {
        val text = "@12:34 -1h -15m! testing"

        val time = TextType.Time(0, 6, "@12:34", LocalTime.of(12, 34))
        val event = TextType.Event(0, 24, "@12:34 -1h -15m! testing",
            EventContent(LocalTime.of(12, 34), listOf("-1h", "-15m!"), "testing"),
            reminderSelection = -1)

        assertEquals(listOf(time, event), detector.classifyAtPosition(text, 0))
        assertEquals(listOf(time, event), detector.classifyAtPosition(text, 6))
        assertEquals(listOf(event.copy(reminderSelection = 0)), detector.classifyAtPosition(text, 7))
        assertEquals(listOf(event.copy(reminderSelection = 0)), detector.classifyAtPosition(text, 10))
        assertEquals(listOf(event.copy(reminderSelection = 1)), detector.classifyAtPosition(text, 11))
        assertEquals(listOf(event.copy(reminderSelection = 1)), detector.classifyAtPosition(text, 16))
        assertEquals(listOf(event.copy(reminderSelection = 2)), detector.classifyAtPosition(text, 17))
    }

    @Test
    fun classifyAtPosition_ignores_other_lines() = runBlocking {
        val text = "first: [link](https://example.com/1)\ntest text\nhttps://example.com/2"

        val results = listOf(
            detector.classifyAtPosition(text, 36),
            detector.classifyAtPosition(text, 37),
            detector.classifyAtPosition(text, 42),
            detector.classifyAtPosition(text, 46),
            detector.classifyAtPosition(text, 47),
        )

        val link1 = listOf(TextType.Link(7, 36, "[link](https://example.com/1)", "link", "https://example.com/1"))
        val link2 = listOf(TextType.Link(47, 68, "https://example.com/2", null, "https://example.com/2"))
        assertEquals(listOf<List<TextType>>(link1, emptyList(), emptyList(), emptyList(), link2), results)
    }

    @Test
    fun classifyAtSelection_includes_position() = runBlocking {
        val text = "start: https://example.com end"

        val results = listOf(
            detector.classifyAtPosition(text, 0, 5),
            detector.classifyAtPosition(text, 5, 12),
            detector.classifyAtPosition(text, 7, 26),
            detector.classifyAtPosition(text, 23, 30),
        )

        val selection0 = TextType.Selection(0, 5, "start")
        val selection1 = TextType.Selection(5, 12, ": https")
        val link = TextType.Link(7, 26, "https://example.com", null, "https://example.com")
        val selection2 = TextType.Selection(7, 26, "https://example.com")
        val selection3 = TextType.Selection(23, 30, "com end")
        assertEquals(
            listOf(
                listOf(selection0),
                listOf(selection1),
                listOf(link, selection2),
                listOf(selection3)
            ), results
        )
    }

}
