package de.ferreum.pto.preferences

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Rect
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import de.ferreum.pto.R
import de.ferreum.pto.backup.ACTION_ZIP_IMPORT
import de.ferreum.pto.backup.EXTRA_THROWABLE
import de.ferreum.pto.util.scrollToUnconditionally
import de.ferreum.pto.util.setDatePickerDate
import de.ferreum.pto.util.setTimePickerTime
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.containsString
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.time.LocalDate
import java.time.LocalTime

internal class PtoPreferencesFragmentTest {

    private lateinit var scenario: ActivityScenario<PtoPreferencesActivity>

    @get:Rule
    val temporaryFolder = TemporaryFolder()

    private lateinit var targetContext: Context
    private lateinit var preferences: SharedPreferences

    @Before
    fun setUp() {
        targetContext = InstrumentationRegistry.getInstrumentation().targetContext
        preferences = PreferenceManager.getDefaultSharedPreferences(targetContext)
        preferences.edit().clear().commit()
    }

    @After
    fun tearDown() {
        preferences.edit().clear().commit()
    }

    @Test
    fun when_started__then_preferences_screen_is_shown() {
        launchPreferencesScreen()

        assertPreferencesScreen()
    }

    @Test
    fun when_started__then_description_values_are_shown() {
        preferences.edit()
            .putString(PtoPreferences.PREF_NEWPAGE_TIME, "12:34")
            .putString(PtoPreferences.PREF_TEMPLATE_PAGE, "2010-03-13")
            .apply()

        launchPreferencesScreen()

        onView(allOf(
            withText(containsString("12:34")),
            hasSibling(withText(R.string.pref_newpage_time_title)),
        )).perform(scrollToUnconditionally()).check(matches(isDisplayed()))
        onView(allOf(
            withText(allOf(containsString("2010"), containsString("13"))),
            hasSibling(withText(R.string.pref_template_page)),
        )).perform(scrollToUnconditionally()).check(matches(isDisplayed()))
    }

    @Test
    fun when_started_with_ACTION_ZIP_IMPORT__then_zip_import_dialog_is_shown() {
        launchPreferencesScreen(ACTION_ZIP_IMPORT)

        assertZipImportScreen()
    }

    @Test
    fun given_arguments_contain_throwable__when_import_screen_is_started__then_it_shows_the_error() {
        launchPreferencesScreen(ACTION_ZIP_IMPORT, throwable = Exception("testing exception"))

        onView(withText(containsString("testing exception")))
            .check(matches(isDisplayed()))
    }

    @Test
    fun given_showed_the_throwable__when_recreated__then_it_does_not_show_zip_import_again() {
        launchPreferencesScreen(ACTION_ZIP_IMPORT, throwable = Exception("testing exception"))

        pressBack()
        scenario.recreate()

        assertPreferencesScreen()
    }

    @Test
    fun when_newpage_time_is_changed__then_description_is_updated() {
        launchPreferencesScreen()
        onView(withText(R.string.pref_newpage_time_title))
            .perform(scrollToUnconditionally(), click())

        onView(isAssignableFrom(TimePicker::class.java))
            .perform(setTimePickerTime(LocalTime.of(10, 20)))
        onView(withText(android.R.string.ok))
            .perform(click())

        onView(allOf(
            withText(containsString("10:20")),
            hasSibling(withText(R.string.pref_newpage_time_title)),
        )).check(matches(isDisplayed()))
    }

    @Test
    fun when_template_date_is_changed__then_description_is_updated() {
        launchPreferencesScreen()
        onView(withText(R.string.pref_use_template))
            .perform(scrollToUnconditionally(), click())
        onView(withText(R.string.pref_template_page))
            .perform(scrollToUnconditionally(Rect(0, 0, 100, 300)), click())

        onView(isAssignableFrom(DatePicker::class.java))
            .perform(setDatePickerDate(LocalDate.of(2024, 10, 14)))
        onView(withText(android.R.string.ok))
            .perform(click())

        onView(allOf(
            withText(allOf(containsString("2024"), containsString("14"))),
            hasSibling(withText(R.string.pref_template_page)),
        )).check(matches(isDisplayed()))
    }

    @Test
    fun when_zip_import_is_clicked__then_zip_import_screen_is_shown() {
        launchPreferencesScreen()
        onView(isAssignableFrom(RecyclerView::class.java))
            .perform(swipeUp(), swipeUp())
        onView(withText(R.string.import_zip_title))
            .perform(scrollToUnconditionally(), click())

        assertZipImportScreen()
    }

    private fun launchPreferencesScreen(action: String? = null, throwable: Throwable? = null) {
        scenario = launchActivity(
            Intent(targetContext, PtoPreferencesActivity::class.java)
                .setAction(action)
                .putExtra(EXTRA_THROWABLE, throwable)
        )
    }

    private fun assertPreferencesScreen() {
        onView(withText(R.string.pref_newpage_time_title))
            .check(matches(isDisplayed()))
    }

    private fun assertZipImportScreen() {
        onView(withHint(R.string.import_choose_zip_hint))
            .perform(scrollToUnconditionally())
            .check(matches(isDisplayed()))
    }

}
