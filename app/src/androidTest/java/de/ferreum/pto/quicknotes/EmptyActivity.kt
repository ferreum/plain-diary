package de.ferreum.pto.quicknotes

import android.app.Activity
import android.os.Handler

internal class EmptyActivity : Activity() {
    override fun onResume() {
        super.onResume()
        Handler(mainLooper).postDelayed(::finish, 500)
    }
}
