package de.ferreum.pto.quicknotes

import android.content.Intent
import android.widget.EditText
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.Espresso.closeSoftKeyboard
import androidx.test.espresso.Espresso.pressBackUnconditionally
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.GeneralLocation
import androidx.test.espresso.action.GeneralSwipeAction
import androidx.test.espresso.action.Press
import androidx.test.espresso.action.Swipe
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import de.ferreum.pto.R
import de.ferreum.pto.quicknotes.NoteAppender.FileAccessor
import de.ferreum.pto.util.doOnEditText
import de.ferreum.pto.util.typedView
import org.hamcrest.Matchers.containsString
import org.hamcrest.Matchers.endsWith
import org.hamcrest.Matchers.matchesRegex
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.io.File

internal class QuickNoteActivityTest {

    private lateinit var scenario: ActivityScenario<QuickNoteActivity>

    @get:Rule
    val temporaryFolder = TemporaryFolder()

    private lateinit var dayFile: File

    @Before
    fun setUp() {
        dayFile = temporaryFolder.newFile("day_file.md")
        QuickNoteActivity.inactivityTimeoutMillis = 30000L
        QuickNoteActivity.fileAccessor = FileAccessor { _, _ -> dayFile }
    }

    @After
    fun tearDown() {
        QuickNoteActivity.inactivityTimeoutMillis = 30000L
    }

    @Test
    fun when_started__then_quicknote_screen_shows() {
        startQuickNoteScreen()

        assertQuickNoteScreen()
    }

    @Test
    fun given_text_is_entered__when_save_is_clicked__then_text_is_appended_and_screen_closes() {
        startQuickNoteScreen()

        onView(withHint(R.string.quicknotes_input_hint))
            .perform(typeText("test"))
        clickSaveButton()

        assertQuickNoteScreenClosed()
        assertAppendedContent("test")
    }

    @Test
    fun given_text_is_empty__when_save_is_clicked__then_empty_message_is_appended_and_screen_closes() {
        startQuickNoteScreen()

        clickSaveButton()

        assertQuickNoteScreenClosed()
        assertAppendedContent("")
    }

    @Test
    fun given_text_is_entered__when_back_pressed__then_text_is_appended() {
        startQuickNoteScreen()

        inputMessageText("test")

        closeSoftKeyboard()
        pressBackUnconditionally()

        assertQuickNoteScreenClosed()
        assertAppendedContent("test")
    }

    @Test
    fun given_text_is_empty__when_back_pressed__then_no_text_is_appended() {
        startQuickNoteScreen()

        closeSoftKeyboard()
        pressBackUnconditionally()

        assertQuickNoteScreenClosed()
        assertAppendedContent()
    }

    @Test
    fun given_text_is_entered__when_side_swiped__then_text_is_appended() {
        startQuickNoteScreen()

        inputMessageText("test")

        onView(withId(R.id.quicknoteContainer))
            .perform(topSwipeLeft())
        Thread.sleep(100)
        onIdle()

        assertQuickNoteScreenClosed()
        assertAppendedContent("test")
    }

    @Test
    fun given_text_is_empty__when_side_swiped__then_no_text_is_appended() {
        startQuickNoteScreen()

        onView(withId(R.id.quicknoteContainer))
            .perform(topSwipeRight())
        Thread.sleep(100)
        onIdle()

        assertQuickNoteScreenClosed()
        assertAppendedContent()
    }

    @Test
    fun when_idle_for_timeout_duration__then_activity_closes_itself() {
        QuickNoteActivity.inactivityTimeoutMillis = 500L
        startQuickNoteScreen()

        Thread.sleep(800L)

        assertQuickNoteScreenClosed()
    }

    @Test
    fun given_text_is_entered__when_idle_for_timeout_duration__then_text_is_appended() {
        QuickNoteActivity.inactivityTimeoutMillis = 500L
        startQuickNoteScreen()

        inputMessageText("test")
        Thread.sleep(800L)

        assertQuickNoteScreenClosed()
        assertAppendedContent("test")
    }

    @Test
    fun given_text_is_entered__when_recreated__then_text_is_preserved_and_not_appended() {
        startQuickNoteScreen()

        inputMessageText("test")
        scenario.recreate()

        assertQuickNoteScreen()
        onView(isAssignableFrom(EditText::class.java))
            .check(matches(withText(endsWith(" test"))))
        assertAppendedContent()
    }

    @Test
    fun given_text_is_entered__when_paused_and_resumed__then_text_is_preserved_and_not_appended() {
        startQuickNoteScreen()

        inputMessageText("test")
        scenario.moveToState(Lifecycle.State.STARTED)
        onIdle()
        scenario.moveToState(Lifecycle.State.RESUMED)

        assertQuickNoteScreen()
        onView(isAssignableFrom(EditText::class.java))
            .check(matches(withText(endsWith(" test"))))
        assertAppendedContent()
    }

    @Test
    fun given_text_is_entered__when_stopped__then_text_is_appended() {
        val testContext = InstrumentationRegistry.getInstrumentation().context
        startQuickNoteScreen()

        inputMessageText("test")
        // Need this EmptyActivity hack because scenario.moveToState(CREATED) leaks an activity
        // that blocks other tests.
        scenario.onActivity { it.startActivity(Intent(testContext, EmptyActivity::class.java)) }
        Thread.sleep(500)
        // how it should be, preferably without sleep
        // scenario.moveToState(Lifecycle.State.CREATED)
        // Thread.sleep(500)

        assertAppendedContent("test")
    }

    @Test
    fun when_interacted_within_timeout_duration__then_activity_does_not_close() {
        QuickNoteActivity.inactivityTimeoutMillis = 500L
        startQuickNoteScreen()

        Thread.sleep(150L)
        assertTrue(scenario.state.isAtLeast(Lifecycle.State.RESUMED))
        onView(withId(R.id.messageInput))
            .perform(swipeUp())
        Thread.sleep(150L)
        assertTrue(scenario.state.isAtLeast(Lifecycle.State.RESUMED))
        onView(withId(R.id.messageInput))
            .perform(swipeDown())
        Thread.sleep(150L)
        assertTrue(scenario.state.isAtLeast(Lifecycle.State.RESUMED))
        onView(withId(R.id.messageInput))
            .perform(swipeUp())
        Thread.sleep(150L)
        assertTrue(scenario.state.isAtLeast(Lifecycle.State.RESUMED))
        onView(withId(R.id.messageInput))
            .perform(swipeDown())
        Thread.sleep(150L)

        assertQuickNoteScreen()
    }

    @Test
    fun when_text_is_changed_within_timeout_duration__then_activity_does_not_close() {
        QuickNoteActivity.inactivityTimeoutMillis = 500L
        startQuickNoteScreen()

        Thread.sleep(300L)
        assertTrue(scenario.state.isAtLeast(Lifecycle.State.RESUMED))
        inputMessageText("first test")
        Thread.sleep(300L)
        assertTrue(scenario.state.isAtLeast(Lifecycle.State.RESUMED))
        clearMessageText()
        Thread.sleep(300L)
        assertTrue(scenario.state.isAtLeast(Lifecycle.State.RESUMED))
        inputMessageText("better test")
        Thread.sleep(300L)

        assertQuickNoteScreen()
    }

    @Test
    fun when_append_fails__then_it_stays_open_and_shows_error_dialog() {
        QuickNoteActivity.fileAccessor = FileAccessor { _, _ -> throw Exception("testing") }
        startQuickNoteScreen()

        inputMessageText("text")
        onView(withText(R.string.quicknotes_confirm_button))
            .perform(click())

        onView(withText(containsString("testing")))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
        onView(withText(android.R.string.ok))
            .inRoot(isDialog())
            .perform(click())
        assertQuickNoteScreen()
    }

    @Test
    fun when_append_fails_with_swipe__then_it_resets_scroll_and_shows_error_dialog() {
        QuickNoteActivity.fileAccessor = FileAccessor { _, _ -> throw Exception("testing") }
        startQuickNoteScreen()

        inputMessageText("text")
        onView(withId(R.id.quicknoteContainer))
            .perform(topSwipeRight())

        onView(withText(containsString("testing")))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
        onView(withText(android.R.string.ok))
            .inRoot(isDialog())
            .perform(click())
        assertQuickNoteScreen()
    }

    @Test
    fun when_started__then_it_shows_prefix_text() {
        startQuickNoteScreen()

        onView(isAssignableFrom(EditText::class.java))
            .check(matches(withText(matchesRegex("""^\+ \d{1,2}:\d\d $"""))))
    }

    @Test
    fun when_text_is_changed__then_it_restores_prefix_text() {
        startQuickNoteScreen()

        onView(isAssignableFrom(EditText::class.java))
            .perform(replaceText("+ test test"))

        onView(isAssignableFrom(EditText::class.java))
            .check(matches(withText(matchesRegex("""^\+ \d{1,2}:\d\d test test$"""))))
    }

    @Test
    fun when_prefix_is_selected__then_it_excludes_prefix_from_selection() {
        startQuickNoteScreen()

        inputMessageText("test")
        onView(isAssignableFrom(EditText::class.java))
            .perform(doOnEditText { setSelection(it.length - 6) })

        onView(isAssignableFrom(EditText::class.java))
            .check(matches(typedView<EditText> {
                it.selectionStart == it.text.length - 4
                    && it.selectionEnd == it.text.length - 4
            }))
    }

    @Test
    fun when_everything_is_selected__then_it_excludes_prefix_from_selection() {
        startQuickNoteScreen()

        inputMessageText("test")
        onView(isAssignableFrom(EditText::class.java))
            .perform(doOnEditText { selectAll() })

        onView(isAssignableFrom(EditText::class.java))
            .check(matches(typedView<EditText> {
                it.selectionStart == it.text.length - 4
                    && it.selectionEnd == it.text.length
            }))
    }

    private fun startQuickNoteScreen() {
        scenario = launchActivity()
    }

    private fun clickSaveButton() {
        onView(withText(R.string.quicknotes_confirm_button))
            .perform(click())
    }

    private fun inputMessageText(text: String) {
        onView(isAssignableFrom(EditText::class.java))
            .perform(typeText(text))
    }

    private fun clearMessageText() {
        onView(isAssignableFrom(EditText::class.java))
            .perform(clearText())
    }

    private fun assertQuickNoteScreen() {
        assertTrue(scenario.state.isAtLeast(Lifecycle.State.RESUMED))
        onView(withId(R.id.messageInput)).check(matches(isDisplayed()))
    }

    private fun assertQuickNoteScreenClosed() {
        assertFalse(scenario.state.isAtLeast(Lifecycle.State.RESUMED))
    }

    private val lineRegex = Regex("""^\+ \d{1,2}:\d\d(?: (.*)?)?$""")
    private fun assertAppendedContent(vararg expectedLines: String) {
        val actual = dayFile.readLines().mapIndexedNotNull { index, line ->
            if (index % 2 == 0) {
                assertEquals("", line)
                null
            } else {
                val match = lineRegex.matchEntire(line)
                checkNotNull(match) { "unmatched line $index [$line]" }
                match.groups[1]?.value.orEmpty()
            }
        }
        assertEquals(expectedLines.toList(), actual)
    }

    /** Swipe left along top of view */
    private fun topSwipeLeft(): ViewAction? = actionWithAssertions(
        GeneralSwipeAction(
            Swipe.FAST,
            GeneralLocation.translate(GeneralLocation.TOP_RIGHT, -0.2f, 0.2f),
            GeneralLocation.TOP_LEFT,
            Press.FINGER
        )
    )

    /** Swipe right along top of view */
    private fun topSwipeRight(): ViewAction? = actionWithAssertions(
        GeneralSwipeAction(
            Swipe.FAST,
            GeneralLocation.translate(GeneralLocation.TOP_LEFT, 0.2f, 0.2f),
            GeneralLocation.TOP_RIGHT,
            Press.FINGER
        )
    )

}
