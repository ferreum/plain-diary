package de.ferreum.pto.search

import android.text.Editable
import android.text.Selection
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.BackgroundColorSpan
import androidx.core.text.getSpans
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class SearchTokensTest {

    private val callbackCalls = mutableListOf<TextContent>()
    private val callback: (Editable) -> Unit = { callbackCalls += it.getContent() }

    private val spanCycler = SpanCycler(listOf(1, 2, 3, 4))

    private lateinit var sanitizer: TokenSpanInputSanitizer

    @Before
    fun setUp() {
        sanitizer = TokenSpanInputSanitizer(callback)
    }

    @Test
    fun watcher_does_nothing_without_spans() {
        withText("test abc") {
            setSelection(8)

            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test abc", 8..8)
        }
    }

    @Test
    fun watcher_does_nothing_for_correct_spans() {
        withText("test abc ") {
            setTokenSpan(0..3, 1)
            setTokenSpan(5..7, 2)
            setSelection(8)

            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test abc ", 8..8, 0..3 to 1, 5..7 to 2)
        }
    }

    @Test
    fun watcher_handles_more_spaces_between_tokens() {
        withText("test  abc ") {
            setTokenSpan(6..8, 1)
            setSelection(10)

            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test  abc ", 10..10, 6..8 to 1)
        }
    }

    @Test
    fun watcher_ensures_space_after_span() {
        withText("test") {
            setTokenSpan(0..3, 1)
            setSelection(4)

            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test ", 5..5, 0..3 to 1)
        }
    }

    @Test
    fun watcher_inserts_space_between_spans() {
        withText("testabc") {
            setTokenSpan(0..3, 1)
            setTokenSpan(4..6, 2)
            setSelection(7)

            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test abc ", 9..9, 0..3 to 1, 5..7 to 2)
        }
    }

    @Test
    fun watcher_inserts_space_between_many_spans() {
        withText("testaaabbbccc") {
            setTokenSpan(0..3, 1)
            setTokenSpan(4..6, 2)
            setTokenSpan(7..9, 3)
            setTokenSpan(10..12, 4)
            setSelection(13)

            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test aaa bbb ccc ", 17..17, 0..3 to 1, 5..7 to 2, 9..11 to 3, 13..15 to 4)
        }
    }

    @Test
    fun watcher_inserts_space_before_free_span() {
        withText("testabc") {
            setTokenSpan(0..3, 1)
            setSelection(7)

            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test abc", 8..8, 0..3 to 1)
        }
    }

    @Test
    fun watcher_inserts_space_after_free_span() {
        withText("testabc") {
            setTokenSpan(4..6, 1)
            setSelection(7)

            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test abc ", 9..9, 5..7 to 1)
        }
    }

    @Test
    fun watcher_moves_cursor_back_when_backspacing_over_space() {
        withText("test ") {
            setTokenSpan(0..3, 1)
            setSelection(5)

            replace(4, 5, "")

            assertContentAndLastCallback("test ", 4..4, 0..3 to 1)
        }
    }

    @Test
    fun cycleAtSelection_does_nothing_on_empty_text() {
        withText("") {
            setSelection(0)

            val result = spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)

            assertEquals(result, null)
            assertContentAndLastCallback("", 0..0)
        }
    }

    @Test
    fun cycleAtSelection_does_nothing_on_only_blanks_after_cursor() {
        withText(" ") {
            setSelection(0)

            val result = spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)

            assertEquals(result, null)
            assertContentAndLastCallback(" ", 0..0)
        }
    }
    @Test
    fun cycleAtSelection_creates_token_of_blanks_before_cursor() {
        withText("  ") {
            setSelection(1)

            val result = spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)

            assertEquals(result, 1)
            assertContentAndLastCallback("  ", 2..2, 0..0 to 1)
        }
    }

    @Test
    fun cycleAtSelection_does_not_include_space_after_cursor_and_moves_cursor() {
        withText("test ") {
            setSelection(4)

            spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test ", 5..5, 0..3 to 1)
        }
    }

    @Test
    fun cycleAtSelection_adds_token_after_previous_token() {
        withText("test abc") {
            setTokenSpan(0..3, 1)
            setSelection(8)

            spanCycler.cycleAtSelection(this, 2)
            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test abc ", 9..9, 0..3 to 1, 5..7 to 2)
        }
    }

    @Test
    fun cycleAtSelection_adds_whitespace_up_to_cursor() {
        withText("test    abc ") {
            setTokenSpan(8..10, 1)
            setSelection(6)

            spanCycler.cycleAtSelection(this, 2)
            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test    abc ", 6..6, 0..5 to 2, 8..10 to 1)
        }
    }

    @Test
    fun cycleAtSelection_cycles_to_next_color() {
        withText("test ") {
            setTokenSpan(0..3, 1)
            setSelection(1)

            spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test ", 1..1, 0..3 to 2)
        }
    }

    @Test
    fun cycleAtSelection_cycles_from_last_to_first_color() {
        withText("test ") {
            setTokenSpan(0..3, 4)
            setSelection(1)

            spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test ", 1..1, 0..3 to 1)
        }
    }

    @Test
    fun cycleAtSelection_cycles_multiple() {
        withText("aaa bbb ccc ddd ") {
            setTokenSpan(0..2, 1)
            setTokenSpan(4..6, 2)
            setTokenSpan(8..10, 3)
            setTokenSpan(12..14, 4)
            setSelection(1)
            spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)
            setSelection(5)
            spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)
            setSelection(9)
            spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)
            setSelection(13)
            spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("aaa bbb ccc ddd ", 13..13,
                    0..2 to 2, 4..6 to 3, 8..10 to 4, 12..14 to 1)
        }
    }

    @Test
    fun cycleAtSelection_cycles_span_ending_at_cursor() {
        withText("test abc ") {
            setTokenSpan(0..3, 1)
            setTokenSpan(5..7, 3)
            setSelection(4)

            spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test abc ", 4..4, 0..3 to 2, 5..7 to 3)
        }
    }

    @Test
    fun cycleAtSelection_cycles_span_starting_at_cursor() {
        withText("test abc ") {
            setTokenSpan(0..3, 1)
            setTokenSpan(5..7, 3)
            setSelection(5)

            spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test abc ", 5..5, 0..3 to 1, 5..7 to 4)
        }
    }

    @Test
    fun cycleAtSelection_creates_span_for_selection_range() {
        withText("test abc ") {
            setTokenSpan(0..3, 2)
            setSelection(5, 8)

            spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test abc ", 5..8, 0..3 to 2, 5..7 to 1)
        }
    }

    @Test
    fun cycleAtSelection_cycles_selection_range() {
        withText("test abc ") {
            setTokenSpan(0..3, 1)
            setTokenSpan(5..7, 3)
            setSelection(5, 8)

            spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test abc ", 5..8, 0..3 to 1, 5..7 to 4)
        }
    }

    @Test
    fun cycleAtSelection_cycles_previous_span_with_cursor_at_end() {
        withText("test ") {
            setTokenSpan(0..3, 1)
            setSelection(5)

            spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test ", 5..5, 0..3 to 2)
        }
    }

    @Test
    fun cycleAtSelection_cycles_single_character() {
        withText("a ") {
            setTokenSpan(0..0, 1)
            setSelection(2)

            spanCycler.cycleAtSelection(this)
            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("a ", 2..2, 0..0 to 2)
        }
    }

    @Test
    fun it_sanitizes_whitespace_on_tokenize_of_selection() {
        withText("test abc ") {
            setTokenSpan(5..7, 1)
            setSelection(0, 5)

            spanCycler.cycleAtSelection(this, 2)
            sanitizer.sanitizeText(this)

            assertContentAndLastCallback("test  abc ", 0..6, 0..4 to 2, 6..8 to 1)
        }
    }

    @Test
    fun toTokenList_lists_existing_tokens() {
        withText("test abc") {
            setTokenSpan(0..3, 1)
            setTokenSpan(5..7, 1)
            setSelection(8)

            sanitizer.sanitizeText(this)

            assertEquals(listOf(
                    TextWithColor("test", 1),
                    TextWithColor("abc", 1)), toTokenList())
        }
    }

    @Test
    fun toTokenList_returns_whole_free_token() {
        withText("test abc") {
            setSelection(8)

            sanitizer.sanitizeText(this)

            assertEquals(listOf("test abc"), toTokenList().map { it.text })
        }
    }

    @Test
    fun toTokenList_includes_free_token() {
        withText("test abc") {
            setTokenSpan(0..3, 1)
            setSelection(8)

            sanitizer.sanitizeText(this)

            assertEquals(listOf("test", "abc"), toTokenList().map { it.text })
        }
    }

    private fun withText(text: String, block: Editable.() -> Unit) {
        SpannableStringBuilder(text).apply {
            setSpan(sanitizer, 0, length, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        }.block()
    }

    private fun Spannable.setSelection(start: Int, end: Int = start) {
        Selection.setSelection(this, start, end)
    }

    private fun Spanned.assertContentAndLastCallback(
            string: String,
            selection: IntRange,
            vararg colorSpans: Pair<IntRange, Int>,
    ) {
        val expected = TextContent(string, selection, listOf(*colorSpans))
        assertContent(expected, "current content")
        callbackCalls.last().assertEquals(expected, "last callback")
    }

    private fun Spanned.assertContent(expected: TextContent, msg: String? = null) {
        getContent().assertEquals(expected, msg)
    }

    private fun Spanned.getContent() = TextContent(toString(), getSelection(), getColorSpans())

    private fun Spanned.getSelection(): IntRange {
        return Selection.getSelectionStart(this)..Selection.getSelectionEnd(this)
    }

    private fun Spanned.getColorSpans(): List<Pair<IntRange, Int>> {
        val tokenSpans = getSpans<BackgroundColorSpan>(0, length)
        tokenSpans.sortBy { getSpanStart(it) }
        return tokenSpans.map { span ->
            getSpanStart(span) until getSpanEnd(span) to span.backgroundColor
        }
    }

    data class TextContent(
            val string: String,
            val selection: IntRange,
            val colorSpans: List<Pair<IntRange, Int>>,
    ) {
        fun assertEquals(expected: TextContent, msg: String? = null) {
            val m = msg?.let { "$msg " } ?: ""
            assertEquals("${m}string", expected.string, string)
            assertEquals("${m}selection", expected.selection, selection)
            assertEquals("${m}colorSpans", expected.colorSpans, colorSpans)
        }
    }

}
