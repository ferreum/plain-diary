package de.ferreum.pto.textadjust

import android.text.Selection
import android.text.SpannableString
import android.view.MotionEvent
import androidx.core.text.buildSpannedString
import androidx.test.platform.app.InstrumentationRegistry
import de.ferreum.pto.R
import de.ferreum.pto.textadjust.InplaceTextAdjuster.AdjustContext
import de.ferreum.pto.textadjust.InplaceTextAdjuster.TextWithSelection
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

internal class EventReminderGeneratorTest {

    private var stepSize: Int = 0

    private lateinit var generator: EventReminderGenerator

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        stepSize = context.resources.getDimensionPixelSize(R.dimen.drag_adjust_step_size)
        generator = EventReminderGenerator(context)
    }

    @Test
    fun adjust_adjustsImplicitEmpty_onTime() {
        assertEquals("@12:00 -3m|", testAdjust("@12:|00", 0, 3))
    }

    @Test
    fun adjust_adjustsImplicitFlagged_onTime() {
        assertEquals("@12:00 -3m!|", testAdjust("@12:|00!", 0, 3))
    }

    @Test
    fun adjust_adjustsImplicitFlagged_onTime_withMoreSpecs() {
        assertEquals("@12:00 -3m!| -1h", testAdjust("@12:|00! -1h", 0, 3))
    }

    @Test
    fun adjust_adjustsImplicitFlagged_onSpec_withMoreSpecs() {
        assertEquals("@12:00 -3m!| -1h", testAdjust("@12:00!| -1h", 0, 3))
    }

    @Test
    fun adjust_adjustsImplicitEmpty_onTail() {
        assertEquals("@12:00 -3m| test", testAdjust("@12:00 test|", 0, 3))
    }

    @Test
    fun adjust_adjustsImplicitEmpty_onTail_betweenSpaces() {
        assertEquals("@12:00 -3m| test  test", testAdjust("@12:00 test | test", 0, 3))
    }

    @Test
    fun adjust_adjustsImplicitEmpty_betweenSpaces() {
        assertEquals("@12:00 -3m| test", testAdjust("@12:00 | test", 0, 3))
    }

    @Test
    fun adjust_insertsNewAfterImplicitFlagged_betweenSpaces() {
        assertEquals("@12:00! -3m| test", testAdjust("@12:00! | test", 0, 3))
    }

    @Test
    fun adjust_insertsNewAfterImplicitFlagged_onTail() {
        assertEquals("@12:00! -3m| test", testAdjust("@12:00! test|", 0, 3))
    }

    @Test
    fun adjust_insertsNewAfterImplicitFlagged_betweenSpecs() {
        assertEquals("@12:00! -3m| -1h test", testAdjust("@12:00! | -1h test", 0, 3))
    }

    @Test
    fun adjust_adjustsImplicitFlagged_onTime_withWhitespace() {
        assertEquals("@12:00 -3m!| -1h test", testAdjust("@12|:00 ! -1h test", 0, 3))
    }

    @Test
    fun adjust_adjustsImplicitFlagged_onSpec_withWhitespace() {
        assertEquals("@12:00 -3m!| -1h test", testAdjust("@12:00 |! -1h test", 0, 3))
    }

    @Test
    fun adjust_insertsNewAfterImplicitFlagged_betweenSpecs_withWhitespace() {
        assertEquals("@12:00! -3m| -1h test", testAdjust("@12:00 ! | -1h test", 0, 3))
    }

    @Test
    fun adjust_insertsNewAfterImplicitFlagged_betweenTimeAndFlag() {
        assertEquals("@12:00 -3m!| -1h test", testAdjust("@12:00 | ! -1h test", 0, 3))
    }

    @Test
    fun adjust_adjustsImplicitEmpty_onSpec() {
        assertEquals("@12:00 -3m| test", testAdjust("@12:00| test", 0, 3))
    }

    @Test
    fun adjust_adjustsImplicitFlagged_onSpec() {
        assertEquals("@12:00 -3m!| test", testAdjust("@12:00!| test", 0, 3))
    }

    @Test
    fun adjust_insertsNewAfterSpec_onTail() {
        assertEquals("@12:00 -1h -3m| test", testAdjust("@12:00 -1h |test", 0, 3))
    }

    @Test
    fun adjust_insertsNewAfterSpec_onTail_withImplicitFlagged() {
        assertEquals("@12:00! -1h -3m| test", testAdjust("@12:00! -1h |test", 0, 3))
    }

    @Test
    fun adjust_adjustsSpec_onSpec() {
        assertEquals("@12:00 -4m| test", testAdjust("@12:00 -1|m test", 0, 3))
    }

    @Test
    fun adjust_insertsNewFirstSpec_onTime() {
        assertEquals("@12:00 -3m| -1h test", testAdjust("@12:|00 -1h test", 0, 3))
    }

    @Test
    fun adjust_insertsNewLastSpec_onTail() {
        assertEquals("@12:00 -1h -3m| test", testAdjust("@12:00 -1h te|st", 0, 3))
    }

    @Test
    fun adjust_modifiesLeftSpec_onLastSpecChar() {
        assertEquals("@12:00 -8m| -15m test", testAdjust("@12:00 -5m| -15m test", 0, 3))
    }

    @Test
    fun adjust_modifiesLeftSpec_onFirstSpecChar() {
        assertEquals("@12:00 -5m -18m| test", testAdjust("@12:00 -5m -15m| test", 0, 3))
    }

    @Test
    fun adjust_adjustsLastSpecNumber() {
        assertEquals("@12:00 -1h8m| test", testAdjust("@12:00 -1h5m| test", 0, 3))
    }

    @Test
    fun adjust_insertsNew_betweenSpecs() {
        assertEquals("@12:00 -1h -3m| -2h test", testAdjust("@12:00 -1h | -2h test", 0, 3))
    }

    @Test
    fun adjust_insertsNewFirst_betweenSpecs() {
        assertEquals("@12:00 -3m| -1h -2h test", testAdjust("@12:00 | -1h -2h test", 0, 3))
    }

    @Test
    fun adjust_insertsNewLast_betweenSpecs() {
        assertEquals("@12:00 -1h -2h -3m| test", testAdjust("@12:00 -1h -2h | test", 0, 3))
    }

    @Test
    fun adjust_insertsNewLast_betweenWhitespaceOnTail() {
        assertEquals("@12:00 -1h -2h -3m| test  test", testAdjust("@12:00 -1h -2h test | test", 0, 3))
    }

    @Test
    fun adjust_turnsSingleNegativeUnitPositive() {
        assertEquals("@12:00 +8m| test", testAdjust("@12:00 -5m| test", 0, -13))
    }

    @Test
    fun adjust_turnsSinglePositiveUnitNegative() {
        assertEquals("@12:00 -3m| test", testAdjust("@12:00 +5m| test", 0, 8))
    }

    @Test
    fun adjust_clampsLastSpecNumberToZero() {
        assertEquals("@12:00 -1h0m| test", testAdjust("@12:00 -1h5m| test", 0, -15))
    }

    @Test
    fun adjust_adjustsSeconds() {
        assertEquals("@12:00 -8s| test", testAdjust("@12:00 -5s| test", 0, 3))
    }

    @Test
    fun adjust_adjustsMinutes() {
        assertEquals("@12:00 -8m| test", testAdjust("@12:00 -5m| test", 0, 3))
    }

    @Test
    fun adjust_adjustsHours() {
        assertEquals("@12:00 -8h| test", testAdjust("@12:00 -5h| test", 0, 3))
    }

    @Test
    fun adjust_adjustsDays() {
        assertEquals("@12:00 -8d| test", testAdjust("@12:00 -5d| test", 0, 3))
    }

    @Test
    fun adjust_insertsPositive() {
        assertEquals("@12:00 +3m| test", testAdjust("@12:00 |test", 0, -3))
    }

    @Test
    fun adjust_insertsPositive_withSpec_onTail() {
        assertEquals("@12:00 -1h +3m| test", testAdjust("@12:00 -1h |test", 0, -3))
    }

    @Test
    fun adjust_insertsPositive_withSpec_onTime() {
        assertEquals("@12:00 +3m| -1h test", testAdjust("@12:00| -1h test", 0, -3))
    }

    @Test
    fun adjust_insertsPositive_withSpec_betweenSpecs() {
        assertEquals("@12:00 -1h +3m| -2h test", testAdjust("@12:00 -1h | -2h test", 0, -3))
    }

    private fun testAdjust(currentText: String, prevStep: Int, currentStep: Int, startText: String? = null): String? {
        val currentSpannedText = makeText(currentText)
        var matchStart = 0
        var matchEnd = 0
        val editorText = buildSpannedString {
            append("first line\n")
            matchStart = length
            append(currentSpannedText).append("\n")
            matchEnd = length - 1
            append("last line\n")
        }
        return generator.adjust(
            startText ?: currentSpannedText.toString(),
            currentSpannedText.toString(),
            event(0),
            event(prevStep),
            event(currentStep),
            AdjustContext(editorText, matchStart, matchEnd),
        )?.toTestString()
    }

    private fun TextWithSelection.toTestString(): String {
        return buildString {
            append(text)
            insert(selection, "|")
        }
    }

    private fun makeText(text: String): CharSequence {
        val sel = text.indexOf("|")
        return SpannableString(text.replaceFirst("|", "")).apply {
            Selection.setSelection(this, sel)
        }
    }

    private fun event(step: Int): MotionEvent {
        val coords = MotionEvent.PointerCoords()
        coords.y = (step * stepSize).toFloat()
        return MotionEvent.obtain(0, 0, 0, 1,
            arrayOf(MotionEvent.PointerProperties()),
            arrayOf(coords), 0, 0, 0f, 0f, 0, 0, 0, 0)
    }
}
