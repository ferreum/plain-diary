package de.ferreum.pto.util

import android.graphics.Rect
import android.view.View
import android.widget.DatePicker
import android.widget.EditText
import android.widget.TimePicker
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.ViewMatchers
import org.hamcrest.BaseMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import java.time.LocalDate
import java.time.LocalTime

internal fun scrollToUnconditionally(rect: Rect? = null) = object : ViewAction {
    override fun getConstraints(): Matcher<View> = ViewMatchers.isAssignableFrom(View::class.java)
    override fun getDescription(): String = "scroll to view unconditionally"
    override fun perform(uiController: UiController, view: View?) {
        view?.requestRectangleOnScreen(
            rect ?: Rect(0, 0, view.width, view.height),
            true
        )
        uiController.loopMainThreadUntilIdle()
    }
}

internal fun setDatePickerDate(date: LocalDate): ViewAction {
    return object : ViewAction {
        override fun getConstraints(): Matcher<View> = Matchers.allOf(
            ViewMatchers.isAssignableFrom(DatePicker::class.java),
            ViewMatchers.isDisplayingAtLeast(90),
        )

        override fun getDescription(): String = "set DatePicker date to $date"

        override fun perform(uiController: UiController?, view: View?) {
            view as DatePicker
            view.updateDate(date.year, date.monthValue - 1, date.dayOfMonth)
        }
    }
}

internal fun setTimePickerTime(time: LocalTime): ViewAction {
    return object : ViewAction {
        override fun getConstraints(): Matcher<View> = Matchers.allOf(
            ViewMatchers.isAssignableFrom(TimePicker::class.java),
            ViewMatchers.isDisplayingAtLeast(90),
        )

        override fun getDescription(): String = "set TimePicker time to $time"

        override fun perform(uiController: UiController?, view: View?) {
            view as TimePicker
            view.hour = time.hour
            view.minute = time.minute
        }
    }
}

internal fun doOnEditText(action: EditText.(text: CharSequence) -> Unit): ViewAction {
    return object : ViewAction {
        override fun getConstraints(): Matcher<View> = Matchers.allOf(
            ViewMatchers.isAssignableFrom(EditText::class.java),
        )

        override fun getDescription(): String = "perform action on EditText"

        override fun perform(uiController: UiController?, view: View?) {
            view as EditText
            view.action(view.text)
        }
    }
}

internal inline fun <reified T : View> typedView(crossinline match: (T) -> Boolean): Matcher<in View> {
    return object : BaseMatcher<View>() {
        override fun describeTo(description: Description?) {
            description?.appendText("typedView of ${T::class.java.name}")
        }

        override fun matches(actual: Any?): Boolean {
            return ViewMatchers.isAssignableFrom(T::class.java).matches(actual) && match(actual as T)
        }
    }
}
