package de.ferreum.pto.util

import android.net.Uri
import org.junit.Assert
import org.junit.Test
import java.time.LocalDate

class IntentsTest {

    @Test
    fun getDateFromUri_matches_date_uri() {
        val results = listOf(
                getDateFromUri(Uri.parse("date:2020-01-02")),
                getDateFromUri(Uri.parse("date:2020.03.04")),
                getDateFromUri(Uri.parse("date:2020/05/06")),
        )

        Assert.assertEquals(listOf(
                LocalDate.of(2020, 1, 2),
                LocalDate.of(2020, 3, 4),
                LocalDate.of(2020, 5, 6),
        ), results)
    }

    @Test
    fun getDateFromUri_matches_diary_date_uri() {
        val result = getDateFromUri(Uri.parse("diary://date/2020/01/02"))

        Assert.assertEquals(LocalDate.of(2020, 1, 2), result)
    }

    @Test
    fun getDateFromUri_returns_null_on_invalid_date_uri() {
        val results = listOf(
                getDateFromUri(Uri.parse("date:2020-13-02")),
                getDateFromUri(Uri.parse("date:2020.03.32")),
                getDateFromUri(Uri.parse("date:2020_05_06")),
        )

        Assert.assertEquals(arrayOfNulls<LocalDate>(3).toList(), results)
    }

    @Test
    fun getDateFromUri_returns_null_on_other_uris() {
        val results = listOf(
                getDateFromUri(Uri.parse("diary://test/2020/13/02")),
                getDateFromUri(Uri.parse("date:abc")),
                getDateFromUri(Uri.parse("file://test/2020/10/11")),
        )

        Assert.assertEquals(arrayOfNulls<LocalDate>(3).toList(), results)
    }

}
