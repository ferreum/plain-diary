package de.ferreum.pto.util

import kotlin.reflect.KClass

inline fun assertThrows(throwable: KClass<out Throwable>, block: () -> Unit) {
    assertThrows(
        throwable = { throwable.isInstance(it) },
        msg = "$throwable",
        block = block,
    )
}

inline fun assertThrows(throwable: Throwable, block: () -> Unit) {
    assertThrows(
        throwable = { it === throwable },
        msg = "$throwable",
        block = block,
    )
}

inline fun assertThrows(throwable: (Throwable) -> Boolean, msg: String, block: () -> Unit) {
    runCatching(block).fold(
        onSuccess = { throw AssertionError("expected $msg to be thrown but nothing was thrown") },
        onFailure = { t ->
            if (!throwable(t)) {
                throw AssertionError("expected $msg to be thrown but $t was thrown", t)
            }
        }
    )
}
