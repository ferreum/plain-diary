package de.ferreum.pto.util

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider
import androidx.test.platform.app.InstrumentationRegistry
import de.ferreum.pto.Constants
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import kotlin.coroutines.EmptyCoroutineContext

class UriInspectorImplTest {

    @get:Rule
    val temporaryFolder = TemporaryFolder()

    private lateinit var targetContext: Context

    private lateinit var inspector: UriInspector

    @Before
    fun setUp() {
        targetContext = InstrumentationRegistry.getInstrumentation().targetContext
        inspector = UriInspectorImpl(
            targetContext.contentResolver,
            EmptyCoroutineContext,
        )
    }

    @Test
    fun given_schema_is_content__when_getDisplayName_is_called__then_it_queries_the_display_name() =
        runBlocking {
            val file = temporaryFolder.newFile("test.zip")
            val uri = FileProvider.getUriForFile(
                targetContext,
                targetContext.packageName + Constants.FILE_PROVIDER_AUTHORITY_SUFFIX,
                file
            )

            val result = inspector.getDisplayName(uri)

            assertEquals("test.zip", result)
        }

    @Test
    fun given_uri_is_invalid__when_getDisplayName_is_called__then_it_returns_null() = runBlocking {
        val uri = Uri.parse("content://test/invalid/content/uri/999")

        val result = inspector.getDisplayName(uri)

        assertNull(result)
    }

}
