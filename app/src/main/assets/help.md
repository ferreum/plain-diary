PTO - Plaintext Organizer
-------------------------

Stay organized with one plaintext file per day.

## Features

* Powerful [text editor](#the-editor) with undo/redo, autosave, and more.
* Take [quick notes](#quick-notes) using a notification, without unlocking your
  device.
* Reminder notifications for upcoming [events](#event-times-and-reminders)
  (`@12:34 foobar`).
* Indicate reminders and notes on adjacent pages with [Hints](#hints).
* Fast, responsive chronological search through all files.
* Save and open links to other apps and webpages.
* Uses no network connection. All data is local on the device.
* Optional template for new pages.
* Backup/import pages to/from ZIP file.

## Format

In general any format should work fine. You can decide how to format your
notes. There are some elements that enable special features:

### Event times and reminders

Event times are indicated with `@` followed by a time of day, e.g. `@12:34`.

PTO can send notifications for upcoming events. This must be enabled with the
"Enable reminder notifications" preference under settings. For reliable
notification timing, a battery optimization exemption is requested. If this
exemption is not granted, notifications are not guaranteed to arrive on time.

These are the rules and features for event notifications:

* The time must be formatted like `@12:34`. For compatibility, the format
  `@:12:34` is also allowed.
* The text must be at the start of a line.
* Only 24 hour format supported for now.
* The reminder notification can be sent before the given time by adding a time
  offset: `@12:34 -1h` would send a notification one hour before the given
  time. Supported units are `s` (seconds), `m` (minutes), `h` (hours), `d`
  days. Multiple units can be combined, e.g. `@12:34 -1h30m` would notify at
  11:04.
* Multiple reminders are allowed by adding multiple times,
  e.g. `@12:34 -1h -15m`
* For technical reasons, the reminder duration should not exceed one day.
* A reminder can be sent after the event time with a positive value like `+5m`.
  * The duration should not cross day boundaries.
* A reminder notification can be made silent by suffixing it with `?`,
  e.g. `@12:34 -2h?` This applies only to that reminder: `@12:34 -2h? -1h`
  would send a silent notification at 10:34 and a regular notification at
  11:34. This can also be used without duration: `@12:34?` would send a silent
  notification at 12:34.
* A reminder can be marked important by suffixing it with `!`. Such reminders
  play the alarm sound until dismissed and may wake the device with a
  fullscreen display.
* The time and durations may be followed by a title, which is shown in the
  notification.

Future events on adjacent pages are indicated as [hints](#hints).

### Links

Links are recognized with markdown syntax. The add button can be used to insert
links in the expected format. "Sharing" a link to the PTO app inserts the link
in the correct format on the chosen page.

There are three supported formats:

* URL with description: `[description](https://example.com/)`
* URL: `<https://example.com/>`
* Plain URL: `https://example.com/`

URLs need not be http(s). Different URIs can be sent to other installed apps
which support them.

Individual pages can be linked using the scheme `date:YYYY-MM-DD`, so `[New
Year](date:2020-01-01)` would link to the first page of 2020. In the editor,
plain dates in the format `YYYY-MM-DD` are also recognized as links.

Links can be opened or modified with [contextual actions](#contextual-actions).

### Notes

Notes are intended as temporary quick entries to review later. The format is
simply `+ text` at the start of a line (where `text` can be arbitrary text).

Notes on adjacent pages are indicated as [hints](#hints).

The [quick notes](#quick-notes) feature uses this format.

## The editor

The editor is the main interface of the app. The app starts with the editor for
the current day.

### Undo/Redo

Changes can be reverted and restored using the undo and redo buttons. Beware
that undo history is cleared when when the app is restarted.

Undo/redo steps are very small. To quickly undo/redo more changes, press the
button and swipe down vertically. The wanted change can be quickly found by
swiping up and down. When returning to the start position, the original text
from before pressing down will be restored.

### Time button

This button features multiple functions to modify times of day in the editor:

* When tapped, it inserts the current time of day.
  * If there is a time value under the cursor, a time picker dialog is shown to
    modify it.
* Swipe up or down to adjust a time value minute-by-minute.

The button is contextual to the cursor position:

* If there is a time value under the cursor, it modifies that time.
* If there is a an [event line](#event-times-and-reminders) under the cursor,
  it modifies the time of the event.
* If not on a time value, this inserts a new time, starting with the current
  time of day.
* If at the start of the line, and the time is in the future, it is prefixed
  with `@`. This gets that time recognized as an
  [event time](#event-times-and-reminders) and will trigger a reminder, if
  enabled under settings.

### Reminder button

This button shows up if the cursor is on an
[event line](#event-times-and-reminders):

* When tapped, it brings up a dialog to edit the events's title and reminders
  in separate fields, and toggle the important (`!`) and silent (`?`) flags.
* Swipe up or down to adjust reminder times.
  * If there is a reminder time under the cursor, it adjusts the last unit of
    that reminder time (would change the minutes in `-1h15m`).
  * Otherwise, it inserts a new reminder time, starting with `-1m`. Swipe down
    to move the reminder back in time.

### Contextual actions

The bottom toolbar offers actions based on the content under the text cursor:

* [Links](#links): link symbol: Edit link text and URL separately in a dialog.
* [Links](#links): square-arrow symbol: Open the link (click) or copy the URL
  to clipboard (long click).
* Date (`YYYY-MM-DD`): square-arrow symbol: Open the page of that date (click) or
  copy the text to clipboard (long click).
* Time (`HH:MM`): See [time button](#time-button).
* Event reminders (`@12:34 -Xm foobar`): See [reminder button](#reminder-button).

### Hints

To help remembering notes and upcoming events, the editor indicates
[notes](#notes) and future [event times](#event-times-and-reminders) in
adjacent pages. Such entries are shown as hints on the sides of the screen. To
avoid obstructing editor content, they are hidden when interacting with the
editor.

### Navigation to other pages

There are multiple ways to navigate between pages:

* Swipe the toolbars (top and bottom) sideways to access the next or previous
  page.
* Click the date in the top toolbar to select a date with a calendar.
* In search results, a selected page is shown in a preview mode. Clicking the
  edit button navigates to the editor for that page.
* Pages can be linked within PTO from other pages (see [Links](#links)).

### File state indicator

Can have the following states:

* check mark &ndash; all good, the content of the editor is saved.
* diskette &ndash; unsaved changes have been made. The file will be auto-saved
  periodically while editing and when leaving the editor. The symbol can also
  be clicked to save immediately.
* red exclamation mark &ndash; error while loading or saving the file. Click it
  to see the error.

## Quick notes

PTO can show a notification that allows taking a note from anywhere, including
the lock screen without unlocking the device. There are two ways to get this
notification:

- Start it manually using the toolbar menu on the editor. This starts the
  notification once.
- Enable the "Autostart Notification" preference in the settings menu. This
  starts the notification whenever the app is opened and on device boot.

Clicking the button on the notification shows a text field to enter a note and
a button to save it. The text is appended to the current day's page, with a `+`
prefix and the current time. This format is recognized as a [note](#notes).

- If no text was entered, a note containing only the current time of day is
  saved.
- If text was entered, closing the screen by any means also saves the note.
- The screen has a short timeout of 30 seconds if left idle.
- The screen can be closed quickly by swiping sideways.

Further customizations under settings:

- The title text can be changed.
- The icon can be made invisible in the statusbar.
- The notification can be made more compact by disabling its expanded mode.
  Beware this prevents the notification from being dismissed. Re-enable the
  expanded notification if you want to get rid of it.

## Import/Export (Backup)

All pages can be exported to and imported from a `.zip` file in the settings
menu. When export completes, the notification allows saving to the file to
downloads or sharing it with another app.

These backups can be re-imported in the settings menu. Beware that this may
overwrite existing pages if the checkbox is enabled. If the zip file contains
unexpected files, they are ignored and listed in the import dialog.

## Error/Crash log

Error logging is disabled by default. The option under settings enables a
completely transparent log: crash information and internal errors are appended
to the selected day's page (`2000-01-03` by default), and not processed in any
other way.

Keep this enabled to help finding issues if they happen. If there is a bug in
the app, the information appearing in the file has to be provided for
debugging, for example on the
[issue tracker](https://gitlab.com/ferreum/pto/-/issues) or by email.

***Note: Please check the error log for personal data or anything else you
don't want to show, before giving it to anyone. Replace that part of the text
and explain it on any issue you post.***

***This file should be checked regularly if enabled. Otherwise, if there are
internal errors, it could grow indefinitely over time.***

To quickly navigate to the selected file, a menu item is shown at the top of
the settings screen.

## Origin

PTO is a (now distant) fork of _billthefarmer_'s
[Diary](https://github.com/billthefarmer/diary) with a major overhaul of the
code using modern Android programming principles. _billthefarmer_'s
[Diary](https://github.com/billthefarmer/diary) itself is a fork of
[Diary](http://git.savannah.gnu.org/cgit/diary.git) on `savannah.gnu.org`.
Before renaming to PTO in version 3.0.0, this app was called "Plain Diary".

Main differences include:

* Completely refactored and modernized codebase.
* Completely rewritten editor and other UI.
* Removed "markdown" view and the internet access permission.
* Completely new file search implementation.
* Strict responsiveness improvements by moving file operations off the UI
  thread.
* Store all files in internal app storage.
* Reliability fixes; auto-save while editing to prevent ever losing more than
  20 seconds of work (and no cancel button that deletes your changes).
* Quality-of-life improvements in the editor: undo/redo, time adjustment, link
  interactions, etc.
* Additional features for usage as a generic organizer
  (see [Features](#features)).

## Privacy

PTO stores files in internal storage, accessible only to the app itself.

Support for using external storage (inherited from origin projects) has been
removed with version 3.0.0. To use files from external storage, they have to be
re-imported from `backup.zip`.

The app has no network access.
