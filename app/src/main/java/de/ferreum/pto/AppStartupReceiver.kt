package de.ferreum.pto

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import de.ferreum.pto.quicknotes.QuickNotesNotifier
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

class AppStartupReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {
        when (intent?.action) {
            Intent.ACTION_BOOT_COMPLETED,
            ACTION_QUICKBOOT_POWERON,
            Intent.ACTION_MY_PACKAGE_REPLACED,
            -> {
                val res = goAsync()
                context.applicationScope.launch {
                    try {
                        val preferences = context.preferencesRepository.preferencesFlow.first()
                        if (preferences.isAutostartQuickNotes) {
                            QuickNotesNotifier.showNotification(context, preferences)
                        }
                    } finally {
                        res.finish()
                    }
                }
            }
        }
    }

    companion object {
        /**
         * Presumably run after some quick restart features.
         */
        const val ACTION_QUICKBOOT_POWERON = "android.intent.action.QUICKBOOT_POWERON"
    }
}
