package de.ferreum.pto

import java.time.LocalDate

object Constants {
    const val FILE_PROVIDER_AUTHORITY_SUFFIX: String = ".fileprovider"

    const val YEAR_MIN: Int = 0

    const val YEAR_MAX: Int = 9999

    /**
     * Supported range of years.
     */
    val YEAR_RANGE: IntRange = YEAR_MIN..YEAR_MAX

    /**
     * Earliest supported date.
     */
    @JvmField
    val PAGE_DATE_MIN: LocalDate = LocalDate.of(YEAR_MIN, 1, 1)

    /**
     * Latest supported date.
     */
    @JvmField
    val PAGE_DATE_MAX: LocalDate = LocalDate.of(YEAR_MAX, 12, 31)

    @JvmField
    val MIN_EPOCH_DAY: Long = PAGE_DATE_MIN.toEpochDay()

    @JvmField
    val MAX_EPOCH_DAY: Long = PAGE_DATE_MAX.toEpochDay()

}
