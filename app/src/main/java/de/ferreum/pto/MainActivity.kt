package de.ferreum.pto

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.view.View
import android.view.textclassifier.TextClassificationManager
import android.view.textclassifier.TextClassifier
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import de.ferreum.pto.keepalive.KeepAliveService
import de.ferreum.pto.page.PasteViewModel
import de.ferreum.pto.page.PtoPageContract.ACTION_SHOW_PAGE
import de.ferreum.pto.page.PtoPageContract.EXTRA_ANIMATE_PAGE_CHANGE
import de.ferreum.pto.page.PtoPageContract.EXTRA_EPOCH_DAY
import de.ferreum.pto.page.PtoPageContract.EXTRA_REMINDER_TEXT_SELECTION
import de.ferreum.pto.page.PtoPageContract.EXTRA_SEARCH_INPUT
import de.ferreum.pto.page.PtoPageContract.EXTRA_TEXT_SELECTION
import de.ferreum.pto.page.PtoPageContract.EXTRA_TEXT_SELECTION_END
import de.ferreum.pto.page.PtoPageContract.PageIntentHandler
import de.ferreum.pto.page.PtoPageContract.isMediaIntent
import de.ferreum.pto.page.PtoPagerFragment
import de.ferreum.pto.page.ViewModelFactory
import de.ferreum.pto.quicknotes.QuickNotesNotifier
import de.ferreum.pto.search.SearchFragment
import de.ferreum.pto.util.getTypedParcelable
import de.ferreum.pto.util.ui.InsetsProvider
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import timber.log.Timber
import java.time.LocalDate
import java.time.LocalTime

class MainActivity : AppCompatActivity(), MainPtoNavigation, InsetsProvider {

    private var pagerIntent: Intent? = null

    private val pasteViewModel: PasteViewModel by viewModels { ViewModelFactory(this) }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            Timber.d("onServiceConnected")
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Timber.d("onServiceDisconnected")
        }
    }

    override val pageInsets = MutableStateFlow(Insets.NONE)

    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val fragmentContainer = findViewById<View>(R.id.fragment_container)
        ViewCompat.setOnApplyWindowInsetsListener(fragmentContainer) { _, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or WindowInsetsCompat.Type.displayCutout()
                    or WindowInsetsCompat.Type.ime()
            )
            pageInsets.value = insets
            windowInsets
        }

        pagerIntent = savedInstanceState?.getTypedParcelable(SAVE_PAGER_INTENT)

        supportFragmentManager.registerFragmentLifecycleCallbacks(object : FragmentManager.FragmentLifecycleCallbacks() {
            override fun onFragmentResumed(fm: FragmentManager, fragment: Fragment) {
                tryDeliverPagerIntent(fragment)
            }
        }, false)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, PtoPagerFragment(), TAG_PAGER_FRAGMENT)
                .commit()
            checkPagerIntent(intent)
        }

        if (Build.VERSION.SDK_INT >= 26) {
            lifecycleScope.launch {
                lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                    observeTextClassifierPref()
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        checkPagerIntent(intent ?: return)
    }

    override fun onResume() {
        super.onResume()
        bindService(Intent(this, KeepAliveService::class.java), serviceConnection, BIND_AUTO_CREATE)
        lifecycleScope.launch {
            val preferences = preferencesRepository.preferencesFlow.first()
            if (preferences.isAutostartQuickNotes) {
                // run non-essential task in app scope
                val context = applicationContext
                applicationScope.launch {
                    QuickNotesNotifier.showNotification(context, preferences)
                }
            }
        }
    }

    override fun onPause() {
        unbindService(serviceConnection)
        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(SAVE_PAGER_INTENT, pagerIntent)
    }

    override fun showSearch(searchInput: CharSequence?) {
        val fragmentManager = supportFragmentManager
        if (fragmentManager.popBackStackImmediate(TAG_SEARCH_FRAGMENT, 0)) {
            return
        }
        var searchFragment = fragmentManager.findFragmentByTag(TAG_SEARCH_FRAGMENT) as SearchFragment?
        val pagerFragment = fragmentManager.findFragmentByTag(TAG_PAGER_FRAGMENT)!!
        if (searchFragment == null) {
            searchFragment = SearchFragment()
            fragmentManager.beginTransaction()
                .add(R.id.fragment_container, searchFragment, TAG_SEARCH_FRAGMENT)
                .setMaxLifecycle(searchFragment, Lifecycle.State.CREATED)
                .commitNow()
        }
        searchInput?.let {
            searchFragment.setSearchInput(searchInput)
        }
        fragmentManager.beginTransaction()
            .setMaxLifecycle(searchFragment, Lifecycle.State.RESUMED)
            .setMaxLifecycle(pagerFragment, Lifecycle.State.CREATED)
            .addToBackStack(TAG_SEARCH_FRAGMENT)
            .commit()
    }

    override fun sendPtoPagerIntent(pagerIntent: Intent) {
        checkPagerIntent(pagerIntent)
    }

    private fun checkPagerIntent(intent: Intent) {
        if (isMediaIntent(intent)) {
            pasteViewModel.setPasteIntent(intent)
        } else if (ACTION_SHOW_PAGE == intent.action) {
            clearBackStack()
            pagerIntent = intent
            val f = supportFragmentManager.findFragmentByTag(TAG_PAGER_FRAGMENT)
            if (f != null && f.isResumed) {
                tryDeliverPagerIntent(f)
            }
        }
    }

    private fun clearBackStack() {
        supportFragmentManager.popBackStack(null, POP_BACK_STACK_INCLUSIVE)
    }

    private fun tryDeliverPagerIntent(fragment: Fragment) {
        if (pagerIntent != null) {
            if (TAG_PAGER_FRAGMENT == fragment.tag) {
                (fragment as PageIntentHandler).onPageIntent(pagerIntent!!)
                pagerIntent = null
            }
        }
    }

    @RequiresApi(26)
    private suspend fun observeTextClassifierPref() {
        preferencesRepository.preferencesFlow
            .map { it.isTextClassifierEnabled }
            .distinctUntilChanged()
            .collect {
                val tcm = getSystemService(TEXT_CLASSIFICATION_SERVICE) as TextClassificationManager
                if (it) {
                    tcm.setTextClassifier(null)
                } else {
                    tcm.setTextClassifier(TextClassifier.NO_OP)
                }
            }
    }

    companion object {
        private const val TAG_SEARCH_FRAGMENT = "search"
        private const val TAG_PAGER_FRAGMENT = "pager"
        private const val SAVE_PAGER_INTENT = "pagerIntent"

        fun createPageIntent(
            context: Context,
            date: LocalDate,
            selection: IntRange? = null,
            searchInput: CharSequence? = null,
            reminderSelection: LocalTime? = null,
            animatePageChange: Boolean? = null,
        ): Intent {
            return Intent(context, MainActivity::class.java).apply {
                setAction(ACTION_SHOW_PAGE)
                putExtra(EXTRA_EPOCH_DAY, date.toEpochDay())
                if (selection != null) {
                    putExtra(EXTRA_TEXT_SELECTION, selection.first)
                    putExtra(EXTRA_TEXT_SELECTION_END, selection.last)
                }
                if (searchInput != null) {
                    putExtra(EXTRA_SEARCH_INPUT, searchInput)
                }
                if (reminderSelection != null) {
                    putExtra(EXTRA_REMINDER_TEXT_SELECTION, reminderSelection)
                }
                if (animatePageChange != null) {
                    putExtra(EXTRA_ANIMATE_PAGE_CHANGE, animatePageChange)
                }
            }
        }
    }

}
