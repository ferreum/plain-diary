package de.ferreum.pto

import android.content.Intent

interface MainPtoNavigation {

    fun showSearch(searchInput: CharSequence?)

    fun sendPtoPagerIntent(pagerIntent: Intent)

}
