package de.ferreum.pto

import android.app.Application
import android.content.Context
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import androidx.emoji2.text.EmojiCompatInitializer
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.preference.PreferenceManager
import de.ferreum.pto.backup.BackupService
import de.ferreum.pto.backup.FolderImportService
import de.ferreum.pto.backup.ZipImporter
import de.ferreum.pto.backup.ZipImporterImpl
import de.ferreum.pto.files.PageContentService
import de.ferreum.pto.files.PageRepository
import de.ferreum.pto.files.PageWriteSignal
import de.ferreum.pto.files.PtoPageContentServiceProvider
import de.ferreum.pto.keepalive.KeepAliveService
import de.ferreum.pto.keepalive.KeepAliveSignal
import de.ferreum.pto.page.undo.UndoHistory
import de.ferreum.pto.preferences.DarkmodeObserver
import de.ferreum.pto.preferences.DayFileLogger
import de.ferreum.pto.preferences.Logger
import de.ferreum.pto.preferences.MinuteTicker
import de.ferreum.pto.preferences.PtoFileHelper
import de.ferreum.pto.preferences.PtoPreferences
import de.ferreum.pto.preferences.PtoPreferencesRepository
import de.ferreum.pto.preferences.PtoPreferencesRepositoryImpl
import de.ferreum.pto.preferences.TodayProvider
import de.ferreum.pto.preferences.TodayProviderImpl
import de.ferreum.pto.quicknotes.QuickNotesNotifier
import de.ferreum.pto.reminder.AlarmServiceStateProvider
import de.ferreum.pto.reminder.AlarmSignal
import de.ferreum.pto.reminder.CachingWakeupScheduler
import de.ferreum.pto.reminder.DismissedRemindersStore
import de.ferreum.pto.reminder.ReminderKeepAliveGuard
import de.ferreum.pto.reminder.ReminderNotifier
import de.ferreum.pto.reminder.ReminderParser
import de.ferreum.pto.reminder.ReminderService
import de.ferreum.pto.reminder.ReminderStateRepository
import de.ferreum.pto.reminder.ReminderStatusPresenter
import de.ferreum.pto.reminder.UpcomingPageWatcher
import de.ferreum.pto.reminder.WakeupSchedulerImpl
import de.ferreum.pto.util.TimeProvider
import de.ferreum.pto.util.UriInspector
import de.ferreum.pto.util.UriInspectorImpl
import de.ferreum.pto.util.logAliveFlows
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException

internal class PtoApplication : Application() {
    lateinit var applicationScope: CoroutineScope
        private set

    lateinit var logger: Logger
        private set

    lateinit var alarmSignal: AlarmSignal
        private set

    lateinit var pageWriteSignal: PageWriteSignal
        private set

    lateinit var keepAliveSignal: KeepAliveSignal
        private set

    lateinit var alarmServiceStateProvider: AlarmServiceStateProvider
        private set

    lateinit var preferencesRepository: PtoPreferencesRepository
        private set

    lateinit var fileHelper: Deferred<PtoFileHelper>
        private set

    lateinit var pageContentServiceProvider: PtoPageContentServiceProvider
        private set

    lateinit var reminderStateRepository: ReminderStateRepository
        private set

    lateinit var zipImporter: ZipImporter

    lateinit var minuteTicker: MinuteTicker
        private set

    lateinit var todayProvider: TodayProvider
        private set

    lateinit var reminderService: ReminderService
        private set

    lateinit var reminderStatusPresenter: ReminderStatusPresenter
        private set

    val uriInspector: UriInspector by lazy { UriInspectorImpl(contentResolver, Dispatchers.IO) }

    override fun onCreate() {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(ThreadPolicy.Builder().apply {
                detectDiskReads()
                detectDiskWrites()
                penaltyFlashScreen()
                penaltyLog()
            }.build())
            StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder().apply {
                detectActivityLeaks()
                detectLeakedClosableObjects()
                detectLeakedSqlLiteObjects()
                detectLeakedRegistrationObjects()
                penaltyLog()
            }.build())
            Timber.plant(Timber.DebugTree())
        }
        super.onCreate()

        QuickNotesNotifier.createNotificationChannel(this)
        BackupService.createNotificationChannel(this)
        KeepAliveService.createNotificationChannel(this)
        FolderImportService.createNotificationChannel(this)

        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            logger.logCriticalException(throwable, "exception in applicationScope")
        }

        applicationScope = CoroutineScope(Dispatchers.Default.limitedParallelism(4) + SupervisorJob() + exceptionHandler)
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        preferencesRepository = PtoPreferencesRepositoryImpl(
            sharedPreferences,
            applicationScope,
            errorLogger = {
                applicationScope.launch {
                    logger.suspendLogException(it, "exception in preferences flow")
                }
            },
        )

        fileHelper = applicationScope.async {
            val fileHelper = PtoFileHelper(filesDir)
            withContext(Dispatchers.IO) {
                if (!fileHelper.pagesDir.mkdirs() && !fileHelper.pagesDir.exists()) {
                    throw IOException("could not create internal pages directory")
                }
            }
            fileHelper
        }

        val logger = DayFileLogger(
            applicationScope,
            preferencesRepository,
            fileHelper,
            getString(R.string.build_revision)
        )
        this.logger = logger
        applicationScope.launch { logger.init() }

        val defaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler { thread, throwable ->
            logger.logCriticalException(throwable, "uncaught exception")
            defaultExceptionHandler?.uncaughtException(thread, throwable)
        }

        minuteTicker = MinuteTicker(
            this,
            applicationScope,
            logger,
        )
        todayProvider = TodayProviderImpl(
            minuteTicker,
            preferencesRepository,
            TimeProvider.SYSTEM,
            applicationScope,
        )

        alarmSignal = AlarmSignal()
        pageWriteSignal = PageWriteSignal()
        alarmServiceStateProvider = AlarmServiceStateProvider()

        keepAliveSignal = KeepAliveSignal(this, applicationScope, logger)

        pageContentServiceProvider = PtoPageContentServiceProvider(applicationScope) { coroutineScope, currentDate ->
            PageContentService(
                coroutineScope,
                currentDate,
                PageRepository(
                    fileHelper,
                    Dispatchers.IO,
                    currentDate,
                    pageWriteSignal,
                ),
                preferencesRepository,
                fileHelper,
                UndoHistory(),
                keepAliveSignal,
                pageWriteSignal,
                Dispatchers.Main,
                Dispatchers.IO,
            )
        }
        val wakeupScheduler = CachingWakeupScheduler(WakeupSchedulerImpl(this))
        val upcomingPageWatcher = UpcomingPageWatcher(
            applicationScope,
            pageReader = { date ->
                val file = fileHelper.await().getDayFile(date)
                try {
                    withContext(Dispatchers.IO) { file.readText() }
                } catch (e: FileNotFoundException) {
                    "" // missing page, do not log error
                } catch (e: Exception) {
                    Timber.tag("UpcomingPageWatcher").e(e, "file read failed")
                    ""
                }
            },
            preferencesRepository,
            pageWriteSignal,
            wakeupScheduler,
            alarmSignal,
            TimeProvider.SYSTEM,
            logger,
            ProcessLifecycleOwner.get(),
        )
        val reminderNotifier = ReminderNotifier(this)
        val reminderSharedPrefs = getSharedPreferences("reminder", MODE_PRIVATE)
        reminderStateRepository = ReminderStateRepository(
            DismissedRemindersStore(
                applicationScope,
                File(filesDir, DISMISSED_REMINDERS_FILENAME),
                Dispatchers.IO,
                logger,
            )
        )
        reminderService = ReminderService(
            applicationScope,
            upcomingPageWatcher,
            reminderSharedPrefs,
            preferencesRepository,
            ReminderParser(TimeProvider.SYSTEM),
            wakeupScheduler,
            alarmSignal,
            reminderNotifier,
            TimeProvider.SYSTEM,
            reminderStateRepository,
            logger,
        )
        zipImporter = ZipImporterImpl(contentResolver, Dispatchers.IO)

        ReminderKeepAliveGuard(
            applicationScope,
            keepAliveSignal,
            reminderService,
            alarmSignal,
            this,
        ).run()

        reminderStatusPresenter = ReminderStatusPresenter(
            applicationScope,
            reminderService,
            minuteTicker,
            TimeProvider.SYSTEM,
            alarmServiceStateProvider,
        )

        reminderNotifier.setup()
        reminderService.run()

        if (sharedPreferences.getBoolean(PtoPreferences.PREF_SUPPORT_EMOJI_ENABLED, true)) {
            EmojiCompatInitializer().create(this)
        }

        DarkmodeObserver(applicationScope, preferencesRepository).apply {
            init(sharedPreferences)
            observeDarkmodePreference()
        }

        if (BuildConfig.DEBUG) {
            applicationScope.launch {
                while (true) {
                    delay(15000)
                    logAliveFlows()
                }
            }
        }
    }

    companion object {
        private const val DISMISSED_REMINDERS_FILENAME = "state/reminders"
    }
}

private val Context.ptoApplication get() = applicationContext as PtoApplication

val Context.alarmSignal get() = ptoApplication.alarmSignal
val Context.pageWriteSignal get() = ptoApplication.pageWriteSignal
val Context.keepAliveSignal get() = ptoApplication.keepAliveSignal
val Context.zipImporter get() = ptoApplication.zipImporter
val Context.applicationScope get() = ptoApplication.applicationScope
val Context.preferencesRepository get() = ptoApplication.preferencesRepository
val Context.fileHelper get() = ptoApplication.fileHelper
val Context.uriInspector get() = ptoApplication.uriInspector
val Context.pageContentServiceProvider get() = ptoApplication.pageContentServiceProvider
val Context.logger get() = ptoApplication.logger
val Context.minuteTicker get() = ptoApplication.minuteTicker
val Context.todayProvider get() = ptoApplication.todayProvider
internal val Context.alarmServiceStateProvider get() = ptoApplication.alarmServiceStateProvider
internal val Context.reminderService get() = ptoApplication.reminderService
internal val Context.reminderStateRepository get() = ptoApplication.reminderStateRepository
internal val Context.reminderStatusPresenter get() = ptoApplication.reminderStatusPresenter
