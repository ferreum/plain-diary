package de.ferreum.pto.backup

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.ServiceInfo
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_LOW
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.ServiceCompat
import androidx.core.content.FileProvider
import de.ferreum.pto.Constants
import de.ferreum.pto.R
import de.ferreum.pto.applicationScope
import de.ferreum.pto.fileHelper
import de.ferreum.pto.logger
import de.ferreum.pto.util.tryNotify
import kotlinx.coroutines.CompletableJob
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.delay
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import kotlinx.coroutines.withContext
import java.io.File

class BackupService : Service() {

    private lateinit var notifManager: NotificationManagerCompat

    private lateinit var serviceJob: CompletableJob
    private lateinit var serviceScope: CoroutineScope
    private var backupJob: Job? = null

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        notifManager = NotificationManagerCompat.from(this)
        serviceJob = Job(applicationScope.coroutineContext.job)
        serviceScope = applicationScope + serviceJob
        serviceJob.invokeOnCompletion {
            stopSelf()
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action) {
            ACTION_BACKUP -> startBackup()
            ACTION_SAVE_TO_DOWNLOADS -> if (Build.VERSION.SDK_INT >= 29) {
                startSaveBackupToDownloads()
            }
        }
        serviceJob.complete()
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        serviceScope.cancel()
        super.onDestroy()
    }

    @SuppressLint("InlinedApi")
    private fun startBackup() {
        val oldJob = backupJob
        ServiceCompat.startForeground(
            this,
            R.id.backup_notification,
            buildNotification().build(),
            ServiceInfo.FOREGROUND_SERVICE_TYPE_DATA_SYNC
        )
        backupJob = serviceScope.launch {
            oldJob?.cancelAndJoin()
            val baseDir = fileHelper.await().pagesDir
            notifManager.cancel(R.id.backup_notification_result)

            val progressChannel = Channel<Pair<Int, Int>>(Channel.CONFLATED)
            val progressJob = launch(Dispatchers.Default) {
                progressChannel.consumeEach { (progress, max) ->
                    updateProgress(progress, max)
                    delay(200)
                }
            }

            val result = runCatching {
                if (!baseDir.isDirectory) {
                    throw IllegalArgumentException("could not find base directory")
                }
                val zipFile = getInternalBackupFile()
                ZipBackup.backup(baseDir, zipFile) { progress, max ->
                    progressChannel.send(Pair(progress, max))
                }
                zipFile
            }

            progressJob.cancel()
            ensureActive()
            notifManager.tryNotify(
                R.id.backup_notification_result,
                result.fold(
                    onSuccess = { zipFile ->
                        createSuccessNotification(zipFile)
                    },
                    onFailure = { throwable ->
                        logger.suspendLogException(throwable, "backup failure")
                        createErrorNotification(throwable)
                    }
                )
            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun startSaveBackupToDownloads() {
        val oldJob = backupJob
        val notification = buildSaveToDownloadsNotification(
            getText(R.string.backup_notification_saving_to_downloads_title)
        ).setContentText(getText(R.string.backup_notification_saving_to_downloads_content))
            .setProgress(100, 50, true)
            .build()
        ServiceCompat.startForeground(
            this,
            R.id.backup_save_to_downloads_notification,
            notification,
            ServiceInfo.FOREGROUND_SERVICE_TYPE_DATA_SYNC
        )
        backupJob = serviceScope.launch {
            oldJob?.cancelAndJoin()
            notifManager.cancel(R.id.backup_save_to_downloads_notification_result)
            notifManager.cancel(R.id.backup_notification_result)
            val result = runCatching {
                withContext(Dispatchers.IO) {
                    getInternalBackupFile().inputStream().use { input ->
                        DownloadsFileSaver(contentResolver, Dispatchers.IO)
                            .saveFileToDownloads(input, "pto_backup", "application/zip")
                    }
                }
            }
            notifManager.tryNotify(
                R.id.backup_save_to_downloads_notification_result,
                result.fold(
                    onSuccess = {
                        buildSaveToDownloadsNotification(
                            getText(R.string.backup_notification_saved_to_downloads_content_success)
                        ).build()
                    },
                    onFailure = { throwable ->
                        logger.suspendLogException(throwable, "save to downloads failed")
                        buildSaveToDownloadsNotification(
                            getText(R.string.backup_notification_saved_to_downloads_content_error)
                        ).setContentText(throwable.toString())
                            .setStyle(NotificationCompat.BigTextStyle())
                            .build()
                    }
                )
            )
        }
    }

    private fun getInternalBackupFile() = File(cacheDir, "pto_backup.zip")

    private fun updateProgress(progress: Int, max: Int) {
        notifManager.tryNotify(
            R.id.backup_notification, buildNotification()
            .setProgress(max, progress, false)
            .build()
        )
    }

    private fun buildSaveToDownloadsNotification(title: CharSequence): NotificationCompat.Builder {
        return NotificationCompat.Builder(this, getString(R.string.backup_notification_channel))
            .setSmallIcon(R.drawable.ic_backup)
            .setContentTitle(title)
            .setSilent(true)
            .setPriority(PRIORITY_LOW)
    }

    private fun buildNotification(): NotificationCompat.Builder {
        return NotificationCompat.Builder(this, getString(R.string.backup_notification_channel))
            .setSmallIcon(R.drawable.ic_backup)
            .setContentTitle(getText(R.string.backup_notification_title))
            .setProgress(100, 50, true)
            .setSilent(true)
            .setForegroundServiceBehavior(NotificationCompat.FOREGROUND_SERVICE_IMMEDIATE)
    }

    private fun createSuccessNotification(zipFile: File): Notification {
        val uri = FileProvider.getUriForFile(
            this,
            packageName + Constants.FILE_PROVIDER_AUTHORITY_SUFFIX,
            zipFile
        )
        val intent = Intent(Intent.ACTION_SEND)
            .setType("application/octet-stream")
            .putExtra(Intent.EXTRA_STREAM, uri)
        val sharePIntent = PendingIntent.getActivity(
            this, R.id.backup_share_intent, intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
        val builder =
            NotificationCompat.Builder(this, getString(R.string.backup_notification_channel))
                .setSmallIcon(R.drawable.ic_backup)
                .setContentTitle(getText(R.string.backup_notification_title_success))
                .setDefaults(NotificationCompat.DEFAULT_SOUND or NotificationCompat.DEFAULT_VIBRATE)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .addAction(0, getText(R.string.share), sharePIntent)
        if (Build.VERSION.SDK_INT >= 29) {
            builder.addAction(
                0, getText(R.string.backup_notification_save_to_downloads_action),
                PendingIntent.getForegroundService(
                    this, R.id.backup_save_to_downloads_intent,
                    Intent(this, BackupService::class.java)
                        .setAction(ACTION_SAVE_TO_DOWNLOADS),
                    PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
                )
            )
        }
        return builder.build()
    }

    private fun createErrorNotification(throwable: Throwable): Notification {
        return NotificationCompat.Builder(this, getString(R.string.backup_notification_channel))
            .setSmallIcon(R.drawable.ic_backup)
            .setContentTitle(getText(R.string.backup_notification_title_error))
            .setContentText(throwable.toString())
            .setDefaults(NotificationCompat.DEFAULT_SOUND or NotificationCompat.DEFAULT_VIBRATE)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setStyle(NotificationCompat.BigTextStyle())
            .build()
    }

    companion object {
        const val ACTION_BACKUP = "ACTION_BACKUP"
        const val ACTION_SAVE_TO_DOWNLOADS = "ACTION_SAVE_TO_DOWNLOADS"

        fun createBackupIntent(context: Context) =
            Intent(context, BackupService::class.java)
                .setAction(ACTION_BACKUP)

        fun createNotificationChannel(context: Context) {
            val notifManager = NotificationManagerCompat.from(context)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notifManager.createNotificationChannel(
                    NotificationChannel(
                        context.getString(R.string.backup_notification_channel),
                        context.getText(R.string.backup_notification_channel_title),
                        NotificationManager.IMPORTANCE_HIGH
                    )
                )
            }
        }
    }
}
