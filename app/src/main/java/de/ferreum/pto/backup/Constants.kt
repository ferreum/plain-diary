package de.ferreum.pto.backup

internal const val ACTION_ZIP_IMPORT = "de.ferreum.pto.ZIP_IMPORT"

internal const val EXTRA_IMPORT_PARAMS = "importParams"
internal const val EXTRA_THROWABLE = "de.ferreum.pto.THROWABLE"
