package de.ferreum.pto.backup

import android.content.ContentResolver
import android.content.ContentValues
import android.net.Uri
import android.os.CancellationSignal
import android.os.ParcelFileDescriptor
import android.provider.MediaStore
import androidx.annotation.RequiresApi
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withContext
import java.io.FileInputStream
import java.io.FileOutputStream
import kotlin.coroutines.CoroutineContext

@RequiresApi(29)
class DownloadsFileSaver(
    private val contentResolver: ContentResolver,
    private val ioContext: CoroutineContext,
) {

    suspend fun saveFileToDownloads(
        fileInputStream: FileInputStream,
        name: String,
        mimeType: String,
    ) = withContext(ioContext) {
        val downloadsUri = MediaStore.Downloads.getContentUri(
            MediaStore.VOLUME_EXTERNAL_PRIMARY
        )
        val values = ContentValues().apply {
            put(MediaStore.Downloads.DISPLAY_NAME, name)
            put(MediaStore.Downloads.MIME_TYPE, mimeType)
            put(MediaStore.Downloads.IS_PENDING, 1)
        }
        val uri = checkNotNull(contentResolver.insert(downloadsUri, values)) {
            "downloads provider returned null Uri"
        }
        val result = runCatching {
            openDownloadsFile(uri).use { pfd ->
                FileOutputStream(pfd.fileDescriptor).use { output ->
                    val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
                    fileInputStream.use { input ->
                        var n: Int
                        while (input.read(buffer).also { n = it } > 0) {
                            ensureActive()
                            output.write(buffer, 0, n)
                        }
                    }
                }
            }
        }
        result.fold(
            onSuccess = {
                contentResolver.update(uri, ContentValues().apply {
                    put(MediaStore.Downloads.IS_PENDING, 0)
                }, null, null)
            },
            onFailure = { throwable ->
                contentResolver.delete(uri, null, null)
                throw throwable
            }
        )
    }

    private suspend fun openDownloadsFile(uri: Uri): ParcelFileDescriptor {
        val cancellationSignal = CancellationSignal()
        val pfd = suspendCancellableCoroutine { cont ->
            cont.invokeOnCancellation {
                cancellationSignal.cancel()
            }
            cont.resumeWith(
                runCatching {
                    contentResolver.openFileDescriptor(uri, "w", cancellationSignal)
                }
            )
        }
        checkNotNull(pfd) { "got null Uri when opening downloads file" }
        return pfd
    }

}
