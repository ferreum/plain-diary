package de.ferreum.pto.backup

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.awaitCancellation
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import java.util.Collections

internal class FakeFolderImportController : FolderImportController {

    private val _importState = MutableStateFlow<ImportState>(ImportState.None)
    override val importState: Flow<ImportState> get() = _importState

    fun setImportState(importState: ImportState) {
        _importState.value = importState
    }

    var startImportCalls = emptyList<StartImportCall>()
        private set
    override fun startImport(params: FolderImportParams) {
        startImportCalls = startImportCalls + StartImportCall(params)
    }
    data class StartImportCall(val params: FolderImportParams)

    private val inhibitors: MutableSet<Any> = Collections.synchronizedSet(mutableSetOf())
    override fun inhibitResultNotifications(scope: CoroutineScope) {
        scope.launch {
            val token = Any()
            try {
                inhibitors.add(token)
                awaitCancellation()
            } finally {
                inhibitors.remove(token)
            }
        }
    }

    val isResultNotificationsInhibited: Boolean get() = inhibitors.isNotEmpty()
}
