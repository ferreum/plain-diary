package de.ferreum.pto.backup

import android.net.Uri
import java.io.File

class FakeZipImporter : ZipImporter {

    var answer: suspend Call.() -> ImportReport = { ImportReport(emptyList(), 0, 0) }

    var calls = emptyList<Call>()
        private set

    override suspend fun importZipFile(zipFile: Uri, destFolder: File, overwrite: Boolean): ImportReport {
        val call = Call(zipFile, destFolder, overwrite)
        calls = calls + call
        return call.answer()
    }

    data class Call(
        val zipFile: Uri,
        val destFolder: File,
        val overwrite: Boolean,
    )

}
