package de.ferreum.pto.backup

import android.os.Parcelable
import de.ferreum.pto.preferences.FolderLocation
import de.ferreum.pto.preferences.ZipFileLocation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.parcelize.Parcelize

interface FolderImportController {
    val importState: Flow<ImportState>

    fun startImport(params: FolderImportParams)

    fun inhibitResultNotifications(scope: CoroutineScope)
}

sealed class ImportState {
    data object None : ImportState()
    data class Active(val params: FolderImportParams) : ImportState()
    data class Failed(val throwable: Throwable) : ImportState()
    data class Completed(val report: ImportReport) : ImportState()
}

@Parcelize
data class FolderImportParams(
    val zipFileLocation: ZipFileLocation,
    val destFolder: FolderLocation,
    val overwrite: Boolean,
    val tryMove: Boolean,
) : Parcelable
