package de.ferreum.pto.backup

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.ServiceInfo
import android.os.Binder
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.ServiceCompat
import androidx.core.content.ContextCompat
import de.ferreum.pto.R
import de.ferreum.pto.applicationScope
import de.ferreum.pto.logger
import de.ferreum.pto.preferences.PtoPreferencesActivity
import de.ferreum.pto.util.tryNotify
import de.ferreum.pto.zipImporter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.awaitCancellation
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import java.util.Collections.synchronizedSet

class FolderImportService : Service() {

    private lateinit var notifManager: NotificationManagerCompat
    private lateinit var scope: CoroutineScope

    private val importState = MutableStateFlow<ImportState>(ImportState.None)

    private val isForeground = MutableStateFlow(false)

    private val inhibitors: MutableSet<Any> = synchronizedSet(mutableSetOf())

    override fun onBind(intent: Intent?): IBinder = LocalBinder()

    inner class LocalBinder : Binder() {
        val controller = object : FolderImportController {
            override val importState: Flow<ImportState>
                get() = this@FolderImportService.importState

            override fun startImport(params: FolderImportParams) {
                this@FolderImportService.startImport(params)
            }

            override fun inhibitResultNotifications(scope: CoroutineScope) {
                scope.launch {
                    val token = Any()
                    inhibitors.add(token)
                    try {
                        awaitCancellation()
                    } finally {
                        inhibitors.remove(token)
                    }
                }
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        isForeground.value = false
        notifManager = NotificationManagerCompat.from(this)
        scope = applicationScope + SupervisorJob(applicationScope.coroutineContext.job)
    }

    @SuppressLint("InlinedApi")
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent?.action == ACTION_IMPORT_FOREGROUND) {
            ServiceCompat.startForeground(
                this,
                R.id.import_notification,
                buildNotification(this)
                    .setProgress(10000, 5000, true)
                    .build(),
                ServiceInfo.FOREGROUND_SERVICE_TYPE_DATA_SYNC
            )
            isForeground.value = true
        }
        return START_NOT_STICKY
    }

    private fun startImport(params: FolderImportParams) {
        if (importState.value is ImportState.Active) {
            // only one import at a time
            return
        }
        importState.value = ImportState.Active(params)
        scope.launch {
            startAndAwaitForeground()
            val result = runCatching {
                zipImporter.importZipFile(
                    zipFile = params.zipFileLocation.uri,
                    destFolder = params.destFolder.toAbsoluteFile(filesDir),
                    overwrite = params.overwrite,
                )
            }
            if (inhibitors.isEmpty()) {
                startCompletionNotification(params, result)
            }
            importState.value = result.fold(
                onSuccess = { ImportState.Completed(it) },
                onFailure = {
                    logger.suspendLogException(it, "folder import failed")
                    ImportState.Failed(it)
                },
            )
            result.onFailure { throw it }
        }.apply {
            invokeOnCompletion {
                ServiceCompat.stopForeground(
                    this@FolderImportService,
                    ServiceCompat.STOP_FOREGROUND_REMOVE
                )
                isForeground.value = false
                stopSelf()
            }
        }
    }

    private suspend fun startAndAwaitForeground() {
        // - starting by intent allows surviving unbind when UI is closed
        // - foreground service additionally allows surviving removal from recents
        ContextCompat.startForegroundService(this, Intent(this, FolderImportService::class.java)
            .setAction(ACTION_IMPORT_FOREGROUND))
        // Await foreground state to prevent cleanup and stopping service before
        // startForeground was called.
        isForeground.first { it }
    }

    override fun onDestroy() {
        scope.cancel()
        isForeground.value = false
        super.onDestroy()
    }

    private fun startCompletionNotification(
        params: FolderImportParams?,
        result: Result<ImportReport>,
    ) {
        notifManager.tryNotify(
            R.id.quicknotes_notification,
            result.fold(
                onSuccess = { report ->
                    buildNotification(applicationContext)
                        .setContentTitle(getText(R.string.import_complete_title))
                        .setContentText(report.formatNotes(this))
                        .setAutoCancel(true)
                        .build()
                },
                onFailure = { throwable ->
                    buildNotification(applicationContext)
                        .setContentText(getString(R.string.import_notification_failed_message))
                        .setContentIntent(createErrorIntent(params, throwable))
                        .setAutoCancel(true)
                        .build()
                }
            )
        )
    }

    private fun createErrorIntent(params: FolderImportParams?, throwable: Throwable): PendingIntent {
        val intent = Intent(this, PtoPreferencesActivity::class.java)
            .setAction(ACTION_ZIP_IMPORT)
            .putExtra(EXTRA_THROWABLE, throwable)
            .putExtra(EXTRA_IMPORT_PARAMS, params)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        return PendingIntent.getActivity(
            this,
            R.id.import_intent_launch_activity,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )
    }

    companion object {
        private const val ACTION_IMPORT_FOREGROUND = "de.ferreum.pto.IMPORT_FOREGROUND"

        private fun buildNotification(context: Context): NotificationCompat.Builder {
            val contentPIntent = PendingIntent.getActivity(
                context,
                R.id.import_intent_launch_activity,
                Intent(context, PtoPreferencesActivity::class.java)
                    .setAction(ACTION_ZIP_IMPORT)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP),
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
            return NotificationCompat.Builder(
                context,
                context.getString(R.string.import_notification_channel)
            ).setSmallIcon(R.drawable.ic_notif_small)
                .setContentIntent(contentPIntent)
                .setContentTitle(context.getText(R.string.import_notification_title))
                .setSilent(true)
        }

        fun createNotificationChannel(context: Context) {
            val notifManager = NotificationManagerCompat.from(context)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notifManager.createNotificationChannel(
                    NotificationChannel(
                        context.getString(R.string.import_notification_channel),
                        context.getText(R.string.import_notification_channel_title),
                        NotificationManager.IMPORTANCE_HIGH
                    )
                )
            }
        }
    }
}
