package de.ferreum.pto.backup

import android.content.Context
import de.ferreum.pto.R

fun ImportReport.formatNotes(context: Context, withFileList: Boolean = false): String {
    return buildString {
        if (importedFiles == 0) {
            appendLine(context.getString(R.string.import_complete_message_no_files))
        } else if (skippedFiles.isEmpty()) {
            appendLine(context.getString(R.string.import_complete_message_ok))
        } else {
            appendLine(context.getString(R.string.import_complete_message_with_notes))
        }
        appendLine(context.getString(R.string.import_note_number_imported_files, importedFiles))
        if (renamedFiles != 0) {
            appendLine(context.getString(R.string.import_note_number_renamed_files, renamedFiles))
        }
        if (skippedFiles.isNotEmpty()) {
            if (withFileList) {
                appendLine()
                appendLine(context.getString(R.string.import_note_skipped_files_list,
                    skippedFiles.size))
                skippedFiles.forEach {
                    appendLine(it)
                }
            } else {
                appendLine(context.getString(R.string.import_note_number_skipped_files,
                    skippedFiles.size))
            }
        }
    }.removeSuffix("\n")
}
