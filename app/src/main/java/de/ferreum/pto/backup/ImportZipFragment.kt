package de.ferreum.pto.backup

import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.widget.CompoundButton
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE
import androidx.constraintlayout.widget.Group
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import de.ferreum.pto.R
import de.ferreum.pto.backup.ImportZipViewModel.UiState
import de.ferreum.pto.page.ViewModelFactory
import de.ferreum.pto.util.getTypedSerializable
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class ImportZipFragment : DialogFragment() {

    private val viewModel: ImportZipViewModel by viewModels { ViewModelFactory(this) }

    private var startedJob: Job? = null

    private val fileChooserRequest =
        registerForActivityResult(ActivityResultContracts.OpenDocument()) { uri ->
            if (uri != null) viewModel.setZipUri(uri)
        }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
            if (binder is FolderImportService.LocalBinder) {
                viewModel.connectController(binder.controller)
            }
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            viewModel.disconnectController()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            arguments?.getTypedSerializable<Throwable>(EXTRA_THROWABLE)?.let {
                viewModel.showError(it)
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(requireContext())
            .setTitle(R.string.import_zip_title)
            .setView(R.layout.dialog_import_zip)
            .setPositiveButton(android.R.string.ok, null)
            .create()
    }

    override fun onStart() {
        super.onStart()
        val dialog = requireDialog() as AlertDialog
        dialog.setButton(BUTTON_POSITIVE, getText(android.R.string.ok), null, null)
        val okButton = dialog.getButton(BUTTON_POSITIVE)
        val chooseZipFileTextView: TextView = dialog.findViewById(R.id.chooseZipFileTextView)!!
        val allowOverwritingFilesCheckBox: CompoundButton =
            dialog.findViewById(R.id.allowOverwritingFilesCheckBox)!!
        val errorTextView: TextView = dialog.findViewById(R.id.errorTextView)!!
        val chooseZipFileGroup: Group = dialog.findViewById(R.id.chooseZipFileGroup)!!
        val activeGroup: Group = dialog.findViewById(R.id.activeGroup)!!
        val completedMessageText: TextView = dialog.findViewById(R.id.completedMessageText)!!
        okButton.setOnClickListener {
            viewModel.confirm(overwrite = allowOverwritingFilesCheckBox.isChecked)
        }
        chooseZipFileTextView.setOnClickListener {
            try {
                fileChooserRequest.launch(arrayOf("application/zip"))
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(
                    requireContext(),
                    R.string.import_chooser_not_found_error,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        startedJob?.cancel()
        startedJob = lifecycleScope.launch {
            launch {
                viewModel.zipDisplayName.collectLatest {
                    chooseZipFileTextView.text = it
                }
            }
            launch {
                viewModel.state.collectLatest { state ->
                    okButton.isEnabled = state.isConfirmEnabled
                    chooseZipFileGroup.isVisible = state is UiState.Idle
                    activeGroup.isVisible = state is UiState.Active
                    errorTextView.isVisible = state is UiState.Error
                    completedMessageText.isVisible = state is UiState.Complete
                    when (state) {
                        UiState.Active -> Unit
                        is UiState.Complete -> {
                            completedMessageText.text = state.report
                                .formatNotes(requireContext(), withFileList = true)
                        }
                        is UiState.Error -> {
                            Timber.w(state.throwable)
                            errorTextView.text = state.throwable.toString()
                        }
                        UiState.Idle -> Unit
                        UiState.Loading -> Unit
                        UiState.Finished -> dismiss()
                    }
                }
            }
        }
        requireContext().bindService(
            Intent(requireContext(), FolderImportService::class.java),
            serviceConnection, Context.BIND_AUTO_CREATE
        )
    }

    override fun onStop() {
        startedJob?.cancel()
        viewModel.disconnectController()
        requireContext().unbindService(serviceConnection)
        super.onStop()
    }

    companion object {
        val FRAGMENT_TAG: String = ImportZipFragment::class.java.simpleName
    }

}
