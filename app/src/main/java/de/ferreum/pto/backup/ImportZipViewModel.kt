package de.ferreum.pto.backup

import android.net.Uri
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.ferreum.pto.preferences.FolderLocation.InternalStorage
import de.ferreum.pto.preferences.ZipFileLocation
import de.ferreum.pto.util.Event
import de.ferreum.pto.util.UriInspector
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.getAndUpdate
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class ImportZipViewModel(
    private val uriInspector: UriInspector,
    private val savedStateHandle: SavedStateHandle,
) : ViewModel() {

    private var controllerJob: Job? = null

    private val _state = MutableStateFlow<UiState>(UiState.Loading)

    private val currentError = MutableStateFlow<Throwable?>(null)

    val state: Flow<UiState> = _state.combine(currentError) { state, error ->
        error?.let { UiState.Error(it) } ?: state
    }

    private val startingImport = MutableSharedFlow<Event<FolderImportParams>>(
        replay = 1,
        onBufferOverflow = BufferOverflow.DROP_OLDEST,
    )

    val zipDisplayName = savedStateHandle.getStateFlow<Uri?>(KEY_ZIP_URI, null)
        .map { it?.let { uriInspector.getDisplayName(it) ?: it.lastPathSegment } }

    fun setZipUri(uri: Uri) {
        savedStateHandle[KEY_ZIP_URI] = uri
    }

    fun confirm(overwrite: Boolean) {
        if (currentError.getAndUpdate { null } != null) {
            return
        }
        when (_state.value) {
            UiState.Loading -> Unit
            UiState.Idle -> startImport(overwrite)
            UiState.Active -> Unit
            is UiState.Complete -> _state.value = UiState.Finished
            is UiState.Error -> _state.value = UiState.Idle
            UiState.Finished -> Unit
        }
    }

    private fun startImport(overwrite: Boolean) {
        viewModelScope.launch {
            val uri: Uri = savedStateHandle[KEY_ZIP_URI] ?: return@launch
            startingImport.tryEmit(
                Event(
                    FolderImportParams(
                        zipFileLocation = ZipFileLocation(uri),
                        destFolder = InternalStorage,
                        overwrite = overwrite,
                        tryMove = false,
                    )
                )
            )
        }
    }

    fun connectController(importController: FolderImportController) {
        controllerJob?.cancel()
        controllerJob = viewModelScope.launch {
            importController.inhibitResultNotifications(this)
            launch {
                startingImport.collect { event ->
                    event.once {
                        try {
                            importController.startImport(it)
                        } catch (tr: Throwable) {
                            ensureActive()
                            currentError.value = tr
                        }
                    }
                }
            }
            importController.importState.collectLatest { state ->
                _state.value = when (state) {
                    is ImportState.Active -> UiState.Active
                    is ImportState.Completed -> UiState.Complete(state.report)
                    is ImportState.Failed -> UiState.Error(state.throwable)
                    ImportState.None -> UiState.Idle
                }
            }
        }
    }

    fun disconnectController() {
        controllerJob?.cancel()
    }

    fun showError(throwable: Throwable) {
        currentError.value = throwable
    }

    sealed class UiState {
        data object Loading : UiState()
        data object Idle : UiState()
        data object Active : UiState()
        data class Complete(val report: ImportReport) : UiState()
        data class Error(val throwable: Throwable) : UiState()
        data object Finished : UiState()

        val isConfirmEnabled: Boolean
            get() = when (this) {
                Idle, is Complete, is Error -> true
                else -> false
            }
    }

    companion object {
        private const val KEY_ZIP_URI = "zipUri"
    }

}
