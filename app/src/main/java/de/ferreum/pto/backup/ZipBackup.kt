package de.ferreum.pto.backup

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.withContext
import java.io.File
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import kotlin.math.max

internal object ZipBackup {

    suspend fun backup(
        baseFolder: File,
        zipFile: File,
        progressListener: suspend (progress: Int, max: Int) -> Unit
    ) = withContext(Dispatchers.IO) {
        var fileCount = -1
        var progressStep = -1

        val contentFlow = flow {
            val count = baseFolder.walkTopDown().count {
                if (it == baseFolder) return@count false
                emit(it)
                true
            }
            fileCount = count
            progressStep = (count / 20).coerceAtLeast(5)
        }.buffer(Channel.UNLIMITED)
            .mapNotNull { file ->
                val content = when {
                    file.isDirectory -> null
                    file.isFile -> file.readBytes()
                    else -> return@mapNotNull null
                }
                FileEntry(file, file.lastModified(), content)
            }
            .buffer(8)

        zipFile.outputStream().use { out ->
            ZipOutputStream(out).use { output ->
                var index = 0
                contentFlow.collect { entry ->
                    if (fileCount >= 0 && index % progressStep == 0) {
                        val maxProgress = max(1, fileCount / progressStep)
                        progressListener(index / progressStep, maxProgress)
                    }
                    coroutineContext.ensureActive()
                    val path = entry.file.path.substring(baseFolder.path.length + 1)
                    if (entry.isDirectory) {
                        output.putNextEntry(ZipEntry(path + File.separator).apply {
                            method = ZipEntry.STORED
                            time = entry.lastModified
                            size = 0
                            compressedSize = 0
                            crc = 0
                        })
                    } else {
                        output.putNextEntry(ZipEntry(path).apply {
                            method = ZipEntry.DEFLATED
                            time = entry.lastModified
                        })
                        output.write(entry.data)
                    }
                    index++
                }
                output.closeEntry()
            }
        }
    }

    private class FileEntry(
        val file: File,
        val lastModified: Long,
        /** `null` for directories */
        val data: ByteArray?,
    ) {
        val isDirectory get() = data == null
    }

}
