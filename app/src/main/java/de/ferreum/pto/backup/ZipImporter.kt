package de.ferreum.pto.backup

import android.net.Uri
import java.io.File

interface ZipImporter {

    suspend fun importZipFile(
        zipFile: Uri,
        destFolder: File,
        overwrite: Boolean = false,
    ): ImportReport

}

data class ImportReport(
    val skippedFiles: List<String>,
    val renamedFiles: Int,
    val importedFiles: Int,
)
