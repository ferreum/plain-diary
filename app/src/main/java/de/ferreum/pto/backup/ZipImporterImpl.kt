package de.ferreum.pto.backup

import android.content.ContentResolver
import android.net.Uri
import kotlinx.coroutines.withContext
import java.io.File
import java.io.IOException
import java.util.zip.ZipInputStream
import kotlin.coroutines.CoroutineContext

class InvalidZipFileException(
    message: String? = null,
    cause: Throwable? = null,
) : RuntimeException(message, cause)

internal class ZipImporterImpl(
    private val contentResolver: ContentResolver,
    private val ioContext: CoroutineContext,
) : ZipImporter {
    override suspend fun importZipFile(zipFile: Uri, destFolder: File, overwrite: Boolean): ImportReport {
        return withContext(ioContext) {
            contentResolver.openInputStream(zipFile).use { input ->
                ZipInputStream(input).use { zip ->
                    importZip(zip, destFolder, overwrite)
                }
            }
        }
    }

    private fun importZip(
        zip: ZipInputStream,
        destFolder: File,
        overwrite: Boolean,
    ): ImportReport {
        val skippedFiles = mutableListOf<String>()
        var renamedFiles = 0
        val importedFiles = mutableMapOf<String, String>()
        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        while (true) {
            val entry = zip.nextEntry ?: break
            if (entry.isDirectory) {
                continue // creating directories implicitly
            } else if (!isAllowedFilename(entry.name)) {
                skippedFiles.add(entry.name)
                continue
            }
            val targetName = getTargetFilename(entry.name)
            val destFile = File(destFolder, targetName)
            val prevAdded = importedFiles[targetName]
            if (prevAdded == null) {
                if (!overwrite && destFile.exists()) {
                    throw IOException("file already exists: ${entry.name}")
                }
            } else if (prevAdded.endsWith(".txt") && entry.name.endsWith(".md")) {
                // prefer .md file; overwrite .txt
                skippedFiles.add(prevAdded)
            } else if (prevAdded.endsWith(".md") && entry.name.endsWith(".txt")) {
                // prefer .md file, renamed to .txt
                skippedFiles.add(entry.name)
                continue
            } else {
                throw InvalidZipFileException("duplicate file in zip archive: ${entry.name}")
            }
            importedFiles[targetName] = entry.name
            if (entry.name != targetName) {
                renamedFiles++
            }
            destFile.parentFile?.checkedMkDirs()
            destFile.outputStream().use { out ->
                var n: Int
                while (zip.read(buffer).also { n = it } > 0) {
                    out.write(buffer, 0, n)
                }
            }
        }
        return ImportReport(skippedFiles, renamedFiles, importedFiles.size)
    }

    private fun getTargetFilename(name: String): String {
        if (name.endsWith(".md")) {
            return name.removeSuffix(".md") + ".txt"
        }
        return name
    }

    private fun isAllowedFilename(name: String): Boolean {
        return REGEX_ALLOWED_NAMES.matches(name)
    }

    private fun File.checkedMkDirs() {
        if (!mkdirs() && !isDirectory) {
            throw IOException("could not create directory $path")
        }
    }

    companion object {
        private val REGEX_ALLOWED_NAMES = Regex("^[0-9]{4}/[0-9]{2}/[0-9]{2}\\.(md|txt)$")
    }
}
