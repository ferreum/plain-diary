package de.ferreum.pto.files

import androidx.annotation.VisibleForTesting
import de.ferreum.pto.BuildConfig
import de.ferreum.pto.files.PageWriteSignal.PageWrite
import de.ferreum.pto.files.PtoPageContentServiceProvider.RetainPolicy
import de.ferreum.pto.keepalive.KeepAliveSignal
import de.ferreum.pto.page.undo.HistoryCommand
import de.ferreum.pto.page.undo.TextChange
import de.ferreum.pto.page.undo.TextSnapshot
import de.ferreum.pto.page.undo.UndoHistory
import de.ferreum.pto.page.undo.UndoHistory.Companion.NO_CHANGE_ID
import de.ferreum.pto.page.undo.UndoHistory.Companion.NO_TRANSACTION_TAG
import de.ferreum.pto.preferences.PtoFileHelper
import de.ferreum.pto.preferences.PtoPreferencesRepository
import de.ferreum.pto.util.TimeProvider
import de.ferreum.pto.util.combineTerminal
import de.ferreum.pto.util.myDebounce
import de.ferreum.pto.util.traceFlow
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.FileNotFoundException
import java.io.IOException
import java.time.LocalDate
import kotlin.coroutines.CoroutineContext

class PageContentService(
    private val coroutineScope: CoroutineScope,
    val currentDate: LocalDate,
    private val pageRepository: PageRepository,
    preferencesRepository: PtoPreferencesRepository,
    private val fileHelper: Deferred<PtoFileHelper>,
    private val undoHistory: UndoHistory,
    keepAliveSignal: KeepAliveSignal,
    private val pageWriteSignal: PageWriteSignal,
    mainContext: CoroutineContext,
    private val ioContext: CoroutineContext,
) {

    @VisibleForTesting
    internal val autosaveIdleInterval = 3000L
    @VisibleForTesting
    internal val autosaveMaxInterval = 15000L

    private val maxUndoSize: Int = 4 * 1024 * 1024

    @VisibleForTesting
    internal val contentSourceTag = ContentSourceTag()

    private val preferences = preferencesRepository.preferencesFlow

    val undoCounters get() = undoHistory.counters

    private val savedChangeId = MutableStateFlow(NO_CHANGE_ID)
    val isModified = combine(savedChangeId, undoHistory.counters, ::isContentModified)
        .traceFlow { "pageContent $currentDate isModified" }
        .stateIn(coroutineScope, SharingStarted.Eagerly, false)

    private val _isSaving = MutableStateFlow(false)
    val isSaving: StateFlow<Boolean> get() = _isSaving
    private val savingLock = Mutex()

    private val loadingError = MutableStateFlow<Throwable?>(null)
    private val savingError = MutableStateFlow<Throwable?>(null)

    private val isLoaded = MutableStateFlow(false)

    val fileStatus = combine(isLoaded, isSaving, isModified, loadingError, savingError) { isLoaded, saving, modified, loadingError, savingError ->
        when {
            loadingError != null -> FileStatus.Error(loadingError, ErrorSource.LOADING)
            !isLoaded -> FileStatus.Loading
            saving -> FileStatus.Saving
            !modified -> FileStatus.Saved
            savingError != null -> FileStatus.Error(savingError, ErrorSource.SAVING)
            else -> FileStatus.Modified
        }
    }.traceFlow { "pageContent $currentDate fileStatus" }
        .stateIn(coroutineScope, SharingStarted.WhileSubscribed(0, 0), FileStatus.Loading)

    val isEditingAllowed = combine(isLoaded, loadingError) { isLoaded, loadingError ->
        isLoaded && loadingError == null
    }.traceFlow { "pageContent $currentDate isEditingAllowed" }
        .stateIn(coroutineScope, SharingStarted.Eagerly, false)

    val retainPolicy: Flow<RetainPolicy> = combine(
        isModified,
        undoHistory.counters,
    ) { isModified, counters ->
        when {
            isModified -> RetainPolicy.Keep
            counters.undoCount > 0 || counters.redoCount > 0 -> RetainPolicy.Timeout(
                30000L + 5000L * counters.undoCount + 2000L * counters.redoCount
            )
            else -> RetainPolicy.Timeout(5000L)
        }
    }

    init {
        keepAliveSignal.register(coroutineScope, isModified.myDebounce { if (it) 0 else 1000 })
    }

    init {
        if (BuildConfig.DEBUG) {
            Timber.tag(TAG).i("init: %s", currentDate)
            coroutineScope.coroutineContext.job.invokeOnCompletion {
                Timber.tag(TAG).i("teardown: %s", currentDate)
            }
        }
    }

    init {
        val wantAutosave = combine(
            isModified, isSaving, loadingError,
        ) { isModified, isSaving, loadingError ->
            isModified && !isSaving && loadingError == null
        }.stateIn(coroutineScope, SharingStarted.Eagerly, false)

        coroutineScope.launch {
            wantAutosave.combine(undoHistory.counters) { a, _ -> a }
                .collectLatest { wantAutosave ->
                    if (wantAutosave) {
                        delay(autosaveIdleInterval)
                        // launch so it does not get cancelled on wantAutosave change
                        coroutineScope.launch { ensureSaved() }
                    }
                }
        }
        coroutineScope.launch {
            wantAutosave.collectLatest { wantAutosave ->
                if (wantAutosave) {
                    while (true) {
                        delay(autosaveMaxInterval)
                        // launch so it does not get cancelled on wantAutosave change
                        coroutineScope.launch { ensureSaved() }
                    }
                }
            }
        }
    }

    private val loadingRetrySignal = MutableSharedFlow<Unit>(
        replay = 1,
        onBufferOverflow = BufferOverflow.DROP_LATEST
    ).apply {
        tryEmit(Unit)
    }

    private var lastTransactionTag: Int = 0
    fun getNextTransactionTag() = ++lastTransactionTag

    private var savedModTime: Long = -1L
    private var loadedModTime: Long = -1L
    private var suppressSubModTime: Long = -1L
    private val fileContentObserver: Flow<Nothing> = pageRepository.fileInfoFlow
        .onStart { suppressSubModTime = loadedModTime }
        .filter { info ->
            // wait for saving to finish, so it updates savedModTime
            isSaving.first { !it }
            // filter out redundant unchanged event on restart of this flow
            if (suppressSubModTime != -1L) {
                if (info.lastModified == suppressSubModTime) {
                    Timber.tag(TAG).v("suppressing redundant event %s", info)
                    return@filter false
                }
                suppressSubModTime = -1L
            }
            // filter out change events caused by our own saving process
            if (savedModTime == -1L || savedModTime != info.lastModified) {
                true
            } else {
                Timber.tag(TAG).v("suppressing save event %s", info)
                false
            }
        }
        .combineTerminal(loadingRetrySignal) { info, _ ->
            Timber.tag(TAG).i("reloading: %s", info)
            try {
                val content = try {
                    pageRepository.readFile(info.file)
                } catch (e: FileNotFoundException) {
                    loadTemplateOrEmpty()
                }
                loadingError.value = null
                loadedModTime = info.lastModified

                withContext(mainContext) {
                    val change = undoHistory.addChange(TextSnapshot.fromString(content))
                    savedChangeId.value = change.changeId
                    Timber.tag(TAG).d("reloading")
                    _currentContent.tryEmit(EditorContent.from(change, contentSourceTag))
                    isLoaded.value = true
                }
            } catch (e: Throwable) {
                Timber.tag(TAG).e(e, "reloading failed")
                loadingError.value = e
            }
        }
        .catch { loadingError.value = it }
        .traceFlow { "pageContent $currentDate fileObserver" }
        .buffer(Channel.RENDEZVOUS) // configures shareIn
        .shareIn(coroutineScope, SharingStarted.WhileSubscribed(5000), replay = 0)

    fun observeFileContentIn(coroutineScope: CoroutineScope) {
        coroutineScope.launch {
            // hold the (inert) flow until cancelled
            fileContentObserver.collect()
        }
    }

    private val _currentContent = MutableSharedFlow<EditorContent>(
        replay = 1,
        onBufferOverflow = BufferOverflow.DROP_OLDEST,
    )
    val currentContent: Flow<EditorContent> get() = _currentContent

    val hints = PageHintsPresenter(
        currentDate,
        preferencesRepository,
        this,
        TimeProvider.SYSTEM,
    ).hints.stateIn(coroutineScope, SharingStarted.WhileSubscribed(1000, 0), null)

    private suspend fun loadTemplateOrEmpty(): String {
        val preferences = preferences.first()
        if (!preferences.isTemplateEnabled) return ""
        val templateFile = fileHelper.await().getDayFile(preferences.templatePageDate)
        return try {
            withContext(ioContext) { templateFile.readText() }
        } catch (e: IOException) {
            ""
        }
    }

    /**
     * Called when text was changed by the user.
     * - adds the change to the undo history
     * - updates [currentContent] to reflect content with the specified [sourceTag].
     */
    fun replaceContent(
        snapshot: TextSnapshot,
        sourceTag: ContentSourceTag,
        transactionTag: Int = NO_TRANSACTION_TAG,
    ) {
        val current = _currentContent.replayCache.firstOrNull()
        if (current != null
            && snapshot.selectionStart == current.selectionStart
            && snapshot.selectionEnd == current.selectionEnd
            && snapshot.text == current.text
        ) {
            return
        }
        val change = undoHistory.addChange(snapshot, transactionTag)
        undoHistory.trimToSizeInBytes(maxUndoSize)
        _currentContent.tryEmit(
            EditorContent.from(snapshot, change.changeId, sourceTag)
        )
    }

    fun doHistoryCommand(historyCommand: HistoryCommand): Boolean {
        val isUndo = historyCommand.isUndo(undoHistory.counters.value.undoVersion)
        val change = undoHistory.doCommand(historyCommand) ?: return false
        var selStart = change.snapshot.selectionStart
        var selEnd = change.snapshot.selectionEnd
        if (isUndo) {
            undoHistory.getRedoChange()?.snapshot?.let {
                if (it.preSelectionStart != -1 && it.preSelectionEnd != -1
                    && (it.preChangeId == NO_CHANGE_ID || it.preChangeId == change.changeId)
                ) {
                    selStart = it.preSelectionStart
                    selEnd = it.preSelectionEnd
                }
            }
        }
        _currentContent.tryEmit(EditorContent(
            text = change.snapshot.text,
            selectionStart = selStart,
            selectionEnd = selEnd,
            changeId = change.changeId,
            sourceTag = contentSourceTag,
        ))
        return true
    }

    fun checkForChanges() {
        if (loadingError.value != null) {
            loadingRetrySignal.tryEmit(Unit)
        }
        pageRepository.checkForChangedFile()
    }

    fun triggerSave() {
        coroutineScope.launch {
            ensureSaved()
        }
    }

    suspend fun ensureSaved() {
        if (isContentModified(savedChangeId.value, undoHistory.counters.value)) {
            try {
                saveCurrentContent()
                savingError.value = null
            } catch (e: Throwable) {
                savingError.value = e
            }
        }
    }

    private suspend fun saveCurrentContent() {
        val saveContent = _currentContent.replayCache.lastOrNull() ?: return
        Timber.i("performSave: saving change with ID %s", saveContent.changeId)
        savingLock.withLock {
            if (saveContent.changeId == savedChangeId.value) {
                // content is already saved
                return
            }
            _isSaving.value = true
            try {
                val string = saveContent.text
                val modTime = pageRepository.saveFile(string)
                Timber.tag(TAG).i("performSave: save completed")
                savedModTime = modTime ?: 0L
                loadedModTime = modTime ?: 0L
                // Trigger update, in case we had a configuration change and have a new fragment
                // attached to us that got created before we were done saving.
                pageRepository.checkForChangedFile()
                savedChangeId.value = saveContent.changeId
                pageWriteSignal.notifyPageWrite(
                    PageWrite(currentDate, saveContent.text, emptySet())
                )
            } finally {
                _isSaving.value = false
            }
        }
    }

    private fun isContentModified(savedId: Int, counters: UndoHistory.Counters) =
        savedId != NO_CHANGE_ID && counters.changeId != NO_CHANGE_ID && savedId != counters.changeId

    /**
     * Instances of this class are used to identify the source for changes of file content.
     * This allows observers to filter out updates to [currentContent] they caused themselves.
     */
    class ContentSourceTag

    sealed class FileStatus {
        data object Loading : FileStatus()
        data object Saving : FileStatus()
        data object Modified : FileStatus()
        data object Saved : FileStatus()
        data class Error(val throwable: Throwable, val source: ErrorSource) : FileStatus()

        val isBusy get() = this == Loading || this == Saving
    }

    enum class ErrorSource {
        LOADING, SAVING
    }

    data class EditorContent(
        val text: String,
        val selectionStart: Int,
        val selectionEnd: Int,
        val changeId: Int,
        val sourceTag: ContentSourceTag,
    ) {

        val hasSelection get() = selectionStart >= 0 && selectionEnd >= 0

        companion object {
            fun from(change: TextChange, sourceTag: ContentSourceTag) = EditorContent(
                text = change.snapshot.text,
                selectionStart = change.snapshot.selectionStart,
                selectionEnd = change.snapshot.selectionEnd,
                changeId = change.changeId,
                sourceTag = sourceTag,
            )

            fun from(
                snapshot: TextSnapshot,
                changeId: Int,
                sourceTag: ContentSourceTag,
            ) = EditorContent(
                text = snapshot.text,
                selectionStart = snapshot.selectionStart,
                selectionEnd = snapshot.selectionEnd,
                changeId = changeId,
                sourceTag = sourceTag,
            )

            fun fromString(text: String, changeId: Int, sourceTag: ContentSourceTag) = EditorContent(
                text = text,
                selectionStart = -1,
                selectionEnd = -1,
                changeId = changeId,
                sourceTag = sourceTag,
            )
        }
    }

    companion object {
        private const val TAG = "PageContentService"
    }

}
