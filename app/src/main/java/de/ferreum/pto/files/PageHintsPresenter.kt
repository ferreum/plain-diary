package de.ferreum.pto.files

import de.ferreum.pto.preferences.PtoPreferencesRepository
import de.ferreum.pto.reminder.ReminderParser
import de.ferreum.pto.util.TimeProvider
import de.ferreum.pto.util.forLineRanges
import de.ferreum.pto.util.myTransformLatest
import de.ferreum.pto.util.traceFlow
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.conflate
import kotlinx.coroutines.flow.distinctUntilChangedBy
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

internal class PageHintsPresenter(
    private val currentDate: LocalDate,
    preferencesRepository: PtoPreferencesRepository,
    private val pageContentService: PageContentService,
    private val timeProvider: TimeProvider,
) {

    val hints: Flow<String?> = preferencesRepository.preferencesFlow
        .distinctUntilChangedBy { it.newpageTime }
        .myTransformLatest { prefs ->
            pageContentService.currentContent.conflate().collect { content ->
                emit(parseHints(content.text, currentDate, prefs.newpageTime))
                delay(2000) // throttle
            }
        }
        .traceFlow { "hints $currentDate" }

    private fun parseHints(text: String, pageDate: LocalDate, newpageTime: LocalTime): String? {
        val sb = StringBuilder()
        var num = 0
        val now = timeProvider.localDateTime()
        text.forLineRanges loop@{ start, end ->
            if (start == end) return@loop
            val char = when (text[start]) {
                '+' -> if (start + 1 < end && text[start + 1] == ' ') '+' else return@loop
                '@' -> if (text.isFutureEventAt(start, end, pageDate, newpageTime, now)) '@' else return@loop
                else -> return@loop
            }
            if (++num > 6) {
                // replace at least 2 hints (current and previous) with vertical ellipsis
                sb.deleteAt(sb.length - 1)
                sb.append('\u22ee')
                return sb.toString()
            } else {
                if (sb.isNotEmpty()) sb.append('\n')
                sb.append(char)
            }
        }
        return sb.ifEmpty { null }?.toString()
    }

    private fun String.isFutureEventAt(
        start: Int,
        end: Int,
        pageDate: LocalDate,
        newpageTime: LocalTime,
        now: LocalDateTime,
    ): Boolean {
        val time = ReminderParser.getReminderDate(substring(start, end), pageDate, newpageTime)
            ?: return false
        return time > now
    }

}
