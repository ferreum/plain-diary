package de.ferreum.pto.files

import de.ferreum.pto.Constants
import de.ferreum.pto.preferences.PtoFileHelper
import de.ferreum.pto.util.traceFlow
import de.ferreum.pto.util.writeAtomic
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.time.LocalDate
import kotlin.coroutines.CoroutineContext

class PageRepository(
    private val fileHelper: Deferred<PtoFileHelper>,
    private val ioContext: CoroutineContext,
    private val currentDate: LocalDate,
    private val pageWriteSignal: PageWriteSignal,
) {

    private val _fileCheckSignal = MutableSharedFlow<Boolean>(
        replay = 1,
        onBufferOverflow = BufferOverflow.DROP_OLDEST,
    ).apply { tryEmit(false) }

    val fileInfoFlow: Flow<FileInfo> = flow {
        if (currentDate.year !in Constants.YEAR_RANGE) {
            throw IllegalArgumentException("date is outside of supported range: $currentDate")
        }

        val file = fileHelper.await().getDayFile(currentDate)

        val changeFlow: Flow<Boolean> = merge(
            _fileCheckSignal,
            pageWriteSignal.observePageWrites().mapNotNull {
                if (it.date == currentDate) true else null
            },
        )
        var didEmit = false
        var latestModTime = -1L
        var receivedWriteSignal = false
        changeFlow.collect { isWriteSignal ->
            Timber.tag("FileRepository")
                .v("%s isWriteSignal=%s latest=%s", currentDate, isWriteSignal, latestModTime)
            val modTime = withContext(ioContext) { file.lastModified() }
            if (isWriteSignal) {
                val wasReceivedWriteSignal = receivedWriteSignal
                receivedWriteSignal = true
                if (!wasReceivedWriteSignal && modTime == latestModTime) {
                    // filter out redundant write signal after manual check
                    return@collect
                }
            } else {
                if (didEmit && modTime == latestModTime) {
                    // filter out redundant modTime-based and initial updates
                    return@collect
                }
                receivedWriteSignal = false
            }
            latestModTime = modTime
            didEmit = true
            Timber.tag("FileRepository")
                .i("new info=%s modTime=%s isWriteSignal=%s", file, latestModTime, isWriteSignal)

            emit(FileInfo(currentDate, file, latestModTime))
        }
    }.traceFlow { "fileInfo $currentDate" }

    fun checkForChangedFile() {
        _fileCheckSignal.tryEmit(false)
    }

    suspend fun saveFile(content: String): Long? {
        val file = fileHelper.await().getDayFile(currentDate)
        return withContext(ioContext) {
            if (content.isEmpty()) {
                Timber.tag("saveFile").i("deleting %s", file)
                if (file.delete()) {
                    // delete month/year directories if empty--delete fails on non-empty dirs, returning false
                    val parentFile = file.parentFile
                    if (parentFile?.delete() == true) {
                        parentFile.parentFile?.delete()
                    }
                }
                null
            } else {
                Timber.tag("saveFile").i("saving %s", file)
                file.parentFile!!.mkdirs()
                file.writeAtomic { writer ->
                    writer.write(content)
                }
                file.lastModified()
            }
        }
    }

    suspend fun readFile(file: File) = withContext(ioContext) {
        file.readText()
    }

    data class FileInfo(
        val date: LocalDate,
        val file: File,
        val lastModified: Long,
    )

}
