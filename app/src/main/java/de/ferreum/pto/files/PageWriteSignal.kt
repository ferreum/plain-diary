package de.ferreum.pto.files

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import java.time.LocalDate

class PageWriteSignal {

    private val pageWrites = MutableSharedFlow<PageWrite>()

    fun observePageWrites(): Flow<PageWrite> = pageWrites

    suspend fun notifyPageWrite(date: PageWrite) {
        pageWrites.emit(date)
    }

    data class PageWrite(
        val date: LocalDate,
        val content: String?,
        val flags: Set<PageWriteFlag> = emptySet(),
    )

    enum class PageWriteFlag {
        // Events cannot be changed by this write.
        NO_EVENTS,
    }

}
