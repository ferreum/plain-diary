package de.ferreum.pto.files

import de.ferreum.pto.util.myMapLatest
import de.ferreum.pto.util.traceFlow
import kotlinx.coroutines.CompletableJob
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.awaitCancellation
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.isActive
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import java.time.LocalDate

class PtoPageContentServiceProvider(
    private val coroutineScope: CoroutineScope,
    private val factory: (coroutineScope: CoroutineScope, currentDate: LocalDate) -> PageContentService,
) {

    private val instances = mutableMapOf<LocalDate, InstanceRecord>()
    private val lock = Any()

    fun obtain(currentDate: LocalDate, userScope: CoroutineScope): PageContentService {
        return synchronized(lock) {
            val record = instances[currentDate] ?: createRecord(currentDate)
            val userJob = record.createUserJob()
            userScope.coroutineContext.job.invokeOnCompletion { userJob.cancel() }
            record.instance
        }
    }

    private fun createRecord(currentDate: LocalDate): InstanceRecord {
        val recordScope = coroutineScope + Job(coroutineScope.coroutineContext.job)
        val instance = factory(recordScope, currentDate)
        val record = InstanceRecord(currentDate, instance, recordScope)
        instances[currentDate] = record
        return record
    }

    private fun InstanceRecord.createUserJob(): Job {
        check(recordScope.isActive)
        val job = joinedJob?.let { Job(it) }
        if (job?.isActive == true) return job
        return startJoinedJob()
    }

    private fun InstanceRecord.startJoinedJob(): Job {
        check(joinedJob?.isActive != true)
        check(recordScope.isActive)
        retainJob?.cancel()
        joinedJob = Job(coroutineScope.coroutineContext.job).apply {
            invokeOnCompletion {
                retainJob = recordScope.launch {
                    retain()
                    synchronized(lock) {
                        ensureActive()
                        cleanupRecordLocked(currentDate, this@startJoinedJob)
                    }
                }
            }
        }
        return Job(joinedJob!!).also {
            joinedJob!!.complete() // make joinedJob complete when all users cancel
        }
    }

    private suspend fun InstanceRecord.retain() {
        instance.retainPolicy.myMapLatest { retainPolicy ->
            when (retainPolicy) {
                RetainPolicy.Drop -> null
                is RetainPolicy.Timeout -> {
                    delay(retainPolicy.millis)
                    null
                }
                RetainPolicy.Keep -> awaitCancellation()
            }
        }.traceFlow { "pageContentService retain $currentDate" }.first()
    }

    private fun cleanupRecordLocked(currentDate: LocalDate, record: InstanceRecord) {
        val r = instances.remove(currentDate)
        check(r === record)
        r!!.recordScope.cancel()
    }

    private data class InstanceRecord(
        val currentDate: LocalDate,
        val instance: PageContentService,
        val recordScope: CoroutineScope,
        var joinedJob: CompletableJob? = null,
        var retainJob: Job? = null,
    )

    sealed class RetainPolicy {
        data object Drop : RetainPolicy()
        data class Timeout(val millis: Long) : RetainPolicy()
        data object Keep : RetainPolicy()
    }

}
