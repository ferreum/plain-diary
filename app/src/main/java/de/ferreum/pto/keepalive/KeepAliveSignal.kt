package de.ferreum.pto.keepalive

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.ServiceInfo
import android.os.Binder
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.ServiceCompat
import de.ferreum.pto.R
import de.ferreum.pto.applicationScope
import de.ferreum.pto.keepAliveSignal
import de.ferreum.pto.preferences.Logger
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import timber.log.Timber

@SuppressLint("InlinedApi")
private const val TYPE_SHORT_SERVICE = ServiceInfo.FOREGROUND_SERVICE_TYPE_SHORT_SERVICE

class KeepAliveService : Service() {

    private var stopForegroundJob: Job? = null

    private val binder = Binder()
    override fun onBind(intent: Intent?): IBinder = binder

    override fun onCreate() {
        super.onCreate()
        Timber.i("onCreate")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_NOT_STICKY
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        if (!keepAliveSignal.isKeepAliveRequested.value) {
            Timber.i("onTaskRemoved: keepalive not requested")
            stopSelf()
            return
        }

        if (stopForegroundJob?.isActive == true) {
            // already requested
            return
        }

        Timber.i("onTaskRemoved: starting foreground for keepalive request")
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_notif_small)
            .setContentTitle(getText(R.string.keepalive_notification_title))
            .setContentText(getText(R.string.keepalive_notification_content))
            .build()
        ServiceCompat.startForeground(
            this,
            R.id.keepalive_notification,
            notification,
            TYPE_SHORT_SERVICE
        )

        stopForegroundJob = applicationScope.launch {
            keepAliveSignal.isKeepAliveRequested.first { !it }
            Timber.i("stopping foreground")
            stopForeground(STOP_FOREGROUND_REMOVE)
        }

        super.onTaskRemoved(rootIntent)
    }

    override fun onDestroy() {
        Timber.i("onDestroy")
        stopForegroundJob?.cancel()
        super.onDestroy()
    }

    companion object {
        private const val CHANNEL_ID = "keepalive"

        fun createNotificationChannel(context: Context) {
            NotificationManagerCompat.from(context).createNotificationChannel(NotificationChannel(
                CHANNEL_ID,
                context.getText(R.string.keepalive_notification_channel_title),
                NotificationManager.IMPORTANCE_LOW,
            ))
        }
    }

}

class KeepAliveSignal(
    private val context: Context,
    private val coroutineScope: CoroutineScope,
    private val logger: Logger,
) {

    private val handles = hashSetOf<Any>()

    private val serviceIntent = Intent(context, KeepAliveService::class.java)

    private val _isKeepAliveRequested = MutableStateFlow(false)
    val isKeepAliveRequested: StateFlow<Boolean> = _isKeepAliveRequested

    private val signalChannel = Channel<Any>(Channel.RENDEZVOUS)

    init {
        coroutineScope.launch {
            for (signal in signalChannel) {
                when (signal) {
                    is Signal.Active -> addHandle(signal.handle)
                    is Signal.Inactive -> removeHandle(signal.handle)
                }
            }
        }

        coroutineScope.launch {
            var isStarted = false
            _isKeepAliveRequested.collectLatest { start ->
                if (start == isStarted) return@collectLatest
                if (start) {
                    Timber.i("starting keepalive service")
                    try {
                        context.startService(serviceIntent)
                        isStarted = true
                    } catch (e: IllegalStateException) {
                        logger.suspendLogException(e, "starting service failed; ignoring")
                    }
                } else {
                    delay(500)
                    Timber.i("stopping keepalive service")
                    context.stopService(serviceIntent)
                    isStarted = false
                }
            }
        }
    }

    fun register(handleScope: CoroutineScope, isKeepAlive: Flow<Boolean>) {
        handleScope.launch {
            val handle = Any()
            val active = Signal.Active(handle)
            val inactive = Signal.Inactive(handle)
            try {
                isKeepAlive.distinctUntilChanged()
                    .collect { signalChannel.send(if (it) active else inactive) }
            } finally {
                coroutineScope.launch {
                    signalChannel.send(inactive)
                }
            }
        }
    }

    private fun addHandle(handle: Any) {
        if (handles.add(handle) && handles.size == 1) {
            Timber.d("start requesting service")
            _isKeepAliveRequested.value = true
        }
    }

    private fun removeHandle(handle: Any) {
        if (handles.remove(handle) && handles.isEmpty()) {
            Timber.d("stop requesting service")
            _isKeepAliveRequested.value = false
        }
    }

    private sealed class Signal {
        data class Active(val handle: Any) : Signal()
        data class Inactive(val handle: Any) : Signal()
    }

}
