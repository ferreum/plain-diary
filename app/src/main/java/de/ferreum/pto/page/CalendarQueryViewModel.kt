package de.ferreum.pto.page

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import de.ferreum.pto.page.calendar.CalendarEvent
import de.ferreum.pto.page.calendar.CalendarQueryHandlerImpl
import de.ferreum.pto.util.Event
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.ZoneId

class CalendarQueryViewModel(application: Application) : AndroidViewModel(application) {

    private val queryHandler = CalendarQueryHandlerImpl(application.contentResolver)

    private var currentQuery: Job? = null

    private val _eventResults = MutableStateFlow(Event.handled<List<CalendarEvent>>())
    val eventResults: StateFlow<Event<List<CalendarEvent>>> get() = _eventResults

    private val isQuerying = MutableStateFlow(false)
    val isBusy: StateFlow<Boolean> get() = isQuerying

    private val _currentError = MutableStateFlow<Throwable?>(null)
    val currentError: StateFlow<Throwable?> get() = _currentError

    fun queryEvents(localDate: LocalDate) {
        val date = localDate.atStartOfDay(ZoneId.systemDefault())
        currentQuery?.cancel()
        isQuerying.value = true
        currentQuery = viewModelScope.launch(Dispatchers.Default) {
            val result = queryHandler.queryEvents(date.toInstant(), date.plusDays(1).toInstant())
                .onCompletion { isQuerying.value = false }
                .catch { _currentError.value = it }
                .toList()
                .sortedBy { it.startTime }
            _eventResults.value = Event(result)
        }
    }

    fun clearCurrentError() {
        _currentError.value = null
    }

    fun cancel() {
        currentQuery?.cancel()
        isQuerying.value = false
    }

}
