package de.ferreum.pto.page

import de.ferreum.pto.search.FileMatcher
import de.ferreum.pto.search.MatchRange
import de.ferreum.pto.search.SearchInputHandler
import de.ferreum.pto.search.SearchToken
import de.ferreum.pto.util.myMapLatest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.BufferOverflow.DROP_OLDEST
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.plus
import kotlin.coroutines.CoroutineContext

class ContentSearchPresenter(
    coroutineScope: CoroutineScope,
    computeContext: CoroutineContext,
) : SearchInputHandler {

    private val fileMatcher = FileMatcher(computeContext)

    private var isSearchEnabled = false
    private val searchTokens = MutableStateFlow<Collection<SearchToken>>(emptyList())
    private val fileContent = MutableSharedFlow<String>(replay = 1, onBufferOverflow = DROP_OLDEST)

    val searchMatches: StateFlow<List<MatchRange>> = searchTokens
        .filter { tokens -> !tokens.any { it is SearchToken.ErrorToken } }
        .combine(fileContent) { searchTokens, text ->
            Pair(searchTokens, text)
        }.distinctUntilChanged().myMapLatest { (searchTokens, text) ->
            fileMatcher.findMatchRanges(text, searchTokens, stopWhenUnmatched = false).orEmpty()
        }.stateIn(coroutineScope + computeContext, SharingStarted.Eagerly, emptyList())

    override fun setSearchTokens(tokens: Collection<SearchToken>) {
        this.searchTokens.value = tokens
    }

    fun enableSearch(text: CharSequence) {
        isSearchEnabled = true
        fileContent.tryEmit(text.toString())
    }

    fun disableSearch() {
        isSearchEnabled = false
        fileContent.tryEmit("")
    }

    fun onTextChanged(text: CharSequence) {
        if (isSearchEnabled) {
            fileContent.tryEmit(text.toString())
        }
    }

    fun isSearchInputEmpty(): Boolean = searchTokens.value.isEmpty()

}
