package de.ferreum.pto.page

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.withStarted
import de.ferreum.datepicker.CustomDatePickerDialog
import de.ferreum.datepicker.DayDecorator
import de.ferreum.datepicker.Decoration
import de.ferreum.datepicker.MonthDecorator
import de.ferreum.pto.Constants
import de.ferreum.pto.R
import de.ferreum.pto.fileHelper
import de.ferreum.pto.logger
import de.ferreum.pto.preferences.Logger
import de.ferreum.pto.preferences.PtoFileHelper
import de.ferreum.pto.preferences.PtoPreferences
import de.ferreum.pto.preferences.todayDate
import de.ferreum.pto.preferencesRepository
import de.ferreum.pto.util.PAGE_ISO_DATE_FORMATTER
import de.ferreum.pto.util.SmallNumberSet
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.IOException
import java.time.LocalDate
import java.time.Month
import java.time.YearMonth
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import kotlin.coroutines.coroutineContext

class DatePickerDialogFragment : DialogFragment() {
    private lateinit var preferences: Flow<PtoPreferences>
    private lateinit var fileHelper: Deferred<PtoFileHelper>
    private lateinit var startDate: LocalDate

    private lateinit var logger: Logger

    override fun onAttach(context: Context) {
        super.onAttach(context)
        logger = context.logger
        preferences = context.preferencesRepository.preferencesFlow
        fileHelper = context.fileHelper
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        val epochDay = args?.getLong(EXTRA_EPOCH_DAY, -1) ?: -1
        startDate = if (epochDay == -1L) LocalDate.now() else LocalDate.ofEpochDay(epochDay)

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                preferences.map { it.isIsoDateFormatEnabled }
                    .distinctUntilChanged()
                    .collect { useIsoFormat ->
                        (dialog as CustomDatePickerDialog?)?.dateFormatter = if (useIsoFormat) {
                            PAGE_ISO_DATE_FORMATTER
                        } else {
                            DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)
                        }
                    }
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = CustomDatePickerDialog(
            requireContext(),
            listener = { date: LocalDate -> handleDateSelection(date) },
            startDate = startDate,
            dayDecorators = listOf(DayTodayDecorator(), DayEntryDecorator()),
            monthDecorators = listOf(MonthTodayDecorator(), MonthEntryDecorator()),
            dateFormatter = PAGE_ISO_DATE_FORMATTER,
        )
        dialog.setButton(DialogInterface.BUTTON_NEUTRAL, getText(R.string.today)) { _, _ ->
            lifecycleScope.launch {
                val date = preferences.first().todayDate
                lifecycle.withStarted {
                    handleDateSelection(date)
                }
            }
        }
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getText(android.R.string.cancel)) { d, _ ->
            d.cancel()
        }
        return dialog
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        getCallback().onDateSelectionCanceled()
    }

    private fun handleDateSelection(date: LocalDate) {
        getCallback().onDateSelected(date)
    }

    private fun getCallback(): DateSelectionCallback {
        return if (parentFragment is DateSelectionCallback) {
            parentFragment as DateSelectionCallback
        } else {
            requireActivity() as DateSelectionCallback
        }
    }

    private inner class DayTodayDecorator : DayDecorator {
        private val today = lifecycleScope.async {
            preferences.first().todayDate
        }

        override suspend fun decorate(date: LocalDate): Decoration? {
            return if (date == today.await()) {
                Decoration.TextAppearance(R.style.Pto_TextAppearance_DatePicker_Day_Today)
            } else {
                null
            }
        }
    }

    private inner class DayEntryDecorator : DayDecorator {
        private val cache = HashMap<YearMonth, Deferred<SmallNumberSet>>()

        override suspend fun decorate(date: LocalDate): Decoration? {
            if (date == startDate) {
                return Decoration.Background(ContextCompat.getDrawable(requireContext(),
                    R.drawable.pto_datepicker_day_background_current
                )!!)
            }
            val days = getMonthCache(YearMonth.from(date))
            return if (date.dayOfMonth in days) {
                Decoration.Background(ContextCompat.getDrawable(requireContext(),
                    R.drawable.pto_datepicker_day_background_entry_nocurrent
                )!!)
            } else {
                null
            }
        }

        private suspend fun getMonthCache(month: YearMonth): SmallNumberSet {
            var days = cache[month]
            if (days == null) {
                days = lifecycleScope.async(Dispatchers.Default) {
                    if (month.year !in Constants.YEAR_RANGE) {
                        return@async SmallNumberSet.empty()
                    }
                    try {
                        fileHelper.await().getExistingDaysInMonth(month.year, month.monthValue)
                    } catch (e: IOException) {
                        Timber.tag("EntryDecorator").e(e)
                        SmallNumberSet.empty()
                    }
                }
                cache[month] = days
            }
            return try {
                days.await()
            } catch (e: Throwable) {
                coroutineContext.ensureActive()
                logger.suspendLogException(e, tag = "DayEntryDecorator")
                SmallNumberSet.empty()
            }
        }
    }

    private inner class MonthTodayDecorator : MonthDecorator {
        private val today = lifecycleScope.async {
            preferences.first().todayDate
        }

        override suspend fun decorate(yearMonth: YearMonth): Decoration? {
            val today = this.today.await()
            return if (yearMonth.year == today.year && yearMonth.month == today.month) {
                Decoration.TextAppearance(R.style.Pto_TextAppearance_DatePicker_YearMonth_Today)
            } else {
                null
            }
        }
    }

    private inner class MonthEntryDecorator : MonthDecorator {
        private val cache = HashMap<Int, Deferred<Set<Month>>>()

        override suspend fun decorate(yearMonth: YearMonth): Decoration? {
            val months = getYearCache(yearMonth.year)
            currentCoroutineContext().ensureActive()
            return if (yearMonth.month in months) {
                Decoration.Background(ContextCompat.getDrawable(requireContext(),
                    R.drawable.pto_datepicker_month_background_entry
                )!!)
            } else {
                null
            }
        }

        private suspend fun getYearCache(year: Int): Set<Month> {
            var months = cache[year]
            if (months == null) {
                months = lifecycleScope.async(Dispatchers.Default) {
                    if (year !in Constants.YEAR_RANGE) {
                        return@async emptySet()
                    }
                    try {
                        fileHelper.await().getExistingMonthsInYear(year)
                    } catch (e: IOException) {
                        Timber.tag("MonthEntryDecorator").e(e)
                        emptySet()
                    }
                }
                cache[year] = months
            }
            return try {
                months.await()
            } catch (e: Throwable) {
                coroutineContext.ensureActive()
                logger.suspendLogException(e, tag = "MonthEntryDecorator")
                emptySet()
            }
        }
    }

    interface DateSelectionCallback {
        fun onDateSelected(date: LocalDate)
        fun onDateSelectionCanceled()
    }

    companion object {
        const val EXTRA_EPOCH_DAY = "epochDay"
    }
}
