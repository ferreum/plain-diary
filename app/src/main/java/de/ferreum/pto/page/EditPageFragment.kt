package de.ferreum.pto.page

import android.Manifest
import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.View.AccessibilityDelegate
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.view.accessibility.AccessibilityEvent
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.addCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView.INVISIBLE
import androidx.appcompat.widget.AppCompatTextView.LAYOUT_DIRECTION_RTL
import androidx.appcompat.widget.AppCompatTextView.VISIBLE
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.core.os.HandlerCompat
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.widget.NestedScrollView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.withResumed
import de.ferreum.pto.MainActivity
import de.ferreum.pto.R
import de.ferreum.pto.files.PageContentService
import de.ferreum.pto.files.PageContentService.EditorContent
import de.ferreum.pto.files.PageContentService.ErrorSource
import de.ferreum.pto.files.PageContentService.FileStatus
import de.ferreum.pto.page.PreemptiveFocusHandler.Focus
import de.ferreum.pto.page.PtoPageContract.ACTION_SHOW_PAGE
import de.ferreum.pto.page.PtoPageContract.EXTRA_REMINDER_TEXT_SELECTION
import de.ferreum.pto.page.PtoPageContract.EXTRA_TEXT_SELECTION
import de.ferreum.pto.page.PtoPageContract.EXTRA_TEXT_SELECTION_END
import de.ferreum.pto.page.PtoPageContract.PageIntentHandler
import de.ferreum.pto.page.PtoPageContract.getPageDateFromArguments
import de.ferreum.pto.page.PtoPageContract.navigateToDate
import de.ferreum.pto.page.PtoPageContract.navigateToIndexPage
import de.ferreum.pto.page.PtoPageContract.sendPtoPagerIntent
import de.ferreum.pto.page.PtoPageContract.showSearch
import de.ferreum.pto.page.PtoPageContract.toPageTitle
import de.ferreum.pto.page.calendar.CalendarEvent
import de.ferreum.pto.page.content.EventContent
import de.ferreum.pto.page.content.LinkEditDialogFragment
import de.ferreum.pto.page.content.TextType
import de.ferreum.pto.page.content.TimePickerDialogFragment
import de.ferreum.pto.page.content.getInitCursorPosition
import de.ferreum.pto.page.media.MarkdownMediaIntentHandler
import de.ferreum.pto.page.undo.HistoryCommand.Redo
import de.ferreum.pto.page.undo.HistoryCommand.Undo
import de.ferreum.pto.page.undo.UndoHistory
import de.ferreum.pto.page.undo.UndoHistoryStepAdjustHandler
import de.ferreum.pto.preferences.PtoPreferencesActivity
import de.ferreum.pto.preferences.todayDate
import de.ferreum.pto.quicknotes.QuickNotesNotifier
import de.ferreum.pto.reminder.AlarmActivity
import de.ferreum.pto.reminder.EventEditDialogFragment
import de.ferreum.pto.reminder.ReminderParser
import de.ferreum.pto.reminder.ReminderStatusPresenter.ReminderStatus
import de.ferreum.pto.reminder.formatNotificationTime
import de.ferreum.pto.reminderStatusPresenter
import de.ferreum.pto.search.SearchInputHelper
import de.ferreum.pto.textadjust.ButtonDragAdjustDetector
import de.ferreum.pto.textadjust.DragAdjustHandler
import de.ferreum.pto.textadjust.EventReminderGenerator
import de.ferreum.pto.textadjust.InplaceTextAdjuster
import de.ferreum.pto.textadjust.LinearDragStepAdjustHandler
import de.ferreum.pto.textadjust.TimeTextGenerator
import de.ferreum.pto.textadjust.asInverted
import de.ferreum.pto.util.Markdown
import de.ferreum.pto.util.awaitPostFrame
import de.ferreum.pto.util.combineTerminal
import de.ferreum.pto.util.getTypedSerializableExtra
import de.ferreum.pto.util.getWantedSelectionScrollPosition
import de.ferreum.pto.util.launchCompleteOnceInState
import de.ferreum.pto.util.myDebounce
import de.ferreum.pto.util.repeatOnAttach
import de.ferreum.pto.util.setSoftInputWanted
import de.ferreum.pto.util.showNotificationsRequiredDialog
import de.ferreum.pto.util.traceFlow
import de.ferreum.pto.util.tryStartActivity
import de.ferreum.pto.util.ui.InsetsProvider
import de.ferreum.pto.util.updateInputType
import de.ferreum.pto.widget.CustomSnackBar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.PrintWriter
import java.io.StringWriter
import java.time.Instant
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

/**
 * Lifecycle notes inside ViewPager:
 *
 * ### started
 * - cause: page is created
 * - state: view is created and may be attached or detached
 * - events: onCreate, onCreateView, onStart
 *
 * ### started -> resumed
 * - cause: pager settles and is idle on the current page
 * - state: view is created and attached
 * - events: onResume
 *
 * ### resumed -> started
 * - cause: pager settles and is idle on a different page
 * - state: view is created and may be attached or detached
 * - events: onPause
 *
 * ### started -> destroyed
 * - cause: removal from pager
 *   - pager moves more than 2 pages away from current page
 *   - happens during scroll, without settling
 * - state: view is detached and destroyed
 * - events: onPause (if resumed), onStop, onDestroyView, onDestroy
 */
class EditPageFragment : Fragment(R.layout.fragment_edit_page),
    PageIntentHandler, PreemptiveFocusHandler,
    TimePickerDialogFragment.Callback,
    EventEditDialogFragment.Callback,
    LinkEditDialogFragment.LinkCallback {

    private val viewModel: EditPageViewModel by viewModels {
        ViewModelFactory(this, currentDate = currentDate)
    }

    private val calendarQueryViewModel: CalendarQueryViewModel by viewModels()

    private val fileHintsViewModel: FileHintsViewModel by viewModels {
        ViewModelFactory(this, currentDate = currentDate)
    }

    private val pasteViewModel: PasteViewModel by activityViewModels {
        ViewModelFactory(activity = requireActivity() as AppCompatActivity)
    }

    private lateinit var notifManager: NotificationManagerCompat

    private var toolbar: Toolbar? = null
    private var bottomToolbar: ViewGroup? = null
    private var scrollView: NestedScrollView? = null
    private var editText: PtoPageEditText? = null
    private var timeTextGenerator: TimeTextGenerator? = null
    private var searchInputHelper: SearchInputHelper? = null
    private var snackBar: CustomSnackBar? = null
    private var contentTextWatcher: TextSnapshotWatcher? = null

    private lateinit var currentDate: LocalDate

    private var deferredActivationJob: Job? = null
    private var observerJob: Job? = null
    private var delayJob: Job? = null

    private val checkEditTextScrollRunnable = Runnable { checkEditTextScroll() }
    private val handler = HandlerCompat.createAsync(Looper.getMainLooper())

    private var navigationJob: Job? = null
        set(value) {
            field?.cancel()
            field = value
        }

    private var needsScrollToCursor = false

    private lateinit var contentSourceTag: PageContentService.ContentSourceTag
    private val isContentLoaded = MutableStateFlow(false)

    private val activeLifecycleOwner = object : LifecycleOwner {
        override val lifecycle get() = activeLifecycle
    }
    private val activeLifecycle: LifecycleRegistry = LifecycleRegistry(activeLifecycleOwner)

    private var pageFocusState = Focus.UNFOCUSED

    private val quickNotePermissionRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { success ->
            if (success) {
                startQuickNotesNotification()
            } else {
                requireContext().showNotificationsRequiredDialog()
            }
        }

    private val calendarPermissionRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { result ->
            if (result) {
                calendarQueryViewModel.queryEvents(currentDate)
            }
        }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        notifManager = NotificationManagerCompat.from(requireContext())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentDate = arguments.getPageDateFromArguments()

        activeLifecycle.currentState = Lifecycle.State.CREATED

        val searchCloseBackPressCallback = requireActivity().onBackPressedDispatcher.addCallback(activeLifecycleOwner) {
            viewModel.setSearchMode(false)
        }
        searchCloseBackPressCallback.isEnabled = false

        lifecycleScope.launch {
            activeLifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.isSearchMode
                        .traceFlow { "page $currentDate searchModeBackpressCallback" }
                        .collect {
                            searchCloseBackPressCallback.isEnabled = it
                        }
                }

                launch {
                    viewModel.startEvent
                        .traceFlow { "page $currentDate startEvent" }
                        .collect { e -> e.once { startEvent(it) } }
                }

                calendarQueryViewModel.eventResults
                    .traceFlow { "page $currentDate eventResults" }
                    .collect {
                        awaitContentLoaded()
                        it.valueOnce?.insertCalendarEvents()
                    }
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        contentSourceTag = PageContentService.ContentSourceTag()

        val scrollBlockingLayout: ScrollBlockingLayout = view.findViewById(R.id.scrollBlockingLayout)
        val toolbarContainer: View = view.findViewById(R.id.toolbarContainer)
        toolbar = view.findViewById(R.id.toolbar)
        val toolbarTitle: TextView = view.findViewById(R.id.toolbarTitle)
        scrollView = view.findViewById(R.id.scrollView)
        editText = view.findViewById(R.id.editText)
        val progressBar: View = view.findViewById(R.id.progressBar)
        val prevHintsTextView: TextView = view.findViewById(R.id.prevHintsText)
        val nextHintsTextView: TextView = view.findViewById(R.id.nextHintsText)
        snackBar = view.findViewById(R.id.snackBar)
        bottomToolbar = view.findViewById(R.id.bottomToolbar)

        val toolbar = toolbar!!
        val scrollView = scrollView!!
        val editText = editText!!
        val snackBar = snackBar!!
        val bottomToolbar = bottomToolbar!!

        contentTextWatcher = TextSnapshotWatcher(editText) { snapshot, transactionTag ->
            viewModel.onTextChanged(snapshot, contentSourceTag, transactionTag)
        }
        val contentTextWatcher = contentTextWatcher!!

        if (savedInstanceState != null && viewModel.isSearchMode.value) {
            ensureBottomBarCreated()
        }

        scrollBlockingLayout.onTouchDownListener = {
            fileHintsViewModel.triggerHide()
        }

        toolbar.setOnMenuItemClickListener(this::handleOptionItemClick)
        toolbarTitle.setOnClickListener {
            showDatePagePicker(currentDate)
        }

        progressBar.setOnClickListener {
            calendarQueryViewModel.cancel()
        }

        editText.doAfterTextChanged {
            handler.post(checkEditTextScrollRunnable)
            if (!contentTextWatcher.isBlockingTextChangeEvents) {
                fileHintsViewModel.triggerHide()
            }
        }

        editText.accessibilityDelegate = object : AccessibilityDelegate() {
            override fun onPopulateAccessibilityEvent(host: View, event: AccessibilityEvent) {
                super.onPopulateAccessibilityEvent(host, event)
                if (event.eventType == AccessibilityEvent.TYPE_VIEW_TEXT_SELECTION_CHANGED) {
                    // Accessibility event may replace text including watcher span.
                    // Ensure it's still set up and handle change event otherwise.
                    val text = editText.text!!
                    if (text.getSpanStart(contentTextWatcher) < 0) {
                        contentTextWatcher.setup(editText)
                        contentTextWatcher.notifyContentReplaced(
                            text.toString(),
                            editText.selectionStart,
                            editText.selectionEnd,
                            UndoHistory.NO_CHANGE_ID
                        )
                    }
                }
            }
        }

        scrollView.addOnLayoutChangeListener { _, _, top, _, bottom, _, oldTop, _, oldBottom ->
            val oldHeight = oldBottom - oldTop
            val height = bottom - top
            if (needsScrollToCursor || oldHeight > height) {
                needsScrollToCursor = false
                scrollView.getWantedSelectionScrollPosition(editText)
                    ?.let { scrollView.scrollTo(0, it) }
            }
        }
        // smooth scrolling sometimes scrolls to the wrong position when resized in some situations
        scrollView.isSmoothScrollingEnabled = false

        // disabling editText prevents focusing the view, closing the keyboard when switching
        // to a new page. To prevent this, block all edits with a filter instead
        editText.filters = arrayOf(InputFilter { _, _, _, dest, dstart, dend ->
            if (!contentTextWatcher.isBlockingTextChangeEvents
                && !viewModel.isEditingAllowed.value
            ) {
                dest.subSequence(dstart, dend)
            } else {
                null // accept replacement
            }
        })

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    awaitPostFrame()
                    try {
                        viewModel.currentContent
                            .traceFlow { "page $currentDate content" }
                            .collect {
                                if (it.sourceTag != contentSourceTag) {
                                    loadNewContent(it, contentTextWatcher)
                                }
                                isContentLoaded.value = true
                            }
                    } finally {
                        isContentLoaded.value = false
                    }
                }

                view.repeatOnAttach {
                    launch {
                        viewModel.preferences
                            .traceFlow { "page $currentDate preferences" }
                            .collect {
                                val enabled = it.isIsoDateFormatEnabled
                                toolbarTitle.text = currentDate.toPageTitle(requireContext(), enabled)
                                timeTextGenerator?.newpageTime = it.newpageTime
                                editText.textSize = it.editorFontSize.toFloat()
                                editText.isEmojiCompatEnabled = it.isSupportEmojiEnabled
                                editText.updateInputType(
                                    InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS,
                                    !it.isSuggestionsEnabled
                                )
                            }
                    }

                    launch {
                        (requireActivity() as InsetsProvider).pageInsets.collect { insets ->
                            toolbarContainer.setPadding(insets.left, insets.top, insets.right, 0)
                            scrollView.setPadding(insets.left, 0, insets.right, 0)
                            snackBar.updateLayoutParams<MarginLayoutParams> {
                                leftMargin = insets.left
                                rightMargin = insets.right
                            }
                            bottomToolbar.setPadding(insets.left, 0, insets.right, insets.bottom)
                        }
                    }

                    calendarQueryViewModel.isBusy
                        .traceFlow { "page $currentDate isBusy" }
                        .collect {
                            progressBar.isVisible = it
                        }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.RESUMED) {
                launch {
                    try {
                        fileHintsViewModel.hints
                            .traceFlow { "page $currentDate hints" }
                            .collect { (prev, next) ->
                                prev?.let { prevHintsTextView.text = prev }
                                prevHintsTextView.setHintVisibility(visible = prev != null, prev = true)
                                next?.let { nextHintsTextView.text = next }
                                nextHintsTextView.setHintVisibility(visible = next != null, prev = false)
                            }
                    } finally {
                        nextHintsTextView.setHintVisibility(visible = false, prev = false)
                        prevHintsTextView.setHintVisibility(visible = false, prev = true)
                    }
                }

                calendarQueryViewModel.currentError
                    .traceFlow { "page $currentDate currentError" }
                    .collect {
                        it?.let { buildErrorDialog(R.string.calendarquery_error_title, it).show() }
                    }
            }
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        searchInputHelper?.onViewStateRestored()
    }

    private fun updateSearchMode(searchInputClearButton: View, text: Editable?, isSearchMode: Boolean) {
        searchInputClearButton.isVisible = isSearchMode && text?.isNotEmpty() ?: false
    }

    override fun onStart() {
        super.onStart()
        viewModel.checkForChanges()
        viewModel.closeSearchIfEmpty()
    }

    override fun onResume() {
        super.onResume()
        deferredActivationJob?.cancel()
        ensureMenuCreated()
        ensureBottomBarCreated()
        activeLifecycle.currentState = Lifecycle.State.RESUMED
        viewModel.checkForChanges()
        startObserver(true)
        fileHintsViewModel.showHints()

        editText?.requestFocus()
        lifecycle.launchCompleteOnceInState(Lifecycle.State.RESUMED, stopOnCancel = true) {
            checkShowSoftInput()
        }
    }

    override fun onPreemptiveFocusChanged(focus: Focus) {
        Timber.d("onPreemptiveFocusChanged: %s %s", currentDate, focus)

        pageFocusState = focus
        if (focus == Focus.FOCUSED) {
            editText?.requestFocus()
        }

        if (isResumed) {
            // always active while resumed
            return
        }

        when (focus) {
            Focus.PEEK -> {
                startObserver(false)
                deferredActivationJob?.cancel()
                deferredActivationJob = viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
                    delay(250)
                    startViewsStaggered()
                }
            }
            Focus.FOCUSED -> {
                startObserver(true)
                deferredActivationJob?.cancel()
                deferredActivationJob = viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
                    startViewsStaggered()
                }
            }
            Focus.UNFOCUSED -> {
                deferredActivationJob?.cancel()
                deferredActivationJob = viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
                    delay(500) // short delay if unfocused while not resumed
                    awaitPostFrame()
                    stopActiveLifecycle()
                }
            }
        }
    }

    private suspend fun startViewsStaggered() {
        if (activeLifecycle.currentState != Lifecycle.State.RESUMED) {
            awaitPostFrame()
        }
        if (ensureMenuCreated()) {
            awaitPostFrame()
        }
        if (ensureBottomBarCreated()) {
            awaitPostFrame()
        }
        activeLifecycle.currentState = Lifecycle.State.RESUMED
    }

    private suspend fun checkShowSoftInput() {
        val wantKeyboard = viewModel.comparedToday
            .traceFlow { "page $currentDate today" }
            .first { it != null }!! >= 0
        editText?.wantsSoftInput = wantKeyboard
        requireActivity().setSoftInputWanted(wantKeyboard)
    }

    private fun startObserver(immediate: Boolean) {
        if (immediate) delayJob?.cancel()
        if (observerJob != null) return
        observerJob = lifecycleScope.launch {
            if (!immediate) {
                delayJob = launch { delay(300) }
                delayJob?.join()
            }
            viewModel.observeFileContent(this)
        }
    }

    private fun stopObserver() {
        observerJob?.cancel()
        delayJob?.cancel()
        observerJob = null
        delayJob = null
    }

    override fun onPause() {
        viewModel.triggerSave()
        deferredActivationJob?.cancel()
        deferredActivationJob = lifecycleScope.launch {
            delay(10000) // fragment was resumed; keep it active longer
            stopActiveLifecycle()
        }
        editText?.wantsSoftInput = false
        super.onPause()
    }

    override fun onStop() {
        // if still visible, it's the activity stopping
        if (!isVisible) {
            // otherwise, page is swiped away or being removed
            stopActiveLifecycle()
        }
        super.onStop()
    }

    override fun onDestroyView() {
        pageFocusState = Focus.UNFOCUSED
        stopActiveLifecycle()
        bottomToolbar = null
        contentTextWatcher = null
        snackBar = null
        searchInputHelper = null
        timeTextGenerator = null
        editText = null
        scrollView = null
        toolbar = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        stopActiveLifecycle()
        activeLifecycle.currentState = Lifecycle.State.DESTROYED
        super.onDestroy()
    }

    private fun stopActiveLifecycle() {
        deferredActivationJob?.cancel()
        activeLifecycle.currentState = Lifecycle.State.CREATED
        stopObserver()
    }

    private fun handleOptionItemClick(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.today -> launchNavigationJob {
                navigateToDate(viewModel.preferences.first().todayDate)
            }
            R.id.goToDate -> showDatePagePicker(currentDate)
            R.id.showNotification -> {
                if (Build.VERSION.SDK_INT >= 33) {
                    quickNotePermissionRequest.launch(Manifest.permission.POST_NOTIFICATIONS)
                } else {
                    startQuickNotesNotification()
                }
            }
            R.id.index -> launchNavigationJob {
                navigateToIndexPage(viewModel.preferences.first())
            }
            R.id.search -> {
                if (viewModel.isSearchMode.value) {
                    viewModel.setSearchMode(false)
                    showSearch()
                } else {
                    viewModel.setSearchMode(true)
                    searchInputHelper?.searchInput?.requestFocus()
                }
            }
            R.id.searchEverywhere -> showSearch()
            R.id.settings -> startActivity(Intent(requireContext(), PtoPreferencesActivity::class.java))
        }
        return true
    }

    private fun loadNewContent(content: EditorContent, textSnapshotWatcher: TextSnapshotWatcher) {
        val editText = editText!!
        var needUpdate = false
        textSnapshotWatcher.withoutTextChangeEvents {
            val text = editText.text!!
            var updatedContent = content
            if (!content.hasSelection && text.isNotEmpty() && content.text.startsWith(text)) {
                updatedContent = content.copy(
                    selectionStart = editText.selectionStart,
                    selectionEnd = editText.selectionEnd,
                )
            }
            text.replace(0, text.length, content.text)
            textSnapshotWatcher.setup(editText)
            needUpdate = !updatedContent.hasSelection
            initCursorPosition(editText, updatedContent, textSnapshotWatcher)
        }
        if (needUpdate) {
            textSnapshotWatcher.scheduleUpdate()
        }

        Timber.tag("EditPageFragment").i("loadNewContent: loaded change with ID %d", content.changeId)
        // override smooth scroll when content updates
        if (scrollView!!.isLayoutRequested) {
            needsScrollToCursor = true
        }
    }

    private fun initCursorPosition(
        editText: EditText,
        content: EditorContent,
        textSnapshotWatcher: TextSnapshotWatcher,
    ) {
        if (content.hasSelection) {
            editText.setSelection(content.selectionStart, content.selectionEnd)
            textSnapshotWatcher.notifyContentReplaced(
                content.text, content.selectionStart, content.selectionEnd, content.changeId)
        } else {
            val pos = editText.text?.let { getInitCursorPosition(it) }
                ?: content.text.length
            editText.setSelection(pos)
            textSnapshotWatcher.notifyContentReplaced(content.text, pos, pos, content.changeId)
        }
    }

    override fun onTimeSelected(time: LocalTime, textType: TextType?) {
        lifecycle.launchCompleteOnceInState(Lifecycle.State.STARTED) {
            awaitContentLoaded()
            val prevTime = (textType as? TextType.Time)?.time
            replaceOrInsertText(editText!!, textType) { text, selStart, _ ->
                timeTextGenerator!!.getTimeText(time, text, selStart, prevTime = prevTime)
            }
        }
    }

    override fun onEventEdited(eventContent: EventContent, textType: TextType?) {
        lifecycle.launchCompleteOnceInState(Lifecycle.State.STARTED) {
            awaitContentLoaded()
            val (newText) = ReminderParser.formatReminder(eventContent)
            replaceOrInsertText(editText!!, textType, newText)
        }
    }

    override fun onLinkChanged(text: String, target: String, textType: TextType?) {
        lifecycle.launchCompleteOnceInState(Lifecycle.State.STARTED) {
            awaitContentLoaded()
            replaceOrInsertText(editText!!, textType, Markdown.formatLink(text, target))
        }
    }

    private fun replaceOrInsertText(editText: EditText, textType: TextType?, newText: String) {
        replaceOrInsertText(editText, textType) { _, _, _ -> newText }
    }

    private inline fun replaceOrInsertText(
        editText: EditText,
        textType: TextType?,
        newText: (text: CharSequence, start: Int, end: Int) -> String,
    ) {
        val text = editText.editableText
        if (textType == null || !tryReplaceText(textType, editText,
                newText(text, textType.sourceStart, textType.sourceEnd))) {
            val cursor = editText.selectionStart
            editText.text.insert(cursor, newText(text, cursor, cursor))
        }
    }

    private fun tryReplaceText(textType: TextType?, editText: EditText, newText: String): Boolean {
        if (textType == null) return false
        if (!textType.source.regionMatches(0, editText.text, textType.sourceStart, textType.source.length)) {
            return false
        }
        editText.text.replace(textType.sourceStart, textType.sourceEnd, newText)
        return true
    }

    override fun onPageIntent(intent: Intent) {
        if (intent.action == ACTION_SHOW_PAGE) {
            lifecycleScope.launch {
                awaitContentLoaded()
                withResumed {
                    intent.getCharSequenceExtra(PtoPageContract.EXTRA_SEARCH_INPUT)?.let {
                        ensureBottomBarCreated()
                        searchInputHelper!!.setSearchInput(it)
                        viewModel.setSearchMode(true)
                    }
                    val reminderTime =
                        intent.getTypedSerializableExtra<LocalTime>(EXTRA_REMINDER_TEXT_SELECTION)
                    if (reminderTime != null) {
                        val offset = ReminderParser.indexOfEventByTime(
                            editText!!.text!!, reminderTime)
                        if (offset != null) {
                            editText!!.setSelection(offset)
                        }
                        return@withResumed
                    }
                    val startPos = intent.getIntExtra(EXTRA_TEXT_SELECTION, -1)
                    if (startPos >= 0) {
                        val textLength = editText!!.text!!.length
                        val start = startPos.coerceAtMost(textLength)
                        val endPos = intent.getIntExtra(EXTRA_TEXT_SELECTION_END, -1)
                        editText!!.setSelection(start, endPos.coerceIn(start, textLength))
                    }
                }
            }
        }
    }

    private fun showDatePagePicker(startDate: LocalDate) {
        (parentFragment as PtoPagerContainer).showPageDatePicker(startDate)
    }

    private fun startEvent(event: StartEvent) {
        when (event) {
            is StartEvent.LinkEditor -> startLinkEditor(event.textTypes)
            is StartEvent.TimeEditor -> startTimePicker(event.type)
            is StartEvent.EventEditor -> startEventEditor(event.textTypes)
            is StartEvent.Date -> navigateToDate(event.date)
            is StartEvent.Link -> {
                val intent = Intent(Intent.ACTION_VIEW, event.uri)
                tryStartActivity(requireContext(), intent) { snackBar?.showText(it) }
            }
            StartEvent.InsertTime -> editText?.apply {
                val selStart = selectionStart
                val text = editableText
                text.replace(
                    selStart,
                    selectionEnd,
                    timeTextGenerator!!.getTimeText(LocalTime.now(), text, selStart)
                )
            }

            is StartEvent.SetClipboardText -> setClipboardText(event.text)
        }
    }

    private fun List<CalendarEvent>.insertCalendarEvents() {
        val formatter = DateTimeFormatter.ofPattern("H:mm")
        val zone = ZoneId.systemDefault()
        val text = joinToString(separator = "") {
            val time = formatter.format(it.startTime.atZone(zone).toLocalTime())
            "\n@:$time ${it.title}\n"
        }
        val position = editText!!.selectionStart
        editText!!.text!!.insert(position, text)
    }

    private fun showFileError(fileStatus: FileStatus.Error) {
        val title = when (fileStatus.source) {
            ErrorSource.LOADING -> R.string.filestatus_error_title_loading
            ErrorSource.SAVING -> R.string.filestatus_error_title_saving
        }
        buildErrorDialog(title, fileStatus.throwable)
            .setPositiveButton(android.R.string.ok, null)
            .show()
    }

    private fun buildErrorDialog(title: Int, throwable: Throwable): AlertDialog.Builder {
        val sw = StringWriter()
        throwable.printStackTrace(PrintWriter(sw))
        return AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setMessage(sw.toString())
            .setNeutralButton(R.string.settings) { _, _ ->
                startActivity(Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                    data = Uri.fromParts("package", requireContext().packageName, null)
                })
            }
            .setPositiveButton(android.R.string.ok, null)
            .setOnDismissListener {
                calendarQueryViewModel.clearCurrentError()
            }
    }

    private fun setClipboardText(text: String) {
        val cm = requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        cm.setPrimaryClip(ClipData.newPlainText(null, text))
        snackBar?.showText(
            getText(R.string.editor_action_link_copied_to_clipboard_message),
            1500.milliseconds
        )
    }

    private fun startTimePicker(textType: TextType.Time) {
        if (childFragmentManager.findFragmentByTag(TAG_TIME_PICKER) != null) {
            // don't show more than one
            return
        }
        val fragment = TimePickerDialogFragment.withArgs(textType)
        fragment.show(childFragmentManager, TAG_TIME_PICKER)
    }

    private fun startEventEditor(textType: TextType.Event) {
        if (childFragmentManager.findFragmentByTag(TAG_EVENT_EDIT_FRAGMENT) != null) {
            // don't show more than one
            return
        }
        val fragment = EventEditDialogFragment.withArgs(textType)
        fragment.show(childFragmentManager, TAG_EVENT_EDIT_FRAGMENT)
    }

    private fun startLinkEditor(textTypes: List<TextType>) {
        if (childFragmentManager.findFragmentByTag(TAG_LINK_EDIT_FRAGMENT) != null) {
            // don't show more than one
            return
        }
        val fragment = LinkEditDialogFragment()
        fragment.arguments = Bundle().apply {
            val textType = textTypes.firstOrNull { it is TextType.Link }
                ?: textTypes.firstOrNull { it is TextType.Selection }
            when (textType) {
                null -> Unit
                is TextType.Link -> {
                    putString(LinkEditDialogFragment.EXTRA_TEXT, textType.text)
                    putString(LinkEditDialogFragment.EXTRA_TARGET, textType.uri)
                }
                else -> {
                    putString(LinkEditDialogFragment.EXTRA_TEXT, textType.source)
                }
            }
            putParcelable(LinkEditDialogFragment.EXTRA_TEXT_TYPE, textType)
        }
        fragment.show(childFragmentManager, TAG_LINK_EDIT_FRAGMENT)
    }

    private fun startQuickNotesNotification() {
        launchNavigationJob {
            QuickNotesNotifier.showNotification(requireContext(), viewModel.preferences.first())
        }
    }

    private fun launchNavigationJob(block: suspend CoroutineScope.() -> Unit) {
        // Check for started state here, as we don't want navigation to happen
        // in a deferred way when we get resumed at a later time.
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
            navigationJob = lifecycleScope.launch(block = block)
        }
    }

    private fun pasteMediaIntent(intent: Intent) {
        lifecycleScope.launch(Dispatchers.Default) {
            val links = MarkdownMediaIntentHandler.getShareIntentLinks(intent)
                .ifEmpty { return@launch }
            insertLinks(links)
        }
    }

    private suspend fun insertLinks(links: List<MarkdownMediaIntentHandler.Link>) {
        awaitContentLoaded()
        withContext(Dispatchers.Main.immediate) {
            lifecycle.withResumed {
                val text = editText!!.text!!
                val joined = links.joinToString { it.text + "\n" }
                text.replace(editText!!.selectionStart, editText!!.selectionEnd, joined)
            }
        }
    }

    private suspend fun awaitContentLoaded() {
        isContentLoaded.first { it }
    }

    private fun checkEditTextScroll() {
        val editText = editText ?: return
        val scrollView = scrollView ?: return
        if (editText.isLayoutRequested) {
            // scrollView's layout change listener applies the scroll
            needsScrollToCursor = true
        } else {
            scrollView.getWantedSelectionScrollPosition(editText)
                ?.let { scrollView.scrollTo(0, it) }
        }
    }

    private fun ensureMenuCreated(): Boolean {
        if (toolbar!!.menu.size() == 0) {
            createOptionsMenu()
            return true
        } else {
            return false
        }
    }

    private fun ensureBottomBarCreated(): Boolean {
        val bottomToolbar = bottomToolbar!!

        if (bottomToolbar.childCount <= 1) {
            val view = layoutInflater.inflate(R.layout.include_editor_bottom_bar, bottomToolbar)
            startBottomToolbar(view)
            return true
        } else {
            return false
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun startBottomToolbar(view: View) {
        val pasteButton: ImageButton = view.findViewById(R.id.pasteButton)
        val timeButton: ImageButton = view.findViewById(R.id.addTimeButton)
        val eventButton: ImageButton = view.findViewById(R.id.eventButton)
        val editLinkButton: View = view.findViewById(R.id.editLinkButton)
        val openLinkButton: View = view.findViewById(R.id.openLinkButton)
        val addElementButton: View = view.findViewById(R.id.addElementMenuButton)
        val undoButton: View = view.findViewById(R.id.undoButton)
        val redoButton: View = view.findViewById(R.id.redoButton)
        val saveStatusImage: ImageView = view.findViewById(R.id.saveStatusImage)
        val searchInput: EditText = view.findViewById(R.id.searchInput)
        val searchInputClearButton: View = view.findViewById(R.id.searchInputClearButton)
        val searchInputToEverywhereButton: View = view.findViewById(R.id.searchInputToEverywhereButton)
        val searchCloseButton: View = view.findViewById(R.id.searchCloseButton)
        val normalToolbarGroup: Group = view.findViewById(R.id.normalToolbarGroup)
        val searchToolbarGroup: Group = view.findViewById(R.id.searchToolbarGroup)

        val editText = editText!!
        val contentTextWatcher = contentTextWatcher!!

        val dragAdjustmentListener: (Boolean) -> Unit = { started ->
            contentTextWatcher.transactionTag = if (started) {
                viewModel.getNextTransactionTag()
            } else {
                UndoHistory.NO_TRANSACTION_TAG
            }
        }

        pasteButton.setOnClickListener {
            if (!isContentLoaded.value) return@setOnClickListener
            pasteViewModel.getAndRemovePasteIntent()?.let { intent ->
                pasteMediaIntent(intent)
            }
        }

        timeTextGenerator = TimeTextGenerator(
            requireContext(),
            currentDate,
            LocalTime.MIDNIGHT
        )
        val timeDragAdjustDetector = ButtonDragAdjustDetector(requireContext(),
            DragAdjustHandler(editText, InplaceTextAdjuster(timeTextGenerator!!), dragAdjustmentListener))
        timeButton.setOnTouchListener(timeDragAdjustDetector)
        timeButton.setOnClickListener {
            viewModel.onTimeClick()
        }

        val eventReminderGenerator = EventReminderGenerator(requireContext())
        val eventDragAdjustDetector = ButtonDragAdjustDetector(requireContext(),
            DragAdjustHandler(editText, InplaceTextAdjuster(eventReminderGenerator), dragAdjustmentListener))
        eventButton.setOnTouchListener(eventDragAdjustDetector)
        eventButton.setOnClickListener {
            viewModel.editSelectedEvent()
        }

        val undoAdjustHandler = UndoHistoryStepAdjustHandler(viewModel::undoVersion) {
            viewModel.doHistoryCommand(it)
        }
        undoButton.setOnTouchListener(ButtonDragAdjustDetector(requireContext(),
            LinearDragStepAdjustHandler(requireContext(), editText, undoAdjustHandler.asInverted())))
        redoButton.setOnTouchListener(ButtonDragAdjustDetector(requireContext(),
            LinearDragStepAdjustHandler(requireContext(), editText, undoAdjustHandler)))
        undoButton.setOnClickListener {
            viewModel.doHistoryCommand(Undo)
        }
        redoButton.setOnClickListener {
            viewModel.doHistoryCommand(Redo)
        }

        editLinkButton.setOnClickListener {
            viewModel.editSelectedLink()
        }
        openLinkButton.setOnClickListener {
            viewModel.openSelectedLink()
        }
        openLinkButton.setOnLongClickListener {
            viewModel.copySelectedLinkToClipboard()
            true
        }
        addElementButton.setOnClickListener { v ->
            v.showContextMenu(v.width.toFloat(), v.height.toFloat())
        }
        addElementButton.setOnCreateContextMenuListener { menu, _, _ ->
            MenuInflater(requireContext()).inflate(R.menu.fragment_edit_add_element, menu)
            menu.findItem(R.id.addEvents).setOnMenuItemClickListener {
                calendarPermissionRequest.launch(Manifest.permission.READ_CALENDAR)
                true
            }
            menu.findItem(R.id.addLink).setOnMenuItemClickListener {
                viewModel.editSelectedLink()
                true
            }
        }

        saveStatusImage.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                when (val status = viewModel.fileStatus.value) {
                    is FileStatus.Error -> showFileError(status)
                    FileStatus.Modified -> viewModel.triggerSave()
                    else -> {}
                }
            }
        }

        val searchInputHelper = SearchInputHelper(
            viewLifecycleOwner,
            searchInput,
            viewModel,
            onMessage = { snackBar?.showText(it) },
        )
        this.searchInputHelper = searchInputHelper
        searchInputHelper.start()
        searchInputHelper.onViewStateRestored()
        searchCloseButton.setOnClickListener {
            viewModel.setSearchMode(false)
            editText.requestFocus()
        }
        searchInput.doAfterTextChanged { text ->
            updateSearchMode(searchInputClearButton, text, viewModel.isSearchMode.value)
        }
        searchInputClearButton.setOnClickListener {
            searchInput.text.apply { replace(0, length, "") }
        }
        searchInputToEverywhereButton.setOnClickListener {
            showSearch(searchInputHelper.sanitizedSearchInputText.takeIf { it.isNotBlank() })
        }

        // only called once for created view
        // noinspection RepeatOnLifecycleWrongUsage
        viewLifecycleOwner.lifecycleScope.launch {
            launch {
                // one-shot for initial update; shared flow may have run before creation
                val prefs = viewModel.preferences.first()
                timeTextGenerator?.newpageTime = prefs.newpageTime
            }

            activeLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.isEditingAllowed
                        .traceFlow { "page $currentDate isEditingAllowed" }
                        .collect {
                            undoButton.isEnabled = it
                            redoButton.isEnabled = it
                            timeButton.isEnabled = it
                            editLinkButton.isEnabled = it
                            openLinkButton.isEnabled = it
                            addElementButton.isEnabled = it
                        }
                }

                launch {
                    viewModel.undoCounters
                        .traceFlow { "page $currentDate undoCounters" }
                        .collect { counters ->
                            undoButton.isActivated = counters.undoCount > 0
                            redoButton.isActivated = counters.redoCount > 0
                        }
                }

                launch {
                    viewModel.fileStatus
                        .myDebounce { if (it.isBusy) 35L else 0L }
                        .traceFlow { "page $currentDate fileStatus" }
                        .collect { status ->
                            saveStatusImage.setImageResource(when (status) {
                                FileStatus.Loading,
                                FileStatus.Saving -> R.drawable.ic_filestatus_busy
                                FileStatus.Modified -> R.drawable.ic_filestatus_modified
                                FileStatus.Saved -> R.drawable.ic_filestatus_saved
                                is FileStatus.Error -> R.drawable.ic_filestatus_error
                            })
                            val clickable = status is FileStatus.Error || status == FileStatus.Modified
                            saveStatusImage.isClickable = clickable
                            if (!clickable) {
                                // it get stuck if pressed during state change otherwise
                                saveStatusImage.isPressed = false
                            }

                            if (!status.isBusy) delay(100) // throttle
                        }
                }

                launch {
                    val matchHighlighter = MatchHighlighter()
                    viewModel.searchMatches
                        .traceFlow { "page $currentDate searchMatches" }
                        .collect { matches ->
                            matchHighlighter.highlight(editText, matches)
                        }
                }

                launch {
                    combineTerminal(
                        viewModel.isSearchMode,
                        pasteViewModel.isPasting,
                    ) { isSearchMode, isPasting ->
                        normalToolbarGroup.isVisible = !isSearchMode || isPasting
                        searchToolbarGroup.isVisible = isSearchMode && !isPasting
                        updateSearchMode(searchInputClearButton, searchInput.text, isSearchMode)
                    }.traceFlow { "page $currentDate bottomButtonGroups" }.collect()
                }

                combineTerminal(
                    viewModel.toolbarState,
                    pasteViewModel.isPasting,
                ) { state, isPasting ->
                    pasteButton.isVisible = isPasting

                    val timeVisible = state.timeButtonState.isVisible && !isPasting
                    val timeChanged = timeVisible != timeButton.isVisible
                    timeButton.isVisible = timeVisible
                    if (timeVisible) {
                        timeButton.setImageResource(state.timeButtonState.imageResource)
                    }

                    if (timeChanged) {
                        // Fix eventButton positioning according to timeButton visibility without
                        // moving timeButton around. Center it only if timeButton is gone.
                        eventButton.updateLayoutParams<ConstraintLayout.LayoutParams> {
                            val value = if (timeButton.isVisible) {
                                ConstraintLayout.LayoutParams.UNSET
                            } else {
                                ConstraintLayout.LayoutParams.PARENT_ID
                            }
                            startToStart = value
                            endToEnd = value
                        }
                    }
                    eventButton.isVisible = state.isEventButtonVisible && !isPasting
                        && !state.isOpenLinkButtonVisible && !state.isEditLinkButtonVisible

                    openLinkButton.isVisible = state.isOpenLinkButtonVisible && !isPasting
                    editLinkButton.isVisible = state.isEditLinkButtonVisible && !isPasting
                }.traceFlow { "page $currentDate bottomButtonStates" }.collect()
            }
        }
    }

    private fun createOptionsMenu() {
        toolbar!!.inflateMenu(R.menu.fragment_edit)
        val menu = toolbar!!.menu
        menu.findItem(R.id.reminderState)

        val todayItem = menu.findItem(R.id.today)
        val indexItem = menu.findItem(R.id.index)
        val notifItem = menu.findItem(R.id.showNotification)
        val searchItem = menu.findItem(R.id.search)
        val reminderItem = menu.findItem(R.id.reminderState)

        // OK: only called once for created view
        // noinspection RepeatOnLifecycleWrongUsage
        viewLifecycleOwner.lifecycleScope.launch {
            activeLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launchReminderStateMenuFlow(reminderItem)

                launch {
                    viewModel.comparedToday.traceFlow { "page $currentDate todayItem" }.collect {
                        todayItem.isEnabled = it != 0
                    }
                }

                launch {
                    viewModel.isSearchMode
                        .traceFlow { "page $currentDate isSearchMode" }
                        .collect { isSearchMode ->
                            val iconId = if (isSearchMode) R.drawable.ic_action_search_folder else R.drawable.ic_action_search
                            val titleId = if (isSearchMode) R.string.search_everywhere else R.string.search
                            searchItem.icon = ContextCompat.getDrawable(requireContext(), iconId)
                            searchItem.title = requireContext().getString(titleId)
                        }
                }

                viewModel.preferences
                    .traceFlow { "page $currentDate menuVisibilities" }
                    .collect {
                        indexItem.isVisible = it.isIndexPageEnabled
                        notifItem.isVisible = !it.isAutostartQuickNotes
                    }
            }
        }
    }

    private fun CoroutineScope.launchReminderStateMenuFlow(reminderStateItem: MenuItem) = launch {
        val statusPresenter = requireContext().reminderStatusPresenter

        val timeTextView: TextView =
            reminderStateItem.actionView!!.findViewById(R.id.alarmTime)
        val iconView: ImageView =
            reminderStateItem.actionView!!.findViewById(R.id.icon)

        var iconAnimation: Animation? = null
        launch {
            iconView.repeatOnAttach {
                // View loses animation when detached (view pager swipe). Restore it manually.
                iconAnimation?.let { iconView.startAnimation(it) }

                // noinspection RepeatOnLifecycleWrongUsage
                viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                    statusPresenter.status
                        .traceFlow { "page $currentDate reminderFullscreenCheck" }
                        .collect { status ->
                            val isForegroundWarning = status is ReminderStatus.Upcoming
                                && status.reminder.isForeground
                                && !notifManager.canUseFullScreenIntent()

                            updateReminderIcon(iconView, isForegroundWarning)
                        }
                }
            }
        }

        statusPresenter.status.traceFlow { "page $currentDate reminderStatus" }.collect { status ->
            reminderStateItem.isVisible = status != ReminderStatus.None
            when (status) {
                ReminderStatus.None -> {
                    iconView.clearAnimation()
                    iconAnimation = null
                    return@collect
                }

                is ReminderStatus.Upcoming -> {
                    val reminderTime = status.reminder.reminderInstant
                    val time = reminderTime
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime()

                    val timeFormatter = DateTimeFormatter.ofPattern("H:mm")
                    val timeStr = time.format(timeFormatter)
                    timeTextView.text = when {
                        status.reminder.isForeground -> "$timeStr!"
                        else -> timeStr
                    }

                    reminderStateItem.actionView!!.contentDescription =
                        getString(R.string.reminder_status_next_at, formatNotificationTime(time))
                    reminderStateItem.actionView!!.setOnClickListener {
                        if (status.reminder.isForeground
                            && !notifManager.canUseFullScreenIntent()
                            && Build.VERSION.SDK_INT >= 34
                        ) {
                            showFullscreenPermissionDialog(status.reminder)
                            return@setOnClickListener
                        }
                        navigateToReminder(status.reminder)
                    }
                    reminderStateItem.actionView!!.setOnLongClickListener {
                        var text = formatReminderSnackbarText(status.reminder)
                        if (status.reminder.isForeground && !notifManager.canUseFullScreenIntent()) {
                            val noticeText = getString(R.string.reminder_fullscreen_permission_not_granted)
                            text += "\n\n$noticeText"
                        }
                        snackBar?.showText(text, 3.seconds)
                        true
                    }
                    iconView.clearAnimation()
                    iconAnimation = null
                }

                is ReminderStatus.OngoingForeground -> {
                    timeTextView.text = null
                    reminderStateItem.actionView!!.contentDescription =
                        getString(R.string.reminder_status_foreground)
                    reminderStateItem.actionView!!.setOnClickListener {
                        startActivity(AlarmActivity.createAlarmIntent(requireContext()))
                    }
                    reminderStateItem.actionView!!.setOnLongClickListener {
                        snackBar?.showText(getString(R.string.reminder_status_foreground), 3.seconds)
                        true
                    }
                    val loadAnimation = AnimationUtils.loadAnimation(
                        requireContext(), R.anim.action_alarm_clock_ringing)
                    iconAnimation = loadAnimation
                    iconView.startAnimation(loadAnimation)
                }
            }
        }
    }

    private fun updateReminderIcon(iconView: ImageView, isForegroundWarning: Boolean) {
        if ((iconView.getTag(R.id.action_icon_type) ?: false) != isForegroundWarning) {
            iconView.setTag(R.id.action_icon_type, isForegroundWarning)
            val icon = if (isForegroundWarning) {
                R.drawable.ic_action_alarm_warning
            } else {
                R.drawable.ic_action_alarm
            }
            iconView.setImageResource(icon)
        }
    }

    private fun navigateToReminder(reminder: ReminderParser.Reminder) {
        if (reminder.pageDate == currentDate) {
            snackBar?.showText(formatReminderSnackbarText(reminder), 1.seconds)
        }
        sendPtoPagerIntent(
            MainActivity.createPageIntent(
                requireContext(),
                reminder.pageDate,
                reminderSelection = reminder.targetTime.toLocalTime(),
                animatePageChange = true,
            )
        )
    }

    @RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
    private fun showFullscreenPermissionDialog(reminder: ReminderParser.Reminder) {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.reminder_fullscreen_permission_title)
            .setMessage(R.string.reminder_fullscreen_permission_message)
            .setNeutralButton(R.string.reminder_foreground_open_page_button) { _, _ ->
                navigateToReminder(reminder)
            }
            .setPositiveButton(R.string.settings) { _, _ ->
                startActivity(Intent(Settings.ACTION_MANAGE_APP_USE_FULL_SCREEN_INTENT).apply {
                    data = Uri.fromParts("package", requireContext().packageName, null)
                })
            }
            .show()
    }

    private fun formatReminderSnackbarText(reminder: ReminderParser.Reminder): String {
        val duration = Instant.now().until(reminder.reminderInstant, ChronoUnit.SECONDS)
        val hours = duration / 3600
        val minutes = duration % 3600 / 60
        val text = if (hours > 0) {
            getString(R.string.reminder_status_next_in_hm, hours, minutes)
        } else if (minutes > 0) {
            getString(R.string.reminder_status_next_in_m, minutes)
        } else {
            getString(R.string.reminder_status_next_in_s, duration % 60)
        }
        return text
    }

    private fun TextView.setHintVisibility(visible: Boolean, prev: Boolean) {
        if (animation == null && visible == isVisible) return
        val rtl = layoutDirection == LAYOUT_DIRECTION_RTL
        val animId = if (visible) {
            if (prev != rtl) R.anim.hints_appear_left else R.anim.hints_appear_right
        } else {
            if (prev != rtl) R.anim.hints_disappear_left else R.anim.hints_disappear_right
        }
        val anim = AnimationUtils.loadAnimation(context, animId).also {
            it.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation?) {
                    visibility = VISIBLE
                }
                override fun onAnimationEnd(animation: Animation?) {
                    if (!visible) visibility = INVISIBLE
                }
                override fun onAnimationRepeat(animation: Animation?) {}
            })
        }
        animation?.cancel()
        startAnimation(anim)
    }

    companion object {
        private const val TAG_TIME_PICKER = "timePicker"
        private const val TAG_EVENT_EDIT_FRAGMENT = "eventEdit"
        private const val TAG_LINK_EDIT_FRAGMENT = "linkEdit"
    }

}
