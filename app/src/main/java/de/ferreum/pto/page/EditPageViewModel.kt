package de.ferreum.pto.page

import androidx.annotation.DrawableRes
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.ferreum.pto.R
import de.ferreum.pto.files.PageContentService.ContentSourceTag
import de.ferreum.pto.files.PageContentService.FileStatus
import de.ferreum.pto.files.PtoPageContentServiceProvider
import de.ferreum.pto.page.content.TextType
import de.ferreum.pto.page.undo.HistoryCommand
import de.ferreum.pto.page.undo.TextSnapshot
import de.ferreum.pto.preferences.PtoPreferencesRepository
import de.ferreum.pto.preferences.TodayProvider
import de.ferreum.pto.preferences.comparedToday
import de.ferreum.pto.search.SearchInputHandler
import de.ferreum.pto.search.SearchToken
import de.ferreum.pto.util.myMapLatest
import de.ferreum.pto.util.myTransformLatest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import java.time.LocalDate
import kotlin.coroutines.CoroutineContext

class EditPageViewModel(
    private val savedStateHandle: SavedStateHandle,
    currentDate: LocalDate,
    todayProvider: TodayProvider,
    pageContentServiceProvider: PtoPageContentServiceProvider,
    private val preferencesRepository: PtoPreferencesRepository,
    computeContext: CoroutineContext,
) : ViewModel(), SearchInputHandler {

    private val searchPresenter = ContentSearchPresenter(viewModelScope, computeContext)

    private val fileContentService = pageContentServiceProvider.obtain(currentDate, viewModelScope)

    private val textTypePresenter = TextTypePresenter(
        viewModelScope,
        computeContext,
        fileContentService.currentContent
            .map { it.toStringWithSelection() },
    )
    val startEvent get() = textTypePresenter.startEvent

    val preferences get() = preferencesRepository.preferencesFlow

    val currentContent get() = fileContentService.currentContent

    val undoCounters get() = fileContentService.undoCounters
    val undoVersion get() = fileContentService.undoCounters.value.undoVersion

    val fileStatus: StateFlow<FileStatus> get() = fileContentService.fileStatus
    val isEditingAllowed get() = fileContentService.isEditingAllowed

    val searchMatches get() = searchPresenter.searchMatches

    val isSearchMode = savedStateHandle.getStateFlow(KEY_IS_SEARCH_MODE, false)

    val comparedToday = todayProvider.comparedToday(currentDate)
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(), null)

    val toolbarState: Flow<ToolbarState> = isSearchMode
        .myTransformLatest { isSearchMode ->
            if (isSearchMode) {
                emit(ToolbarState(
                    TimeButtonState.Gone,
                    isEventButtonVisible = false,
                    isOpenLinkButtonVisible = false,
                    isEditLinkButtonVisible = false,
                ))
            } else {
                emitAll(textTypePresenter.textTypes.myMapLatest { textTypes ->
                    val onTime = textTypes.any { it is TextType.Time }
                    val showTime = onTime || !textTypes.any { it !is TextType.Selection }
                    ToolbarState(
                        timeButtonState = when {
                            !showTime -> TimeButtonState.Gone
                            onTime -> TimeButtonState.Modify
                            else -> TimeButtonState.Add
                        },
                        isEventButtonVisible = textTypes.any { it is TextType.Event },
                        isOpenLinkButtonVisible = textTypes.any { it is TextType.Link || it is TextType.Date },
                        isEditLinkButtonVisible = textTypes.any { it is TextType.Link }
                    )
                })
            }
        }

    init {
        viewModelScope.launch {
            fileContentService.currentContent.collect { content ->
                searchPresenter.onTextChanged(content.text)
            }
        }
        viewModelScope.launch {
            isSearchMode.collect { isSearchMode ->
                if (isSearchMode) {
                    val text = fileContentService.currentContent.first().text
                    ensureActive()
                    searchPresenter.enableSearch(text)
                } else {
                    searchPresenter.disableSearch()
                }
            }
        }
    }

    fun observeFileContent(coroutineScope: CoroutineScope) =
        fileContentService.observeFileContentIn(coroutineScope)

    /**
     * Called when text was changed by the user.
     * - adds the change to the undo history
     * - updates [currentContent] to reflect content with the specified [sourceTag]
     */
    fun onTextChanged(snapshot: TextSnapshot, sourceTag: ContentSourceTag, transactionTag: Int) {
        val oldVersion = fileContentService.undoCounters.value.undoVersion
        fileContentService.replaceContent(snapshot, sourceTag, transactionTag)
        if (fileContentService.undoCounters.value.undoVersion != oldVersion) {
            setSearchMode(false)
        }
    }

    fun doHistoryCommand(historyCommand: HistoryCommand) =
        fileContentService.doHistoryCommand(historyCommand)

    fun getNextTransactionTag() = fileContentService.getNextTransactionTag()

    fun closeSearchIfEmpty() {
        if (searchPresenter.isSearchInputEmpty()) {
            setSearchMode(false)
        }
    }

    fun checkForChanges() {
        fileContentService.checkForChanges()
    }

    fun triggerSave() {
        fileContentService.triggerSave()
    }

    override fun setSearchTokens(tokens: Collection<SearchToken>) {
        searchPresenter.setSearchTokens(tokens)
    }

    fun setSearchMode(searchMode: Boolean) {
        savedStateHandle[KEY_IS_SEARCH_MODE] = searchMode
    }

    fun onTimeClick() {
        textTypePresenter.onTimeClick()
    }

    fun editSelectedEvent() {
        textTypePresenter.editSelectedEvent()
    }

    fun editSelectedLink() {
        textTypePresenter.editSelectedLink()
    }

    fun openSelectedLink() {
        textTypePresenter.openSelectedLink()
    }

    fun copySelectedLinkToClipboard() {
        textTypePresenter.copySelectedLinkToClipboard()
    }

    companion object {
        private const val KEY_IS_SEARCH_MODE = "isSearchMode"
    }
}

data class ToolbarState(
    val timeButtonState: TimeButtonState,
    val isEventButtonVisible: Boolean,
    val isOpenLinkButtonVisible: Boolean,
    val isEditLinkButtonVisible: Boolean,
)

enum class TimeButtonState(val isVisible: Boolean, @DrawableRes val imageResource: Int) {
    Gone(false, 0),
    Modify(true, R.drawable.ic_action_time),
    Add(true, R.drawable.ic_action_add_time)
}
