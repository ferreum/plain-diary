package de.ferreum.pto.page

import android.os.SystemClock
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.ferreum.pto.files.PtoPageContentServiceProvider
import de.ferreum.pto.util.myTransformLatest
import de.ferreum.pto.util.traceFlow
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.plus
import java.time.LocalDate
import kotlin.coroutines.CoroutineContext

internal class FileHintsViewModel(
    pageContentServiceProvider: PtoPageContentServiceProvider,
    currentDate: LocalDate,
    computeContext: CoroutineContext,
) : ViewModel() {

    private val prevDate = currentDate.minusDays(1)
    private val nextDate = currentDate.plusDays(1)

    private var lastHide = -1L

    private val showWithDelay = MutableSharedFlow<Boolean>(
        replay = 1,
        onBufferOverflow = BufferOverflow.DROP_OLDEST,
    )

    val hints: Flow<Pair<String?, String?>> = flow {
        coroutineScope {
            val prevContentService = pageContentServiceProvider.obtain(prevDate, this)
            val nextContentService = pageContentServiceProvider.obtain(nextDate, this)
            val combined = combine(prevContentService.hints, nextContentService.hints, ::Pair)

            emitAll(
                showWithDelay
                    .myTransformLatest { withDelay ->
                        if (withDelay) {
                            emit(false)
                            delay(15000)
                        }
                        emit(true)
                    }
                    .distinctUntilChanged()
                    .myTransformLatest { wantHints ->
                        if (wantHints) {
                            coroutineScope {
                                prevContentService.observeFileContentIn(this)
                                nextContentService.observeFileContentIn(this)
                                emitAll(combined)
                            }
                        } else {
                            emit(Pair(null, null))
                        }
                    }
                    .traceFlow { "adjacentHints $currentDate" }
            )
        }
    }.stateIn(
        viewModelScope + computeContext,
        SharingStarted.WhileSubscribed(5000, 0),
        Pair(null, null)
    )

    fun showHints() {
        lastHide = -1
        showWithDelay.tryEmit(false)
    }

    fun triggerHide() {
        val now = SystemClock.uptimeMillis()
        if (lastHide < 0 || now - lastHide > 1000) {
            lastHide = now
            showWithDelay.tryEmit(true)
        }
    }

}
