package de.ferreum.pto.page

import android.text.Spannable
import android.text.Spanned
import android.text.style.BackgroundColorSpan
import android.widget.TextView
import de.ferreum.pto.search.MatchRange

class MatchHighlighter {

    private val highlightSpans = mutableListOf<Any?>()

    private fun getHighlightSpan(index: Int): Any? {
        while (index >= highlightSpans.size) {
            highlightSpans.add(BackgroundColorSpan(HIGHLIGHT_COLOR))
        }
        return highlightSpans[index]
    }

    fun highlight(textView: TextView, matches: List<MatchRange>) {
        val textLength = textView.text.length
        val text = textView.text
        val spannable = if (text is Spannable) {
            text.getSpans(0, text.length, BackgroundColorSpan::class.java).forEach {
                text.removeSpan(it)
            }
            text
        } else {
            textView.setText(text, TextView.BufferType.EDITABLE)
            textView.text as Spannable
        }
        for ((index, match) in matches.withIndex()) {
            val start = match.start.coerceIn(0, textLength)
            val end = match.end.coerceIn(0, textLength)
            if (start < end) {
                spannable.setSpan(getHighlightSpan(index), start, end,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }
    }

    companion object {
        private const val HIGHLIGHT_COLOR = 0x6055ff55
    }

}
