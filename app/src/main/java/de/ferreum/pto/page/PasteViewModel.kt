package de.ferreum.pto.page

import android.content.Intent
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

internal class PasteViewModel(
    private val handle: SavedStateHandle,
) : ViewModel() {

    private val pasteIntent = handle.getStateFlow<Intent?>(STATE_PASTE_INTENT, null)
    val isPasting: Flow<Boolean> = pasteIntent.map { it != null }

    fun getAndRemovePasteIntent() =
        pasteIntent.value.also { handle[STATE_PASTE_INTENT] = null }

    fun setPasteIntent(intent: Intent) {
        handle[STATE_PASTE_INTENT] = intent
    }

    companion object {
        private const val STATE_PASTE_INTENT = "de.ferreum.pto.page.PASTE_INTENT"
    }

}
