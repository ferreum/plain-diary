package de.ferreum.pto.page

interface PreemptiveFocusHandler {
    fun onPreemptiveFocusChanged(focus: Focus)

    enum class Focus { PEEK, FOCUSED, UNFOCUSED }
}
