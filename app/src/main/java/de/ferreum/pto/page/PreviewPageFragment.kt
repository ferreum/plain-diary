package de.ferreum.pto.page

import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.Selection
import android.text.method.KeyListener
import android.view.ActionMode
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isEmpty
import androidx.core.view.updateLayoutParams
import androidx.core.widget.NestedScrollView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import de.ferreum.pto.MainActivity
import de.ferreum.pto.R
import de.ferreum.pto.page.PtoPageContract.EXTRA_EPOCH_DAY
import de.ferreum.pto.page.PtoPageContract.EXTRA_SEARCH_INPUT
import de.ferreum.pto.page.PtoPageContract.EXTRA_TEXT_SELECTION
import de.ferreum.pto.page.PtoPageContract.EXTRA_TEXT_SELECTION_END
import de.ferreum.pto.page.PtoPageContract.sendPtoPagerIntent
import de.ferreum.pto.page.PtoPageContract.toPageTitle
import de.ferreum.pto.preferencesRepository
import de.ferreum.pto.search.SearchInputHelper
import de.ferreum.pto.search.setupClearButton
import de.ferreum.pto.util.getWantedSelectionScrollPosition
import de.ferreum.pto.util.setupSoftInputHidden
import de.ferreum.pto.widget.CustomSnackBar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import java.time.LocalDate

class PreviewPageFragment : Fragment(R.layout.fragment_page_preview) {

    private val viewModel: PreviewPageViewModel by viewModels {
        ViewModelFactory(this, currentDate = currentDate)
    }

    private var toolbar: Toolbar? = null
    private var textView: TextView? = null

    private lateinit var currentDate: LocalDate
    private var startOffset = -1
    private var endOffset = -1
    private var isSettingText = false
    private var needsScrollToCursor = false

    private var searchInputHelper: SearchInputHelper? = null

    private lateinit var startedScope: CoroutineScope

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val epochDay = arguments?.getLong(EXTRA_EPOCH_DAY, Long.MIN_VALUE) ?: Long.MIN_VALUE
        currentDate = if (epochDay != Long.MIN_VALUE) {
            LocalDate.ofEpochDay(epochDay)
        } else {
            LocalDate.now()
        }
        startOffset = arguments?.getInt(EXTRA_TEXT_SELECTION, -1) ?: -1
        endOffset = arguments?.getInt(EXTRA_TEXT_SELECTION_END, -1) ?: -1
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val scrollView: NestedScrollView = view.findViewById(R.id.scrollView)
        val toolbarContainer: View = view.findViewById(R.id.toolbarContainer)
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        val textView: TextView = view.findViewById(R.id.textView)
        val searchInput: EditText = view.findViewById(R.id.searchInput)
        val searchInputClearButton: View = view.findViewById(R.id.searchInputClearButton)
        val snackBar: CustomSnackBar = view.findViewById(R.id.snackBar)
        val bottomToolbar: View = view.findViewById(R.id.bottomToolbar)
        this.toolbar = toolbar
        this.textView = textView

        ViewCompat.setOnApplyWindowInsetsListener(view) { _, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or WindowInsetsCompat.Type.displayCutout()
                    or WindowInsetsCompat.Type.ime()
            )
            toolbarContainer.setPadding(insets.left, insets.top, insets.right, 0)
            scrollView.setPadding(insets.left, 0, insets.right, 0)
            snackBar.updateLayoutParams<MarginLayoutParams> {
                leftMargin = insets.left
                rightMargin = insets.right
            }
            bottomToolbar.setPadding(insets.left, 0, insets.right, insets.bottom)
            windowInsets.inset(insets)
        }

        setupReadonlyText(textView)

        requireContext().preferencesRepository.preferencesFlow
            .map { it.isIsoDateFormatEnabled }
            .distinctUntilChanged()
            .onEach { toolbar.title = currentDate.toPageTitle(requireContext(), it) }
            .launchIn(viewLifecycleOwner.lifecycleScope)

        scrollView.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            if (needsScrollToCursor) {
                needsScrollToCursor = false
                scrollView.getWantedSelectionScrollPosition(textView)
            }
        }
        scrollView.isSmoothScrollingEnabled = false
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.fileContent.collect { text ->
                    isSettingText = true
                    textView.setText(text, TextView.BufferType.EDITABLE)
                    isSettingText = false
                    textView.isCursorVisible = true
                    val textLength = textView.text.length
                    val start = startOffset.coerceIn(0, textLength)
                    val end = endOffset.coerceIn(start, textLength)
                    Selection.setSelection(textView.editableText, start, end)
                    if (scrollView.isLayoutRequested) {
                        needsScrollToCursor = true
                    }
                }
            }
        }

        val searchInputHelper = SearchInputHelper(viewLifecycleOwner,
            searchInput,
            viewModel,
            onMessage = { snackBar.showText(it) },
        )
        this.searchInputHelper = searchInputHelper
        searchInputHelper.start()
        searchInput.setupClearButton(searchInputClearButton)
        textView.doAfterTextChanged {
            viewModel.onTextChanged(it ?: "")
        }
        viewModel.start(textView.text)
        val matchHighlighter = MatchHighlighter()
        viewModel.searchMatches
            .onEach { matches ->
                matchHighlighter.highlight(textView, matches)
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                requireContext().preferencesRepository.preferencesFlow
                    .distinctUntilChangedBy { it.editorFontSize }
                    .collect { textView.textSize = it.editorFontSize.toFloat() }
            }
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState == null) {
            arguments?.getCharSequence(EXTRA_SEARCH_INPUT)?.let {
                searchInputHelper!!.setSearchInput(it)
            }
        }
        searchInputHelper!!.onViewStateRestored()
    }

    override fun onStart() {
        super.onStart()
        startedScope = CoroutineScope(Job(viewLifecycleOwner.lifecycleScope.coroutineContext.job))
        viewModel.observeFileContentIn(startedScope)
    }

    override fun onResume() {
        super.onResume()
        if (toolbar!!.menu.isEmpty()) {
            createOptionsMenu()
        }
        setupSoftInputHidden(textView!!)
    }

    override fun onStop() {
        startedScope.cancel()
        super.onStop()
    }

    override fun onDestroyView() {
        searchInputHelper = null
        super.onDestroyView()
    }

    private fun handleOptionsItemClick(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.editAction -> startEditor()
        }
        return true
    }

    private fun createOptionsMenu() {
        toolbar!!.setOnMenuItemClickListener(this::handleOptionsItemClick)
        toolbar!!.setNavigationOnClickListener { parentFragmentManager.popBackStack() }
        toolbar!!.inflateMenu(R.menu.fragment_page_preview)
    }

    private fun prepareActionMode(menu: Menu) {
        // remove editing entries
        menu.findItem(android.R.id.cut)?.isVisible = false
        menu.findItem(android.R.id.paste)?.isVisible = false
        menu.findItem(android.R.id.pasteAsPlainText)?.isVisible = false
        if (menu.findItem(R.id.openInEditor) == null) {
            menu.add(Menu.NONE, R.id.openInEditor, 1, R.string.action_edit).apply {
                setIcon(R.drawable.ic_action_edit)
                setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
            }
        }
    }

    private fun onActionItemClicked(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.openInEditor) {
            startEditor()
            return true
        }
        return false
    }

    private fun startEditor() {
        val selStart = Selection.getSelectionStart(textView!!.text)
        val selEnd = Selection.getSelectionEnd(textView!!.text)
        sendPtoPagerIntent(
            MainActivity.createPageIntent(
                requireContext(),
                currentDate,
                selection = selStart..<selEnd,
                searchInput = searchInputHelper?.sanitizedSearchInputText,
            )
        )
    }

    private fun setupReadonlyText(textView: TextView) {
        textView.keyListener = object : KeyListener {
            override fun getInputType(): Int = 0

            override fun onKeyDown(view: View?, text: Editable?, keyCode: Int, event: KeyEvent?): Boolean {
                return false
            }

            override fun onKeyUp(view: View?, text: Editable?, keyCode: Int, event: KeyEvent?): Boolean {
                return false
            }

            override fun onKeyOther(view: View?, text: Editable?, event: KeyEvent?): Boolean {
                return false
            }

            override fun clearMetaKeyState(view: View?, content: Editable?, states: Int) {
            }
        }
        textView.showSoftInputOnFocus = false
        textView.setTextIsSelectable(true)
        textView.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE
        textView.filters = arrayOf(InputFilter { _, _, _, dest, dstart, dend ->
            if (isSettingText) null else dest.subSequence(dstart, dend)
        })
        textView.customInsertionActionModeCallback = object : ActionMode.Callback {
            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return true
            }

            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                menu ?: return true
                prepareActionMode(menu)
                return true
            }

            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                return this@PreviewPageFragment.onActionItemClicked(item)
            }

            override fun onDestroyActionMode(mode: ActionMode?) {
            }
        }
        textView.customSelectionActionModeCallback = object : ActionMode.Callback {
            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return true
            }

            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                menu ?: return true
                prepareActionMode(menu)
                return true
            }

            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                return this@PreviewPageFragment.onActionItemClicked(item)
            }

            override fun onDestroyActionMode(mode: ActionMode?) {
            }
        }
    }

}
