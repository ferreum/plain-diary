package de.ferreum.pto.page

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.ferreum.pto.files.PtoPageContentServiceProvider
import de.ferreum.pto.search.SearchInputHandler
import de.ferreum.pto.search.SearchToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import java.time.LocalDate
import kotlin.coroutines.CoroutineContext

class PreviewPageViewModel(
    currentDate: LocalDate,
    pageContentServiceProvider: PtoPageContentServiceProvider,
    computeContext: CoroutineContext,
) : ViewModel(), SearchInputHandler {

    private val searchPresenter = ContentSearchPresenter(viewModelScope, computeContext)

    private val pageContentService = pageContentServiceProvider.obtain(currentDate, viewModelScope)

    private val _loadingError = MutableStateFlow<Throwable?>(null)
    val loadingError: StateFlow<Throwable?> get() = _loadingError

    val fileContent: Flow<String> = pageContentService.currentContent.map { it.text }

    val searchMatches get() = searchPresenter.searchMatches

    fun observeFileContentIn(scope: CoroutineScope) {
        pageContentService.observeFileContentIn(scope)
    }

    override fun setSearchTokens(tokens: Collection<SearchToken>) {
        searchPresenter.setSearchTokens(tokens)
    }

    fun start(text: CharSequence) {
        searchPresenter.enableSearch(text)
    }

    fun onTextChanged(text: CharSequence) {
        searchPresenter.onTextChanged(text)
    }

}
