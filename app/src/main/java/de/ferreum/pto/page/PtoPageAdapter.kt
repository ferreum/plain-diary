package de.ferreum.pto.page

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import de.ferreum.pto.Constants.MAX_EPOCH_DAY
import de.ferreum.pto.Constants.MIN_EPOCH_DAY
import de.ferreum.pto.Constants.PAGE_DATE_MIN
import java.time.LocalDate

class PtoPageAdapter(
    private val fragmentManager: FragmentManager,
    lifecycle: Lifecycle
) : FragmentStateAdapter(fragmentManager, lifecycle) {
    var isEnabled = false
        set(value) {
            if (value != field) {
                field = value
                notifyDataSetChanged()
            }
        }

    override fun getItemCount(): Int {
        return if (isEnabled) (MAX_EPOCH_DAY - MIN_EPOCH_DAY + 1).toInt() else 0
    }

    override fun getItemId(position: Int): Long {
        return positionToItemId(position)
    }

    override fun createFragment(position: Int): Fragment {
        return createEditPage(positionToDate(position))
    }

    private fun createEditPage(date: LocalDate): Fragment {
        return EditPageFragment().apply {
            arguments = Bundle().apply {
                putLong(PtoPageContract.EXTRA_EPOCH_DAY, date.toEpochDay())
            }
        }
    }

    fun dateToPosition(localDate: LocalDate): Int {
        return (localDate.toEpochDay() - MIN_EPOCH_DAY).toInt()
    }

    fun positionToDate(position: Int): LocalDate {
        return PAGE_DATE_MIN.plusDays(position.toLong())
    }

    fun findFragmentByPosition(position: Int): Fragment? {
        return findFragmentByItemId(positionToItemId(position))
    }

    fun findFragmentByDate(date: LocalDate): Fragment? {
        return findFragmentByItemId(epochDayToItemId(date.toEpochDay()))
    }

    fun findFragmentByEpochDay(epochDay: Int): Fragment? {
        return findFragmentByItemId(epochDayToItemId(epochDay.toLong()))
    }

    /**
     * Get the [epoch day][LocalDate.ofEpochDay] for the fragment. Returns
     * [Integer.MIN_VALUE] if the fragment is not a page fragment.
     */
    fun getEpochDayForFragment(fragment: Fragment?): Long {
        if (fragment == null) return Int.MIN_VALUE.toLong()

        val tag = fragment.tag

        if (tag == null || !tag.startsWith("f")) return Int.MIN_VALUE.toLong()

        return try {
            tag.substring(1).toLong()
        } catch (e: NumberFormatException) {
            Int.MIN_VALUE.toLong()
        }
    }

    private fun findFragmentByItemId(id: Long): Fragment? {
        // Tag "f" + itemId is specified by base adapter.
        return fragmentManager.findFragmentByTag("f$id")
    }

    private fun positionToItemId(position: Int): Long {
        return epochDayToItemId(positionToEpochDay(position))
    }

    private fun positionToEpochDay(position: Int): Long {
        return position + MIN_EPOCH_DAY
    }

    private fun epochDayToItemId(epochDay: Long): Long {
        return epochDay
    }
}
