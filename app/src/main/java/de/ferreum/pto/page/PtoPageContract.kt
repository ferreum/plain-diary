package de.ferreum.pto.page

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.fragment.app.Fragment
import de.ferreum.pto.MainPtoNavigation
import de.ferreum.pto.preferences.PtoPreferences
import de.ferreum.pto.util.PAGE_ISO_DATE_FORMATTER
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

object PtoPageContract {

    const val ACTION_SHOW_PAGE = "de.ferreum.pto.ACTION_SHOW_PAGE"

    const val EXTRA_EPOCH_DAY = "de.ferreum.pto.EPOCH_DAY"
    const val EXTRA_TEXT_SELECTION = "de.ferreum.pto.TEXT_SELECTION"
    const val EXTRA_TEXT_SELECTION_END = "de.ferreum.pto.TEXT_SELECTION_END"
    const val EXTRA_SEARCH_INPUT = "de.ferreum.pto.SEARCH_INPUT"

    /** [Serializable] LocalTime extra of reminder to select */
    const val EXTRA_REMINDER_TEXT_SELECTION = "de.ferreum.pto.REMINDER_TEXT_SELECTION"

    /** [Boolean] extra indicating smooth page change */
    const val EXTRA_ANIMATE_PAGE_CHANGE = "de.ferreum.pto.ANIMATE_PAGE_CHANGE"

    fun Bundle?.getPageDateFromArguments(): LocalDate {
        val epochDay = this?.getLong(EXTRA_EPOCH_DAY, -1L) ?: -1L
        if (epochDay == -1L) return LocalDate.now()
        return LocalDate.ofEpochDay(epochDay)
    }

    @JvmStatic
    fun isMediaIntent(intent: Intent): Boolean {
        return Intent.ACTION_SEND == intent.action
            || Intent.ACTION_VIEW == intent.action
            || Intent.ACTION_SEND_MULTIPLE == intent.action
    }

    @JvmStatic
    fun LocalDate.toPageTitle(context: Context, useIsoFormat: Boolean): String {
        val config = context.resources.configuration
        val formatter = if (useIsoFormat) {
            PAGE_ISO_DATE_FORMATTER
        } else {
            val isSmall = config.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK == Configuration.SCREENLAYOUT_SIZE_SMALL
                || config.orientation != Configuration.ORIENTATION_LANDSCAPE
            val format = if (isSmall) FormatStyle.MEDIUM else FormatStyle.FULL
            DateTimeFormatter.ofLocalizedDate(format)
        }
        return format(formatter)
    }

    fun Fragment.navigateToIndexPage(preferences: PtoPreferences) {
        navigateToDate(preferences.indexPageDate)
    }

    fun Fragment.navigateToDate(date: LocalDate) {
        (parentFragment as PtoPagerContainer).goToPage(date)
    }

    fun Fragment.showSearch(searchInput: CharSequence? = null) {
        (requireActivity() as MainPtoNavigation).showSearch(searchInput)
    }

    fun Fragment.sendPtoPagerIntent(pagerIntent: Intent) {
        (requireActivity() as MainPtoNavigation).sendPtoPagerIntent(pagerIntent)
    }

    interface PageIntentHandler {
        fun onPageIntent(intent: Intent)
    }

}
