package de.ferreum.pto.page

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import de.ferreum.pto.util.showSoftInput

internal class PtoPageEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
) : AppCompatEditText(context, attrs) {

    /**
     * Whether this EditText should request soft input at the next possible time.
     */
    var wantsSoftInput = false
        set(value) {
            field = value
            if (value) {
                checkShowSoftInput()
            }
        }

    private var checkScheduled = false
    private val checkRunnable = Runnable {
        checkScheduled = false
        if (wantsSoftInput && isAttachedToWindow && hasWindowFocus() && isFocused) {
            wantsSoftInput = false
            showSoftInput()
        }
    }

    override fun getAutofillType(): Int {
        return AUTOFILL_TYPE_NONE
    }

    override fun onWindowFocusChanged(hasWindowFocus: Boolean) {
        super.onWindowFocusChanged(hasWindowFocus)
        if (hasWindowFocus) {
            checkShowSoftInput()
        }
    }

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        if (focused) {
            checkShowSoftInput()
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        checkShowSoftInput()
    }

    private fun checkShowSoftInput() {
        if (!checkScheduled && wantsSoftInput) {
            checkScheduled = true
            postDelayed(checkRunnable, 50L)
        }
    }

}
