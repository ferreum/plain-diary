package de.ferreum.pto.page

import java.time.LocalDate

interface PtoPagerContainer {
    fun goToPage(date: LocalDate)

    fun showPageDatePicker(date: LocalDate)
}
