package de.ferreum.pto.page

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.withStarted
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import de.ferreum.pto.R
import de.ferreum.pto.page.DatePickerDialogFragment.DateSelectionCallback
import de.ferreum.pto.page.PreemptiveFocusHandler.Focus
import de.ferreum.pto.page.PtoPageContract.EXTRA_ANIMATE_PAGE_CHANGE
import de.ferreum.pto.page.PtoPageContract.EXTRA_EPOCH_DAY
import de.ferreum.pto.page.PtoPageContract.PageIntentHandler
import de.ferreum.pto.preferences.PtoPreferencesRepository
import de.ferreum.pto.preferences.TodayProvider
import de.ferreum.pto.preferences.todayDate
import de.ferreum.pto.preferencesRepository
import de.ferreum.pto.todayProvider
import de.ferreum.pto.util.getTypedParcelable
import de.ferreum.pto.util.viewpager.ScrollToPositionFix
import de.ferreum.pto.util.viewpager.currentItemFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import java.time.LocalDate

class PtoPagerFragment : Fragment(R.layout.fragment_pager),
    PtoPagerContainer, PageIntentHandler, DateSelectionCallback {

    private var viewPager: ViewPager2? = null
    private var pageAdapter: PtoPageAdapter? = null

    private var isPositionInitialized = false
    private var pageIntent: Intent? = null

    private lateinit var preferencesRepository: PtoPreferencesRepository
    private lateinit var todayProvider: TodayProvider

    override fun onAttach(context: Context) {
        preferencesRepository = context.preferencesRepository
        todayProvider = context.todayProvider
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isPositionInitialized = savedInstanceState?.getBoolean(SAVE_IS_POSITION_INITIALIZED) ?: false
        pageIntent = savedInstanceState?.getTypedParcelable(SAVE_PAGE_INTENT)

        if (!isPositionInitialized) {
            lifecycleScope.launch {
                val startupDate = preferencesRepository.preferencesFlow.first().todayDate
                lifecycle.withStarted {
                    pageAdapter!!.isEnabled = true
                    viewPager!!.setCurrentItem(pageAdapter!!.dateToPosition(startupDate), false)
                    isPositionInitialized = true
                    onPagerInitialized()
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewPager: ViewPager2 = view.findViewById(R.id.viewPager)
        val pageAdapter = PtoPageAdapter(childFragmentManager, viewLifecycleOwner.lifecycle)
        this.viewPager = viewPager
        this.pageAdapter = pageAdapter

        pageAdapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        pageAdapter.isEnabled = isPositionInitialized
        viewPager.adapter = pageAdapter
        viewPager.registerOnPageChangeCallback(PreemptiveFocusNotifier(viewPager))
        ScrollToPositionFix.createFor(viewPager, viewLifecycleOwner.lifecycleScope)

        childFragmentManager.registerFragmentLifecycleCallbacks(object : FragmentManager.FragmentLifecycleCallbacks() {
            override fun onFragmentResumed(fm: FragmentManager, fragment: Fragment) {
                tryDeliverPageIntent(fragment)
            }
        }, false)

        val todayBackPressCallback = requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            viewLifecycleOwner.lifecycleScope.launch {
                goToPage(preferencesRepository.preferencesFlow.first().todayDate)
            }
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                todayProvider.todayFlow.collectLatest { today ->
                    val todayPosition = pageAdapter.dateToPosition(today)
                    viewPager.currentItemFlow.collect { position ->
                        todayBackPressCallback.isEnabled = position != todayPosition
                    }
                }
            }
        }

        if (isPositionInitialized) {
            pageAdapter.isEnabled = true
            onPagerInitialized()
        }
    }

    private fun onPagerInitialized() {
        pageIntent?.let { tryHandlePageIntent(it) }
    }

    override fun onDestroyView() {
        viewPager = null
        pageAdapter = null
        super.onDestroyView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(SAVE_IS_POSITION_INITIALIZED, isPositionInitialized)
        outState.putParcelable(SAVE_PAGE_INTENT, pageIntent)
    }

    override fun onPageIntent(intent: Intent) {
        tryHandlePageIntent(intent)
    }

    override fun onDateSelected(date: LocalDate) {
        goToPage(date)
    }

    override fun onDateSelectionCanceled() {
    }

    override fun goToPage(date: LocalDate) {
        if (isPositionInitialized) {
            viewPager?.currentItem = pageAdapter!!.dateToPosition(date)
        }
    }

    override fun showPageDatePicker(date: LocalDate) {
        lifecycleScope.launch {
            lifecycle.withStarted {
                // Don't show two at the same time
                if (childFragmentManager.findFragmentByTag(TAG_DATE_PICKER) == null) {
                    val fragment = DatePickerDialogFragment()
                    val args = Bundle()
                    args.putLong(DatePickerDialogFragment.EXTRA_EPOCH_DAY, date.toEpochDay())
                    fragment.arguments = args
                    fragment.show(childFragmentManager, TAG_DATE_PICKER)
                }
            }
        }
    }

    private fun tryHandlePageIntent(intent: Intent) {
        if (intent.action == PtoPageContract.ACTION_SHOW_PAGE) {
            val epochDay = intent.getLongExtra(EXTRA_EPOCH_DAY, Int.MIN_VALUE.toLong())
            if (epochDay >= Int.MIN_VALUE) {
                pageIntent = intent
                if (isPositionInitialized) {
                    val date = LocalDate.ofEpochDay(epochDay)
                    val position = pageAdapter!!.dateToPosition(date)
                    val smooth = intent.getBooleanExtra(EXTRA_ANIMATE_PAGE_CHANGE, false)
                    viewPager!!.setCurrentItem(position, smooth)
                    val f = pageAdapter!!.findFragmentByPosition(position)
                    if (f?.isResumed == true) {
                        tryDeliverPageIntent(f)
                    }
                }
                return
            }
        }
        pageIntent = null
    }

    private fun tryDeliverPageIntent(fragment: Fragment) {
        pageIntent?.let { pageIntent ->
            val epochDay = pageIntent.getLongExtra(EXTRA_EPOCH_DAY, 0)
            val fragmentDay = pageAdapter!!.getEpochDayForFragment(fragment)
            if (fragmentDay == epochDay) {
                if (fragment is PageIntentHandler) {
                    (fragment as PageIntentHandler).onPageIntent(pageIntent)
                }
                this.pageIntent = null
            }
        }
    }

    private fun notifyPreemptiveFocus(position: Int, focus: Focus): Boolean {
        val fragment = pageAdapter!!.findFragmentByPosition(position)
        if (fragment is PreemptiveFocusHandler) {
            (fragment as PreemptiveFocusHandler).onPreemptiveFocusChanged(focus)
            return true
        }
        return false
    }

    inner class PreemptiveFocusNotifier(private val viewPager: ViewPager2) : OnPageChangeCallback() {
        private var scrollPosition = -1
        private var scrollPositionOffset = 0f

        private var focusedPosition = -1
        private var peekPosition = -1
        private var currentPosition = -1

        private fun trySetFocus(selectedPosition: Int, peekPosition: Int) {
            if (selectedPosition != focusedPosition) {
                if (focusedPosition != -1 && focusedPosition != peekPosition) {
                    notifyPreemptiveFocus(focusedPosition, Focus.UNFOCUSED)
                    focusedPosition = -1
                }
                if (selectedPosition != -1 && isVisible(selectedPosition)) {
                    if (notifyPreemptiveFocus(selectedPosition, Focus.FOCUSED)) {
                        focusedPosition = selectedPosition
                    }
                }
            }
            if (peekPosition != this.peekPosition) {
                if (this.peekPosition != -1 && this.peekPosition != selectedPosition) {
                    notifyPreemptiveFocus(this.peekPosition, Focus.UNFOCUSED)
                    this.peekPosition = -1
                }
                if (peekPosition != -1) {
                    if (notifyPreemptiveFocus(peekPosition, Focus.PEEK)) {
                        this.peekPosition = peekPosition
                    }
                }
            }
        }

        override fun onPageSelected(position: Int) {
            if (focusedPosition == position) {
                // ViewPager2 calls clearFocus before onPageSelected. Clear our state so we notify
                // the selected fragment again.
                focusedPosition = -1
            }
            trySetFocus(position, -1)
            currentPosition = position
        }

        override fun onPageScrollStateChanged(state: Int) {
            if (state == ViewPager2.SCROLL_STATE_IDLE) {
                trySetFocus(currentPosition, -1)
            }
        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            scrollPosition = position
            scrollPositionOffset = positionOffset
            if (viewPager.scrollState != ViewPager2.SCROLL_STATE_DRAGGING) {
                trySetFocus(currentPosition, -1)
                return
            }
            val isNext = positionOffset > 0.5
            val selectedPosition = if (isNext) position + 1 else position
            val peekPosition = if (isNext) position else position + 1
            trySetFocus(selectedPosition, peekPosition)
        }

        private fun isVisible(position: Int): Boolean {
            if (scrollPosition < 0) return false
            if (position == scrollPosition) return true
            return scrollPositionOffset > 0.05 && position == scrollPosition + 1
        }
    }

    companion object {
        private const val SAVE_IS_POSITION_INITIALIZED = "isPositionInitialized"
        private const val SAVE_PAGE_INTENT = "pageIntent"

        private const val TAG_DATE_PICKER = "datePicker"
    }

}
