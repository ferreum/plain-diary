package de.ferreum.pto.page

import android.content.Context
import android.graphics.Rect
import android.os.Build
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import de.ferreum.pto.R

class ScrollBlockingLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(
    context,
    attrs,
    defStyleAttr
) {
    private var toolbar: View? = null
    private var bottomToolbar: View? = null

    private val gestureExclusionRect = listOf(Rect())

    var onTouchDownListener: () -> Unit = {}

    override fun addView(child: View, index: Int, params: ViewGroup.LayoutParams) {
        super.addView(child, index, params)
        if (child.id == R.id.toolbarContainer) {
            toolbar = child
        } else if (child.id == R.id.bottomToolbar) {
            bottomToolbar = child
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            toolbar?.let {
                // For gesture navigation on Android SDK 29: prevent accidentally performing a "back"
                // edge-gesture when trying to swipe Toolbar too close to the edge.
                it.getHitRect(gestureExclusionRect[0])
                systemGestureExclusionRects = gestureExclusionRect
            }
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (ev.actionMasked == MotionEvent.ACTION_DOWN) {
            onTouchDownListener.invoke()
            if (!isInView(toolbar, ev) && !isInView(bottomToolbar, ev)) {
                parent.requestDisallowInterceptTouchEvent(true)
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun isInView(view: View?, ev: MotionEvent): Boolean {
        val x = ev.getX(0)
        val y = ev.getY(0)
        return view != null && x > view.left && x < view.right && y > view.top && y < view.bottom
    }
}
