package de.ferreum.pto.page

import android.os.Looper
import android.text.Editable
import android.text.Selection
import android.text.SpanWatcher
import android.text.Spannable
import android.text.Spanned
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TextView
import androidx.core.os.HandlerCompat
import de.ferreum.pto.page.undo.TextSnapshot
import de.ferreum.pto.page.undo.UndoHistory
import de.ferreum.pto.page.undo.UndoHistory.Companion.NO_CHANGE_ID
import kotlinx.coroutines.Runnable

class TextSnapshotWatcher(
    private val textView: TextView,
    private val changeListener: (TextSnapshot, transactionTag: Int) -> Unit,
) : SpanWatcher, TextWatcher {

    var transactionTag: Int = UndoHistory.NO_TRANSACTION_TAG

    /** Flag for not reacting to events caused by programmatic changes */
    var isBlockingTextChangeEvents = false

    private var lastSelectionStart = -1
    private var lastSelectionEnd = -1
    private var currentSelectionStart = -1
    private var currentSelectionEnd = -1
    private var lastTransactionTag = UndoHistory.NO_TRANSACTION_TAG
    private var lastChangeId = NO_CHANGE_ID

    private var lastTextString: String? = null

    private val handler = HandlerCompat.createAsync(Looper.getMainLooper())

    private val updateRunnable = Runnable { performUpdate() }
    private var isUpdateScheduled = false

    override fun onSpanAdded(text: Spannable?, what: Any?, start: Int, end: Int) {
        when (what) {
            Selection.SELECTION_START ->
                currentSelectionStart = start
            Selection.SELECTION_END ->
                currentSelectionEnd = start
            else -> return
        }
        scheduleUpdate()
    }

    override fun onSpanRemoved(text: Spannable?, what: Any?, start: Int, end: Int) {
        when (what) {
            Selection.SELECTION_START ->
                currentSelectionStart = -1
            Selection.SELECTION_END ->
                currentSelectionEnd = -1
            else -> return
        }
        scheduleUpdate()
    }

    override fun onSpanChanged(text: Spannable?, what: Any?, ostart: Int, oend: Int, nstart: Int, nend: Int) {
        when (what) {
            Selection.SELECTION_START ->
                currentSelectionStart = nstart
            Selection.SELECTION_END ->
                currentSelectionEnd = nstart
            else -> return
        }
        scheduleUpdate()
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable) {
        lastTextString = null
        scheduleUpdate()
    }

    @PublishedApi
    internal fun scheduleUpdate() {
        lastTransactionTag = transactionTag
        if (isUpdateScheduled || isBlockingTextChangeEvents) {
            return
        }
        isUpdateScheduled = true
        handler.post(updateRunnable)
    }

    private fun performUpdate() {
        isUpdateScheduled = false
        val text = textView.text
        val start = currentSelectionStart
            .let { if (it >= 0) it else Selection.getSelectionStart(text) }
        val end = currentSelectionEnd
            .let { if (it >= 0) it else Selection.getSelectionEnd(text) }
        val textString = lastTextString ?: text.toString()
        check(start <= textString.length) { "start=$start length=${textString.length}" }
        check(end <= textString.length) { "end=$end length=${textString.length}" }
        changeListener(
            TextSnapshot(
                textString,
                start,
                end,
                lastSelectionStart,
                lastSelectionEnd,
                lastChangeId
            ),
            lastTransactionTag,
        )
        lastChangeId = NO_CHANGE_ID
        lastTextString = textString
        lastSelectionStart = start
        lastSelectionEnd = end
    }

    fun setup(editText: EditText) {
        val text = editText.text
        text.setSpan(this, 0, text.length, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        handler.removeCallbacks(updateRunnable)
    }

    inline fun withoutTextChangeEvents(block: () -> Unit) {
        isBlockingTextChangeEvents = true
        try {
            block()
        } finally {
            isBlockingTextChangeEvents = false
        }
    }

    fun notifyContentReplaced(textString: String, selectionStart: Int, selectionEnd: Int, changeId: Int) {
        require(selectionStart >= -1)
        require(selectionEnd >= -1)
        handler.removeCallbacks(updateRunnable)
        lastTextString = textString
        currentSelectionStart = selectionStart
        currentSelectionEnd = selectionEnd
        lastSelectionStart = selectionStart
        lastSelectionEnd = selectionEnd
        lastChangeId = changeId
    }

}
