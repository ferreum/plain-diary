package de.ferreum.pto.page

import android.net.Uri
import de.ferreum.pto.files.PageContentService
import de.ferreum.pto.page.content.LinkType
import de.ferreum.pto.page.content.TextType
import de.ferreum.pto.page.content.TextTypeDetector
import de.ferreum.pto.page.content.TextTypeDetector.Companion.toTime
import de.ferreum.pto.util.Event
import de.ferreum.pto.util.getDateFromUri
import de.ferreum.pto.util.myMapLatest
import de.ferreum.pto.util.throttle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import kotlinx.coroutines.withContext
import java.time.LocalDate
import kotlin.coroutines.CoroutineContext

class TextTypePresenter(
    viewModelScope: CoroutineScope,
    private val computeContext: CoroutineContext,
    private val content: Flow<StringWithSelection>,
) {
    private val scope = viewModelScope + computeContext

    private var textTypeDetector = TextTypeDetector()

    val textTypes: Flow<List<TextType>> = content
        .throttle(300)
        .myMapLatest { classifyContent(it) }
        .stateIn(scope, SharingStarted.WhileSubscribed(5000, 0), emptyList())

    private var navigationJob: Job? = null

    private val _startEvent = MutableSharedFlow<Event<StartEvent>>(
        onBufferOverflow = BufferOverflow.DROP_OLDEST,
        replay = 1
    )
    val startEvent: SharedFlow<Event<StartEvent>> get() = _startEvent

    fun editSelectedLink() {
        launchInteractionJob {
            _startEvent.emit(Event(StartEvent.LinkEditor(classifyContent(content.first()))))
        }
    }

    fun openSelectedLink() {
        launchInteractionJob {
            classifyLink(content.first())?.let { type ->
                val event = when (type) {
                    is TextType.Date -> StartEvent.Date(type.date)
                    is TextType.Link -> {
                        val uri = Uri.parse(type.uri)
                        val date = getDateFromUri(uri)
                        if (date != null) {
                            StartEvent.Date(date)
                        } else {
                            StartEvent.Link(uri)
                        }
                    }
                }
                _startEvent.emit(Event(event))
            }
        }
    }

    fun copySelectedLinkToClipboard() {
        launchInteractionJob {
            classifyLink(content.first())?.let { type ->
                val link = when (type) {
                    is TextType.Date -> type.source
                    is TextType.Link -> type.uri
                }
                _startEvent.emit(Event(StartEvent.SetClipboardText(link)))
            }
        }
    }

    fun onTimeClick() {
        launchInteractionJob {
            val types = classifyContent(content.first())
            val type = types.firstNotNullOfOrNull { it as? TextType.Time }
            val event = if (type != null) {
                StartEvent.TimeEditor(type)
            } else {
                StartEvent.InsertTime
            }
            _startEvent.emit(Event(event))
        }
    }

    fun editSelectedEvent() {
        launchInteractionJob {
            val type = classifyContent(content.first())
                .firstNotNullOfOrNull { it as? TextType.Event }
                ?: return@launchInteractionJob
            _startEvent.emit(Event(StartEvent.EventEditor(type)))
        }
    }

    private suspend fun classifyLink(content: StringWithSelection): LinkType? {
        return withContext(computeContext) {
            classifyContent(content)
                .firstNotNullOfOrNull { it as? LinkType }
        }
    }

    private fun classifyContent(content: StringWithSelection): List<TextType> {
        val types = textTypeDetector.classifyAtPosition(
            content.text,
            content.start,
            content.end,
        )
        val addTime = if (types.none { it is TextType.Time }) {
            types.firstNotNullOfOrNull { it as? TextType.Event }?.toTime()
        } else null
        return if (addTime != null) types + addTime else types
    }

    private fun launchInteractionJob(block: suspend () -> Unit) {
        navigationJob?.cancel()
        navigationJob = scope.launch {
            block()
        }
    }

}

data class StringWithSelection(
    val text: String,
    val start: Int,
    val end: Int
)

fun PageContentService.EditorContent.toStringWithSelection() = StringWithSelection(
    text,
    selectionStart,
    selectionEnd,
)

sealed class StartEvent {
    data class EventEditor(val textTypes: TextType.Event) : StartEvent()
    data class LinkEditor(val textTypes: List<TextType>) : StartEvent()
    data class TimeEditor(val type: TextType.Time) : StartEvent()
    data class Link(val uri: Uri) : StartEvent()
    data class Date(val date: LocalDate) : StartEvent()
    data object InsertTime : StartEvent()
    data class SetClipboardText(val text: String) : StartEvent()
}
