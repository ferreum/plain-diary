package de.ferreum.pto.page

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import de.ferreum.pto.applicationScope
import de.ferreum.pto.backup.ImportZipViewModel
import de.ferreum.pto.fileHelper
import de.ferreum.pto.logger
import de.ferreum.pto.pageContentServiceProvider
import de.ferreum.pto.pageWriteSignal
import de.ferreum.pto.preferencesRepository
import de.ferreum.pto.quicknotes.NoteAppender
import de.ferreum.pto.quicknotes.QuickNoteViewModel
import de.ferreum.pto.search.SearchViewModel
import de.ferreum.pto.todayProvider
import de.ferreum.pto.uriInspector
import de.ferreum.pto.util.TimeProvider
import kotlinx.coroutines.Dispatchers
import java.time.LocalDate

internal class ViewModelFactory(
    context: Context,
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle? = null,
    private val currentDate: LocalDate? = null,
    private val noteAppenderFileAccessor: NoteAppender.FileAccessor? = null,
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    constructor(fragment: Fragment, defaultArgs: Bundle? = null, currentDate: LocalDate? = null) : this(fragment.requireContext(), fragment, defaultArgs, currentDate)
    constructor(activity: AppCompatActivity, defaultArgs: Bundle? = null) : this(context = activity, activity, defaultArgs)

    private val appContext = context.applicationContext

    override fun <T : ViewModel> create(key: String, modelClass: Class<T>, handle: SavedStateHandle): T {
        @Suppress("UNCHECKED_CAST")
        return when (modelClass) {
            EditPageViewModel::class.java -> EditPageViewModel(
                handle,
                requireCurrentDate(modelClass),
                appContext.todayProvider,
                appContext.pageContentServiceProvider,
                appContext.preferencesRepository,
                Dispatchers.Default,
            )
            PasteViewModel::class.java -> PasteViewModel(
                handle
            )
            FileHintsViewModel::class.java -> FileHintsViewModel(
                appContext.pageContentServiceProvider,
                requireCurrentDate(modelClass),
                Dispatchers.Default,
            )
            SearchViewModel::class.java -> SearchViewModel(
                Dispatchers.IO,
                Dispatchers.Default,
                appContext.fileHelper,
                appContext.logger,
            )
            PreviewPageViewModel::class.java -> PreviewPageViewModel(
                requireCurrentDate(modelClass),
                appContext.pageContentServiceProvider,
                Dispatchers.Default
            )
            ImportZipViewModel::class.java -> ImportZipViewModel(
                appContext.uriInspector,
                handle,
            )
            QuickNoteViewModel::class.java -> QuickNoteViewModel(
                appContext.applicationScope,
                NoteAppender(
                    appContext.preferencesRepository,
                    appContext.fileHelper,
                    appContext.pageWriteSignal,
                    noteAppenderFileAccessor,
                ),
                TimeProvider.SYSTEM,
                appContext.logger,
            )
            else -> throw IllegalStateException("ViewModel class not registered: $modelClass")
        } as T
    }

    private fun <T : ViewModel> requireCurrentDate(modelClass: Class<T>) =
        currentDate ?: error("currentDate is required for $modelClass")

}
