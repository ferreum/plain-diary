package de.ferreum.pto.page.calendar

import android.content.ContentResolver
import android.content.ContentValues
import android.net.Uri
import android.os.Build
import android.provider.CalendarContract.Calendars
import android.provider.CalendarContract.Events
import android.provider.CalendarContract.Reminders
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import java.time.Instant
import java.util.TimeZone

interface CalendarQueryHandler {
    suspend fun getDefaultCalendarId(): Long?
    fun queryEvents(startTime: Instant, endTime: Instant): Flow<CalendarEvent>
    suspend fun insertEvent(calendarId: Long, calendarEvent: CalendarEvent): Uri?
    suspend fun insertReminder(uri: Uri, minutes: Int)
}

data class CalendarEvent(
    val title: String,
    val startTime: Instant,
    val endTime: Instant,
)

internal class CalendarQueryHandlerImpl(
    private val contentResolver: ContentResolver,
) : CalendarQueryHandler {

    private val calendarsFlow = flow {
        contentResolver.query(Calendars.CONTENT_URI, CALENDAR_PROJECTION, null, null, null)
            ?.use { cur ->
                while (cur.moveToNext()) {
                    emit(cur.getLong(0))
                }
            }
    }.flowOn(Dispatchers.IO)

    override suspend fun getDefaultCalendarId(): Long? {
        return withContext(Dispatchers.IO) {
            val sortOrder = if (Build.VERSION.SDK_INT >= 17) "${Calendars.IS_PRIMARY} DESC" else null
            contentResolver.query(Calendars.CONTENT_URI, CALENDAR_PROJECTION,
                null, null, sortOrder)?.use { cur ->
                if (cur.moveToFirst()) {
                    cur.getLong(0)
                } else {
                    null
                }
            }
        }
    }

    override fun queryEvents(startTime: Instant, endTime: Instant): Flow<CalendarEvent> = flow {
        calendarsFlow.collect { calendarId ->
            emitAll(eventsFromCalendar(calendarId, startTime, endTime))
        }
    }

    override suspend fun insertEvent(calendarId: Long, calendarEvent: CalendarEvent): Uri? {
        return withContext(Dispatchers.IO) {
            contentResolver.insert(Events.CONTENT_URI, ContentValues().apply {
                put(Events.CALENDAR_ID, calendarId)
                put(Events.TITLE, calendarEvent.title)
                put(Events.DTSTART, calendarEvent.startTime.toEpochMilli())
                put(Events.DTEND, calendarEvent.endTime.toEpochMilli())
                put(Events.EVENT_TIMEZONE, TimeZone.getDefault().displayName)
            })
        }
    }

    override suspend fun insertReminder(uri: Uri, minutes: Int) {
        withContext(Dispatchers.IO) {
            contentResolver.insert(Reminders.CONTENT_URI, ContentValues().apply {
                put(Reminders.EVENT_ID, uri.lastPathSegment!!.toLong())
                put(Reminders.MINUTES, minutes)
                put(Reminders.METHOD, Reminders.METHOD_ALERT)
            })
        }
    }

    private fun eventsFromCalendar(calendarId: Long, startTime: Instant, endTime: Instant): Flow<CalendarEvent> = flow {
        val selection = ("("
            + "${Events.CALENDAR_ID} = $calendarId"
            + " AND ${Events.DTSTART} BETWEEN ${startTime.toEpochMilli()} AND ${endTime.toEpochMilli()}"
            + ")")
        contentResolver.query(Events.CONTENT_URI, EVENT_PROJECTION,
            selection, null, null)?.use { cur ->
            while (cur.moveToNext()) {
                emit(CalendarEvent(
                    cur.getString(0),
                    Instant.ofEpochMilli(cur.getLong(1)),
                    Instant.ofEpochMilli(cur.getLong(2)),
                ))
            }
        }
    }.flowOn(Dispatchers.IO)

    companion object {
        private val CALENDAR_PROJECTION = arrayOf(Calendars._ID)
        private val EVENT_PROJECTION = arrayOf(Events.TITLE, Events.DTSTART, Events.DTEND)
    }

}
