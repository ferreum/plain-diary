package de.ferreum.pto.page.content

import android.text.Spannable
import java.util.regex.Pattern

private val POSN_PATTERN = Pattern.compile("^ ?\\[([<#>])]: ?#(?: ?\\((\\d+)\\))? *$", Pattern.MULTILINE)

fun getInitCursorPosition(text: Spannable): Int? {
    val matcher = POSN_PATTERN.matcher(text)
    val pos = if (matcher.find()) {
        when (matcher.group(1)!!) {
            "<" -> 0
            ">" -> text.length
            "#" -> try {
                matcher.group(2)?.toIntOrNull()
            } catch (e: Exception) {
                null
            }
            else -> null
        }
    } else {
        null
    }
    return pos
}
