package de.ferreum.pto.page.content

import android.app.Dialog
import android.content.ClipData
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatDialogFragment
import de.ferreum.pto.R
import de.ferreum.pto.util.getTypedParcelable
import de.ferreum.pto.util.setSoftInputWanted
import de.ferreum.pto.util.tryStartActivity
import androidx.appcompat.R as AppCompatR

class LinkEditDialogFragment : AppCompatDialogFragment() {

    private var textInput: EditText? = null
    private var linkInput: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        val typedValue = TypedValue()
        requireContext().theme.resolveAttribute(AppCompatR.attr.alertDialogTheme, typedValue, true)
        setStyle(STYLE_NORMAL, typedValue.resourceId)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window!!.setSoftInputWanted(true)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_link_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textInput = view.findViewById<EditText>(R.id.textInput)!!.apply {
            setHorizontallyScrolling(false)
            maxLines = Integer.MAX_VALUE
            setText(arguments?.getString(EXTRA_TEXT))
        }
        linkInput = view.findViewById<EditText>(R.id.linkInput)!!.apply {
            setHorizontallyScrolling(false)
            maxLines = Integer.MAX_VALUE
            setText(arguments?.getString(EXTRA_TARGET))
            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE -> {
                        saveLink()
                        true
                    }
                    else -> false
                }
            }
        }
        view.findViewById<View>(R.id.shareButton).apply {
            setOnClickListener {
                val title = textInput!!.text.toString()
                val uri = Uri.parse(linkInput!!.text.toString())
                tryStartActivity(
                    requireContext(),
                    Intent.createChooser(
                        Intent(Intent.ACTION_SEND).apply {
                            clipData = ClipData.newRawUri(title, uri)
                            putExtra(Intent.EXTRA_TITLE, title)
                            putExtra(Intent.EXTRA_TEXT, uri.toString())
                            type = "text/plain"
                        },
                        title,
                    )
                ) { Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show() }
            }
        }
        view.findViewById<View>(R.id.cancelButton).apply {
            setOnClickListener {
                requireDialog().cancel()
            }
        }
        view.findViewById<View>(R.id.okButton).apply {
            setOnClickListener {
                saveLink()
            }
        }
    }

    private fun saveLink() {
        (parentFragment as? LinkCallback)?.onLinkChanged(
            textInput!!.text.toString(),
            linkInput!!.text.toString(),
            arguments?.getTypedParcelable(EXTRA_TEXT_TYPE),
        )
        requireDialog().dismiss()
    }

    override fun onResume() {
        super.onResume()
        linkInput!!.requestFocus()
    }

    override fun onDestroyView() {
        linkInput = null
        textInput = null
        super.onDestroyView()
    }

    interface LinkCallback {
        fun onLinkChanged(text: String, target: String, textType: TextType?)
    }

    companion object {
        const val EXTRA_TEXT = "text"
        const val EXTRA_TARGET = "target"
        const val EXTRA_TEXT_TYPE = "textType"
    }

}
