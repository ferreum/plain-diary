package de.ferreum.pto.page.content

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.time.LocalDate
import java.time.LocalTime

sealed interface LinkType

sealed class TextType : Parcelable {
    abstract val sourceStart: Int
    abstract val sourceEnd: Int
    abstract val source: String

    @Parcelize
    data class Date(
        override val sourceStart: Int,
        override val sourceEnd: Int,
        override val source: String,
        val date: LocalDate,
    ) : TextType(), LinkType

    @Parcelize
    data class Time(
        override val sourceStart: Int,
        override val sourceEnd: Int,
        override val source: String,
        val time: LocalTime,
    ) : TextType()

    @Parcelize
    data class Event(
        override val sourceStart: Int,
        override val sourceEnd: Int,
        override val source: String,
        val eventContent: EventContent,
        val reminderSelection: Int,
    ) : TextType()

    @Parcelize
    data class Link(
        override val sourceStart: Int,
        override val sourceEnd: Int,
        override val source: String,
        val text: String?,
        val uri: String,
    ) : TextType(), LinkType {
        constructor(range: IntRange, source: String, text: String?, uri: String)
            : this(range.first, range.last + 1, source, text, uri)
    }

    @Parcelize
    data class Selection(
        override val sourceStart: Int,
        override val sourceEnd: Int,
        override val source: String,
    ) : TextType()

    fun withOffset(offset: Int) = when (this) {
        is Date -> copy(sourceStart = sourceStart + offset, sourceEnd = sourceEnd + offset)
        is Time -> copy(sourceStart = sourceStart + offset, sourceEnd = sourceEnd + offset)
        is Event -> copy(sourceStart = sourceStart + offset, sourceEnd = sourceEnd + offset)
        is Link -> copy(sourceStart = sourceStart + offset, sourceEnd = sourceEnd + offset)
        is Selection -> copy(sourceStart = sourceStart + offset, sourceEnd = sourceEnd + offset)
    }
}

@Parcelize
data class EventContent(
    val time: LocalTime,
    val reminders: List<String>,
    val title: String,
) : Parcelable
