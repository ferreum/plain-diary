package de.ferreum.pto.page.content

import androidx.core.text.isDigitsOnly
import de.ferreum.pto.reminder.ReminderParser
import de.ferreum.pto.util.lineRangeAt
import java.time.DateTimeException
import java.time.LocalDate
import java.time.LocalTime

class TextTypeDetector {

    fun classifyAtPosition(text: CharSequence, start: Int, end: Int = start): List<TextType> {
        val range = text.lineRangeAt(start)
        if (range.isEmpty() || range.last < end - 1) return emptyList()

        val substring = text.substring(range)
        return classify(substring, range.first, start - range.first, end - range.first)
    }

    private fun classify(content: String, offset: Int, selStart: Int, selEnd: Int): List<TextType> {
        val types = DETECTOR_GROUP_SEQ.mapNotNullTo(mutableListOf()) { group ->
            group.firstNotNullOfOrNull { detector ->
                findByRegex(detector.regex, content, selStart, selEnd)
                    ?.let { detector.create(it, selStart, selEnd) }
                    ?.withOffset(offset)
            }
        }
        if (selStart < selEnd) {
            types.add(TextType.Selection(
                selStart + offset, selEnd + offset,
                content.substring(selStart, selEnd),
            ))
        }
        return types
    }

    private fun findByRegex(regex: Regex, content: String, selStart: Int, selEnd: Int): MatchResult? {
        return regex.findAll(content).firstOrNull { match ->
            match.range.first <= selStart && match.range.last + 1 >= selEnd
        }
    }

    companion object {

        private interface Detector {
            val regex: Regex

            fun create(match: MatchResult, selStart: Int, selEnd: Int): TextType?
        }

        private val DETECTOR_GROUPS = listOf(
            listOf(DateDetector),

            listOf(TimeDetector),

            listOf(EventDetector),

            listOf(
                object : Detector {
                    override val regex = Regex("""\[([^]]+)]\(([^)]+)\)""")
                    override fun create(match: MatchResult, selStart: Int, selEnd: Int): TextType =
                        TextType.Link(match.range, match.value, match.groupValues[1], match.groupValues[2])
                },
                object : Detector {
                    override val regex = Regex("""<([^\s>]+)>""")
                    override fun create(match: MatchResult, selStart: Int, selEnd: Int): TextType =
                        TextType.Link(match.range, match.value, "", match.groupValues[1])
                },
                object : Detector {
                    override val regex = Regex("""(\b[a-zA-Z][a-zA-Z.]+):\S+[\w/#=%]""")
                    override fun create(match: MatchResult, selStart: Int, selEnd: Int): TextType? {
                        if (match.groupValues[1].isDigitsOnly()) {
                            // avoid recognizing time as link
                            return null
                        }
                        return TextType.Link(match.range, match.value, null, match.value)
                    }
                },
            )
        )

        private val DETECTOR_GROUP_SEQ = DETECTOR_GROUPS.map { it.asSequence() }

        object DateDetector : Detector {
            override val regex = Regex("""(\d+)-(\d{1,2})-(\d{1,2})""")

            override fun create(match: MatchResult, selStart: Int, selEnd: Int): TextType? {
                val year = match.groupValues[1].toIntOrNull() ?: return null
                val month = match.groupValues[2].toIntOrNull() ?: return null
                val day = match.groupValues[3].toIntOrNull() ?: return null
                val date = try {
                    LocalDate.of(year, month, day)
                } catch (e: DateTimeException) {
                    return null
                }
                return TextType.Date(match.range.first, match.range.last + 1, match.value, date)
            }
        }

        object TimeDetector : Detector {
            override val regex = Regex("""(?:^@)?(\d{1,2}):(\d{1,2})""", RegexOption.MULTILINE)

            override fun create(match: MatchResult, selStart: Int, selEnd: Int): TextType? {
                val hour = match.groupValues[1].toIntOrNull() ?: return null
                val minute = match.groupValues[2].toIntOrNull() ?: return null
                val time = try {
                    LocalTime.of(hour, minute)
                } catch (e: DateTimeException) {
                    return null
                }
                return TextType.Time(match.range.first, match.range.last + 1, match.value, time)
            }
        }

        object EventDetector : Detector {
            override val regex = ReminderParser.EVENT_REGEX

            override fun create(match: MatchResult, selStart: Int, selEnd: Int): TextType? {
                return ReminderParser.parseEventTextType(match, selStart)
            }
        }

        fun TextType.Event.toTime(): TextType.Time? {
            val match = TimeDetector.regex.matchAt(source, 0)
                ?: return null
            return TimeDetector.create(
                match,
                match.range.last + 1,
                match.range.last + 1,
            )?.withOffset(sourceStart) as TextType.Time?
        }
    }

}
