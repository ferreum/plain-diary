package de.ferreum.pto.page.content

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import de.ferreum.pto.R
import de.ferreum.pto.util.getTypedParcelable
import java.time.LocalTime

class TimePickerDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val secondOfDay = arguments?.getInt(EXTRA_SECOND_OF_DAY, -1) ?: -1
        val time = if (secondOfDay < 0) {
            LocalTime.now()
        } else {
            LocalTime.ofSecondOfDay(secondOfDay.toLong())
        }
        val listener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            finishWithTime(LocalTime.of(hour, minute))
        }
        return TimePickerDialog(requireContext(), listener, time.hour, time.minute, true).apply {
            setButton(Dialog.BUTTON_NEUTRAL, getText(R.string.now)) { _, _ ->
                finishWithTime(LocalTime.now())
            }
        }
    }

    private fun finishWithTime(time: LocalTime) {
        (parentFragment as? Callback)
            ?.onTimeSelected(time, arguments?.getTypedParcelable(EXTRA_TEXT_TYPE))
        requireDialog().dismiss()
    }

    interface Callback {
        fun onTimeSelected(time: LocalTime, textType: TextType?)
    }

    companion object {
        const val EXTRA_SECOND_OF_DAY = "secondOfDay"
        const val EXTRA_TEXT_TYPE = "textType"

        fun withArgs(
            textType: TextType.Time,
            initialTime: LocalTime = textType.time,
        ) = TimePickerDialogFragment().apply {
            arguments = Bundle().apply {
                putInt(EXTRA_SECOND_OF_DAY, initialTime.toSecondOfDay())
                putParcelable(EXTRA_TEXT_TYPE, textType)
            }
        }
    }

}
