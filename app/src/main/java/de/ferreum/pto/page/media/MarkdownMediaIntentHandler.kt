package de.ferreum.pto.page.media

import android.content.Intent
import android.net.Uri
import de.ferreum.pto.util.Markdown
import de.ferreum.pto.util.getTypedParcelableArrayListExtra
import de.ferreum.pto.util.getTypedParcelableExtra

object MarkdownMediaIntentHandler {

    fun getShareIntentLinks(intent: Intent): List<Link> {
        val type = intent.type
        if (MDTAG_GEO.equals(intent.data?.scheme, ignoreCase = true)) {
            val title = intent.getStringExtra(Intent.EXTRA_TITLE)
                ?: intent.getStringExtra(Intent.EXTRA_SUBJECT)
                ?: TITLE_OSM
            return listOf(makeLink(intent.data!!, title))
        } else if (type.equals(MIMETYPE_TEXT_PLAIN, ignoreCase = true)) {
            val text = intent.getStringExtra(Intent.EXTRA_TEXT)
            val title = intent.getStringExtra(Intent.EXTRA_TITLE)
                ?: intent.getStringExtra(Intent.EXTRA_SUBJECT)

            if (text != null) {
                val uri = Uri.parse(text)
                return if (SCHEME_HTTP.equals(uri?.scheme, ignoreCase = true)
                    || SCHEME_HTTPS.equals(uri?.scheme, ignoreCase = true)
                ) {
                    listOf(makeLink(uri, title))
                } else {
                    listOf(Link(text, null, text))
                }
            }

            val uri = intent.getTypedParcelableExtra<Uri>(Intent.EXTRA_STREAM)
                ?: return emptyList()
            return listOf(makeLink(uri, title))
        } else {
            if (Intent.ACTION_SEND == intent.action) {
                var uri = intent.getTypedParcelableExtra<Uri>(Intent.EXTRA_STREAM)
                    ?: return emptyList()

                val text = intent.getStringExtra(Intent.EXTRA_TEXT)
                if (text != null) {
                    val pathUri = Uri.parse(text)
                    // prefer value from text if it's a valid http URL
                    if (pathUri != null && (SCHEME_HTTP.equals(pathUri.scheme, ignoreCase = true)
                            || SCHEME_HTTPS.equals(pathUri.scheme, ignoreCase = true))
                    ) {
                        uri = pathUri
                    }
                }

                return listOf(makeLink(uri, null))
            } else if (Intent.ACTION_SEND_MULTIPLE == intent.action) {
                val uris = intent.getTypedParcelableArrayListExtra<Uri>(Intent.EXTRA_STREAM)
                    ?: return emptyList()
                return uris.mapNotNull { uri ->
                    uri?.let { makeLink(it, null) }
                }
            } else {
                return emptyList()
            }
        }
    }

    private fun makeLink(uri: Uri, title: String?): Link {
        return Link(title, uri, Markdown.formatLink(title.orEmpty(), uri.toString()))
    }

    data class Link(
        val title: String?,
        val uri: Uri?,
        val text: String,
    )

    private const val MDTAG_GEO = "geo"
    private const val TITLE_OSM = "osm"

    private const val SCHEME_HTTP = "http"
    private const val SCHEME_HTTPS = "https"

    private const val MIMETYPE_TEXT_PLAIN = "text/plain"
}
