package de.ferreum.pto.page.undo

sealed class HistoryCommand {
    fun isUndo(undoVersion: Int) = when (this) {
        is Undo -> true
        is Redo -> false
        is GoToVersion -> version < undoVersion
    }

    data object Undo : HistoryCommand()
    data object Redo : HistoryCommand()
    data class GoToVersion(val version: Int) : HistoryCommand()
}
