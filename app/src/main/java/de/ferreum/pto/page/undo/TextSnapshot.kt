package de.ferreum.pto.page.undo

import de.ferreum.pto.page.undo.UndoHistory.Companion.NO_CHANGE_ID

data class TextSnapshot(
    // Note: text needs to be String, because spanned alternatives
    // don't implement equals()
    val text: String,
    val selectionStart: Int,
    val selectionEnd: Int,
    val preSelectionStart: Int,
    val preSelectionEnd: Int,
    val preChangeId: Int,
) {
    val size get() = text.length

    companion object {
        fun fromString(
            text: String,
            selection: Int = -1,
            selectionEnd: Int = selection,
            preSelection: Int = -1,
            preSelectionEnd: Int = preSelection,
            preChangeId: Int = NO_CHANGE_ID,
        ) = TextSnapshot(text, selection, selectionEnd, preSelection, preSelectionEnd, preChangeId)
    }
}

data class TextChange(
    val snapshot: TextSnapshot,
    val changeId: Int,
    val transactionTag: Int = -1,
)
