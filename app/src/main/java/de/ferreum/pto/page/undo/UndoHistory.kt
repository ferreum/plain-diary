package de.ferreum.pto.page.undo

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class UndoHistory {

    private var undoChanges = ArrayDeque<TextChange>()
    private var redoChanges = ArrayDeque<TextChange>()

    data class Counters(
        val undoCount: Int,
        val redoCount: Int,
        val undoVersion: Int,
        val changeId: Int,
        val totalSize: Int,
    )

    private val _counters = MutableStateFlow(Counters(0, 0, 0, NO_CHANGE_ID, 0))
    val counters: StateFlow<Counters> = _counters

    private var latestChangeId = 0

    fun addChange(snapshot: TextSnapshot, transactionTag: Int = NO_TRANSACTION_TAG): TextChange {
        val currentChange = undoChanges.lastOrNull()

        // detect overwriting last change within transaction
        if (transactionTag != NO_TRANSACTION_TAG && currentChange?.transactionTag == transactionTag) {
            val change = TextChange(
                // preSelection is for text of previous change, so we must preserve it
                snapshot = snapshot.copy(
                    preSelectionStart = currentChange.snapshot.preSelectionStart,
                    preSelectionEnd = currentChange.snapshot.preSelectionEnd,
                ),
                changeId = ++latestChangeId,
                transactionTag = transactionTag,
            )
            redoChanges.clear()
            undoChanges[undoChanges.lastIndex] = change
            updateCounters {
                it.copy(
                    redoCount = 0,
                    changeId = change.changeId,
                    totalSize = it.totalSize - currentChange.snapshot.size + change.snapshot.size,
                )
            }
            return change
        }

        // detect text equal to current snapshot
        currentChange?.takeIf { snapshot.text == it.snapshot.text }?.let {
            // include change in transaction
            val change = currentChange.copy(transactionTag = transactionTag)
            undoChanges[undoChanges.lastIndex] = change
            return change
        }

        val change = TextChange(
            snapshot = snapshot,
            changeId = ++latestChangeId,
            transactionTag = transactionTag
        )

        redoChanges.clear()
        undoChanges.add(change)
        updateCounters {
            Counters(
                undoVersion = it.undoVersion + 1,
                undoCount = undoChanges.size - 1,
                redoCount = 0,
                changeId = change.changeId,
                totalSize = it.totalSize + change.snapshot.size
            )
        }
        return change
    }

    fun getRedoChange(): TextChange? {
        return redoChanges.lastOrNull()
    }

    fun doCommand(command: HistoryCommand): TextChange? {
        return when (command) {
            HistoryCommand.Undo -> undoChange()
            HistoryCommand.Redo -> redoChange()
            is HistoryCommand.GoToVersion -> goToVersion(command.version)
        }
    }

    private fun goToVersion(version: Int): TextChange? {
        val steps = _counters.value.undoVersion - version
        var change: TextChange? = null
        if (steps > 0) {
            repeat(steps) {
                change = undoChange() ?: return change
            }
        } else {
            repeat(-steps) {
                change = redoChange() ?: return change
            }
        }
        return change
    }

    private fun undoChange(): TextChange? {
        if (undoChanges.size < 2) return null
        redoChanges.add(undoChanges.removeLast())
        val snapshot = undoChanges.last()
        updateCounters {
            Counters(
                undoVersion = it.undoVersion - 1,
                undoCount = it.undoCount - 1,
                redoCount = it.redoCount + 1,
                changeId = snapshot.changeId,
                totalSize = it.totalSize,
            )
        }
        return snapshot
    }

    private fun redoChange(): TextChange? {
        val snapshot = redoChanges.removeLastOrNull() ?: return null
        undoChanges.add(snapshot)
        updateCounters {
            Counters(
                undoVersion = it.undoVersion + 1,
                undoCount = it.undoCount + 1,
                redoCount = it.redoCount - 1,
                changeId = snapshot.changeId,
                totalSize = it.totalSize,
            )
        }
        return snapshot
    }

    fun trimToSizeInBytes(sizeBytes: Int) {
        var currentSize = _counters.value.totalSize
        if (currentSize < sizeBytes) return

        while (currentSize > sizeBytes) {
            val change = redoChanges.removeFirstOrNull() ?: break
            currentSize -= change.snapshot.size
        }

        // We always preserve last undo entry, as it's the state that
        // gets saved on next addChange.
        while (currentSize > sizeBytes && undoChanges.size > 1) {
            val change = undoChanges.removeFirstOrNull() ?: break
            currentSize -= change.snapshot.size
        }

        updateCounters {
            it.copy(
                undoCount = (undoChanges.size - 1).coerceAtLeast(0),
                redoCount = redoChanges.size,
                totalSize = currentSize,
            )
        }
    }

    private inline fun updateCounters(function: (Counters) -> Counters) {
        _counters.value = function(_counters.value)
    }

    companion object {
        const val NO_CHANGE_ID = -1
        const val NO_TRANSACTION_TAG = -1
    }

}
