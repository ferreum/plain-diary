package de.ferreum.pto.page.undo

import de.ferreum.pto.textadjust.LinearDragStepAdjustHandler

class UndoHistoryStepAdjustHandler(
    private val changeVersionProvider: () -> Int,
    private val historyCommandExecutor: (HistoryCommand) -> Boolean,
) : LinearDragStepAdjustHandler.AdjustStepListener {

    private var undoRef: Int = -1

    override fun onAdjustStart() {
        undoRef = changeVersionProvider()
    }

    override fun onAdjustStepChange(step: Int): Boolean {
        return goToStep(step)
    }

    override fun onAdjustEnd(step: Int): Boolean {
        return goToStep(step).also { undoRef = -1 }
    }

    private fun goToStep(step: Int): Boolean {
        if (undoRef < 0) return false
        return historyCommandExecutor(HistoryCommand.GoToVersion(undoRef + step))
    }

}
