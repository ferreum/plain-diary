package de.ferreum.pto.preferences

import android.app.Dialog
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import de.ferreum.pto.BuildConfig
import de.ferreum.pto.R
import java.time.Instant

class AboutDialogFragment : AppCompatDialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(requireContext(), theme)
            .setTitle(R.string.appName)
            .setView(R.layout.dialog_about)
            .setPositiveButton(android.R.string.ok, null)
            .create()
    }

    override fun onStart() {
        super.onStart()
        // Version text view
        val versionTextView = requireDialog().findViewById<TextView>(R.id.versionText)
        versionTextView.text = SpannableStringBuilder(versionTextView.text).apply {
            val index = indexOf("%s")
            if (index >= 0) {
                replace(index, index + 2, BuildConfig.VERSION_NAME)
            }
        }
        versionTextView.movementMethod = LinkMovementMethod.getInstance()

        // Build information
        val revisionTextView = requireDialog().findViewById<TextView>(R.id.revisionText)
        revisionTextView.text = getString(R.string.about_revision, getString(R.string.build_revision))
        val builtTextView = requireDialog().findViewById<TextView>(R.id.buildTimeText)
        val buildTime = getString(R.string.build_commit_time).toLongOrNull()
                ?.let { Instant.ofEpochSecond(it) }
        builtTextView.text = getString(R.string.about_committed, buildTime ?: "unknown")

        // Copyright text
        val copyrightTextView = requireDialog().findViewById<TextView>(R.id.copyrightText)
        copyrightTextView.movementMethod = LinkMovementMethod.getInstance()

        // Licence text
        val licenceTextView = requireDialog().findViewById<TextView>(R.id.licenceText)
        licenceTextView.movementMethod = LinkMovementMethod.getInstance()
    }
}
