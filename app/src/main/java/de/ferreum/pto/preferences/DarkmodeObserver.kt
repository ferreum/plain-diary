package de.ferreum.pto.preferences

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

internal class DarkmodeObserver(
    private val scope: CoroutineScope,
    private val preferencesRepository: PtoPreferencesRepository,
) {

    fun init(sharedPreferences: SharedPreferences) {
        applyDarkMode(sharedPreferences.getString(PtoPreferences.PREF_DARKMODE, null).orEmpty())
    }

    fun observeDarkmodePreference() {
        scope.launch {
            preferencesRepository.preferencesFlow
                .distinctUntilChangedBy { it.darkmode }
                .collect {
                    withContext(Dispatchers.Main) {
                        applyDarkMode(it.darkmode)
                    }
                }
        }
    }

    private fun applyDarkMode(prefValue: String) {
        AppCompatDelegate.setDefaultNightMode(
            when (prefValue) {
                "light" -> AppCompatDelegate.MODE_NIGHT_NO
                "dark" -> AppCompatDelegate.MODE_NIGHT_YES
                else -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
            }
        )
    }

}
