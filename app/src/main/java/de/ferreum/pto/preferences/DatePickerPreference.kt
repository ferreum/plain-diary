/*
 * Diary - Personal diary for Android
 *
 * Copyright (C) 2017  Bill Farmer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Bill Farmer william j farmer [at] yahoo [dot] co [dot] uk.
 */
package de.ferreum.pto.preferences

import android.app.DatePickerDialog
import android.content.Context
import android.content.SharedPreferences
import android.content.res.TypedArray
import android.util.AttributeSet
import android.widget.DatePicker
import androidx.preference.Preference
import java.time.LocalDate

class DatePickerPreference(
    context: Context,
    attrs: AttributeSet?
) : Preference(context, attrs), DatePickerDialog.OnDateSetListener {

    private var value = DEFAULT_DATE

    override fun onClick() {
        val prevValue = value
        val dialog = DatePickerDialog(
            context, this,
            value.year,
            value.monthValue - 1,
            value.dayOfMonth
        )
        dialog.setOnCancelListener { setValue(prevValue) }
        dialog.show()
    }

    override fun onSetInitialValue(defaultValue: Any?) {
        val value = getPersistedString(defaultValue as String?)
        setValue(value?.let { LocalDate.parse(it) } ?: DEFAULT_DATE)
    }

    override fun onGetDefaultValue(a: TypedArray, index: Int): Any? = a.getString(index)

    override fun onDateSet(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
        setValue(LocalDate.of(year, month + 1, dayOfMonth))
    }

    private fun setValue(date: LocalDate) {
        value = date
        persistString(value.toString())
        notifyChanged()
    }

    fun getValue(): LocalDate = value

    companion object {
        val DEFAULT_DATE: LocalDate = LocalDate.of(2000, 1, 1)

        fun SharedPreferences.getLocalDate(key: String, defaultValue: LocalDate): LocalDate {
            return getString(key, null)?.let { LocalDate.parse(it) } ?: defaultValue
        }
    }
}
