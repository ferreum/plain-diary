package de.ferreum.pto.preferences

import androidx.preference.Preference.SummaryProvider
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

internal class DateSummaryProvider : SummaryProvider<DatePickerPreference> {
    private val formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)!!

    override fun provideSummary(preference: DatePickerPreference): CharSequence {
        return preference.getValue().format(formatter)
    }
}
