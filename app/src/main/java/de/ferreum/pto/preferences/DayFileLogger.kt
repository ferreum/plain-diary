package de.ferreum.pto.preferences

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.io.PrintWriter
import java.io.StringWriter
import java.time.Instant
import java.time.LocalDate

interface Logger {
    suspend fun suspendLogException(throwable: Throwable, message: String? = null, tag: String? = null)

    fun logCriticalException(throwable: Throwable, message: String? = null, tag: String? = null)
}

class RecordingLogger : Logger {
    var exceptions: List<Throwable> = emptyList()
        private set

    fun clear() {
        exceptions = emptyList()
    }

    fun throwIfAny() {
        exceptions.takeIf { it.isNotEmpty() }?.let { list ->
            throw list.first().also { first ->
                list.drop(1).forEach { first.addSuppressed(it) }
            }
        }
    }

    override suspend fun suspendLogException(throwable: Throwable, message: String?, tag: String?) {
        exceptions = exceptions + throwable
    }

    override fun logCriticalException(throwable: Throwable, message: String?, tag: String?) {
        exceptions = exceptions + throwable
    }
}

class DayFileLogger(
    private val scope: CoroutineScope,
    private val preferencesRepository: PtoPreferencesRepository,
    private val fileHelper: Deferred<PtoFileHelper>,
    private val revision: String,
) : Logger {

    private var stateFlow: StateFlow<Pair<PtoFileHelper, LocalDate>?>? = null
    private lateinit var initJob: Deferred<StateFlow<Pair<PtoFileHelper, LocalDate>?>>

    suspend fun init() {
        initJob = scope.async {
            preferencesRepository.preferencesFlow
                .distinctUntilChangedBy { prefs ->
                    prefs.logfileDate.takeIf { prefs.isLogfileEnabled }
                }
                .map { if (!it.isLogfileEnabled) null else fileHelper.await() to it.logfileDate }
                .stateIn(scope)
        }
        stateFlow = initJob.await()
    }

    override suspend fun suspendLogException(throwable: Throwable, message: String?, tag: String?) {
        logTimber(throwable, message, tag)

        withContext(NonCancellable) {
            val (fileHelper, date) = initJob.await().value ?: return@withContext
            withContext(Dispatchers.IO) {
                fileHelper.getDayFile(date)
                    .appendLog(throwable, message)
            }
        }
    }

    override fun logCriticalException(throwable: Throwable, message: String?, tag: String?) {
        logTimber(throwable, message, tag)

        val (fileHelper, date) = stateFlow?.value ?: return
        fileHelper.getDayFile(date)
            .appendLog(throwable, message)
    }

    private fun logTimber(throwable: Throwable, message: String?, tag: String?) {
        tag?.let { Timber.tag(it) }
        if (message != null) {
            Timber.e(throwable, "%s", message)
        } else {
            Timber.e(throwable)
        }
    }

    private fun File.appendLog(throwable: Throwable, message: String?) {
        parentFile?.mkdirs()
        appendText(formatThrowable(throwable, message))
    }

    private fun formatThrowable(
        throwable: Throwable,
        message: String?,
    ): String {
        val sw = StringWriter()
        PrintWriter(sw).use {
            it.println("")
            it.print(Instant.now().toString())
            if (message != null) {
                it.print(": ")
                it.print(message)
            }
            it.println()
            it.println("revision: $revision")
            throwable.printStackTrace(it)
        }
        return sw.toString()
    }

}
