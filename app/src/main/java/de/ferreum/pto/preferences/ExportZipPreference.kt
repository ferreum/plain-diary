package de.ferreum.pto.preferences

import android.content.Context
import android.util.AttributeSet
import androidx.preference.DialogPreference

internal class ExportZipPreference(
    context: Context,
    attrs: AttributeSet?
) : DialogPreference(context, attrs)
