package de.ferreum.pto.preferences

import android.net.Uri
import android.os.Environment
import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.io.File

@Parcelize
data class ZipFileLocation(val uri: Uri) : Parcelable

sealed class FolderLocation : Parcelable {
    abstract fun toAbsoluteFile(internalFilesDir: File): File

    @Parcelize
    data class ExternalStorage(val file: File) : FolderLocation() {
        override fun toAbsoluteFile(internalFilesDir: File): File {
            return if (file.isAbsolute) {
                file
            } else {
                File(Environment.getExternalStorageDirectory(), file.path)
            }
        }
    }

    @Parcelize
    data object InternalStorage : FolderLocation() {
        override fun toAbsoluteFile(internalFilesDir: File): File {
            return File(internalFilesDir, PtoFileHelper.RELPATH_FILES_INTERNAL_PAGES)
        }
    }
}
