package de.ferreum.pto.preferences

import de.ferreum.pto.preferences.FolderLocation.InternalStorage
import de.ferreum.pto.util.SmallNumberSet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.time.LocalDate
import java.time.Month
import java.util.EnumSet
import java.util.Locale

class PtoFileHelper(internalFilesPath: File) {
    val pagesDir: File = InternalStorage.toAbsoluteFile(internalFilesPath)

    private fun getYearDir(year: Int): File =
        File(pagesDir, YEAR_FORMAT.format(Locale.ROOT, year))

    private fun getMonthDir(year: Int, month: Int): File =
        File(pagesDir, MONTH_PATH_FORMAT.format(Locale.ROOT, year, month))

    fun getDayFile(date: LocalDate): File =
        File(pagesDir, DAY_PATH_FORMAT.format(Locale.ROOT, date.year, date.monthValue, date.dayOfMonth))

    suspend fun getExistingDaysInMonth(year: Int, month: Int): SmallNumberSet {
        var days = SmallNumberSet.empty()
        val monthDir = getMonthDir(year, month)
        val dirList = withContext(Dispatchers.IO) { monthDir.list() }
        for (filename in dirList.orEmpty()) {
            DAY_FILE_REGEX.matchEntire(filename)?.let { match ->
                val number = match.groupValues[1].toInt()
                if (number in 1..31) days += number
            }
        }
        return days
    }

    suspend fun getExistingMonthsInYear(year: Int): Set<Month> {
        val months = EnumSet.noneOf(Month::class.java)
        val yearDir = getYearDir(year)
        val dirList = withContext(Dispatchers.IO) { yearDir.list() }
        for (filename in dirList.orEmpty()) {
            MONTH_DIR_REGEX.matchEntire(filename)?.let { match ->
                months.add(Month.of(match.value.toInt()))
            }
        }
        return months
    }

    companion object {
        private const val DAY_PATH_FORMAT = "%04d/%02d/%02d.txt"
        private const val MONTH_PATH_FORMAT = "%04d/%02d"
        private const val YEAR_FORMAT = "%04d"
        val DAY_FILE_REGEX = Regex("([0-9]{2})\\.txt")
        private val MONTH_DIR_REGEX = Regex("[0-1][0-9]")
        const val RELPATH_FILES_INTERNAL_PAGES = "pages"
    }
}
