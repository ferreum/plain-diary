package de.ferreum.pto.preferences

import java.time.LocalDate
import java.time.LocalTime

data class PtoPreferences(
    val isIndexPageEnabled: Boolean,
    val isTemplateEnabled: Boolean,
    val isAutostartQuickNotes: Boolean,
    val isQuickNotesExpansionEnabled: Boolean,
    val isTextClassifierEnabled: Boolean,
    val isSupportEmojiEnabled: Boolean,
    val isSuggestionsEnabled: Boolean,
    val editorFontSize: Int,
    val isIsoDateFormatEnabled: Boolean,
    val indexPageDate: LocalDate,
    val templatePageDate: LocalDate,
    val customNotificationTitle: String?,
    val useInvisibleNotificationIcon: Boolean,
    val darkmode: String,
    val newpageTime: LocalTime,
    val isEventRemindersEnabled: Boolean,
    val isLogfileEnabled: Boolean,
    val logfileDate: LocalDate,
) {

    companion object {
        const val PREF_DARKMODE = "pref_darkmode"
        const val PREF_USE_INDEX = "pref_use_index"
        const val PREF_NEWPAGE_TIME = "pref_newpage_time"
        const val PREF_INDEX_PAGE = "pref_index_page"
        const val PREF_USE_TEMPLATE = "pref_use_template"
        const val PREF_TEMPLATE_PAGE = "pref_template_page"
        const val PREF_AUTOSTART_QUICKNOTES = "pref_autostart_quicknotes"
        const val PREF_QUICKNOTES_ENABLE_EXPANDED = "pref_quicknotes_enable_expanded"
        const val PREF_QUICKNOTES_NOTIFICATION_TITLE = "pref_quicknotes_notification_title"
        const val PREF_QUICKNOTES_INVISIBLE_ICON = "pref_quicknotes_invisible_icon"
        const val PREF_ENABLE_TEXTCLASSIFIER = "pref_enable_textclassifier"
        const val PREF_SUPPORT_EMOJI_ENABLED = "pref_support_emoji_enabled"
        const val PREF_SUGGESTIONS_ENABLED = "pref_suggestions_enabled"
        const val PREF_EDITOR_FONT_SIZE = "pref_editor_font_size"
        const val PREF_USE_ISODATES = "pref_use_isodates"
        const val PREF_REMINDERS_ENABLED = "pref_reminders_enabled"
        const val PREF_LOGFILE_ENABLE = "pref_use_logfile"
        const val PREF_LOGFILE_DATE = "pref_logfile_date"
    }

}
