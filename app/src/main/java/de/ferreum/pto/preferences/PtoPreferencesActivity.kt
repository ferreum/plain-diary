/*
 * Diary - Personal diary for Android
 *
 * Copyright (C) 2017  Bill Farmer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Bill Farmer william j farmer [at] yahoo [dot] co [dot] uk.
 */
package de.ferreum.pto.preferences

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import de.ferreum.pto.MainActivity
import de.ferreum.pto.R
import de.ferreum.pto.backup.ACTION_ZIP_IMPORT
import de.ferreum.pto.backup.ImportZipFragment
import de.ferreum.pto.preferencesRepository
import de.ferreum.pto.util.getTypedParcelable
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

class PtoPreferencesActivity : AppCompatActivity() {
    private var handlingIntent: Intent? = null

    private var menuJob: Job? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        setSupportActionBar(findViewById(R.id.toolbar))

        val toolbarContainer: View = findViewById(R.id.toolbarContainer)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.content)) { _, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or WindowInsetsCompat.Type.displayCutout()
                    or WindowInsetsCompat.Type.ime()
            )
            toolbarContainer.setPadding(insets.left, insets.top, insets.right, 0)
            windowInsets.inset(0, insets.top, 0, 0)
        }

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setTitle(R.string.settings)
        }
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, PtoPreferencesFragment())
                .commit()
        }
        handlingIntent = if (savedInstanceState == null) {
            intent
        } else {
            savedInstanceState.getTypedParcelable(SAVE_HANDLING_INTENT)
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handlingIntent = intent
    }

    override fun onResume() {
        super.onResume()
        handlingIntent?.let { intent ->
            handlingIntent = null
            if (intent.action == ACTION_ZIP_IMPORT) {
                if (!isImportDialogShown()) {
                    ImportZipFragment().apply {
                        arguments = intent.extras
                        show(supportFragmentManager, ImportZipFragment.FRAGMENT_TAG)
                    }
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(SAVE_HANDLING_INTENT, handlingIntent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.fragment_preferences, menu)
        val logfileItem = menu.findItem(R.id.logfileAction)
        menuJob?.cancel()
        menuJob = lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                preferencesRepository.preferencesFlow.collect {
                    logfileItem.isVisible = it.isLogfileEnabled
                }
            }
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                if (supportFragmentManager.backStackEntryCount > 0) {
                    supportFragmentManager.popBackStack()
                } else {
                    finish()
                    startActivity(Intent(parentActivityIntent)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
                }
                true
            }
            R.id.logfileAction -> {
                lifecycleScope.launch { openLogfile() }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private suspend fun openLogfile() {
        val date = preferencesRepository.preferencesFlow.first().logfileDate
        startActivity(MainActivity.createPageIntent(this, date))
    }

    private fun isImportDialogShown(): Boolean {
        return supportFragmentManager.findFragmentByTag(ImportZipFragment.FRAGMENT_TAG) != null
    }

    companion object {
        private const val SAVE_HANDLING_INTENT = "handlingIntent"
    }
}
