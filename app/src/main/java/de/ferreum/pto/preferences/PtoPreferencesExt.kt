package de.ferreum.pto.preferences

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

/**
 * Returns the date of this day's file, according to the
 * [PtoPreferences.PREF_NEWPAGE_TIME] preference.
 */
val PtoPreferences.todayDate: LocalDate get() = getTodayDate(LocalDateTime.now())

/**
 * Returns the file for the given time, respecting the [PtoPreferences.PREF_NEWPAGE_TIME]
 * preference.
 */
fun PtoPreferences.getTodayDate(now: LocalDateTime): LocalDate {
    return when {
        now.toLocalTime() < newpageTime -> now.minusDays(1).toLocalDate()
        else -> now.toLocalDate()
    }
}

/**
 * Returns the date and time represented by [time] on the page for [pageDate], respecting the
 * [PtoPreferences.PREF_NEWPAGE_TIME] preference.
 */
fun PtoPreferences.getDateTimeOnPage(time: LocalTime, pageDate: LocalDate): LocalDateTime {
    return time.toDateTimeOnPage(pageDate, newpageTime)
}

/**
 * Returns the date and time for `this` on the page for [pageDate], respecting the
 * [newpageTime] according to the [PtoPreferences.PREF_NEWPAGE_TIME] preference.
 */
fun LocalTime.toDateTimeOnPage(pageDate: LocalDate, newpageTime: LocalTime): LocalDateTime {
    return when {
        this < newpageTime -> pageDate.plusDays(1)
        else -> pageDate
    }.atTime(this)
}
