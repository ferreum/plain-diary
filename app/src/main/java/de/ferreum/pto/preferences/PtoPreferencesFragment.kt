/*
 * Diary - Personal diary for Android
 *
 * Copyright (C) 2017  Bill Farmer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Bill Farmer william j farmer [at] yahoo [dot] co [dot] uk.
 */
package de.ferreum.pto.preferences

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.emoji2.text.EmojiCompatInitializer
import androidx.preference.CheckBoxPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.recyclerview.widget.RecyclerView
import de.ferreum.pto.BuildConfig
import de.ferreum.pto.R
import de.ferreum.pto.backup.BackupService
import de.ferreum.pto.backup.ImportZipFragment
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_AUTOSTART_QUICKNOTES
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_INDEX_PAGE
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_LOGFILE_DATE
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_NEWPAGE_TIME
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_REMINDERS_ENABLED
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_SUPPORT_EMOJI_ENABLED
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_TEMPLATE_PAGE
import de.ferreum.pto.util.showNotificationsRequiredDialog
import de.ferreum.pto.util.startNotificationSettings

class PtoPreferencesFragment : PreferenceFragmentCompat(), OnSharedPreferenceChangeListener {

    override fun onCreateRecyclerView(
        inflater: LayoutInflater,
        parent: ViewGroup,
        savedInstanceState: Bundle?
    ): RecyclerView {
        val recyclerView = super.onCreateRecyclerView(inflater, parent, savedInstanceState)
        ViewCompat.setOnApplyWindowInsetsListener(recyclerView) { _, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or WindowInsetsCompat.Type.displayCutout()
                    or WindowInsetsCompat.Type.ime()
            )
            recyclerView.setPadding(insets.left, insets.top, insets.right, insets.bottom)
            windowInsets.inset(insets)
        }
        return recyclerView
    }

    private val quickNotePermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { success ->
        if (!success) {
            preferenceScreen.findPreference<CheckBoxPreference>(PREF_AUTOSTART_QUICKNOTES)
                ?.isChecked = false
            requireContext().showNotificationsRequiredDialog()
        }
    }

    private val reminderPermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { success ->
        if (success) {
            checkPowerManagerExclusion()
        } else {
            preferenceScreen.findPreference<CheckBoxPreference>(PREF_REMINDERS_ENABLED)
                ?.isChecked = false
            requireContext().showNotificationsRequiredDialog()
        }
    }

    private val backupPermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { success ->
        if (success) {
            startBackup()
        } else {
            requireContext().showNotificationsRequiredDialog()
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)

        requirePreference<TimePickerPreference>(PREF_NEWPAGE_TIME).summaryProvider =
            TimeSummaryProvider()
        requirePreference<DatePickerPreference>(PREF_INDEX_PAGE).summaryProvider =
            DateSummaryProvider()
        requirePreference<DatePickerPreference>(PREF_TEMPLATE_PAGE).summaryProvider =
            DateSummaryProvider()
        requirePreference<DatePickerPreference>(PREF_LOGFILE_DATE).summaryProvider =
            DateSummaryProvider()
        requirePreference<Preference>(KEY_ABOUT).summary =
            getString(R.string.pref_about_summ, BuildConfig.VERSION_NAME)
    }

    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences!!
            .registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        preferenceScreen.sharedPreferences!!
            .unregisterOnSharedPreferenceChangeListener(this)
        super.onPause()
    }

    override fun onDisplayPreferenceDialog(preference: Preference) {
        when (preference) {
            is AboutPreference ->
                AboutDialogFragment().show(childFragmentManager, null)

            is ExportZipPreference -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    backupPermissionRequest.launch(Manifest.permission.POST_NOTIFICATIONS)
                } else {
                    startBackup()
                }
            }

            is ImportZipPreference ->
                ImportZipFragment().show(childFragmentManager, ImportZipFragment.FRAGMENT_TAG)

            is ReminderMenuPreference ->
                requireContext().startNotificationSettings(preference.channel)

            else -> super.onDisplayPreferenceDialog(preference)
        }
    }

    override fun onSharedPreferenceChanged(preferences: SharedPreferences, key: String?) {
        when (key) {
            PREF_AUTOSTART_QUICKNOTES -> {
                if (preferences.getBoolean(key, false)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                        quickNotePermissionRequest.launch(Manifest.permission.POST_NOTIFICATIONS)
                    }
                }
            }

            PREF_REMINDERS_ENABLED -> {
                if (preferences.getBoolean(key, false)) {
                    if (Build.VERSION.SDK_INT >= 33) {
                        reminderPermissionRequest.launch(Manifest.permission.POST_NOTIFICATIONS)
                    } else {
                        checkPowerManagerExclusion()
                    }
                }
            }

            PREF_SUPPORT_EMOJI_ENABLED -> {
                if (preferences.getBoolean(key, true)) {
                    EmojiCompatInitializer().create(requireContext())
                }
            }
        }
    }

    private fun startBackup() {
        ContextCompat.startForegroundService(
            requireContext(),
            BackupService.createBackupIntent(requireContext())
        )
    }

    private fun checkPowerManagerExclusion() {
        val pm = requireContext().getSystemService(PowerManager::class.java)
        val packageName = requireContext().packageName
        @SuppressLint("BatteryLife")
        if (!pm.isIgnoringBatteryOptimizations(packageName)) {
            startActivity(
                Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
                    .setData(Uri.parse("package:$packageName"))
            )
        }
    }

    private fun <T : Preference> requirePreference(key: String): T = findPreference(key)!!

    companion object {
        private const val KEY_ABOUT = "pref_about"
    }
}
