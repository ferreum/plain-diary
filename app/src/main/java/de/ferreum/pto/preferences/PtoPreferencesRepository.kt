package de.ferreum.pto.preferences

import android.content.SharedPreferences
import androidx.core.content.edit
import de.ferreum.pto.Constants
import de.ferreum.pto.preferences.DatePickerPreference.Companion.getLocalDate
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_AUTOSTART_QUICKNOTES
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_DARKMODE
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_EDITOR_FONT_SIZE
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_ENABLE_TEXTCLASSIFIER
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_INDEX_PAGE
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_LOGFILE_DATE
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_LOGFILE_ENABLE
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_NEWPAGE_TIME
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_QUICKNOTES_ENABLE_EXPANDED
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_QUICKNOTES_INVISIBLE_ICON
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_QUICKNOTES_NOTIFICATION_TITLE
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_REMINDERS_ENABLED
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_SUGGESTIONS_ENABLED
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_SUPPORT_EMOJI_ENABLED
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_TEMPLATE_PAGE
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_USE_INDEX
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_USE_ISODATES
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_USE_TEMPLATE
import de.ferreum.pto.preferences.TimePickerPreference.Companion.getLocalTime
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.awaitCancellation
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.conflate
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.withContext
import java.time.LocalDate
import java.time.LocalTime

interface PtoPreferencesRepository {
    val preferencesFlow: Flow<PtoPreferences>
}

class PtoPreferencesRepositoryImpl(
    private val preferences: SharedPreferences,
    scope: CoroutineScope,
    errorLogger: suspend (Throwable) -> Unit,
) : PtoPreferencesRepository {
    private var mayReset = true

    override val preferencesFlow: Flow<PtoPreferences> = callbackFlow {
        val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, _ ->
            trySend(Unit)
        }
        try {
            preferences.registerOnSharedPreferenceChangeListener(listener)
            send(Unit)
            awaitCancellation()
        } finally {
            preferences.unregisterOnSharedPreferenceChangeListener(listener)
        }
    }.conflate().map {
        withContext(Dispatchers.IO) {
            PtoPreferences(
                isIndexPageEnabled = preferences.getBoolean(PREF_USE_INDEX, false),
                isTemplateEnabled = preferences.getBoolean(PREF_USE_TEMPLATE, false),
                isAutostartQuickNotes = preferences.getBoolean(PREF_AUTOSTART_QUICKNOTES, false),
                isQuickNotesExpansionEnabled = preferences.getBoolean(PREF_QUICKNOTES_ENABLE_EXPANDED, true),
                isTextClassifierEnabled = preferences.getBoolean(PREF_ENABLE_TEXTCLASSIFIER, true),
                isSupportEmojiEnabled = preferences.getBoolean(PREF_SUPPORT_EMOJI_ENABLED, true),
                isSuggestionsEnabled = preferences.getBoolean(PREF_SUGGESTIONS_ENABLED, true),
                editorFontSize = preferences.getString(PREF_EDITOR_FONT_SIZE, null)?.toInt() ?: 18,
                isIsoDateFormatEnabled = preferences.getBoolean(PREF_USE_ISODATES, true),
                indexPageDate = preferences.getLocalDate(PREF_INDEX_PAGE, LocalDate.of(2000, 1, 2)),
                templatePageDate = preferences.getLocalDate(PREF_TEMPLATE_PAGE, LocalDate.of(2000, 1, 1)),
                customNotificationTitle = preferences.getString(PREF_QUICKNOTES_NOTIFICATION_TITLE, null),
                useInvisibleNotificationIcon = preferences.getBoolean(PREF_QUICKNOTES_INVISIBLE_ICON, false),
                darkmode = preferences.getString(PREF_DARKMODE, null) ?: "system",
                newpageTime = preferences.getLocalTime(PREF_NEWPAGE_TIME, LocalTime.MIDNIGHT),
                isEventRemindersEnabled = preferences.getBoolean(PREF_REMINDERS_ENABLED, false),
                isLogfileEnabled = preferences.getBoolean(PREF_LOGFILE_ENABLE, false),
                logfileDate = preferences.getLocalDate(PREF_LOGFILE_DATE, LocalDate.of(2000, 1, 3)),
            ).also { mayReset = false }
        }
    }.retry { throwable ->
        errorLogger(throwable)
        mayReset && run {
            preferences.edit(commit = true) { clearKeepingLogfile() }
            mayReset = false
            true
        }
    }.conflate().shareIn(scope, SharingStarted.WhileSubscribed(5000, 0), replay = 1)

    private fun SharedPreferences.Editor.clearKeepingLogfile() {
        clear()
        val map = preferences.all
        if (map[PREF_LOGFILE_ENABLE] as? Boolean == true) {
            putBoolean(PREF_LOGFILE_ENABLE, true)
            (map[PREF_LOGFILE_DATE] as? String)?.toSanitizedDateOrNull()?.let {
                putString(PREF_LOGFILE_DATE, it.toString())
            }
        }
    }

    private fun String.toSanitizedDateOrNull(): LocalDate? {
        val date = try {
            LocalDate.parse(this)
        } catch (e: Exception) {
            return null
        }
        if (date.year !in Constants.YEAR_RANGE) return null
        return date
    }

}
