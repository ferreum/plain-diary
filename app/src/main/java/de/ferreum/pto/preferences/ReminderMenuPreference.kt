package de.ferreum.pto.preferences

import android.content.Context
import android.util.AttributeSet
import androidx.preference.DialogPreference
import de.ferreum.pto.R

class ReminderMenuPreference(context: Context, attrs: AttributeSet?) : DialogPreference(context, attrs) {
    val channel: String?

    init {
        val array = context.obtainStyledAttributes(attrs, R.styleable.NotificationMenuPreference)
        channel = array.getString(R.styleable.NotificationMenuPreference_notificationChannel)
        array.recycle()
    }
}
