/*
 * Diary - Personal diary for Android
 *
 * Copyright (C) 2017  Bill Farmer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Bill Farmer william j farmer [at] yahoo [dot] co [dot] uk.
 */
package de.ferreum.pto.preferences

import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Context
import android.content.SharedPreferences
import android.content.res.TypedArray
import android.util.AttributeSet
import android.widget.TimePicker
import androidx.preference.Preference
import java.time.LocalTime

class TimePickerPreference(
    context: Context,
    attrs: AttributeSet?
) : Preference(context, attrs), OnTimeSetListener {

    private var value = DEFAULT_TIME

    override fun onClick() {
        val prevValue = value
        val dialog = TimePickerDialog(context, this, prevValue.hour, prevValue.minute, true)
        dialog.setOnCancelListener { setValue(prevValue) }
        dialog.show()
    }

    override fun onSetInitialValue(defaultValue: Any?) {
        val value = getPersistedString(defaultValue as String?)
        setValue(value?.let { LocalTime.parse(it) } ?: DEFAULT_TIME)
    }

    override fun onGetDefaultValue(a: TypedArray, index: Int): Any? = a.getString(index)

    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
        setValue(LocalTime.of(hourOfDay, minute))
    }

    private fun setValue(time: LocalTime) {
        value = time
        persistString(time.toString())
        notifyChanged()
    }

    fun getValue(): LocalTime = value

    companion object {
        private val DEFAULT_TIME: LocalTime = LocalTime.MIDNIGHT

        fun SharedPreferences.getLocalTime(key: String, defaultValue: LocalTime): LocalTime {
            return getString(key, null)?.let { LocalTime.parse(it) } ?: defaultValue
        }
    }
}
