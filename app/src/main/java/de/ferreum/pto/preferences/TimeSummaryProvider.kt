package de.ferreum.pto.preferences

import androidx.preference.Preference.SummaryProvider
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

internal class TimeSummaryProvider : SummaryProvider<TimePickerPreference> {
    private val formatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)!!

    override fun provideSummary(preference: TimePickerPreference): CharSequence {
        return preference.getValue().format(formatter)
    }
}
