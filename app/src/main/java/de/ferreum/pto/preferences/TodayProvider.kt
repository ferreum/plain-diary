package de.ferreum.pto.preferences

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_TIMEZONE_CHANGED
import android.content.Intent.ACTION_TIME_CHANGED
import android.content.Intent.ACTION_TIME_TICK
import android.content.IntentFilter
import de.ferreum.pto.util.TimeProvider
import de.ferreum.pto.util.myTransformLatest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ProducerScope
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch
import java.time.Instant
import java.time.LocalDate

class MinuteTicker(
    context: Context,
    scope: CoroutineScope,
    logger: Logger,
) {
    @SuppressLint("InlinedApi")
    val ticker = callbackFlow {
        var failsafeJob = launchFailsafeJob()

        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if (intent?.action !in INTENT_ACTIONS) return
                trySend(Unit)
                    .exceptionOrNull()
                    ?.let { logger.logCriticalException(it, "exception in broadcastReceiver") }

                failsafeJob.cancel()
                failsafeJob = launchFailsafeJob()
            }
        }
        context.registerReceiver(receiver,
            IntentFilter().apply {
                INTENT_ACTIONS.forEach { addAction(it) }
            },
            Context.RECEIVER_NOT_EXPORTED,
        )
        send(Unit)
        awaitClose { context.unregisterReceiver(receiver) }
    }.buffer(Channel.CONFLATED)
        .shareIn(scope, SharingStarted.WhileSubscribed(5000, 0), replay = 1)

    /** Failsafe job to update time in case system restricts [ACTION_TIME_TICK]. */
    private fun ProducerScope<Unit>.launchFailsafeJob() = launch {
        while (true) {
            delay(62000)
            send(Unit)
        }
    }

    companion object {
        private val INTENT_ACTIONS = listOf(
            ACTION_TIMEZONE_CHANGED,
            ACTION_TIME_CHANGED,
            ACTION_TIME_TICK,
        )
    }
}

interface TodayProvider {
    val todayFlow: Flow<LocalDate>
}

class TodayProviderImpl(
    minuteTicker: MinuteTicker,
    preferencesRepository: PtoPreferencesRepository,
    timeProvider: TimeProvider,
    scope: CoroutineScope,
) : TodayProvider {
    override val todayFlow = preferencesRepository.preferencesFlow
        .distinctUntilChangedBy { it.newpageTime }
        .myTransformLatest { prefs ->
            minuteTicker.ticker.collect {
                emit(prefs.getTodayDate(timeProvider.localDateTime()))
            }
        }
        .distinctUntilChanged()
        .buffer(Channel.CONFLATED) // configures shareIn
        .shareIn(scope, SharingStarted.WhileSubscribed(0, 0), replay = 1)
}

/**
 * Get updates when the given date starts and stops being in the past, today, or future
 * by the change of time.
 */
fun TodayProvider.comparedToday(date: LocalDate) = todayFlow.map { date.compareTo(it) }

fun MinuteTicker.comparedTime(
    instant: Instant,
    timeProvider: TimeProvider,
) = ticker.map { timeProvider.instant().compareTo(instant) }
