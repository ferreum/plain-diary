package de.ferreum.pto.quicknotes

import de.ferreum.pto.files.PageWriteSignal
import de.ferreum.pto.files.PageWriteSignal.PageWrite
import de.ferreum.pto.files.PageWriteSignal.PageWriteFlag
import de.ferreum.pto.preferences.PtoFileHelper
import de.ferreum.pto.preferences.PtoPreferencesRepository
import de.ferreum.pto.preferences.getTodayDate
import de.ferreum.pto.quicknotes.NoteAppender.FileAccessor
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

internal class NoteAppender(
    private val preferencesRepository: PtoPreferencesRepository,
    private val fileHelper: Deferred<PtoFileHelper>,
    private val pageWriteSignal: PageWriteSignal,
    fileAccessor: FileAccessor? = null,
) {

    private val fileAccessor: FileAccessor =
        fileAccessor ?: FileAccessor { fileHelper, date -> fileHelper.getDayFile(date) }

    suspend fun appendNote(dateTime: LocalDateTime, text: String) {
        val preferences = preferencesRepository.preferencesFlow.first()
        val date = preferences.getTodayDate(dateTime)
        val file = fileAccessor(fileHelper.await(), date)

        val contentBytes = formatText(dateTime, text).toByteArray()

        withContext(Dispatchers.IO) {
            file.parentFile?.mkdirs()

            FileOutputStream(file, true).use { out ->
                out.write(contentBytes)
                out.flush()
                out.fd.sync()
            }
        }

        pageWriteSignal.notifyPageWrite(PageWrite(date, null, WRITE_FLAGS))
    }

    private fun formatText(dateTime: LocalDateTime, inputText: String = ""): String {
        val sb = StringBuilder("\n")
        sb.append(formatPrefix(dateTime))
        if (inputText.isNotEmpty()) {
            val sanitized = inputText.replace('\n', ' ').replace('\r', ' ')
            sb.append(" ").append(sanitized)
        }
        sb.append("\n")
        return sb.toString()
    }

    fun formatPrefix(dateTime: LocalDateTime): String {
        val time = dateTime.atZone(ZoneId.systemDefault())
        val timeText = time.format(TIME_FORMATTER)
        return "+ $timeText"
    }

    fun interface FileAccessor : (PtoFileHelper, LocalDate) -> File

    companion object {
        private val TIME_FORMATTER = DateTimeFormatter.ofPattern("H:mm")
        private val WRITE_FLAGS = setOf(PageWriteFlag.NO_EVENTS)
    }

}
