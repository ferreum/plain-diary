package de.ferreum.pto.quicknotes

import android.text.NoCopySpan
import android.text.Selection
import android.text.SpanWatcher
import android.text.Spannable
import android.text.Spanned

internal class PreventPrefixSelectionSpan(
    var textPrefix: String,
) : SpanWatcher, NoCopySpan {

    override fun onSpanAdded(text: Spannable, what: Any?, start: Int, end: Int) {
        enforceSelection(text, what, start)
    }

    override fun onSpanRemoved(text: Spannable, what: Any?, start: Int, end: Int) {
    }

    override fun onSpanChanged(text: Spannable, what: Any?, ostart: Int, oend: Int, nstart: Int, nend: Int) {
        enforceSelection(text, what, nstart)
    }

    private fun enforceSelection(text: Spannable, what: Any?, pos: Int) {
        when (what) {
            Selection.SELECTION_START -> if (pos < textPrefix.length && text.startsWith(textPrefix)) {
                text.setSpan(
                    Selection.SELECTION_START, textPrefix.length, textPrefix.length,
                    Spanned.SPAN_POINT_POINT or Spanned.SPAN_INTERMEDIATE
                )
            }

            Selection.SELECTION_END -> if (pos < textPrefix.length && text.startsWith(textPrefix)) {
                text.setSpan(
                    Selection.SELECTION_END, textPrefix.length, textPrefix.length,
                    Spanned.SPAN_POINT_POINT
                )
            }
        }
    }

}
