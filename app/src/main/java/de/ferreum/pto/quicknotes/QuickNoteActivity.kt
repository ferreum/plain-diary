package de.ferreum.pto.quicknotes

import android.os.Bundle
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.activity.addCallback
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.Guideline
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import de.ferreum.pto.R
import de.ferreum.pto.page.ViewModelFactory
import de.ferreum.pto.util.combineTerminal
import de.ferreum.pto.util.setupSoftInputVisible
import de.ferreum.pto.util.textObserverFlow
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

internal class QuickNoteActivity : AppCompatActivity() {

    private var messageInput: EditText? = null

    private var timeoutJob: Job? = null

    private val viewModel: QuickNoteViewModel by viewModels {
        ViewModelFactory(this, this, noteAppenderFileAccessor = fileAccessor)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_quicknote)
        val container: View = findViewById(R.id.quicknoteContainer)
        val innerContent: View = findViewById(R.id.innerContent)
        val messageInput: QuickNoteEditText = findViewById(R.id.messageInput)
        this.messageInput = messageInput
        val confirmButton: View = findViewById(R.id.confirmButton)
        val bottomInsetsGuide: Guideline = findViewById(R.id.bottomInsetsGuide)

        ViewCompat.setOnApplyWindowInsetsListener(innerContent) { v, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars()
                    or WindowInsetsCompat.Type.displayCutout()
                    or WindowInsetsCompat.Type.ime()
            )
            v.setPadding(insets.left, insets.top, insets.right, 0)
            bottomInsetsGuide.setGuidelineEnd(insets.bottom)
            WindowInsetsCompat.CONSUMED
        }

        messageInput.setTextPrefix(viewModel.textPrefix)

        val swipeToDismissDetector = SwipeToDismissDetector.forView(container)
        swipeToDismissDetector.setOnSwipeDismissListener {
            viewModel.saveNoteAndClose(messageInput.getNoteText(), saveIfEmpty = false)
        }

        combineTerminal(
            viewModel.isSaving,
            viewModel.isDone,
        ) { a, b ->
            val enable = !a && !b
            confirmButton.isEnabled = enable
            messageInput.isEnabled = enable
        }.launchIn(lifecycleScope)
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.isDone.collect { if (it && !isFinishing) finish() }
            }
        }
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                viewModel.error.collect { event ->
                    event.once {
                        swipeToDismissDetector.reset()
                        showError(it)
                        timeoutJob?.cancel()
                    }
                }
            }
        }

        // Enforce soft-wrapping while not allowing any hard line breaks in the message. Setting
        // this via xml attributes doesn't seem to have the same effect.
        messageInput.setHorizontallyScrolling(false)
        messageInput.maxLines = 5
        messageInput.doAfterTextChanged { resetTimeout() }
        messageInput.textObserverFlow { it.removePrefix(viewModel.textPrefix).isEmpty() }
            .distinctUntilChanged()
            .onEach { empty ->
                val attr = if (empty) {
                    android.R.attr.textColorHint
                } else {
                    android.R.attr.textColorPrimary
                }
                val typedValue = TypedValue()
                check(theme.resolveAttribute(attr, typedValue, true)) { "get attribute failed" }
                messageInput.setTextColor(getColorStateList(typedValue.resourceId))
            }
            .launchIn(lifecycleScope)

        messageInput.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                viewModel.saveNoteAndClose(messageInput.getNoteText())
                true
            } else {
                false
            }
        }
        confirmButton.setOnClickListener {
            viewModel.saveNoteAndClose(messageInput.getNoteText())
        }

        onBackPressedDispatcher.addCallback {
            viewModel.saveNoteAndClose(messageInput.getNoteText(), saveIfEmpty = false)
        }
    }

    override fun onResume() {
        super.onResume()
        setupSoftInputVisible(messageInput!!)
        resetTimeout()
    }

    override fun onPause() {
        timeoutJob?.cancel()
        super.onPause()
    }

    override fun onStop() {
        if (!isChangingConfigurations) {
            viewModel.saveNoteAndClose(messageInput!!.getNoteText(), saveIfEmpty = false)
        }
        super.onStop()
    }

    override fun onDestroy() {
        messageInput = null
        super.onDestroy()
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        resetTimeout()
        return super.dispatchTouchEvent(ev)
    }

    private fun resetTimeout() {
        timeoutJob?.cancel()
        timeoutJob = lifecycleScope.launch {
            delay(inactivityTimeoutMillis)
            viewModel.saveNoteAndClose(messageInput!!.getNoteText(), saveIfEmpty = false)
        }
    }

    private fun showError(throwable: Throwable) {
        AlertDialog.Builder(this)
            .setTitle(R.string.error)
            .setMessage(throwable.toString())
            .setPositiveButton(android.R.string.ok, null)
            .setOnDismissListener { resetTimeout() }
            .show()
    }

    private fun EditText.getNoteText(): String {
        return text.toString().removePrefix(viewModel.textPrefix)
    }

    companion object {
        @VisibleForTesting
        var fileAccessor: NoteAppender.FileAccessor? = null

        @VisibleForTesting
        var inactivityTimeoutMillis: Long = 30000L
    }

}
