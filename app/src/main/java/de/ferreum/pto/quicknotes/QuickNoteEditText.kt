package de.ferreum.pto.quicknotes

import android.content.Context
import android.text.InputFilter
import android.text.Spanned
import android.util.AttributeSet
import android.view.HapticFeedbackConstants
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.text.buildSpannedString

class QuickNoteEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = androidx.appcompat.R.attr.editTextStyle,
) : AppCompatEditText(context, attrs, defStyleAttr) {

    private val preventSelectionSpan = PreventPrefixSelectionSpan("")

    private var textPrefix: String = ""

    private var prevSelStart = -1
    private var prevSelEnd = -1
    private var selDidChange: Boolean = false

    /**
     * EditText notifies selection change within constructor. Need flag to handle init correctly.
     */
    private var initialized = false

    init {
        initialized = true
    }

    fun setTextPrefix(textPrefix: String) {
        this.textPrefix = textPrefix
        preventSelectionSpan.textPrefix = textPrefix
        text!!.apply {
            insert(0, textPrefix)
            setSpan(preventSelectionSpan, 0, length,
                Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        }
        filters = arrayOf(InputFilter { source, start, end, dest, dstart, dend ->
            if (dstart < textPrefix.length) {
                val result = buildString {
                    append(dest, 0, dstart)
                    val insertLen = (end - start).coerceAtMost(textPrefix.length - dstart)
                    append(source, start, start + insertLen)
                    val remainder = textPrefix.length - dstart - insertLen
                    if (remainder > 0) {
                        append(dest, dend, dest.length.coerceAtMost(dend + remainder))
                    }
                }
                val commonPrefix = result.commonPrefixWith(textPrefix)
                if (commonPrefix != textPrefix) {
                    return@InputFilter buildSpannedString {
                        append(textPrefix, dstart, textPrefix.length)
                        if (commonPrefix.length < end) {
                            append(source, commonPrefix.length, end)
                        }
                    }
                }
            }
            null
        })

    }

    override fun onSelectionChanged(selStart: Int, selEnd: Int) {
        if (!initialized || (selStart >= textPrefix.length && selEnd >= textPrefix.length)) {
            selDidChange = selStart != prevSelStart || selEnd != prevSelEnd
            prevSelStart = selStart
            prevSelEnd = selEnd
        }
        super.onSelectionChanged(selStart, selEnd)
    }

    override fun performHapticFeedback(feedbackConstant: Int, flags: Int): Boolean {
        if (feedbackConstant == HapticFeedbackConstants.TEXT_HANDLE_MOVE) {
            if (!selDidChange) {
                // suppress haptic feedback for suppressed selection change
                return false
            }
        }
        return super.performHapticFeedback(feedbackConstant, flags)
    }

}
