package de.ferreum.pto.quicknotes

import androidx.lifecycle.ViewModel
import de.ferreum.pto.preferences.Logger
import de.ferreum.pto.util.Event
import de.ferreum.pto.util.TimeProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

internal class QuickNoteViewModel(
    private val applicationScope: CoroutineScope,
    private val noteAppender: NoteAppender,
    timeProvider: TimeProvider,
    private val logger: Logger,
) : ViewModel() {

    private val startDateTime = timeProvider.localDateTime()

    val textPrefix = noteAppender.formatPrefix(startDateTime) + " "

    private val _isSaving = MutableStateFlow(false)
    val isSaving: Flow<Boolean> get() = _isSaving

    private val _isDone = MutableStateFlow(false)
    val isDone: Flow<Boolean> get() = _isDone

    private val _error = MutableStateFlow(Event.handled<Throwable>())
    val error: Flow<Event<Throwable>> get() = _error

    fun saveNoteAndClose(text: String, saveIfEmpty: Boolean = true) {
        if (_isSaving.value || _isDone.value) return
        if (!saveIfEmpty && text.isEmpty()) {
            _isDone.value = true
            return
        }
        // in applicationScope to allow saving by closing activity
        applicationScope.launch {
            appendNoteAndClose(text)
        }
    }

    private suspend fun appendNoteAndClose(text: String) {
        if (_isDone.value || !_isSaving.compareAndSet(expect = false, update = true)) return
        try {
            noteAppender.appendNote(startDateTime, text)
            _isDone.value = true
        } catch (e: Throwable) {
            logger.suspendLogException(e, "append failed")
            _error.value = Event(e)
        } finally {
            _isSaving.value = false
        }
    }

}
