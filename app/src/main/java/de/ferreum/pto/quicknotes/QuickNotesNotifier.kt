package de.ferreum.pto.quicknotes

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import de.ferreum.pto.MainActivity
import de.ferreum.pto.R
import de.ferreum.pto.preferences.PtoPreferences
import de.ferreum.pto.util.tryNotify

internal object QuickNotesNotifier {

    private const val NOTIFICATION_CHANNEL = "quicknotes"

    fun createNotificationChannel(context: Context) {
        NotificationManagerCompat.from(context).createNotificationChannel(
            NotificationChannel(
                NOTIFICATION_CHANNEL,
                context.getText(R.string.quicknotes_notification_channel_title),
                NotificationManager.IMPORTANCE_LOW
            )
        )
    }

    fun showNotification(context: Context, preferences: PtoPreferences) {
        NotificationManagerCompat.from(context)
            .tryNotify(R.id.quicknotes_notification, createNotification(context, preferences))
    }

    fun createNotification(context: Context, preferences: PtoPreferences): Notification {
        val contentPIntent = PendingIntent.getActivity(context,
            R.id.quicknotes_intent_main,
            Intent(context, QuickNoteActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK),
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT)
        val title = preferences.customNotificationTitle?.takeIf { it.isNotEmpty() }
            ?: context.getText(R.string.quicknotes_notification_title)
        val icon = when {
            preferences.useInvisibleNotificationIcon -> R.drawable.ic_notif_invisible
            else -> R.drawable.ic_notif_small
        }
        return NotificationCompat.Builder(context, NOTIFICATION_CHANNEL).apply {
            setSmallIcon(icon)
            setOngoing(true) // Ongoing to prevent accidentally dismissing it.
            setSilent(true)
            setLocalOnly(true)
            setOnlyAlertOnce(true)
            setContentTitle(title)
            setContentIntent(contentPIntent)
            setShowWhen(false)
            setWhen(0)
            // If set up in the system to hide sensitive content on the lockscreen, this
            // collapsed-only version is shown instead.
            setPublicVersion(
                NotificationCompat.Builder(context, NOTIFICATION_CHANNEL)
                    .setSmallIcon(R.drawable.ic_notif_small)
                    .setContentTitle(title)
                    .setShowWhen(false)
                    .setWhen(0)
                    .build()
            )
            if (preferences.isQuickNotesExpansionEnabled) {
                // is always dismissible on 34+
                if (Build.VERSION.SDK_INT < 34) {
                    val dismissPIntent = PendingIntent.getBroadcast(
                        context,
                        R.id.quicknotes_intent_dismiss,
                        Intent(context, QuickNotesReceiver::class.java)
                            .setAction(QuickNotesReceiver.ACTION_DISMISS_NOTIFICATION),
                        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT,
                    )
                    addAction(
                        NotificationCompat.Action.Builder(null, context.getText(R.string.quicknotes_action_dismiss_notification), dismissPIntent)
                            .setSemanticAction(NotificationCompat.Action.SEMANTIC_ACTION_DELETE)
                            .build()
                    )
                }

                val launchActivityPIntent = PendingIntent.getActivity(
                    context,
                    R.id.quicknotes_intent_launch_activity,
                    Intent(context, MainActivity::class.java),
                    PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT,
                )
                addAction(
                    NotificationCompat.Action(
                        null,
                        context.getText(R.string.quicknotes_notification_launch_button),
                        launchActivityPIntent
                    )
                )
            }
        }.build()
    }
}
