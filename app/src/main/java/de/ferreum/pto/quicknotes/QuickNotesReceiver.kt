package de.ferreum.pto.quicknotes

import android.app.KeyguardManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationManagerCompat
import de.ferreum.pto.R

internal class QuickNotesReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        when (intent?.action) {
            ACTION_DISMISS_NOTIFICATION -> {
                val kgm = context.getSystemService(KeyguardManager::class.java)
                if (!kgm.isKeyguardLocked) {
                    val notifManager = NotificationManagerCompat.from(context)
                    notifManager.cancel(R.id.quicknotes_notification)
                }
            }
        }
    }

    companion object {
        const val ACTION_DISMISS_NOTIFICATION = "de.ferreum.pto.quicknotes.DISMISS_NOTIFICATION"
    }

}
