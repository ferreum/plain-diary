package de.ferreum.pto.quicknotes

import android.annotation.SuppressLint
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.Scroller
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import de.ferreum.pto.R
import kotlin.math.abs
import kotlin.math.sign

class SwipeToDismissDetector(
    private val view: View,
    private val scroller: Scroller,
) : OnTouchListener {
    fun interface OnSwipeDismissListener {
        fun onSwipeDismiss(view: View)
    }

    private val gestureDetector = GestureDetector(view.context, createGestureListener())

    private var swipeInsets: Insets = Insets.NONE
    private var state = SwipeState.RESTING
    private var onSwipeDismissListener: OnSwipeDismissListener? = null
    private val calculateScrollPositionRunnable = Runnable { calculateScrollPosition() }

    fun setOnSwipeDismissListener(onSwipeDismissListener: OnSwipeDismissListener?) {
        this.onSwipeDismissListener = onSwipeDismissListener
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View, event: MotionEvent): Boolean {
        scroller.forceFinished(true)
        view.removeCallbacks(calculateScrollPositionRunnable)
        if (event.actionMasked == MotionEvent.ACTION_DOWN) {
            if (event.x < swipeInsets.left || event.x > (v.width - swipeInsets.right)
                || event.y < swipeInsets.top || event.y > (v.height - swipeInsets.bottom)
            ) {
                return false
            }
        }
        gestureDetector.onTouchEvent(event)
        when (event.actionMasked) {
            MotionEvent.ACTION_UP -> if (scroller.isFinished) checkScrollEnd(0f)
            MotionEvent.ACTION_CANCEL -> startSettleScroll()
        }
        return true
    }

    fun reset() {
        startSettleScroll()
    }

    private fun calculateScrollPosition() {
        if (scroller.computeScrollOffset()) {
            val currX = scroller.currX
            view.scrollX = currX
            if (state == SwipeState.DISMISSING && abs(currX) > view.width) {
                scroller.forceFinished(true)
                handleDismissed()
            } else {
                view.postOnAnimation(calculateScrollPositionRunnable)
            }
        } else {
            if (state == SwipeState.SETTLING) {
                state = SwipeState.RESTING
            } else if (state == SwipeState.DISMISSING) {
                handleDismissed()
            }
        }
    }

    private fun startSettleScroll() {
        scroller.startScroll(
            view.scrollX, view.scrollY, // start
            -view.scrollX, 0 // delta
        )
        state = SwipeState.SETTLING
        calculateScrollPosition()
    }

    private fun handleDismissed() {
        state = SwipeState.DISMISSED
        onSwipeDismissListener?.onSwipeDismiss(view)
    }

    private fun createGestureListener(): GestureDetector.OnGestureListener {
        return object : GestureDetector.OnGestureListener {
            override fun onDown(e: MotionEvent) = false
            override fun onShowPress(e: MotionEvent) {}
            override fun onSingleTapUp(e: MotionEvent) = view.performClick()

            override fun onScroll(
                e1: MotionEvent?,
                e2: MotionEvent,
                distanceX: Float,
                distanceY: Float
            ): Boolean {
                if (e1 == null) return true
                view.scrollX = Math.round(e1.x - e2.x)
                if (abs(view.scrollX) > view.width) {
                    handleDismissed()
                }
                return true
            }

            override fun onLongPress(e: MotionEvent) {
                view.performLongClick(e.x, e.y)
            }

            override fun onFling(
                e1: MotionEvent?,
                e2: MotionEvent,
                velocityX: Float,
                velocityY: Float
            ): Boolean {
                if (e1 == null) return true
                if (abs(velocityX) < abs(velocityY) * 0.2f) {
                    startSettleScroll()
                    return true
                }
                checkScrollEnd(velocityX)
                return true
            }
        }
    }

    private fun checkScrollEnd(velocityX: Float) {
        var vx = velocityX
        if (abs(view.scrollX) > view.width * 0.6) {
            val minVel = view.width * 4.0f
            if (abs(vx) < minVel) {
                vx = minVel * -sign(view.scrollX.toFloat())
            }
        }
        scroller.fling(
            view.scrollX, view.scrollY, // start
            -vx.toInt(), 0, // velocities
            Int.MIN_VALUE, Int.MAX_VALUE, // x
            0, 0 // y
        )
        if (abs(scroller.finalX) < view.width) {
            startSettleScroll()
        } else {
            state = SwipeState.DISMISSING
            calculateScrollPosition()
        }
    }

    private enum class SwipeState {
        RESTING,
        SETTLING,
        DISMISSING,
        DISMISSED,
    }

    companion object {
        fun forView(view: View): SwipeToDismissDetector {
            val scroller = Scroller(view.context, null, false)
            val detector = SwipeToDismissDetector(view, scroller)
            view.setOnTouchListener(detector)
            val yMargin = view.context.resources.getDimensionPixelSize(R.dimen.swipe_to_dismiss_edge_margin_vertical)
            ViewCompat.setOnApplyWindowInsetsListener(view) { _, windowInsets ->
                val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemGestures())
                detector.swipeInsets = Insets.of(
                    insets.left,
                    insets.top + yMargin,
                    insets.right,
                    insets.bottom + yMargin,
                )
                windowInsets
            }
            return detector
        }
    }
}
