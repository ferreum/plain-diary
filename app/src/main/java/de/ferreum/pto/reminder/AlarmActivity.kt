package de.ferreum.pto.reminder

import android.annotation.SuppressLint
import android.app.KeyguardManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.View
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Guideline
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import de.ferreum.pto.MainActivity
import de.ferreum.pto.R
import de.ferreum.pto.alarmServiceStateProvider
import de.ferreum.pto.preferences.getTodayDate
import de.ferreum.pto.preferencesRepository
import de.ferreum.pto.quicknotes.SwipeToDismissDetector
import de.ferreum.pto.reminder.AlarmServiceStateProvider.AlarmState
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Locale

internal class AlarmActivity : AppCompatActivity() {

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {}
        override fun onServiceDisconnected(name: ComponentName?) {}
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_alarm)
        val container: View = findViewById(R.id.quicknoteContainer)
        val innerContent: ConstraintLayout = findViewById(R.id.innerContent)
        val alarmTitleView: TextView = findViewById(R.id.alarmTitle)
        val alarmMessageView: TextView = findViewById(R.id.alarmMessage)
        val muteButton: View = findViewById(R.id.muteButton)
        val dismissButton: View = findViewById(R.id.dismissButton)
        val openPageButton: View = findViewById(R.id.openPageButton)
        val bottomInsetsGuide: Guideline = findViewById(R.id.bottomInsetsGuide)

        ViewCompat.setOnApplyWindowInsetsListener(innerContent) { v, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars()
                    or WindowInsetsCompat.Type.displayCutout()
            )
            v.setPadding(insets.left, insets.top, insets.right, 0)
            bottomInsetsGuide.setGuidelineEnd(insets.bottom)
            WindowInsetsCompat.CONSUMED
        }

        muteButton.setOnClickListener {
            muteAlarm()
        }
        dismissButton.setOnClickListener {
            dismissAlarm()
            finish()
        }
        openPageButton.setOnClickListener {
            lifecycleScope.launch {
                val state = alarmServiceStateProvider.state.first()
                if (state is AlarmState.Started) {
                    openPageAndFinish(state.reminder.targetTime)
                }
            }
        }

        lifecycleScope.launch {
            alarmServiceStateProvider.state.collect {
                muteButton.isVisible = it is AlarmState.Started && !it.isMuted

                when (it) {
                    is AlarmState.Started -> {
                        alarmTitleView.text = "@" + it.reminder.targetTime.format(
                            DateTimeFormatter.ofPattern("H:mm", Locale.getDefault()))
                        alarmMessageView.text = it.reminder.title
                    }

                    AlarmState.Stopped -> {
                        if (!isFinishing) finish()
                    }
                }
            }
        }

        val swipeToDismissDetector = SwipeToDismissDetector.forView(container)
        swipeToDismissDetector.setOnSwipeDismissListener {
            dismissAlarm()
            finish()
        }
    }

    override fun onResume() {
        super.onResume()

        val intent = Intent(this, AlarmService::class.java)
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onPause() {
        unbindService(serviceConnection)
        super.onPause()
    }

    private fun openPageAndFinish(reminderTime: LocalDateTime) {
        lifecycleScope.launch {
            requestKeyguardUnlock()
            val date = preferencesRepository.preferencesFlow.first()
                .getTodayDate(reminderTime)
            val intent = MainActivity.createPageIntent(
                this@AlarmActivity,
                date = date,
                reminderSelection = reminderTime.toLocalTime(),
            )
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            dismissAlarm()
            finish()
        }
    }

    private fun requestKeyguardUnlock() {
        val kgm = getSystemService(KeyguardManager::class.java)
        if (kgm.isDeviceLocked) {
            kgm.requestDismissKeyguard(this@AlarmActivity, null)
        }
    }

    private fun muteAlarm() {
        startService(AlarmService.createMuteAlarmIntent(this))
    }

    private fun dismissAlarm() {
        startService(AlarmService.createStopAlarmIntent(this))
    }

    companion object {
        fun createAlarmIntent(context: Context): Intent {
            return Intent(context, AlarmActivity::class.java)
        }
    }

}
