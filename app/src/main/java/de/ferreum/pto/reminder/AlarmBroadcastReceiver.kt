package de.ferreum.pto.reminder

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import de.ferreum.pto.alarmSignal
import de.ferreum.pto.applicationScope
import de.ferreum.pto.reminderService
import de.ferreum.pto.util.myTransformLatest
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

internal class AlarmBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {
        if (intent?.action != ACTION_ALARM) return

        val requestCode = intent.getIntExtra(EXTRA_REQUEST_CODE, -1)
        if (requestCode == -1) return

        val res = goAsync()
        context.applicationScope.launch {
            val alarmHandle = WakeupScheduler.AlarmHandle(requestCode)
            context.alarmSignal.notifyAlarm(alarmHandle)
            context.reminderService.reminderState.myTransformLatest {
                if (!it.isProcessing) {
                    delay(200)
                    emit(Unit)
                }
            }.first()
        }.invokeOnCompletion { res.finish() }
    }

    companion object {
        const val ACTION_ALARM = "de.ferreum.pto.reminder.ALARM"
        val EXTRA_REQUEST_CODE = AlarmBroadcastReceiver::class.java.simpleName + ".ALARM"

        fun createIntent(context: Context, alarmHandle: WakeupScheduler.AlarmHandle): Intent {
            return Intent(context, AlarmBroadcastReceiver::class.java)
                .setAction(ACTION_ALARM)
                .putExtra(EXTRA_REQUEST_CODE, alarmHandle.requestCode)
        }
    }
}
