package de.ferreum.pto.reminder

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.ServiceInfo
import android.media.AudioAttributes
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import android.os.SystemClock
import android.os.VibrationAttributes
import android.os.VibrationEffect
import android.os.Vibrator
import android.os.VibratorManager
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.ServiceCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.lifecycle.repeatOnLifecycle
import de.ferreum.pto.R
import de.ferreum.pto.alarmServiceStateProvider
import de.ferreum.pto.applicationScope
import de.ferreum.pto.logger
import de.ferreum.pto.preferencesRepository
import de.ferreum.pto.reminder.AlarmServiceStateProvider.AlarmState
import de.ferreum.pto.util.getTypedParcelableExtra
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import kotlinx.coroutines.withContext
import kotlin.math.min

internal class AlarmService : Service() {

    private lateinit var scope: CoroutineScope

    private var alarmJob: Job? = null

    private lateinit var stateProvider: AlarmServiceStateProvider
    private var startedAlarmState: AlarmState.Started? = null

    private val binder = Binder()
    override fun onBind(intent: Intent?): IBinder = binder

    override fun onCreate() {
        super.onCreate()
        stateProvider = alarmServiceStateProvider
        scope = applicationScope + SupervisorJob()

        scope.launch {
            preferencesRepository.preferencesFlow
                .distinctUntilChangedBy { it.isEventRemindersEnabled }
                .collect {
                    if (!it.isEventRemindersEnabled) {
                        stopForeground(STOP_FOREGROUND_REMOVE)
                        stopSelf()
                    }
                }
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent == null) return START_NOT_STICKY

        when (intent.action) {
            ACTION_ALARM -> {
                val reminder = intent.getTypedParcelableExtra<ReminderParser.Reminder>(EXTRA_REMINDER)
                    ?: return START_NOT_STICKY

                val alarmState = AlarmState.Started(reminder, false)
                startedAlarmState = alarmState
                stateProvider.setState(alarmState)

                if (alarmJob?.isCancelled != false) {
                    alarmJob = launchAlarmJob()
                }

                launchOnDemandForegroundRequest(reminder)
            }
            ACTION_MUTE_ALARM -> {
                stopRingtone()
            }
            ACTION_STOP_ALARM -> {
                stateProvider.setState(AlarmState.Stopped)
                stopRingtone()
                stopForeground(STOP_FOREGROUND_REMOVE)
                stopSelf()
            }
        }

        return START_NOT_STICKY
    }

    private fun launchOnDemandForegroundRequest(reminder: ReminderParser.Reminder) {
        scope.launch {
            val pm = getSystemService(PowerManager::class.java)
            if (pm.isIgnoringBatteryOptimizations(packageName)) {
                if (startForeground(reminder)) {
                    return@launch
                }
            }
            ProcessLifecycleOwner.get().repeatOnLifecycle(Lifecycle.State.RESUMED) {
                if (startForeground(reminder)) {
                    this@launch.cancel()
                }
            }
        }
    }

    @SuppressLint("InlinedApi")
    private suspend fun startForeground(reminder: ReminderParser.Reminder): Boolean {
        val notification = ReminderNotifier(this@AlarmService).createNotification(
            ReminderNotifier.CHANNEL_ID_FOREGROUND,
            reminder
        )
        try {
            ServiceCompat.startForeground(
                this,
                R.id.reminder_notification_foreground,
                notification,
                ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK
            )
            return true
        } catch (e: Exception) {
            logger.suspendLogException(e, "AlarmService.startForeground failed", "AlarmService")
            return false
        }
    }

    override fun onDestroy() {
        stateProvider.setState(AlarmState.Stopped)
        dismissNotification()
        scope.cancel()
        super.onDestroy()
    }

    private fun stopRingtone() {
        alarmJob?.cancel()
        alarmJob = null
    }

    private fun dismissNotification() {
        NotificationManagerCompat.from(this)
            .cancel(R.id.reminder_notification_foreground)
    }

    private fun launchAlarmJob() = scope.launch {
        val pm = getSystemService(PowerManager::class.java)
        val wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "pto:alarm_service")
        wakeLock.setReferenceCounted(false)

        var ringtone: Ringtone? = null
        var vibrator: Vibrator? = null
        try {
            wakeLock.acquire(15000)

            val audioStartDelay = 10000L
            delay(audioStartDelay)

            val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)!!
            ringtone = loadRingtone(uri)
            val startTime = SystemClock.elapsedRealtime()
            val startVolume = 0.0f
            ringtone.setVolumeCompat(startVolume)

            ringtone.play()

            var volume = startVolume
            while (volume < 1f) {
                wakeLock.acquire(1000)
                delay(500)
                val now = SystemClock.elapsedRealtime()
                volume = min(1f, startVolume + (now - startTime) / 120000f)
                ringtone.setVolumeCompat(volume)
            }

            vibrator = getVibrator()
            vibrator.vibrateCompat(VibrationEffect.createWaveform(longArrayOf(1500, 500), 0))

            val delayMillis = 600000 + startTime - SystemClock.elapsedRealtime() - audioStartDelay
            wakeLock.acquire(delayMillis + 5000)
            delay(delayMillis)

            notifyMissedReminder()
        } finally {
            kotlin.runCatching { vibrator?.cancel() }
                .onFailure { logger.suspendLogException(it) }
            kotlin.runCatching { ringtone?.stop() }
                .onFailure { logger.suspendLogException(it) }
            stateProvider.setMuted()
            wakeLock.release()
        }
    }

    private fun getVibrator(): Vibrator {
        return if (Build.VERSION.SDK_INT < 31) {
            getSystemService(VIBRATOR_SERVICE) as Vibrator
        } else {
            getSystemService(VibratorManager::class.java).defaultVibrator
        }
    }

    private suspend fun loadRingtone(uri: Uri): Ringtone {
        return withContext(Dispatchers.IO) {
            val ringtone = RingtoneManager.getRingtone(this@AlarmService, uri)
            if (Build.VERSION.SDK_INT >= 28) {
                ringtone.isLooping = true
            }
            ringtone.audioAttributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_ALARM)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build()
            ringtone
        }
    }

    private fun notifyMissedReminder() {
        val state = startedAlarmState ?: return
        ReminderNotifier(this@AlarmService).notifyMissed(state.reminder)
    }

    private fun Ringtone.setVolumeCompat(vol: Float) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            volume = vol
        }
    }

    private fun Vibrator.vibrateCompat(effect: VibrationEffect) {
        if (Build.VERSION.SDK_INT >= 33) {
            vibrate(effect, VibrationAttributes.createForUsage(VibrationAttributes.USAGE_ALARM))
        } else {
            vibrate(effect)
        }
    }

    companion object {
        private const val ACTION_ALARM = "de.ferreum.pto.reminder.ALARM"
        private const val ACTION_MUTE_ALARM = "de.ferreum.pto.reminder.MUTE_ALARM"
        private const val ACTION_STOP_ALARM = "de.ferreum.pto.reminder.STOP_ALARM"
        private const val EXTRA_REMINDER = "de.ferreum.pto.reminder.REMINDER"

        fun createAlarmIntent(context: Context, reminder: ReminderParser.Reminder): Intent {
            return Intent(context, AlarmService::class.java)
                .setAction(ACTION_ALARM)
                .putExtra(EXTRA_REMINDER, reminder)
        }

        fun createStopAlarmIntent(context: Context): Intent {
            return Intent(context, AlarmService::class.java)
                .setAction(ACTION_STOP_ALARM)
        }

        fun createMuteAlarmIntent(context: Context): Intent {
            return Intent(context, AlarmService::class.java)
                .setAction(ACTION_MUTE_ALARM)
        }
    }
}
