package de.ferreum.pto.reminder

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update

internal class AlarmServiceStateProvider {
    private val serviceState = MutableStateFlow<AlarmState>(AlarmState.Stopped)

    val state: Flow<AlarmState> get() = serviceState

    fun setState(state: AlarmState) {
        serviceState.value = state
    }

    fun setMuted() {
        serviceState.update { if (it is AlarmState.Started) it.copy(isMuted = true) else it }
    }

    sealed class AlarmState {
        data object Stopped : AlarmState()
        data class Started(
            val reminder: ReminderParser.Reminder,
            val isMuted: Boolean,
        ) : AlarmState()
    }
}
