package de.ferreum.pto.reminder

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

class AlarmSignal {

    private val alarmFlow = MutableSharedFlow<WakeupScheduler.AlarmHandle>()

    fun observeAlarms(): Flow<WakeupScheduler.AlarmHandle> = alarmFlow

    suspend fun notifyAlarm(alarmHandle: WakeupScheduler.AlarmHandle) {
        alarmFlow.emit(alarmHandle)
    }

}
