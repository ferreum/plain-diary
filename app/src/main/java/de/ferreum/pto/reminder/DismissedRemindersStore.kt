package de.ferreum.pto.reminder

import de.ferreum.pto.preferences.Logger
import de.ferreum.pto.util.writeAtomic
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.time.LocalDateTime
import kotlin.coroutines.CoroutineContext
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds

internal class DismissedRemindersStore(
    scope: CoroutineScope,
    private val stateFile: File,
    private val ioContext: CoroutineContext,
    private val logger: Logger,
) {

    private val cachedRecords = MutableSharedFlow<Set<ReminderRecord>>(replay = 1)

    private val mutex = Mutex()

    private val initJob = scope.launch {
        val records = try {
            readRecords()
        } catch (e: FileNotFoundException) {
            emptySet()
        } catch (e: Exception) {
            logger.suspendLogException(e, "error loading dismissed reminders file; clearing state")
            writeRecords(emptySet())
            emptySet()
        }
        cachedRecords.emit(records)
    }

    val records: Flow<Set<ReminderRecord>> get() = cachedRecords

    suspend inline fun updateState(update: (Set<ReminderRecord>) -> Set<ReminderRecord>) {
        initJob.join()
        mutex.withLock {
            val records = update(cachedRecords.first())
            writeRecords(records)
            cachedRecords.emit(records)
        }
    }

    private suspend fun readRecords(): Set<ReminderRecord> = withContext(ioContext) {
        stateFile.useLines { lines ->
            val iterator = lines.iterator()
            if (!iterator.hasNext()) {
                throw IOException("invalid file")
            }
            val version = iterator.next().toInt()
            if (version > VERSION_MAX) {
                throw IOException("incompatible file version")
            }
            val reminders = mutableSetOf<ReminderRecord>()
            for (line in iterator) {
                val splits = line.split("|")
                if (splits[3] == STATE_DISMISSED) {
                    reminders.add(
                        ReminderRecord(
                            targetTime = LocalDateTime.parse(splits[0]),
                            // persisted sign is inverted
                            reminderOffset = -splits[1].toLong().milliseconds,
                            flags = splits[2],
                        )
                    )
                }
            }
            reminders
        }
    }

    private suspend fun writeRecords(list: Iterable<ReminderRecord>) {
        withContext(ioContext) {
            stateFile.parentFile!!.mkdirs()
            stateFile.writeAtomic { writer ->
                writer.write("$VERSION\n")
                list.forEach { record ->
                    writer.write(formatRecord(record))
                    writer.write("\n")
                }
            }
        }
    }

    private fun formatRecord(record: ReminderRecord): String {
        // persisted sign is inverted
        val duration = -record.reminderOffset.inWholeMilliseconds

        val cleanFlags = record.flags.throwOnInvalidChars()
        return "${record.targetTime}|$duration|$cleanFlags|$STATE_DISMISSED"
    }

    data class ReminderRecord(
        val targetTime: LocalDateTime,
        val reminderOffset: Duration,
        val flags: String,
    )

    private fun String.throwOnInvalidChars(): String {
        if (REGEX_DISALLOWED.containsMatchIn(this)) {
            throw IllegalArgumentException("string contains invalid characters")
        }
        return this
    }

    companion object {
        private const val VERSION = 1
        private const val VERSION_MAX = 99

        private val REGEX_DISALLOWED = Regex("""[|\n]""")

        private const val STATE_DISMISSED = "dismissed"

        fun ReminderParser.Reminder.toRecord() = ReminderRecord(
            targetTime = targetTime,
            reminderOffset = reminderOffset,
            flags = flags,
        )
    }

}
