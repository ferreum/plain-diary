package de.ferreum.pto.reminder

import android.os.Bundle
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.core.widget.doAfterTextChanged
import de.ferreum.pto.R
import de.ferreum.pto.page.content.EventContent
import de.ferreum.pto.page.content.TextType
import de.ferreum.pto.util.getTypedParcelable
import de.ferreum.pto.util.getTypedSerializable
import de.ferreum.pto.util.withRemovedAt
import de.ferreum.pto.util.withReplacedAt
import java.time.LocalTime

class EventEditDialogFragment : AppCompatDialogFragment() {

    private lateinit var eventTime: LocalTime
    private lateinit var eventTitle: String
    private lateinit var reminderTexts: List<String>
    private lateinit var reminderErrors: List<Int?>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val content =
            arguments?.getTypedParcelable<TextType.Event>(EXTRA_TEXT_TYPE)?.eventContent
                ?: EventContent(LocalTime.now(), listOf(""), "")

        eventTime = savedInstanceState?.getTypedSerializable(SAVE_TIME)
            ?: content.time
        eventTitle = savedInstanceState?.getString(SAVE_TITLE)
            ?: content.title
        reminderTexts = savedInstanceState?.getStringArrayList(SAVE_REMINDERS)
            ?: content.reminders
        reminderErrors = reminderTexts.map { null }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        return inflater.inflate(R.layout.fragment_event_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val scrollView = view as NestedScrollView
        val eventTimeLabel: TextView = view.findViewById(R.id.eventTimeLabel)
        val titleInput: EditText = view.findViewById(R.id.titleInput)
        val reminderContainer: ViewGroup = view.findViewById(R.id.reminderContainer)
        val addReminderButton: View = view.findViewById(R.id.addReminderButton)
        val cancelButton: View = view.findViewById(R.id.cancelButton)
        val okButton: View = view.findViewById(R.id.okButton)

        // make dialog behave better with overlapping soft input
        WindowCompat.setDecorFitsSystemWindows(requireDialog().window!!, false)
        requireDialog().window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
        scrollView.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            val bottom = scrollView.getFocusBottom() - scrollView.height + scrollView.paddingBottom
            if (bottom >= 0) scrollView.smoothScrollTo(0, bottom)
        }
        ViewCompat.setOnApplyWindowInsetsListener(scrollView) { v, windowInsets ->
            val insets = windowInsets.getInsets(WindowInsetsCompat.Type.ime())
            v.setPadding(insets.left, insets.top, insets.right, insets.bottom)
            WindowInsetsCompat.CONSUMED
        }

        val (formatted: String) = ReminderParser.formatReminder(
            EventContent(eventTime, emptyList(), "")
        )
        eventTimeLabel.text = getString(R.string.reminder_edit_time_label, formatted)

        titleInput.setText(eventTitle)
        titleInput.doAfterTextChanged { text ->
            eventTitle = text!!.toString()
        }

        addReminderButton.setOnClickListener {
            reminderTexts = reminderTexts.plusElement("")
            reminderErrors = reminderErrors.plusElement(null)
            updateReminderSpecs(reminderContainer)
        }

        cancelButton.setOnClickListener {
            requireDialog().cancel()
        }
        okButton.setOnClickListener {
            normalizeReminders()
            if (verifyReminders()) {
                finishWithResult()
            } else {
                updateReminderSpecs(reminderContainer)
            }
        }

        updateReminderSpecs(reminderContainer)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(SAVE_TIME, eventTime)
        outState.putString(SAVE_TITLE, eventTitle)
        outState.putStringArrayList(SAVE_REMINDERS, ArrayList(reminderTexts))
    }

    private fun updateReminderSpecs(reminderContainer: ViewGroup) {
        val addReminderButton: View = requireView().findViewById(R.id.addReminderButton)

        addReminderButton.isVisible = reminderTexts.size < 5

        val reminderDiff = reminderTexts.size - reminderContainer.childCount
        if (reminderDiff > 0) {
            for (index in reminderContainer.childCount..<reminderTexts.size) {
                reminderContainer.addView(createReminderSpecRow(reminderContainer, index))
            }
        } else if (reminderDiff < 0) {
            reminderContainer.removeViews(reminderTexts.size, -reminderDiff)
        }

        for ((index, spec) in reminderTexts.withIndex()) {
            val row = reminderContainer.getChildAt(index)
            val input: EditText = row.findViewById(R.id.reminderSpecInput)
            val deleteButton: View = row.findViewById(R.id.deleteSpecButton)

            input.setText(spec)
            input.error = reminderErrors[index]?.let { getText(it) }
            deleteButton.isVisible = reminderTexts.size > 1
        }
    }

    private fun createReminderSpecRow(reminderContainer: ViewGroup, index: Int): View {
        val row = layoutInflater.inflate(R.layout.row_reminder_spec, reminderContainer, false)
        val input: EditText = row.findViewById(R.id.reminderSpecInput)
        val cycleFlagButton: View = row.findViewById(R.id.cycleFlagButton)
        val deleteButton: View = row.findViewById(R.id.deleteSpecButton)

        input.filters = arrayOf(InputFilter { source, start, end, dest, dstart, dend ->
            val result = buildString {
                append(dest, 0, dstart)
                append(source, start, end)
                append(dest, dend, dest.length)
            }
            if (!REGEX_DURATION_INPUT_FILTER.matches(result)) {
                dest.subSequence(dstart, dend)
            } else {
                null
            }
        })
        input.doAfterTextChanged { text ->
            val str = text!!.toString()
            if (reminderTexts[index] == str) return@doAfterTextChanged
            if (input.error != null) {
                reminderErrors.withReplacedAt(index, null)
                input.error = null
            }
            reminderTexts = reminderTexts.withReplacedAt(index, str)
        }
        cycleFlagButton.setOnClickListener {
            val text = input.text!!
            when (text.lastOrNull()) {
                '!' -> text.replace(text.length - 1, text.length, "?")
                '?' -> text.replace(text.length - 1, text.length, "")
                else -> text.append("!")
            }
        }
        deleteButton.setOnClickListener {
            reminderTexts = reminderTexts.withRemovedAt(index)
            reminderErrors = reminderErrors.withRemovedAt(index)
            updateReminderSpecs(reminderContainer)
        }

        return row
    }

    private fun normalizeReminders() {
        reminderTexts = reminderTexts.mapIndexed { index, text ->
            if (ReminderParser.FLAG_REGEX.matches(text)) {
                if (index == 0) text else "-0m$text"
            } else {
                if (text.startsWith("-") || text.startsWith("+")) text else "-$text"
            }
        }
    }

    private fun verifyReminders(): Boolean {
        reminderErrors = reminderTexts.mapIndexed { index, text ->
            val isValid = (index == 0 && ReminderParser.FLAG_REGEX.matches(text)
                || ReminderParser.DURATION_REGEX.matches(text))
            if (isValid) null else R.string.reminder_edit_spec_error_invalid
        }
        return reminderErrors.all { it == null }
    }

    private fun finishWithResult() {
        val eventContent = EventContent(eventTime, reminderTexts, eventTitle.trim())
        (parentFragment as? Callback)
            ?.onEventEdited(eventContent, arguments?.getTypedParcelable(EXTRA_TEXT_TYPE))
        requireDialog().dismiss()
    }

    private fun ViewGroup.getFocusBottom(): Int {
        var view = findFocus()
            ?: return -1
        var bottom = view.bottom
        view = view.parent as View
        while (view != this) {
            bottom += view.top
            view = view.parent as View
        }
        return bottom
    }

    interface Callback {
        fun onEventEdited(eventContent: EventContent, textType: TextType?)
    }

    companion object {
        private const val EXTRA_TEXT_TYPE = "event_edit_fragment.textType"

        private const val SAVE_TIME = "event_edit_fragment.time"
        private const val SAVE_TITLE = "event_edit_fragment.title"
        private const val SAVE_REMINDERS = "event_edit_fragment.reminders"

        private val REGEX_DURATION_INPUT_FILTER = Regex("^[+-]?[0-9dhms]*[?!]?$")

        fun withArgs(
            textType: TextType.Event,
        ) = EventEditDialogFragment().apply {
            arguments = Bundle().apply {
                putParcelable(EXTRA_TEXT_TYPE, textType)
            }
        }
    }

}
