package de.ferreum.pto.reminder

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import de.ferreum.pto.applicationScope
import de.ferreum.pto.reminderStateRepository
import de.ferreum.pto.util.getTypedSerializableExtra
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import kotlin.time.Duration.Companion.milliseconds

internal class NotificationBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {
        if (intent?.action == ACTION_DISMISS) {
            val targetTime = intent.getTypedSerializableExtra<LocalDateTime>(EXTRA_TARGET_TIME)
            val reminderOffset = intent.getLongExtra(EXTRA_OFFSET, 0)
            val flags = intent.getStringExtra(EXTRA_FLAGS)
            if (targetTime == null || flags == null) return

            val res = goAsync()
            context.applicationScope.launch {
                context.reminderStateRepository.addDismissedReminder(
                    DismissedRemindersStore.ReminderRecord(
                        targetTime,
                        reminderOffset.milliseconds,
                        flags,
                    )
                )
            }.invokeOnCompletion { res.finish() }
        }
    }

    companion object {
        const val ACTION_DISMISS = "de.ferreum.pto.reminder.DISMISS"
        private val EXTRA_TARGET_TIME = AlarmBroadcastReceiver::class.java.simpleName + ".TARGET_TIME"
        private val EXTRA_OFFSET = AlarmBroadcastReceiver::class.java.simpleName + ".OFFSET"
        private val EXTRA_FLAGS = AlarmBroadcastReceiver::class.java.simpleName + ".FLAGS"

        fun createDismissIntent(context: Context, reminder: ReminderParser.Reminder): Intent {
            return Intent(context, NotificationBroadcastReceiver::class.java)
                .setAction(ACTION_DISMISS)
                .putExtra(EXTRA_TARGET_TIME, reminder.targetTime)
                .putExtra(EXTRA_OFFSET, reminder.reminderOffset.inWholeMilliseconds)
                .putExtra(EXTRA_FLAGS, reminder.flags)
        }
    }
}
