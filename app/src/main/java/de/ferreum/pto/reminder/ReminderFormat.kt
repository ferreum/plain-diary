package de.ferreum.pto.reminder

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

fun formatNotificationTime(targetTime: LocalDateTime, withDate: Boolean = true): String {
    val timeStr = if (withDate && targetTime.toLocalDate() != LocalDate.now()) {
        val timeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
        timeFormatter.format(targetTime)
    } else {
        val timeFormatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)
        timeFormatter.format(targetTime.toLocalTime())
    }
    return timeStr
}
