package de.ferreum.pto.reminder

import android.content.Context
import android.os.PowerManager
import de.ferreum.pto.keepalive.KeepAliveSignal
import de.ferreum.pto.util.myDebounce
import de.ferreum.pto.util.myTransformLatest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.dropWhile
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

/**
 * Ensure processing of notifications on time.
 *
 * - Keep the app process alive during internal notification processing.
 * - When notification is sent, keep the system awake for a short moment to ensure
 *   notifications are processed in time.
 *
 * When active, uses [KeepAliveSignal] to keep the app alive and a wakelock to
 * keep the device awake.
 */
internal class ReminderKeepAliveGuard(
    private val scope: CoroutineScope,
    private val keepAliveSignal: KeepAliveSignal,
    private val reminderService: ReminderService,
    private val alarmSignal: AlarmSignal,
    context: Context,
) {

    private val powerManager = context.getSystemService(PowerManager::class.java)

    fun run() {
        scope.launch {
            val wakelock = powerManager.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK, "pto:reminder_processing")
            wakelock.setReferenceCounted(false)
            val wakeLockRequests = MutableStateFlow(emptySet<String>())
            launch {
                wakeLockRequests
                    .dropWhile { it.isEmpty() }
                    .myDebounce { if (it.isEmpty()) 1000 else 0 }
                    .distinctUntilChangedBy { it.isEmpty() }
                    .collect {
                        if (it.isEmpty()) {
                            wakelock.release()
                        } else {
                            wakelock.acquire(30000)
                        }
                    }
            }

            launch {
                alarmSignal.observeAlarms()
                    .collectLatest {
                        wakeLockRequests.update { it + "alarm" }
                        delay(1000)
                        reminderService.reminderState.myTransformLatest {
                            if (!it.isProcessing) {
                                delay(1000)
                                emit(Unit)
                            }
                        }.first()
                        wakeLockRequests.update { it - "alarm" }
                    }
            }
            keepAliveSignal.register(
                this,
                reminderService.latestNotificationMillis
                    .dropWhile { it == null }
                    .myTransformLatest {
                        emit(true)
                        wakeLockRequests.update { it + "notification" }
                        delay(8000)
                        wakeLockRequests.update { it - "notification" }
                        emit(false)
                    }
            )
        }
    }

}
