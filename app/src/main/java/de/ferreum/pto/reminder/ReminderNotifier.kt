package de.ferreum.pto.reminder

import android.app.NotificationChannel
import android.app.NotificationChannelGroup
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import de.ferreum.pto.MainActivity
import de.ferreum.pto.R
import de.ferreum.pto.util.tryNotify
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

internal class ReminderNotifier(private val context: Context) {

    private val notificationManager = NotificationManagerCompat.from(context)

    fun setup() {
        notificationManager.createNotificationChannelGroup(
            NotificationChannelGroup(
                GROUP_KEY,
                context.getText(R.string.reminder_notifgroup_title),
            )
        )
        notificationManager.createNotificationChannel(
            NotificationChannel(
                CHANNEL_ID_NORMAL,
                context.getText(R.string.reminder_notifchannel_normal_title),
                NotificationManager.IMPORTANCE_HIGH
            ).apply {
                enableVibration(true)
                group = GROUP_KEY
            }
        )
        notificationManager.createNotificationChannel(
            NotificationChannel(
                CHANNEL_ID_SILENT,
                context.getText(R.string.reminder_notifchannel_silent_title),
                NotificationManager.IMPORTANCE_LOW
            ).apply {
                enableVibration(false)
                group = GROUP_KEY
            }
        )
        notificationManager.createNotificationChannel(
            NotificationChannel(
                CHANNEL_ID_FOREGROUND_UPCOMING,
                context.getText(R.string.reminder_notifchannel_foreground_upcoming_title),
                NotificationManager.IMPORTANCE_DEFAULT,
            ).apply {
                enableVibration(false)
                setSound(null, null)
                group = GROUP_KEY
            }
        )
        notificationManager.createNotificationChannel(
            NotificationChannel(
                CHANNEL_ID_FOREGROUND,
                context.getText(R.string.reminder_notifchannel_foreground_title),
                NotificationManager.IMPORTANCE_HIGH
            ).apply {
                group = GROUP_KEY
            }
        )
        notificationManager.createNotificationChannel(
            NotificationChannel(
                CHANNEL_ID_FOREGROUND_MISSED,
                context.getText(R.string.reminder_notifchannel_foreground_missed_title),
                NotificationManager.IMPORTANCE_HIGH
            ).apply {
                group = GROUP_KEY
            }
        )
    }

    fun notify(reminder: ReminderParser.Reminder) {
        val isForeground = reminder.isForeground
        val isSilent = !isForeground && "?" in reminder.flags
        val channelId = when {
            isForeground -> CHANNEL_ID_FOREGROUND
            isSilent -> CHANNEL_ID_SILENT
            else -> CHANNEL_ID_NORMAL
        }

        if (isForeground) {
            context.startService(AlarmService.createAlarmIntent(context, reminder))
        }

        val notificationId = if (isForeground) {
            R.id.reminder_notification_foreground
        } else {
            getNotificationId(reminder.targetTime)
        }

        notificationManager.tryNotify(
            notificationId,
            createNotification(channelId, reminder)
        )
    }

    fun createNotification(
        channelId: String,
        reminder: ReminderParser.Reminder,
    ) = NotificationCompat.Builder(context, channelId).apply {
        setContentTitle(context.getText(R.string.reminder_notif_title))
        setContentText(formatContentText(reminder.targetTime, reminder.title))
        setLocalOnly(true)
        setWhen(reminder.targetInstant.toEpochMilli())
        setGroup(GROUP_KEY)
        setGroupSummary(false)
        if (reminder.isSilent) {
            priority = NotificationCompat.PRIORITY_LOW
            setVibrate(null)
        }
        if (reminder.isForeground) {
            setSmallIcon(R.drawable.ic_notif_alarm)
            val stopPIntent = PendingIntent.getService(
                context, 0, AlarmService.createStopAlarmIntent(context),
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
            )
            setDeleteIntent(stopPIntent)
            addAction(
                NotificationCompat.Action.Builder(
                    null,
                    context.getString(R.string.quicknotes_action_dismiss_notification),
                    stopPIntent
                ).build()
            )
            val fullscreenPIntent = PendingIntent.getActivity(
                context, 0,
                AlarmActivity.createAlarmIntent(context)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION),
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
            )
            setFullScreenIntent(fullscreenPIntent, true)
            setContentIntent(fullscreenPIntent)
            setCategory(NotificationCompat.CATEGORY_ALARM)
            setForegroundServiceBehavior(NotificationCompat.FOREGROUND_SERVICE_IMMEDIATE)
        } else {
            setSmallIcon(R.drawable.ic_notif_small)
            setCategory(NotificationCompat.CATEGORY_EVENT)
            setContentIntent(
                getPagePendingIntent(reminder.pageDate, reminder.targetTime.toLocalTime())
            )
            setAutoCancel(true)
        }
    }.build()

    fun setUpcoming(reminder: ReminderParser.Reminder?) {
        if (reminder != null) {
            notificationManager.tryNotify(
                R.id.reminder_notification_upcoming,
                NotificationCompat.Builder(context, CHANNEL_ID_FOREGROUND_UPCOMING).apply {
                    setSmallIcon(R.drawable.ic_notif_alarm)
                    setCategory(NotificationCompat.CATEGORY_EVENT)
                    setContentTitle(context.getText(R.string.reminder_notif_foreground_upcoming_title))

                    val displayTime = reminder.targetInstant
                        .plusMillis(reminder.reminderOffset.inWholeMilliseconds)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime()
                    val timeFormatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)
                    setContentText(context.getString(
                        R.string.reminder_notif_upcoming_message_format,
                        timeFormatter.format(displayTime.toLocalTime())
                    ))
                    setContentIntent(
                        getPagePendingIntent(reminder.pageDate, reminder.targetTime.toLocalTime())
                    )

                    setLocalOnly(true)
                    setOngoing(true)
                    setShowWhen(false)
                    setOnlyAlertOnce(true)
                    setWhen(reminder.reminderInstant.toEpochMilli())
                    setGroup(GROUP_KEY)
                    setGroupSummary(false)

                    // public version without dismiss action
                    setPublicVersion(build())

                    val dismissPIntent = PendingIntent.getBroadcast(
                        context, 0,
                        NotificationBroadcastReceiver.createDismissIntent(context, reminder),
                        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
                    )
                    addAction(
                        NotificationCompat.Action.Builder(
                            null,
                            context.getString(R.string.reminder_notif_upcoming_action_dismiss),
                            dismissPIntent
                        ).setSemanticAction(NotificationCompat.Action.SEMANTIC_ACTION_DELETE)
                            .setAuthenticationRequired(true)
                            .build()
                    )
                }.build()
            )
        } else {
            notificationManager.cancel(R.id.reminder_notification_upcoming)
        }
    }

    fun notifyMissed(reminder: ReminderParser.Reminder, silent: Boolean = false) {
        notificationManager.tryNotify(
            getNotificationId(reminder.targetTime),
            NotificationCompat.Builder(context, CHANNEL_ID_FOREGROUND_MISSED).apply {
                setSmallIcon(R.drawable.ic_notif_alarm)
                setContentTitle(context.getText(R.string.reminder_notif_foreground_missed_title))
                setContentText(formatContentText(reminder.targetTime, reminder.title))
                setCategory(NotificationCompat.CATEGORY_EVENT)
                setContentIntent(getPagePendingIntent(
                    reminder.pageDate, reminder.targetTime.toLocalTime()))
                setLocalOnly(true)
                setWhen(reminder.targetInstant.toEpochMilli())
                if (silent) {
                    setVibrate(null)
                    setSound(null)
                }
                setGroup(GROUP_KEY)
                setGroupSummary(false)
            }.build()
        )
    }

    private fun formatContentText(targetTime: LocalDateTime, title: String): String {
        val timeStr = formatNotificationTime(targetTime)
        return if (title.isBlank()) {
            context.getString(R.string.reminder_notif_message_format_empty, timeStr)
        } else {
            context.getString(R.string.reminder_notif_message_format, title, timeStr)
        }
    }

    private fun getPagePendingIntent(localDate: LocalDate, localTime: LocalTime): PendingIntent {
        return PendingIntent.getActivity(
            context,
            PINTENT_ID_BASE or (localDate.toEpochDay().toInt() and PINTENT_ID_MASK),
            MainActivity.createPageIntent(context, localDate, reminderSelection = localTime),
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private fun getNotificationId(targetTime: LocalDateTime): Int {
        val second = targetTime.toEpochSecond(ZoneOffset.UTC)
        return NOTIF_ID_BASE or (second.toInt() and NOTIF_ID_MASK)
    }

    companion object {
        private const val GROUP_KEY = "reminders"
        private const val CHANNEL_ID_NORMAL = "reminder"
        private const val CHANNEL_ID_SILENT = "reminder.silent"
        const val CHANNEL_ID_FOREGROUND = "reminder.foreground"
        private const val CHANNEL_ID_FOREGROUND_UPCOMING = "reminder.foreground.upcoming"
        private const val CHANNEL_ID_FOREGROUND_MISSED = "reminder.foreground.missed"
        private const val NOTIF_ID_BASE = 0x9000000
        private const val NOTIF_ID_MASK =  0xffffff
        private const val PINTENT_ID_BASE = 0x1200000
        private const val PINTENT_ID_MASK =   0xfffff
    }
}
