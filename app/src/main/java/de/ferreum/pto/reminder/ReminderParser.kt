package de.ferreum.pto.reminder

import android.os.Parcelable
import de.ferreum.pto.page.content.EventContent
import de.ferreum.pto.page.content.TextType
import de.ferreum.pto.preferences.toDateTimeOnPage
import de.ferreum.pto.util.TimeProvider
import de.ferreum.pto.util.forLineRanges
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.ensureActive
import kotlinx.parcelize.Parcelize
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

internal class ReminderParser(
    private val timeProvider: TimeProvider,
) {

    suspend fun parsePageReminders(
        newpageTime: LocalTime,
        pageDate: LocalDate,
        pageText: String,
    ): List<Reminder> {
        val coroutineContext = currentCoroutineContext()
        val reminders = mutableListOf<Reminder>()
        pageText.forLineRanges { start, end ->
            if (start == end || pageText[start] != '@') return@forLineRanges
            val line = pageText.substring(start, end)
            coroutineContext.ensureActive()
            EVENT_REGEX.find(line)?.let { match ->
                val time = match.groupValues[1].toLocalTimeOrNull()
                    ?: return@let
                val targetTime = time.toDateTimeOnPage(pageDate, newpageTime)
                val (specs, tail) = parseTail(line, match.groups[2]!!.range.first)
                specs.mapTo(reminders) { (offset, flags) ->
                    Reminder(
                        targetTime = targetTime,
                        targetInstant = targetTime.atZone(timeProvider.timeZone()).toInstant(),
                        reminderOffset = offset,
                        flags = flags,
                        title = tail.trim(),
                        pageDate = pageDate,
                        source = line,
                    )
                }
            }
        }
        return reminders
    }

    data class ReminderSpec(
        val offset: Duration,
        val flags: String,
        val range: IntRange,
        val source: String,
    )

    @Parcelize
    data class Reminder(
        val targetTime: LocalDateTime,
        val targetInstant: Instant,
        val reminderOffset: Duration,
        val flags: String,
        val title: String,
        val pageDate: LocalDate,
        val source: String,
    ) : Parcelable {
        val isForeground get() = "!" in flags

        val isSilent get() = "?" in flags && "!" !in flags

        val reminderInstant get() = targetInstant.plusMillis(reminderOffset.inWholeMilliseconds)!!
    }

    companion object {
        val EVENT_REGEX = Regex("^@:?(\\d{1,2}:\\d{1,2})\\b(.*)$", setOf(RegexOption.MULTILINE))
        val DURATION_REGEX = Regex("\\s*([+-](?:\\d+[smhd])+)\\b([?!]?)(?:\\s|$)")
        val FLAG_REGEX = Regex("\\s*([?!]?)(?:\\s|$)")
        private val TIME_FORMAT = DateTimeFormatter.ofPattern("H:mm")

        fun parseEventTextType(match: MatchResult, selection: Int): TextType.Event? {
            val time = match.groupValues[1].toLocalTimeOrNull()
                ?: return null

            val (specs, tail) = parseTail(match.value, match.groups[2]!!.range.first - match.range.first)

            val index = specs.indexOfFirst { it.range.last + 1 >= selection }
            val reminderSelection = when {
                index < 0 -> specs.size
                specs[index].range.first - 1 >= selection -> index - 1
                else -> index
            }

            return TextType.Event(
                match.range.first,
                match.range.last + 1,
                match.groupValues[0],
                EventContent(
                    time,
                    specs.map { it.source },
                    tail.trim(),
                ),
                reminderSelection,
            )
        }

        fun formatReminder(eventContent: EventContent, reminderSelection: Int = -1): Pair<String, Int?> {
            val timeStr = eventContent.time.format(TIME_FORMAT)
            var pos: Int? = null
            val text = buildString {
                append("@").append(timeStr)
                if (eventContent.reminders.isEmpty() && reminderSelection == 0) {
                    pos = length
                }
                val keepImplicit = eventContent.reminders.size <= 1
                    || eventContent.reminders.first().isNotEmpty()
                for ((index, spec) in eventContent.reminders.withIndex()) {
                    if (index == 0 && keepImplicit && FLAG_REGEX.matches(spec)) {
                        append(spec)
                    } else if (spec.isEmpty()) {
                        append(" -0m")
                    } else {
                        append(" ").append(spec)
                    }
                    if (index == reminderSelection) {
                        pos = length
                    }
                }
                if (eventContent.title.isNotEmpty()) {
                    append(" ")
                    append(eventContent.title)
                }
            }
            return text to pos
        }

        private fun parseTail(text: String, tailStart: Int): Pair<List<ReminderSpec>, String> {
            val reminders = mutableListOf<ReminderSpec>()
            var startPos = tailStart

            val flagMatch = FLAG_REGEX.matchAt(text, startPos)
            if (flagMatch != null && flagMatch.groupValues[1].isNotEmpty()) {
                startPos = flagMatch.range.last + 1
                reminders.add(ReminderSpec(
                    Duration.ZERO, flagMatch.groupValues[1],
                    flagMatch.groups[1]!!.range, flagMatch.value.trim()))
            }

            while (startPos < text.length) {
                val durationMatch = DURATION_REGEX.matchAt(text, startPos) ?: break
                val offset = durationMatch.groupValues[1].parseReminderOffset()
                val flags = durationMatch.groupValues[2]
                val range = durationMatch.groups[1]!!.range.first .. durationMatch.groups[2]!!.range.last
                reminders.add(ReminderSpec(offset, flags, range, durationMatch.value.trim()))
                startPos = durationMatch.range.last + 1
            }

            if (reminders.isEmpty()) {
                reminders.add(ReminderSpec(Duration.ZERO, "", IntRange.EMPTY, ""))
            }
            return Pair(reminders, text.substring(startPos))
        }

        private fun String.parseReminderOffset(): Duration {
            var sign = 0L
            var millis = 0L
            var prev = 0
            for (index in indices) {
                val char = get(index)
                if (index == 0) {
                    sign = when {
                        startsWith("-") -> -1L
                        startsWith("+") -> 1L
                        else -> throw IllegalArgumentException("invalid character: $char")
                    }
                    prev = 1
                } else {
                    if (char.isDigit()) continue
                    val unit = when (char) {
                        's' -> 1L
                        'm' -> 60L
                        'h' -> 3600L
                        'd' -> 86400L
                        else -> throw IllegalArgumentException("invalid character: $char")
                    }
                    val number = substring(prev, index).toLong()
                    millis += number * unit
                    prev = index + 1
                }
            }
            return (millis * sign).seconds
        }

        fun getReminderDate(
            line: String,
            pageDate: LocalDate,
            newpageTime: LocalTime,
        ): LocalDateTime? {
            val match = EVENT_REGEX.find(line) ?: return null
            val timeStr = match.groupValues[1]
            val time = try {
                LocalTime.parse(timeStr, TIME_FORMAT)
            } catch (e: DateTimeParseException) {
                return null
            }
            return time.toDateTimeOnPage(pageDate, newpageTime)
        }

        fun indexOfEventByTime(text: CharSequence, time: LocalTime): Int? {
            return EVENT_REGEX.findAll(text)
                .firstOrNull { it.groupValues[1].toLocalTimeOrNull() == time }
                ?.groups?.first()?.range?.first
        }

        private fun String.toLocalTimeOrNull(): LocalTime? {
            return try {
                LocalTime.parse(this, TIME_FORMAT)
            } catch (e: DateTimeParseException) {
                null
            }
        }
    }

}
