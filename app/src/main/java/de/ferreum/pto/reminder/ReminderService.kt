package de.ferreum.pto.reminder

import android.content.SharedPreferences
import androidx.collection.ArrayMap
import androidx.core.content.edit
import de.ferreum.pto.R
import de.ferreum.pto.files.PageWriteSignal.PageWriteFlag
import de.ferreum.pto.preferences.Logger
import de.ferreum.pto.preferences.PtoPreferences
import de.ferreum.pto.preferences.PtoPreferencesRepository
import de.ferreum.pto.preferences.getTodayDate
import de.ferreum.pto.reminder.DismissedRemindersStore.Companion.toRecord
import de.ferreum.pto.reminder.DismissedRemindersStore.ReminderRecord
import de.ferreum.pto.reminder.ReminderParser.Reminder
import de.ferreum.pto.reminder.UpcomingPageWatcher.PageUpdate
import de.ferreum.pto.reminder.WakeupScheduler.Wakeup
import de.ferreum.pto.util.TimeProvider
import de.ferreum.pto.util.combineTerminal
import de.ferreum.pto.util.myTransformLatest
import de.ferreum.pto.util.throttle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.time.Instant
import java.time.LocalDate

internal class ReminderService(
    private val scope: CoroutineScope,
    private val upcomingPageWatcher: UpcomingPageWatcher,
    private val reminderSharedPreferences: SharedPreferences,
    private val preferencesRepository: PtoPreferencesRepository,
    private val reminderParser: ReminderParser,
    private val wakeupScheduler: WakeupScheduler,
    alarmSignal: AlarmSignal,
    private val reminderNotifier: ReminderNotifier,
    private val timeProvider: TimeProvider,
    private val reminderStateRepository: ReminderStateRepository,
    private val logger: Logger,
) {

    private var latestCheckInstant: Instant?
        get() = reminderSharedPreferences.getLong(KEY_LATEST_CHECK_TIME, -1)
            .takeIf { it > 0 }?.let { Instant.ofEpochMilli(it) }
        set(value) {
            reminderSharedPreferences.edit {
                if (value != null) {
                    putLong(KEY_LATEST_CHECK_TIME, value.toEpochMilli())
                } else {
                    remove(KEY_LATEST_CHECK_TIME)
                }
            }
        }

    private val alarmObserverFlow = alarmSignal.observeAlarms()
        .mapNotNull { if (it == notifyAlarm || it == softAlarm) Unit else null }
        .onStart { emit(Unit) }

    enum class ReminderState {
        INIT, UPDATING, IDLE, LOADED;

        val isProcessing get() = this == INIT || this == UPDATING
    }

    private val _reminderState = MutableStateFlow(ReminderState.INIT)
    val reminderState: Flow<ReminderState> get() = _reminderState
    val latestNotificationMillis = MutableStateFlow<Long?>(null)

    private val _nextLoudReminder = MutableStateFlow<Reminder?>(null)
    val nextReminder: Flow<Reminder?> get() = _nextLoudReminder

    fun run() {
        scope.launch {
            preferencesRepository.preferencesFlow
                .distinctUntilChangedBy { prefs ->
                    prefs.newpageTime.takeIf { prefs.isEventRemindersEnabled }
                }
                .throttle(1000)
                .collectLatest { prefs ->
                    if (prefs.isEventRemindersEnabled) {
                        observePageReminders(prefs)
                    } else {
                        _reminderState.value = ReminderState.IDLE
                        reminderNotifier.setUpcoming(null)
                        _nextLoudReminder.value = null
                        wakeupScheduler.clearWakeup(notifyAlarm)
                        wakeupScheduler.clearWakeup(softAlarm)
                    }
                }
        }
    }

    private suspend fun observePageReminders(prefs: PtoPreferences) {
        upcomingPageWatcher.observeContent(filterFlags = setOf(PageWriteFlag.NO_EVENTS))
            .extractReminders(prefs)
            .filterDismissed()
            .distinctUntilChanged()
            .combineTerminal(alarmObserverFlow) { reminders, _ ->
                _reminderState.update {
                    when (it) {
                        ReminderState.INIT,
                        ReminderState.IDLE,
                        -> ReminderState.INIT

                        else -> ReminderState.UPDATING
                    }
                }
                checkReminders(reminders)
            }
            .retry(5) { throwable ->
                logger.suspendLogException(throwable, "observePageReminders failed")
                delay(30000)
                true
            }
            .collect()
    }

    private fun Flow<PageUpdate>.extractReminders(prefs: PtoPreferences): Flow<List<Reminder>> {
        // hoisted state assumes single flow execution per call
        var latestReminders = emptyMap<LocalDate, List<Reminder>>()
        var reminderMap = emptyMap<LocalDate, List<Reminder>>()

        return transform { event ->
            val today = prefs.getTodayDate(timeProvider.localDateTime())

            // early update to reminderState
            if (!containsRequiredDays(latestReminders, today)) {
                _reminderState.value = ReminderState.INIT
                upcomingPageWatcher.triggerCurrentDayCheck()
            }

            val list = reminderParser.parsePageReminders(
                prefs.newpageTime, event.date, event.content)
            reminderMap = takeRequiredPages(today, reminderMap, event.date, list)
            if (containsRequiredDays(reminderMap, today) && reminderMap != latestReminders) {
                latestReminders = reminderMap
                emit(
                    reminderMap.values.flatten()
                        .sortedBy { it.notifyEpochMillis }
                )
            }
        }
    }

    private fun Flow<List<Reminder>>.filterDismissed(): Flow<List<Reminder>> {
        return myTransformLatest { reminders ->
            val existingSet: Set<ReminderRecord> = reminders.mapTo(HashSet()) { it.toRecord() }
            emitAll(
                reminderStateRepository.dismissedReminders
                    .map { dismissedSet ->
                        val deleted = dismissedSet - existingSet
                        if (deleted.isNotEmpty()) {
                            reminderStateRepository.forgetDismissedReminders(deleted)
                        }
                        dismissedSet.filter { it in existingSet }
                    }
                    .distinctUntilChanged()
                    .map { dismissed ->
                        reminders.filter { it.toRecord() !in dismissed }
                    }
            )
        }
    }

    private fun takeRequiredPages(
        today: LocalDate,
        prevMap: Map<LocalDate, List<Reminder>>,
        pageDate: LocalDate,
        pageReminders: List<Reminder>,
    ): Map<LocalDate, List<Reminder>> {
        if (prevMap[pageDate] == pageReminders) {
            // short-circuit without filter; flow must update with new data on day change
            return prevMap
        }

        val epochToday = today.toEpochDay()
        val map = ArrayMap<LocalDate, List<Reminder>>()
        for ((date, reminders) in prevMap) {
            if (date != pageDate && date.toEpochDay() in epochToday..epochToday + 2) {
                map[date] = reminders
            }
        }
        map[pageDate] = pageReminders
        return map
    }

    private fun containsRequiredDays(
        reminders: Map<LocalDate, List<Reminder>>,
        today: LocalDate,
    ): Boolean {
        return today in reminders
            && today.plusDays(1) in reminders
            && today.plusDays(2) in reminders
    }

    private fun checkReminders(reminders: List<Reminder>) {
        val now = timeProvider.instant()
        val nowMillis = now.toEpochMilli()

        var softWakeupMillis = Long.MAX_VALUE
        var exactWakeupMillis = Long.MAX_VALUE

        val upcomingForegroundReminder = reminders.firstOrNull {
            it.isForeground && it.notifyEpochMillis > nowMillis
        }?.takeIf { reminder ->
            val notifyTime = reminder.notifyEpochMillis - UPCOMING_MILLIS
            if (notifyTime < nowMillis) {
                true
            } else {
                softWakeupMillis = notifyTime
                false
            }
        }
        reminderNotifier.setUpcoming(upcomingForegroundReminder)

        val earliest = minOf(now, latestCheckInstant ?: now.minusSeconds(86400))
        val earliestMillis = earliest.toEpochMilli()
        for (reminder in reminders) {
            val notifyMillis = reminder.notifyEpochMillis
            if (notifyMillis >= earliestMillis) {
                if (notifyMillis <= nowMillis) {
                    if (reminder.isMissed(nowMillis)) {
                        reminderNotifier.notifyMissed(reminder, silent = true)
                        continue
                    }
                    latestNotificationMillis.value = timeProvider.elapsedRealtime()
                    reminderNotifier.notify(reminder)
                } else {
                    exactWakeupMillis = notifyMillis
                    break
                }
            }
        }

        if (softWakeupMillis != Long.MAX_VALUE) {
            wakeupScheduler.scheduleWakeup(
                softAlarm,
                Wakeup.inexact(Instant.ofEpochMilli(softWakeupMillis), wakeDevice = false)
            )
        } else {
            wakeupScheduler.clearWakeup(softAlarm)
        }

        if (exactWakeupMillis != Long.MAX_VALUE) {
            wakeupScheduler.scheduleWakeup(
                notifyAlarm,
                Wakeup.exact(Instant.ofEpochMilli(exactWakeupMillis))
            )
        } else {
            wakeupScheduler.clearWakeup(notifyAlarm)
        }

        latestCheckInstant = now
        _nextLoudReminder.value = reminders.firstOrNull {
            !it.isSilent && it.notifyEpochMillis > nowMillis
        }
        _reminderState.value = ReminderState.LOADED
    }

    private fun Reminder.isMissed(nowMillis: Long) =
        nowMillis - notifyEpochMillis > SKIP_MAX_MISSED_MILLIS
            && reminderOffset.inWholeMilliseconds < -SKIP_OFFSET_THRESHOLD_MILLIS

    private val Reminder.notifyEpochMillis
        get() = targetInstant.toEpochMilli() + reminderOffset.inWholeMilliseconds

    companion object {
        const val KEY_LATEST_CHECK_TIME = "reminder.latestCheck"
        const val UPCOMING_MILLIS = 3600_000L * 2 + 60_000L * 30

        /**
         * Threshold for reminder offsets above which reminder may be skipped if too far from
         * intended time. Protection against triggering reminders at unwanted times when day change
         * wakeup is delayed.
         *
         * [SKIP_MAX_MISSED_MILLIS] applies.
         */
        const val SKIP_OFFSET_THRESHOLD_MILLIS = 3600_000L * 26

        /**
         * Reminders are skipped if missed for more than this, if [SKIP_OFFSET_THRESHOLD_MILLIS]
         * also applies.
         */
        const val SKIP_MAX_MISSED_MILLIS = 60_000L * 15

        val notifyAlarm = WakeupScheduler.AlarmHandle(R.id.reminder_alarm_notify)
        val softAlarm = WakeupScheduler.AlarmHandle(R.id.reminder_alarm_notify_soft)
    }

}
