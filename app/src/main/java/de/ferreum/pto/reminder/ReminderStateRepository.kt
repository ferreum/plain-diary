package de.ferreum.pto.reminder

internal class ReminderStateRepository(
    private val dismissedRemindersStore: DismissedRemindersStore,
) {

    val dismissedReminders = dismissedRemindersStore.records

    suspend fun addDismissedReminder(record: DismissedRemindersStore.ReminderRecord) {
        dismissedRemindersStore.updateState { it + record }
    }

    suspend fun forgetDismissedReminders(records: Set<DismissedRemindersStore.ReminderRecord>) {
        dismissedRemindersStore.updateState { it - records }
    }

}
