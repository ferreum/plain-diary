package de.ferreum.pto.reminder

import de.ferreum.pto.preferences.MinuteTicker
import de.ferreum.pto.preferences.comparedTime
import de.ferreum.pto.reminder.AlarmServiceStateProvider.AlarmState
import de.ferreum.pto.reminder.ReminderParser.Reminder
import de.ferreum.pto.util.TimeProvider
import de.ferreum.pto.util.myTransformLatest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn

internal class ReminderStatusPresenter(
    scope: CoroutineScope,
    reminderService: ReminderService,
    minuteTicker: MinuteTicker,
    timeProvider: TimeProvider,
    alarmServiceStateProvider: AlarmServiceStateProvider,
) {

    val status = combine(
        reminderService.nextReminder,
        alarmServiceStateProvider.state,
    ) { nextReminder, alarmState ->
        if (alarmState is AlarmState.Started) {
            ReminderStatus.OngoingForeground
        } else if (nextReminder != null) {
            ReminderStatus.Upcoming(nextReminder)
        } else {
            ReminderStatus.None
        }
    }.myTransformLatest { status ->
        if (status is ReminderStatus.Upcoming) {
            val showInstant = status.reminder.reminderInstant
                .minusMillis(UPCOMING_REMINDER_MAX_MILLIS)
            emitAll(minuteTicker.comparedTime(showInstant, timeProvider)
                .distinctUntilChanged()
                .map { if (it < 0) ReminderStatus.None else status })
        } else {
            emit(status)
        }
    }.stateIn(scope, SharingStarted.WhileSubscribed(0, 0), ReminderStatus.None)

    sealed class ReminderStatus {
        data object None : ReminderStatus()
        data class Upcoming(val reminder: Reminder) : ReminderStatus()
        data object OngoingForeground : ReminderStatus()
    }

    companion object {
        private const val UPCOMING_REMINDER_MAX_MILLIS = 3600_000L * 12
    }

}
