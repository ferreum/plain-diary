package de.ferreum.pto.reminder

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.repeatOnLifecycle
import de.ferreum.pto.R
import de.ferreum.pto.files.PageWriteSignal
import de.ferreum.pto.files.PageWriteSignal.PageWrite
import de.ferreum.pto.files.PageWriteSignal.PageWriteFlag
import de.ferreum.pto.preferences.Logger
import de.ferreum.pto.preferences.PtoPreferences
import de.ferreum.pto.preferences.PtoPreferencesRepository
import de.ferreum.pto.preferences.getTodayDate
import de.ferreum.pto.reminder.UpcomingPageWatcher.Companion.daychangeAlarm
import de.ferreum.pto.util.TimeProvider
import de.ferreum.pto.util.myTransformLatest
import de.ferreum.pto.util.throttle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.filterNot
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes

/**
 * Continuously observes upcoming pages, starting with the current day.
 *
 * The current implementation always checks at most two pages; current day and the next day.
 *
 * Respects the [PtoPreferences.PREF_NEWPAGE_TIME] preference.
 *
 * Schedules [daychangeAlarm] to wake up the app when a new day starts. On start of a new day,
 * the content of the new day and the following day is emitted.
 *
 * Observes file writes published with [pageWriteSignal].
 */
internal class UpcomingPageWatcher(
    scope: CoroutineScope,
    private val pageReader: suspend (LocalDate) -> String,
    private val preferencesRepository: PtoPreferencesRepository,
    private val pageWriteSignal: PageWriteSignal,
    private val wakeupScheduler: WakeupScheduler,
    private val alarmSignal: AlarmSignal,
    private val timeProvider: TimeProvider,
    private val logger: Logger,
    private val processLifecycleOwner: LifecycleOwner,
) {

    private val currentDayCheckSignal = MutableSharedFlow<Unit>(
        replay = 1,
        onBufferOverflow = BufferOverflow.DROP_OLDEST,
    ).apply { tryEmit(Unit) }

    private val sharedPageCheckFlow = preferencesRepository.preferencesFlow
        .distinctUntilChangedBy { prefs ->
            prefs.newpageTime.takeIf { prefs.isEventRemindersEnabled }
        }
        .throttle(1000)
        .myTransformLatest { prefs ->
            if (prefs.isEventRemindersEnabled) {
                emitAll(pageCheckFlow(prefs))
            } else {
                wakeupScheduler.clearWakeup(daychangeAlarm)
            }
        }
        .retry(5) { throwable ->
            logger.suspendLogException(throwable, "sharedPageCheckFlow failed")
            delay(30000)
            true
        }
        .shareIn(scope, SharingStarted.Eagerly)
        .onStart {
            val prefs = preferencesRepository.preferencesFlow.first()
            val today = prefs.getTodayDate(timeProvider.localDateTime())
            emit(PageWrite(today, null))
            emit(PageWrite(today.plusDays(1), null))
            emit(PageWrite(today.plusDays(2), null))
        }

    /**
     * Observe continuous updated content of pages for upcoming days.
     *
     * [filterFlags] are flags for write events which shall be ignored.
     */
    fun observeContent(
        filterFlags: Set<PageWriteFlag> = emptySet(),
    ): Flow<PageUpdate> {
        return preferencesRepository.preferencesFlow
            .distinctUntilChangedBy { it.newpageTime }
            .throttle(1000)
            .myTransformLatest { prefs ->
                observePageContent(prefs, filterFlags)
            }
    }

    fun triggerCurrentDayCheck() {
        currentDayCheckSignal.tryEmit(Unit)
    }

    private fun pageCheckFlow(prefs: PtoPreferences): Flow<PageWrite> = channelFlow {
        launch {
            var currentToday = prefs.getTodayDate(timeProvider.localDateTime())
            currentDayCheckSignal.collect {
                val now = timeProvider.localDateTime()
                val today = prefs.getTodayDate(now)
                scheduleDayChangeWakeup(prefs.newpageTime, now)
                if (today != currentToday) {
                    currentToday = today
                    send(PageWrite(today, null))
                    send(PageWrite(today.plusDays(1), null))
                    send(PageWrite(today.plusDays(2), null))
                }
            }
        }

        launch {
            processLifecycleOwner.repeatOnLifecycle(Lifecycle.State.RESUMED) {
                currentDayCheckSignal.tryEmit(Unit)
            }
        }
        launch {
            // Check occasionally in case alarm is delayed.
            // Delay will likely take much longer, because system freezes app in background.
            while (true) {
                delay(30.minutes)
                currentDayCheckSignal.tryEmit(Unit)
            }
        }
        launch {
            alarmSignal.observeAlarms().collect { alarm ->
                if (alarm == daychangeAlarm) {
                    currentDayCheckSignal.tryEmit(Unit)
                }
            }
        }
        launch {
            pageWriteSignal.observePageWrites()
                .collect { send(it) }
        }
        awaitClose()
    }.buffer(Channel.RENDEZVOUS)

    private suspend fun FlowCollector<PageUpdate>.observePageContent(
        prefs: PtoPreferences,
        filterFlags: Set<PageWriteFlag>,
    ) {
        sharedPageCheckFlow
            .filterNot { write -> write.flags.any { it in filterFlags } }
            .collect { pageWrite ->
                val pageEpochDay = pageWrite.date.toEpochDay()
                val epochToday = prefs.getTodayDate(timeProvider.localDateTime()).toEpochDay()
                if (pageEpochDay in epochToday..(epochToday + 2)) {
                    val content = pageWrite.content
                        ?: pageReader(pageWrite.date)
                    emit(PageUpdate(pageWrite.date, content))
                }
            }
    }

    private fun scheduleDayChangeWakeup(newpageTime: LocalTime, now: LocalDateTime) {
        val date = now.toLocalDate()
        val effectiveDate = if (now.toLocalTime() < newpageTime) date else date.plusDays(1)
        val alarmTime = effectiveDate.atTime(newpageTime)
        wakeupScheduler.scheduleWakeup(daychangeAlarm, WakeupScheduler.Wakeup(
            alarmTime.atZone(timeProvider.timeZone()).toInstant().plusSeconds(3600 * 4),
            16.hours,
        ))
    }

    data class PageUpdate(val date: LocalDate, val content: String)

    companion object {
        val daychangeAlarm = WakeupScheduler.AlarmHandle(R.id.reminder_alarm_daychange)
    }

}
