package de.ferreum.pto.reminder

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.AlarmManagerCompat
import de.ferreum.pto.reminder.WakeupScheduler.AlarmHandle
import de.ferreum.pto.reminder.WakeupScheduler.Wakeup
import de.ferreum.pto.util.containsNull
import java.time.Instant
import kotlin.time.Duration

interface WakeupScheduler {

    fun scheduleWakeup(
        alarmHandle: AlarmHandle,
        wakeup: Wakeup
    )

    fun clearWakeup(alarmHandle: AlarmHandle)

    data class AlarmHandle(val requestCode: Int)

    data class Wakeup(
        val instant: Instant,
        val window: Duration,
        val exact: Boolean = true,
        val wakeDevice: Boolean = true,
    ) {
        companion object {
            fun inexact(instant: Instant, wakeDevice: Boolean = true) =
                Wakeup(instant, Duration.ZERO, exact = false, wakeDevice)
            fun exact(instant: Instant, wakeDevice: Boolean = true) =
                Wakeup(instant, Duration.ZERO, exact = true, wakeDevice)
        }
    }

}

/**
 * Cache current wakeup requests and only forward changes to [wrapped].
 *
 * Assumes no races between requests with same [AlarmHandle].
 */
internal class CachingWakeupScheduler(private val wrapped: WakeupScheduler) : WakeupScheduler {
    private val cacheLock = Any()

    @Volatile
    private var cachedStates = mapOf<AlarmHandle, Wakeup?>()

    override fun scheduleWakeup(alarmHandle: AlarmHandle, wakeup: Wakeup) {
        if (cachedStates[alarmHandle] == wakeup) {
            // alarm already requested
            return
        }

        wrapped.scheduleWakeup(alarmHandle, wakeup)

        synchronized(cacheLock) {
            cachedStates = cachedStates + (alarmHandle to wakeup)
        }
    }

    override fun clearWakeup(alarmHandle: AlarmHandle) {
        if (cachedStates.containsNull(alarmHandle)) {
            // alarm already cleared
            return
        }

        wrapped.clearWakeup(alarmHandle)

        synchronized(cacheLock) {
            cachedStates = cachedStates + (alarmHandle to null)
        }
    }
}

internal class WakeupSchedulerImpl(
    private val context: Context,
) : WakeupScheduler {
    private val alarmManager = context.getSystemService(AlarmManager::class.java)

    override fun scheduleWakeup(alarmHandle: AlarmHandle, wakeup: Wakeup) {
        val pintent = getPendingIntent(
            alarmHandle, foreground = wakeup.window == Duration.ZERO && wakeup.wakeDevice
        )!!

        val type = if (wakeup.wakeDevice) AlarmManager.RTC_WAKEUP else AlarmManager.RTC

        if (wakeup.window == Duration.ZERO || !wakeup.exact) {
            if (wakeup.exact && AlarmManagerCompat.canScheduleExactAlarms(alarmManager)) {
                alarmManager.setExact(type, wakeup.instant.toEpochMilli(), pintent)
            } else {
                alarmManager.set(type, wakeup.instant.toEpochMilli(), pintent)
            }
        } else {
            alarmManager.setWindow(type, wakeup.instant.toEpochMilli(),
                wakeup.window.inWholeMilliseconds, pintent)
        }
    }

    override fun clearWakeup(alarmHandle: AlarmHandle) {
        val pintent = getPendingIntent(alarmHandle, create = false)
        if (pintent != null) {
            alarmManager.cancel(pintent)
            pintent.cancel()
        }
    }

    private fun getPendingIntent(
        alarmHandle: AlarmHandle,
        foreground: Boolean = false,
        create: Boolean = true,
    ): PendingIntent? {
        val updateFlag = if (create) {
            PendingIntent.FLAG_UPDATE_CURRENT
        } else {
            PendingIntent.FLAG_NO_CREATE
        }
        val intent = AlarmBroadcastReceiver.createIntent(context, alarmHandle)
        if (foreground) {
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND)
        }
        return PendingIntent.getBroadcast(
            context,
            alarmHandle.requestCode,
            intent,
            PendingIntent.FLAG_IMMUTABLE or updateFlag
        )
    }
}
