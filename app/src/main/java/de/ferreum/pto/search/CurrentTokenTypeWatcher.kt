package de.ferreum.pto.search

import android.text.SpanWatcher
import android.text.Spannable
import android.text.style.BackgroundColorSpan
import android.widget.EditText
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.withStarted
import kotlinx.coroutines.launch

class CurrentTokenTypeWatcher(
    private val lifecycleOwner: LifecycleOwner,
    private val editText: EditText,
    private val tokenTypes: List<TokenType>,
    private val onCurrentTokenTypeChanged: (TokenType?) -> Unit,
) : SpanWatcher {
    private var currentTokenType: TokenType? = null
    private var isCheckScheduled = false

    override fun onSpanAdded(text: Spannable, what: Any?, start: Int, end: Int) {
        scheduleCheck()
    }

    override fun onSpanRemoved(text: Spannable, what: Any?, start: Int, end: Int) {
        scheduleCheck()
    }

    override fun onSpanChanged(text: Spannable, what: Any?, ostart: Int, oend: Int, nstart: Int, nend: Int) {
        scheduleCheck()
    }

    fun scheduleCheck() {
        isCheckScheduled = true
        lifecycleOwner.lifecycleScope.launch {
            lifecycleOwner.withStarted {
                isCheckScheduled = false
                checkCurrentType(editText.text)
            }
        }
    }

    private fun checkCurrentType(text: Spannable) {
        val tokenType = when (val sel = text.getSelectionInfo()) {
            is SelectionInfo.Empty -> tokenTypes.first()
            is SelectionInfo.Free -> tokenTypes.first()
            is SelectionInfo.OnSpan -> sel.span.toTokenType()
            is SelectionInfo.Unmatched -> null
        }
        if (currentTokenType != tokenType) {
            currentTokenType = tokenType
            onCurrentTokenTypeChanged(tokenType)
        }
    }

    private fun BackgroundColorSpan.toTokenType() =
        tokenTypes.firstOrNull { it.color == backgroundColor } ?: tokenTypes.first()
}
