package de.ferreum.pto.search

import de.ferreum.pto.util.isRegionBlank
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.withContext
import java.time.LocalDate
import kotlin.coroutines.CoroutineContext

class FileMatcher(
    private val computeContext: CoroutineContext,
) {

    suspend fun matchFile(date: LocalDate, text: String, searchTokens: Collection<SearchToken>): FileSearchResult? {
        return withContext(computeContext) {
            findMatchRanges(text, searchTokens, stopWhenUnmatched = true)?.let { ranges ->
                createSearchResult(date, text, ranges)
            }
        }
    }

    suspend fun findMatchRanges(text: String, searchTokens: Collection<SearchToken>, stopWhenUnmatched: Boolean = false): List<MatchRange>? {
        return withContext(computeContext) {
            val ranges = mutableListOf<MatchRange>()
            for (token in searchTokens) {
                coroutineContext.ensureActive()
                val matched = token.match(text) { range ->
                    ranges.add(range)
                }
                if (stopWhenUnmatched && !matched) {
                    return@withContext null
                }
            }
            coroutineContext.ensureActive()
            if (stopWhenUnmatched && ranges.isEmpty()) return@withContext null
            ranges.sortBy { it.start }
            mergeRanges(ranges)
        }
    }

    /**
     * Create matches from search result.
     * [ranges] need to be sorted by entries' [first][IntRange.first] value.
     */
    private fun createSearchResult(date: LocalDate, text: String, ranges: List<MatchRange>): FileSearchResult {
        val matches = mutableListOf<SearchMatch>()
        val sb = StringBuilder()
        var sbSkippedChars = 0
        var lineStart = -1
        var lineEnd = -1
        var prevEnd = 0
        var prevLineEnd = 0
        for (range in ranges) {
            if (range.start > lineEnd) {
                lineStart = text.lastIndexOf('\n', range.start - 1).let { if (it < 0) 0 else it + 1 }
            }
            if (range.end > lineEnd) {
                lineEnd = text.indexOf('\n', range.end - 1).let { if (it < 0) text.length else it }
            }
            val startIndex = (range.start - MATCH_MARGIN_LEFT)
                .coerceAtLeast(lineStart)
                .let { if (it - lineStart < SNAP_MARGIN_START) lineStart else it }
                .coerceAtLeast(prevEnd)
            val endIndex = (range.end + MATCH_MARGIN_RIGHT)
                .coerceAtMost(lineEnd)
                .let { if (lineEnd - it < SNAP_MARGIN_END) lineEnd else it }

            if (startIndex < endIndex) {
                if (startIndex > prevEnd) {
                    if (lineStart > prevLineEnd) {
                        // starting a new line
                        if (!text.isRegionBlank(prevEnd, prevLineEnd)) {
                            // end of last line wasn't included
                            sb.append("…")
                        }
                        if (prevEnd > 0) {
                            sb.append("\n")
                        }
                        if (!text.isRegionBlank(lineStart, startIndex)) {
                            sb.append("…")
                        }
                    } else {
                        // continuing same line after a gap
                        sb.append("…")
                    }
                }

                sb.append(text, startIndex, endIndex)
                sbSkippedChars = endIndex - sb.length
            }

            matches.add(SearchMatch(
                range.start,
                range.start - sbSkippedChars,
                range.end - range.start,
            ))

            prevEnd = endIndex
            prevLineEnd = lineEnd
        }
        if (!text.isRegionBlank(prevEnd, lineEnd)) {
            sb.append("…")
        }
        return FileSearchResult(date, sb.toString(), matches)
    }

    private fun mergeRanges(ranges: List<MatchRange>): List<MatchRange> {
        if (ranges.size < 2) return ranges
        val result = mutableListOf<MatchRange>()
        var currentStart = -1
        var currentEnd = -1
        for (range in ranges) {
            if (currentStart != -1) {
                if (range.end < currentEnd) continue
                if (range.start <= currentEnd) {
                    currentEnd = range.end
                    continue
                }
                result.add(MatchRange(currentStart, currentEnd))
            }
            currentStart = range.start
            currentEnd = range.end
        }
        result.add(MatchRange(currentStart, currentEnd))
        return result
    }

    private inline fun SearchToken.match(
        text: String,
        crossinline receiver: (MatchRange) -> Unit,
    ): Boolean {
        return when (this) {
            is SearchToken.PlainToken -> {
                var searchPos = 0
                var didMatch = false
                while (true) {
                    val pos = text.indexOf(this.text, searchPos, true)
                    if (pos < 0) break
                    val length = this.text.length

                    receiver(MatchRange(pos, pos + length))

                    didMatch = true
                    searchPos = pos + length
                }
                didMatch
            }
            is SearchToken.RegexToken -> {
                regex.findAll(text)
                    .onEach {
                        receiver(MatchRange(it.range.first, it.range.last + 1))
                    }
                    .count() != 0
            }
            is SearchToken.ErrorToken -> {
                // should exclude errorTokens from search
                false
            }
        }
    }

    companion object {
        /** How many chars to include on the left of match. */
        private const val MATCH_MARGIN_LEFT = 16
        /** How many chars to include on the right of match. */
        private const val MATCH_MARGIN_RIGHT = 16
        /** How many chars to shap to start of line. */
        private const val SNAP_MARGIN_START = 20
        /** How many chars to shap to end of line. */
        private const val SNAP_MARGIN_END = 20
    }

}
