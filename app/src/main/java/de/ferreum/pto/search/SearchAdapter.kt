package de.ferreum.pto.search

import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.ferreum.pto.R
import de.ferreum.pto.util.PAGE_ISO_DATE_FORMATTER
import java.time.LocalDate

class SearchAdapter : RecyclerView.Adapter<SearchAdapter.Holder>() {

    var dateFormatter = PAGE_ISO_DATE_FORMATTER
        set(value) {
            if (field !== value) {
                field = value
                notifyItemRangeChanged(0, itemCount)
            }
        }

    var clickedResultDate: LocalDate? = null

    var onSearchResultClick: (FileSearchResult) -> Unit = {}

    private var list = mutableListOf<FileSearchResult>()

    private val ssb = SpannableStringBuilder()

    private val matchStyleSpans = mutableListOf<StyleSpan>()

    private var todayIndex: Int = -1

    var today: LocalDate = LocalDate.now()
        set(value) {
            field = value
            recalculateTodayIndex(value)
        }

    fun clear() {
        val size = itemCount
        list.clear()
        todayIndex = -1
        notifyItemRangeRemoved(0, size)
    }

    fun addAll(searchResults: List<FileSearchResult>) {
        val size = searchResults.size
        if (size > 0) {
            list.addAll(searchResults)
            val prevTodayItem = if (todayIndex >= 0) 1 else 0
            var addedTodayItem = 0
            if (todayIndex < 0) {
                todayIndex = list.findTodayIndex(today, start = list.size - size)
                if (todayIndex >= 0) addedTodayItem = 1
            }
            notifyItemRangeInserted(
                list.size - size + prevTodayItem,
                size + addedTodayItem + prevTodayItem,
            )
        }
    }

    fun getItemDate(position: Int) = list[positionToListIndex(position)].date

    override fun getItemCount() = list.size + (if (todayIndex >= 0) 1 else 0)

    override fun getItemViewType(position: Int): Int {
        return if (position == todayIndex) TYPE_TODAY else TYPE_RESULT
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            TYPE_RESULT -> {
                ResultHolder(inflater.inflate(R.layout.row_search_result, parent, false)).apply {
                    itemView.setOnClickListener {
                        val index = positionToListIndex(bindingAdapterPosition)
                        if (index in list.indices) {
                            onSearchResultClick(list[index])
                        }
                    }
                }
            }

            TYPE_TODAY -> TodayHolder(inflater.inflate(R.layout.row_search_today, parent, false))

            else -> throw IllegalArgumentException("invalid viewType value")
        }
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        when (holder) {
            is ResultHolder -> {
                val item = list[positionToListIndex(position)]
                holder.titleTextView.text = dateFormatter.format(item.date)
                holder.contentTextView.text = formatResult(item)
                holder.itemView.isActivated = item.date == clickedResultDate
            }

            is TodayHolder -> Unit
        }
    }

    private fun formatResult(item: FileSearchResult): SpannableStringBuilder {
        return ssb.apply {
            clearSpans()
            replace(0, length, item.text)
            for ((index, match) in item.matches.withIndex()) {
                val textStart = match.textOffset
                val textEnd = (textStart + match.length).coerceAtMost(length)
                if (textStart < textEnd) {
                    setSpan(
                        getMatchSpan(index), textStart, textEnd,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                }
            }
        }
    }

    private fun getMatchSpan(index: Int): StyleSpan {
        while (index > matchStyleSpans.size - 1) {
            matchStyleSpans.add(StyleSpan(Typeface.BOLD))
        }
        return matchStyleSpans[index]
    }

    private fun positionToListIndex(position: Int): Int {
        val todayIndex = todayIndex
        return if (todayIndex < 0 || position < todayIndex) {
            position
        } else {
            position - 1
        }
    }

    private fun recalculateTodayIndex(today: LocalDate) {
        val oldIndex = todayIndex
        val newIndex = list.findTodayIndex(today)
        if (oldIndex >= 0) {
            if (newIndex >= 0) {
                if (oldIndex != newIndex) {
                    notifyItemMoved(oldIndex, newIndex)
                } // else same index
            } else {
                notifyItemRemoved(oldIndex)
            }
        } else {
            if (newIndex >= 0) {
                notifyItemInserted(newIndex)
            } // else no today indicator
        }
        todayIndex = newIndex
    }

    private fun List<FileSearchResult>.findTodayIndex(today: LocalDate, start: Int = 0): Int {
        if (isEmpty() || first().date <= today || last().date > today) return -1
        val index = binarySearch(fromIndex = start) { today.compareTo(it.date) }
            .let { if (it < 0) it.inv() else it }
        return if (index in 1..<size) index else -1
    }

    sealed class Holder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class ResultHolder(itemView: View) : Holder(itemView) {
        val titleTextView: TextView = itemView.findViewById(R.id.title)
        val contentTextView: TextView = itemView.findViewById(R.id.content)
    }

    class TodayHolder(itemView: View) : Holder(itemView)

    companion object {
        private const val TYPE_RESULT = 1
        private const val TYPE_TODAY = 2
    }

}
