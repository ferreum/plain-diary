package de.ferreum.pto.search

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.Guideline
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter.StateRestorationPolicy
import de.ferreum.pto.R
import de.ferreum.pto.page.PreviewPageFragment
import de.ferreum.pto.page.PtoPageContract.EXTRA_EPOCH_DAY
import de.ferreum.pto.page.PtoPageContract.EXTRA_SEARCH_INPUT
import de.ferreum.pto.page.PtoPageContract.EXTRA_TEXT_SELECTION
import de.ferreum.pto.page.ViewModelFactory
import de.ferreum.pto.preferences.TodayProvider
import de.ferreum.pto.preferencesRepository
import de.ferreum.pto.todayProvider
import de.ferreum.pto.util.PAGE_ISO_DATE_FORMATTER
import de.ferreum.pto.util.getTypedSerializable
import de.ferreum.pto.util.setupSoftInputVisible
import de.ferreum.pto.widget.CustomSnackBar
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import timber.log.Timber
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class SearchFragment : Fragment(R.layout.fragment_search) {

    private val viewModel: SearchViewModel by viewModels { ViewModelFactory(this) }

    private lateinit var todayProvider: TodayProvider

    private var recyclerView: RecyclerView? = null
    private var searchAdapter: SearchAdapter? = null
    private var searchInputHelper: SearchInputHelper? = null

    private var lastScrollDate: LocalDate? = null
    private var clickedResultDate: LocalDate? = null

    override fun onAttach(context: Context) {
        todayProvider = context.todayProvider
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        clickedResultDate = savedInstanceState?.getTypedSerializable(SAVE_CLICKED_RESULT_DATE)
        val day = savedInstanceState?.getLong(SAVE_LAST_SCROLL_DAY, Long.MIN_VALUE)
        lastScrollDate = day?.takeIf { it != Long.MIN_VALUE }?.let { LocalDate.ofEpochDay(it) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val backButton: View = view.findViewById(R.id.backButton)
        val searchInput: EditText = view.findViewById(R.id.searchInput)
        val searchInputClearButton: View = view.findViewById(R.id.searchInputClearButton)
        val toolbar: View = view.findViewById(R.id.toolbar)
        val errorTextView: TextView = view.findViewById(R.id.errorTextView)
        recyclerView = view.findViewById(R.id.recyclerView)
        val progressBar: View = view.findViewById(R.id.progressBar)
        val snackBar: CustomSnackBar = view.findViewById(R.id.snackBar)
        val bottomInsetsGuide: Guideline = view.findViewById(R.id.bottomInsetsGuide)

        val recyclerView = recyclerView!!

        ViewCompat.setOnApplyWindowInsetsListener(view) { _, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or WindowInsetsCompat.Type.displayCutout()
                    or WindowInsetsCompat.Type.ime()
            )
            toolbar.setPadding(insets.left, insets.top, insets.right, 0)
            recyclerView.setPadding(insets.left, 0, insets.right, insets.bottom)
            snackBar.updateLayoutParams<MarginLayoutParams> {
                leftMargin = insets.left
                rightMargin = insets.right
            }
            bottomInsetsGuide.setGuidelineEnd(insets.bottom)
            windowInsets.inset(insets)
        }

        backButton.setOnClickListener {
            parentFragmentManager.popBackStack()
        }

        val searchInputHelper = SearchInputHelper(viewLifecycleOwner,
            searchInput,
            viewModel,
            onMessage = { snackBar.showText(it) },
        )
        this.searchInputHelper = searchInputHelper
        searchInputHelper.start()
        searchInput.setupClearButton(searchInputClearButton)

        recyclerView.itemAnimator = null

        val searchAdapter = SearchAdapter().also { searchAdapter = it }
        recyclerView.adapter = searchAdapter

        requireContext().preferencesRepository.preferencesFlow
            .map { it.isIsoDateFormatEnabled }
            .distinctUntilChanged()
            .onEach { useIsoFormat ->
                searchAdapter.dateFormatter = if (useIsoFormat) {
                    PAGE_ISO_DATE_FORMATTER
                } else {
                    DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)
                }
            }.launchIn(viewLifecycleOwner.lifecycleScope)

        searchAdapter.onSearchResultClick = { result ->
            if (viewLifecycleOwner.lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                clickedResultDate = result.date
                searchAdapter.clickedResultDate = result.date
                val fragment = PreviewPageFragment()
                fragment.arguments = Bundle().apply {
                    putLong(EXTRA_EPOCH_DAY, result.date.toEpochDay())
                    val match = result.matches.first()
                    putInt(EXTRA_TEXT_SELECTION, match.fileOffset)
                    putCharSequence(
                        EXTRA_SEARCH_INPUT,
                        searchInputHelper.sanitizedSearchInputText
                    )
                }
                parentFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commitAllowingStateLoss()
            }
        }

        Timber.tag(TAG).d("onCreateView: lastScrollDate=%s", lastScrollDate)
        searchAdapter.stateRestorationPolicy = StateRestorationPolicy.PREVENT
        searchAdapter.clickedResultDate = clickedResultDate
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                todayProvider.todayFlow.collect { searchAdapter.today = it }
            }
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                observeSearchResults(progressBar, searchAdapter)
            }
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.errorTokens.collect { errors ->
                    errorTextView.isVisible = errors.isNotEmpty()
                    errorTextView.text = errors.joinToString(separator = "\n") {
                        if (it.throwable is TextMessageException) {
                            it.throwable.getFormattedText(requireContext())
                        } else {
                            "${it.text}: ${it.throwable}"
                        }
                    }
                }
            }
        }
    }

    private suspend fun observeSearchResults(
        progressBar: View, searchAdapter: SearchAdapter
    ) {
        viewModel.searchResults.collectLatest { flow ->
            progressBar.isVisible = true
            searchAdapter.clear()
            flow.collect { event ->
                when (event) {
                    SearchCompletion -> {
                        Timber.tag(TAG).v("searchResults: search completed")
                        progressBar.isVisible = false
                        lastScrollDate?.let {
                            searchAdapter.stateRestorationPolicy = StateRestorationPolicy.ALLOW
                            lastScrollDate = null
                        }
                    }

                    is SearchResults -> {
                        Timber.tag(TAG).v("searchResults: received %d entries, lastScrollDate=%s", event.results.size, lastScrollDate)
                        searchAdapter.addAll(event.results)
                        lastScrollDate?.let { date ->
                            if (date >= event.results.last().date) {
                                Timber.tag(TAG).v("searchResults: enable state restoration")
                                searchAdapter.stateRestorationPolicy = StateRestorationPolicy.ALLOW
                                lastScrollDate = null
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        searchInputHelper!!.onViewStateRestored()
    }

    override fun onResume() {
        super.onResume()
        setupSoftInputVisible(searchInputHelper!!.searchInput)
        checkPendingSearchInput()
    }

    override fun onDestroyView() {
        saveScrollPosition()
        searchInputHelper = null
        recyclerView = null
        searchAdapter = null
        super.onDestroyView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        recyclerView?.let { saveScrollPosition() }
        outState.putSerializable(SAVE_CLICKED_RESULT_DATE, clickedResultDate)
        outState.putLong(SAVE_LAST_SCROLL_DAY, lastScrollDate?.toEpochDay() ?: Long.MIN_VALUE)
    }

    private fun saveScrollPosition() {
        val lm = recyclerView!!.layoutManager!!
        val position = lm.getChildAt(lm.childCount - 1)?.let { lm.getPosition(it) }
        lastScrollDate = position?.let { searchAdapter!!.getItemDate(it) }
        Timber.tag(TAG).d("saveScrollPosition: lastScrollDate=%s", lastScrollDate)
    }

    fun setSearchInput(searchInput: CharSequence) {
        val args = arguments ?: Bundle().also { arguments = it }
        args.putCharSequence(EXTRA_SEARCH_INPUT, searchInput)
        checkPendingSearchInput()
    }

    private fun checkPendingSearchInput() {
        arguments?.getCharSequence(EXTRA_SEARCH_INPUT)?.let { searchInput ->
            searchInputHelper?.searchInput?.text?.apply {
                replace(0, length, searchInput)
                arguments?.remove(EXTRA_SEARCH_INPUT)
            }
        }
    }

    companion object {
        private const val TAG = "SearchFragment"

        private const val SAVE_CLICKED_RESULT_DATE = "clickedResultDate"
        private const val SAVE_LAST_SCROLL_DAY = "lastScrollDay"
    }

}
