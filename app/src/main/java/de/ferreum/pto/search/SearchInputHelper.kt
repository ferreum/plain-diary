package de.ferreum.pto.search

import android.content.Context
import android.os.Build
import android.text.SpannableString
import android.text.Spanned
import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.ColorInt
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.LifecycleOwner
import de.ferreum.pto.R

fun EditText.setupClearButton(clearButton: View) {
    doAfterTextChanged { text ->
        clearButton.isVisible = text?.isNotEmpty() == true
    }
    clearButton.setOnClickListener {
        text.apply { replace(0, length, "") }
    }
}

interface SearchInputHandler {
    fun setSearchTokens(tokens: Collection<SearchToken>)
}

class SearchInputHelper(
    viewLifecycleOwner: LifecycleOwner,
    val searchInput: EditText,
    private val searchInputHandler: SearchInputHandler,
    private val onMessage: (CharSequence) -> Unit = {},
) {

    private val context get() = searchInput.context

    private val spanCycler = SpanCycler(SPAN_COLORS)
    private val tokenSpanInputSanitizer = TokenSpanInputSanitizer {
        searchInputHandler.setSearchTokens(searchInput.text.toTokenList()
            .map { TOKEN_MAPPER(it) })
    }
    private val currentTokenTypeWatcher = CurrentTokenTypeWatcher(viewLifecycleOwner, searchInput, TOKEN_TYPES) { type ->
        updateImeTokenTypeLabel(type, searchInput)
    }

    val sanitizedSearchInputText: CharSequence
        get() = SpannableString(searchInput.text).apply {
            removeSpan(tokenSpanInputSanitizer)
            removeSpan(currentTokenTypeWatcher)
        }

    fun setSearchInput(input: CharSequence) {
        searchInput.text.apply { replace(0, length, input) }
    }

    fun start() {
        searchInput.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                spanCycler.cycleAtSelection(searchInput.text)?.let { notifyTokenType(it) }
                tokenSpanInputSanitizer.sanitizeText(searchInput.text)
                true
            } else {
                false
            }
        }

        val callback = object : ActionMode.Callback {
            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return true
            }

            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                menu ?: return true
                ensureCreateTokenItem(menu)
                updateClearTokenItem(searchInput, menu)
                return true
            }

            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                when (item?.itemId) {
                    R.id.changeTokenType -> {
                        spanCycler.cycleAtSelection(searchInput.text)?.let { notifyTokenType(it) }
                        tokenSpanInputSanitizer.sanitizeText(searchInput.text)
                    }
                    R.id.clearToken -> {
                        searchInput.text.clearTokensAtSelection()
                        tokenSpanInputSanitizer.sanitizeText(searchInput.text)
                    }
                    else -> return false
                }
                return true
            }

            override fun onDestroyActionMode(mode: ActionMode?) {
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            searchInput.customInsertionActionModeCallback = callback
        }
        searchInput.customSelectionActionModeCallback = callback
    }

    fun onViewStateRestored() {
        searchInput.text.setSpan(tokenSpanInputSanitizer, 0, searchInput.text.length,
            Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        tokenSpanInputSanitizer.sanitizeText(searchInput.text)

        searchInput.text.setSpan(currentTokenTypeWatcher, 0, searchInput.text.length,
            Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        currentTokenTypeWatcher.scheduleCheck()
    }

    private fun notifyTokenType(@ColorInt color: Int) {
        val type = TOKEN_TYPES.firstOrNull { it.color == color } ?: TOKEN_TYPES.first()
        val format = context.getString(R.string.search_token_changed_message)
        val typename = context.getString(type.longName)
        val index = format.indexOf("%s")
        val spannable = SpannableString(format.replaceRange(index..index+1, typename))
        spannable.setTokenSpan(index, index + typename.length, type.color)
        onMessage(spannable)
    }

    private fun ensureCreateTokenItem(menu: Menu) {
        if (menu.findItem(R.id.changeTokenType) == null) {
            menu.add(Menu.NONE, R.id.changeTokenType, 1, R.string.search_action_change_token_type).apply {
                setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
            }
        }
    }

    private fun updateClearTokenItem(searchInput: EditText, menu: Menu) {
        menu.removeItem(R.id.clearToken)
        val tokenCount = searchInput.text.getTokenCountAtSelection()
        if (tokenCount > 0) {
            val title = searchInput.resources.getQuantityString(R.plurals.search_action_clear_token, tokenCount)
            menu.add(Menu.NONE, R.id.clearToken, 1, title).apply {
                setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
            }
        }
    }

    private fun updateImeTokenTypeLabel(type: TokenType?, searchInput: EditText) {
        val label = context.getText(type?.shortName ?: R.string.search_token_type_unmatched)
        searchInput.setImeActionLabel(label, EditorInfo.IME_ACTION_SEARCH)
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        // call it twice, as a single request seems to do nothing in most situations
        imm.restartInput(searchInput)
        imm.restartInput(searchInput)
    }

    companion object {
        private const val COLOR_GREY = 0x80757575.toInt()
        private const val COLOR_GREEN = 0x6055ff66
//        private const val COLOR_ORANGE = 0x60ffc855
//        private const val COLOR_RED = 0x60ff5555
        private const val COLOR_VIOLET = 0x60bb55ff

        private val TOKEN_TYPES = listOf(
            TokenType(COLOR_GREY, PlainTokenCreator,
                R.string.search_token_type_short_plain,
                R.string.search_token_type_long_plain),
            TokenType(COLOR_GREEN, WordBoundTokenCreator,
                R.string.search_token_type_short_word,
                R.string.search_token_type_long_word),
//            COLOR_ORANGE,
//            COLOR_RED,
            TokenType(COLOR_VIOLET, RegexTokenCreator,
                R.string.search_token_type_short_regex,
                R.string.search_token_type_long_regex),
        )

        private val SPAN_COLORS = TOKEN_TYPES.map { it.color }

        private val TOKEN_MAPPER = SearchTokenMapper(
            TOKEN_TYPES.associate { it.color to it.creator },
            PlainTokenCreator,
        )
    }

}
