package de.ferreum.pto.search

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.ferreum.pto.Constants
import de.ferreum.pto.preferences.Logger
import de.ferreum.pto.preferences.PtoFileHelper
import de.ferreum.pto.search.SearchToken.ErrorToken
import de.ferreum.pto.util.myDebounce
import de.ferreum.pto.util.myMapLatest
import de.ferreum.pto.util.optBuffer
import de.ferreum.pto.util.startWithReplayCache
import de.ferreum.pto.util.timeWindow
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.channels.BufferOverflow.DROP_OLDEST
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.conflate
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.takeWhile
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.job
import kotlinx.coroutines.plus
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.time.DateTimeException
import java.time.LocalDate
import java.time.YearMonth
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext

class SearchViewModel(
    private val ioContext: CoroutineContext,
    private val computeContext: CoroutineContext,
    private val fileHelper: Deferred<PtoFileHelper>,
    private val logger: Logger,
) : ViewModel(), SearchInputHandler {

    @VisibleForTesting
    var fileReadThrottleMillis = 0L

    @VisibleForTesting
    var searchParallelism = 8

    private val fileMatcher = FileMatcher(computeContext)

    private val searchTokens = MutableSharedFlow<Collection<SearchToken>>(
        replay = 1,
        onBufferOverflow = DROP_OLDEST
    )

    private val dayFiles: Deferred<Flow<FileListingEntry>> = viewModelScope.async {
        dayFilesForPagesDirFlow(fileHelper.await().pagesDir)
            .onCompletion { emit(FileListingEntry.ListingComplete) }
            .shareIn(viewModelScope + ioContext, SharingStarted.WhileSubscribed(5000, 0), replay = Int.MAX_VALUE)
    }

    val errorTokens = searchTokens.map { list ->
        list.filterIsInstance<ErrorToken>()
    }

    private var currentQueryScope: Job? = null

    val searchResults: Flow<Flow<SearchResultEvent>> = searchTokens
        .filter { list -> !list.any { it is ErrorToken } }
        .myDebounce { if (it.any(SearchToken::isLong)) 500 else 0 }
        .distinctUntilChanged()
        .myMapLatest { searchTokens ->
            Timber.tag(TAG).i("searchResults: new search: %s", searchTokens)
            currentQueryScope?.cancelAndJoin()
            val queryScope = viewModelScope + computeContext +
                Job(viewModelScope.coroutineContext.job)
            currentQueryScope = queryScope.coroutineContext.job
            if (searchTokens.any { it.isLong }) {
                searchFiles(searchTokens, dayFiles.await())
                    .onStart { Timber.tag(TAG).d("searchResults: starting search for %s", searchTokens) }
                    .onCompletion { Timber.tag(TAG).d("searchResults: completed search for %s (%s)", searchTokens, it?.toString() ?: "complete") }
                    .map { SearchResultEvent.ofResults(listOf(it)) }
                    .onCompletion { emit(SearchCompletion) }
                    .shareIn(queryScope, SharingStarted.Lazily, replay = Int.MAX_VALUE)
                    .startWithReplayCache { flow -> flow.timeWindow { if (it is SearchResults) 50 else 0 } }
                    .transform { transformSearchResultEventWindow(it) }
                    .flowOn(computeContext)
            } else {
                flowOf(SearchCompletion)
            }
        }
        .conflate()
        .shareIn(viewModelScope + computeContext, SharingStarted.Eagerly, replay = 1)

    private suspend fun FlowCollector<SearchResultEvent>.transformSearchResultEventWindow(list: List<SearchResultEvent>) {
        if (list.size == 1) {
            emit(list.single())
        } else {
            var res: MutableList<FileSearchResult>? = null
            for (item in list) {
                if (item is SearchResults) {
                    if (res == null) {
                        res = item.results.toMutableList()
                    } else {
                        res.addAll(item.results)
                    }
                } else {
                    res?.let { emit(SearchResults(it)) }
                    res = null
                    emit(item)
                }
            }
            res?.let { emit(SearchResults(it)) }
        }
    }

    private fun dayFilesForPagesDirFlow(homeDir: File): Flow<FileListingEntry> = flow {
        Timber.tag(TAG).d("dayFilesForHomeDirFlow: start listing for %s", homeDir)
        for (yearDir in getYearDirs(homeDir)) {
            val year = yearDir.name.toIntOrNull() ?: continue
            val monthDirs = getMonthDirs(yearDir)
            for (monthDir in monthDirs) {
                val yearMonth = try {
                    YearMonth.of(year, monthDir.name.toIntOrNull() ?: continue)
                } catch (e: DateTimeException) {
                    continue
                }
                for (dayFile in getDayFiles(monthDir)) {
                    val numberStr = dayFile.name.substring(0, 2)
                    val date = try {
                        yearMonth.atDay(numberStr.toIntOrNull() ?: continue)
                    } catch (e: DateTimeException) {
                        continue
                    }
                    emit(FileListingEntry.DayFile(date, dayFile))
                }
            }
        }
        Timber.tag(TAG).d("dayFilesForHomeDirFlow: finished listing for %s", homeDir)
    }

    private fun searchFiles(searchTokens: Collection<SearchToken>, dayFiles: Flow<FileListingEntry>): Flow<FileSearchResult> = flow {
        coroutineScope {
            dayFiles.takeWhile { it is FileListingEntry.DayFile }.map { entry ->
                entry as FileListingEntry.DayFile
                async { getFileMatches(searchTokens, entry) }
            }.optBuffer(searchParallelism).collect { deferred ->
                deferred.await()?.let { emit(it) }
            }
        }
    }

    private suspend fun getFileMatches(searchTokens: Collection<SearchToken>, entry: FileListingEntry.DayFile): FileSearchResult? {
        return try {
            if (fileReadThrottleMillis > 0) delay(fileReadThrottleMillis)
            val text = withContext(ioContext) { entry.file.readText() }
            fileMatcher.matchFile(entry.date, text, searchTokens)
        } catch (e: Exception) {
            coroutineContext.ensureActive()
            logger.suspendLogException(e, "search failed for file ${entry.file}", tag = TAG)
            null
        }
    }

    private fun getDayFiles(monthDir: File): List<File> {
        val list = monthDir.listFiles().orEmpty()
            .filterTo(mutableListOf()) { file ->
                val match = DAY_FILE_REGEX.matchEntire(file.name)
                val value = match?.groups?.get(1)?.value?.toIntOrNull() ?: return@filterTo false
                value in 1..31 && file.isFile
            }
        list.sortByDescending { it.name }
        return list
    }

    private fun getMonthDirs(yearDir: File): List<File> {
        val list = yearDir.listFiles().orEmpty()
            .filterTo(mutableListOf()) {
                val num = it.name.toIntOrNull() ?: return@filterTo false
                num in 1..12 && it.isDirectory
            }
        list.sortByDescending { it.name }
        return list
    }

    private fun getYearDirs(homeDir: File): List<File> {
        val list = homeDir.listFiles().orEmpty()
            .filterTo(mutableListOf()) {
                it.name.toIntOrNull() in Constants.YEAR_RANGE
                    && it.isDirectory
            }
        list.sortByDescending { it.name.toInt() }
        return list
    }

    override fun setSearchTokens(tokens: Collection<SearchToken>) {
        searchTokens.tryEmit(tokens)
    }

    private sealed class FileListingEntry {
        data object ListingComplete : FileListingEntry()
        data class DayFile(val date: LocalDate, val file: File) : FileListingEntry()
    }

    companion object {
        private val DAY_FILE_REGEX = PtoFileHelper.DAY_FILE_REGEX

        private const val TAG = "SearchViewModel"
    }

}

data class FileSearchResult(
    val date: LocalDate,
    val text: String,
    val matches: List<SearchMatch>,
)

data class SearchMatch(
    val fileOffset: Int,
    val textOffset: Int,
    val length: Int,
)

data class MatchRange(val start: Int, val end: Int)

sealed class SearchResultEvent {
    companion object {
        fun ofResults(results: List<FileSearchResult>): SearchResultEvent = SearchResults(results)
    }
}
data class SearchResults(val results: List<FileSearchResult>) : SearchResultEvent()
data object SearchCompletion : SearchResultEvent()
