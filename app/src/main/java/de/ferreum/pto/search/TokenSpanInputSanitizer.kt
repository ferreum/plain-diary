package de.ferreum.pto.search

import android.text.Editable
import android.text.Selection
import android.text.TextWatcher

class TokenSpanInputSanitizer(private val afterTextChanged: (text: Editable) -> Unit = {}) : TextWatcher {

    /** flag to prevent infinite loop while sanitizing */
    private var isSanitizing = false

    /** flag to allow backspacing over space between spans */
    private var isSpaceDeletion = false

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
        if (!isSanitizing) {
            isSpaceDeletion = after == 0 && count == 1 && s[start].isWhitespace()
        }
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable) {
        if (!isSanitizing) {
            doSanitize(s)
            afterTextChanged.invoke(s)
        }
    }

    fun sanitizeText(text: Editable) {
        isSpaceDeletion = false
        doSanitize(text)
        afterTextChanged.invoke(text)
    }

    private fun doSanitize(text: Editable) {
        isSanitizing = true
        // separate overlapping spans
        var prevRange = EMPTY_RANGE
        text.forEachTokenSpan loop@{ span, range ->
            if (span != null) {
                val newRange = if (range.first <= prevRange.last + 1) {
                    (prevRange.last + 1)..range.last
                } else {
                    range
                }
                if (newRange.isEmpty()) {
                    // needed? are they removed when range is deleted?
                    text.removeSpan(span)
                    return@loop
                }
                if (newRange != range) {
                    text.setSpan(span, range.first, range.last + 1, text.getSpanFlags(span))
                }
            }
            prevRange = range
        }
        // ensure space between spans
        var lastAddedSpace = -1
        text.forEachTokenSpan { span, range ->
            var changed = false
            val first = if (range.first == lastAddedSpace) {
                // undo expansion of this range by the previous space insertion
                changed = true
                range.first + 1
            } else {
                range.first
            }
            val last = range.last
            val nextIndex = last + 1
            if (text.length - 1 < nextIndex || !text[nextIndex].isWhitespace()) {
                if (span != null || nextIndex != text.length) {
                    // Note: this shifts ranges of following spans. No need to consider this in
                    // following calculations, because span's ranges are retrieved on iteration.
                    text.insert(nextIndex, " ")
                    if (isSpaceDeletion) {
                        val selStart = Selection.getSelectionStart(text)
                        if (selStart == nextIndex + 1) {
                            Selection.setSelection(text, nextIndex)
                        }
                    }
                    lastAddedSpace = nextIndex
                    // explicitly override span range, so insert doesn't expand span
                    changed = true
                }
            }
            if (span != null && changed) {
                text.setSpan(span, first, last + 1, text.getSpanFlags(span))
            }
        }
        isSanitizing = false
    }

    companion object {
        private val EMPTY_RANGE = -2..-2
    }

}
