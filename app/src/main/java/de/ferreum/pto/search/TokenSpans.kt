@file:Suppress("ReplaceRangeToWithUntil", "ConvertTwoComparisonsToRangeCheck")

package de.ferreum.pto.search

import android.text.Selection
import android.text.Spannable
import android.text.Spanned
import android.text.style.BackgroundColorSpan
import androidx.annotation.ColorInt
import androidx.annotation.Size
import androidx.core.text.getSpans
import de.ferreum.pto.util.isBlankAfter

data class TextWithColor(val text: String, val color: Int)

fun Spanned.toTokenList(): List<TextWithColor> {
    val tokens = mutableListOf<TextWithColor>()
    forEachTokenSpan { span, range ->
        val text = substring(range)
        if (span != null || text.isNotBlank()) {
            tokens.add(TextWithColor(text, span?.backgroundColor ?: 0))
        }
    }
    return tokens
}

inline fun Spanned.forEachTokenSpan(block: Spanned.(span: BackgroundColorSpan?, range: IntRange) -> Unit) {
    val tokenSpans = getSpans<BackgroundColorSpan>(0, length)
    tokenSpans.sortBy { getSpanStart(it) }
    var prevLast = -1
    tokenSpans.forEach { span ->
        var spanFirst = getSpanStart(span)
        var spanLast = getSpanEnd(span) - 1
        maybeRunOnFreeTextRange(prevLast, spanFirst) { range ->
            block(null, range)
            // get range again, because block may have changed it
            spanFirst = getSpanStart(span)
            spanLast = getSpanEnd(span) - 1
        }
        block(span, spanFirst..spanLast)
        prevLast = spanLast
    }
    maybeRunOnFreeTextRange(prevLast, length) { range ->
        block(null, range)
    }
}

inline fun Spanned.maybeRunOnFreeTextRange(prevLast: Int, nextFirst: Int, block: (IntRange) -> Unit) {
    val range = trimFreeTextRange((prevLast + 1)..(nextFirst - 1), 0)
    if (!range.isEmpty()) {
        block(range)
    }
}

class SpanCycler(
    @Size(min = 1)
    val colors: List<Int>,
) {
    @ColorInt
    fun cycleAtSelection(text: Spannable, @ColorInt color: Int? = null): Int? {
        val selStart = Selection.getSelectionStart(text)
        val selEnd = Selection.getSelectionEnd(text)

        val selType = text.getSelectionInfo()
        val newColor = when {
            selType is SelectionInfo.Empty -> return null
            color != null -> color
            selType is SelectionInfo.OnSpan -> getNextColor(selType.span.backgroundColor)
            else -> colors.first()
        }
        selType.spans.forEach { text.removeSpan(it) }
        text.setTokenSpan(selType.range, newColor)

        if (selStart == selEnd && selStart == selType.range.last + 1 && text.length > selStart && text.isBlankAfter(selStart)) {
            Selection.setSelection(text, selStart + 1)
        }

        return newColor
    }

    private fun getNextColor(color: Int): Int {
        val index = colors.indexOf(color)
        return if (index < 0 || index >= colors.size - 1) {
            colors[0]
        } else {
            colors[index + 1]
        }
    }
}

private val emptySpans = emptyArray<BackgroundColorSpan>()

fun Spanned.getSelectionInfo(): SelectionInfo {
    val selStart = Selection.getSelectionStart(this)
    val selEnd = Selection.getSelectionEnd(this)
    return if (selStart == selEnd) {
        getSelectionInfoAtPosition(selStart)
    } else {
        getSelectionInfoAtRange(selStart, selEnd)
    }
}

private fun Spanned.getSelectionInfoAtPosition(pos: Int): SelectionInfo {
    if (pos == 0 && isBlankAfter(pos)) return SelectionInfo.Empty
    val spans = getSpans<BackgroundColorSpan>(0, length)
    spans.sortBy { getSpanStart(it) }
    var freeFirst = 0
    var freeLast = length - 1
    // also find the span and its range to the left of the cursor
    var leftSpan: BackgroundColorSpan? = null
    var leftFirst = -1
    var leftLast = -1
    for (span in spans) {
        val first = getSpanStart(span)
        val last = getSpanEnd(span) - 1
        if (pos in first - 1..last + 1) {
            return SelectionInfo.OnSpan(arrayOf(span), span, first..last)
        }
        if (last < pos && last > leftLast) {
            leftSpan = span
            leftFirst = first
            leftLast = last
        }
        if (last < pos && last + 1 > freeFirst) {
            freeFirst = last + 1
        }
        if (first > pos && first - 1 < freeLast) {
            freeLast = first - 1
            if (first > 0 && this[first - 1].isWhitespace()) {
                // trim whitespace before start of token
                freeLast--
            }
        }
    }
    val freeRange = trimFreeTextRange(freeFirst..freeLast, pos)
    if (freeRange.isEmpty() && leftSpan != null && leftLast == pos - 2 && this[pos - 1].isWhitespace()) {
        // use span to the left of the cursor if separated by a whitespace
        return SelectionInfo.OnSpan(arrayOf(leftSpan), leftSpan, leftFirst..leftLast)
    }
    return SelectionInfo.Free(freeRange)
}

private fun Spanned.getSelectionInfoAtRange(selStart: Int, selEnd: Int): SelectionInfo {
    val spans = getSpans(selStart, selEnd, BackgroundColorSpan::class.java)
    var foundSpan: BackgroundColorSpan? = null
    var foundStart = -1
    var foundEnd = -1
    for (span in spans) {
        val start = getSpanStart(span)
        val end = getSpanEnd(span)
        if (start > selStart || end < selEnd) {
            return SelectionInfo.Unmatched(spans, selStart until selEnd)
        }
        if (foundSpan == null) {
            foundSpan = span
            foundStart = start
            foundEnd = end
        } else {
            return SelectionInfo.Unmatched(spans, selStart until selEnd)
        }
    }
    return foundSpan?.let {
        SelectionInfo.OnSpan(spans, it, foundStart until foundEnd)
    } ?: SelectionInfo.Free(selStart until selEnd)
}

sealed class SelectionInfo(val spans: Array<BackgroundColorSpan>) {
    abstract val span: BackgroundColorSpan?
    abstract val range: IntRange

    /** Input is empty **/
    data object Empty : SelectionInfo(emptySpans) {
        override val span: Nothing? get() = null
        override val range: IntRange get() = IntRange.EMPTY
    }

    /** Selection is not connected to any spans **/
    class Free(override val range: IntRange) : SelectionInfo(emptySpans) {
        override val span: Nothing? get() = null
    }

    /** Selection is contained by a token span **/
    class OnSpan(
        spans: Array<BackgroundColorSpan>,
        override val span: BackgroundColorSpan,
        override val range: IntRange,
    ) : SelectionInfo(spans)

    /** Selection overlaps with token span bounds */
    class Unmatched(
        spans: Array<BackgroundColorSpan>,
        override val range: IntRange,
    ) : SelectionInfo(spans) {
        override val span: Nothing? get() = null
    }
}

fun Spannable.setTokenSpan(range: IntRange, @ColorInt color: Int) {
    setTokenSpan(range.first, range.last + 1, color)
}

fun Spannable.setTokenSpan(start: Int, end: Int, @ColorInt color: Int) {
    setSpan(BackgroundColorSpan(color), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
}

fun Spanned.trimFreeTextRange(range: IntRange, endLimit: Int): IntRange {
    if (range.isEmpty()) return range
    var first = range.first
    var last = range.last.coerceAtMost(length - 1)
    if (first > 0 && this[first].isWhitespace()) {
        first++
    }
    while (last >= endLimit && this[last].isWhitespace()) {
        last--
    }
    return first..last
}

fun Spanned.getTokenCountAtSelection(): Int {
    val selStart = Selection.getSelectionStart(this)
    val selEnd = Selection.getSelectionEnd(this)
    val spans = getSpans<BackgroundColorSpan>(selStart, selEnd)
    return spans.size
}

fun Spannable.clearTokensAtSelection() {
    val start = Selection.getSelectionStart(this)
    val end = Selection.getSelectionEnd(this)
    getSpans<BackgroundColorSpan>(start, end).forEach { removeSpan(it) }
}
