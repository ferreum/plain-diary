package de.ferreum.pto.search

import android.content.Context
import android.os.Parcelable
import androidx.annotation.ColorInt
import androidx.annotation.StringRes
import de.ferreum.pto.R
import kotlinx.parcelize.Parcelize
import java.util.regex.PatternSyntaxException
import kotlin.text.RegexOption.IGNORE_CASE
import kotlin.text.RegexOption.MULTILINE
import kotlin.text.RegexOption.UNIX_LINES

class TextMessageException(
    cause: Throwable? = null,
    private val text: String?,
    private val index: Int,
    private val description: String?,
    @StringRes
    val messageRes: Int,
): Exception(cause) {
    fun getFormattedText(context: Context): CharSequence {
        val text = context.getString(messageRes)
        return if (this.text != null || index >= 0 || description != null) {
            StringBuilder(text).also { sb ->
                if (description != null) {
                    sb.append(": ")
                    sb.append(description)
                }
                if (this.text != null) {
                    sb.append("\n")
                    sb.append(this.text)
                }
                if (index >= 0) {
                    sb.append("\n")
                    repeat(index - 1) { sb.append(" ") }
                    sb.append("^")
                }
            }
        } else {
            text
        }
    }
}

typealias SearchTokenCreator = (text: String, color: Int) -> SearchToken

data class TokenType(
    @ColorInt
    val color: Int,
    val creator: SearchTokenCreator,
    @StringRes
    val shortName: Int,
    @StringRes
    val longName: Int,
)

class SearchTokenMapper(
    private val tokenTypes: Map<Int, SearchTokenCreator>,
    private val fallback: SearchTokenCreator,
) {
    operator fun invoke(textWithColor: TextWithColor) = try {
        val func = tokenTypes[textWithColor.color] ?: fallback
        func(textWithColor.text, textWithColor.color)
    } catch (e: Throwable) {
        SearchToken.ErrorToken(textWithColor.text, textWithColor.color, e)
    }
}

object PlainTokenCreator : SearchTokenCreator {
    override fun invoke(text: String, color: Int) = SearchToken.PlainToken(text)
}

object WordBoundTokenCreator : SearchTokenCreator {
    private val segmentsRegex = Regex("""\w+|\h+|[^\w\h]+""")
    private val wordRegex = Regex("""\w+""")

    private val options = setOf(MULTILINE, UNIX_LINES, IGNORE_CASE)

    override fun invoke(text: String, color: Int): SearchToken {
        val builder = StringBuilder()
        val segments = segmentsRegex.findAll(text)
            .map { it.value to it.value.segType() }
            .dropWhile { it.second == SegType.Blank }
            .toMutableList()
        while (segments.isNotEmpty() && segments.last().second == SegType.Blank) {
            segments.removeAt(segments.lastIndex)
        }
        if (segments.isEmpty()) {
            throw TextMessageException(null, null, -1, null, R.string.search_token_error_blank_wordbound_token)
        }
        var prevSegType: SegType? = null
        for ((segment, segType) in segments) {
            appendSeparator(builder, prevSegType, segType)
            builder.append(when (segType) {
                SegType.Blank -> """\h+"""
                SegType.Word, SegType.Special -> Regex.escape(segment)
            })
            prevSegType = segType
        }
        appendSeparator(builder, prevSegType, null)
        return SearchToken.RegexToken(Regex(builder.toString(), options))
    }

    private fun appendSeparator(builder: StringBuilder, fromType: SegType?, toType: SegType?) {
        builder.append(when (fromType to toType) {
            // word-bounds at start or end of input
            null to SegType.Word -> """\b"""
            SegType.Word to null -> """\b"""

            // optional whitespace on word <-> special char bounds
            SegType.Word to SegType.Special -> """\h*"""
            SegType.Special to SegType.Word -> """\h*"""

            // others: no separator pattern
            else -> ""
        })
    }

    private fun String.segType() = when {
        isBlank() -> SegType.Blank
        wordRegex.matches(this) -> SegType.Word
        else -> SegType.Special
    }

    private enum class SegType {
        Blank, Word, Special
    }
}

object RegexTokenCreator : SearchTokenCreator {
    private val options = setOf(MULTILINE, UNIX_LINES, IGNORE_CASE)

    override fun invoke(text: String, color: Int) = try {
        SearchToken.RegexToken(Regex(text, options))
    } catch (e: PatternSyntaxException) {
        throw TextMessageException(e, text, e.index, e.description, R.string.search_token_error_invalid_regex)
    }
}

sealed class SearchToken : Parcelable {
    abstract val isLong: Boolean
    @Parcelize
    data class PlainToken(val text: String) : SearchToken() {
        override val isLong get() = text.length > 2
    }
    @Parcelize
    data class RegexToken(val regex: Regex) : SearchToken() {
        override val isLong get() = true

        override fun equals(other: Any?): Boolean {
            if (other === this) return true
            if (other !is RegexToken) return false
            return regex.pattern == other.regex.pattern
        }

        override fun hashCode(): Int {
            return regex.pattern.hashCode()
        }
    }
    @Parcelize
    data class ErrorToken(val text: String, val color: Int, val throwable: Throwable) : SearchToken() {
        override val isLong get() = false
    }
}
