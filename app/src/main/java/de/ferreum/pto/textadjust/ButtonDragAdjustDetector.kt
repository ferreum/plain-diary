package de.ferreum.pto.textadjust

import android.content.Context
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewConfiguration
import kotlin.math.abs

class ButtonDragAdjustDetector(
    context: Context,
    private val listener: DragAdjustListener,
) : OnTouchListener {
    interface DragAdjustListener {
        /**
         * Start adjustment for given [startEvent].
         */
        fun onDragAdjustStart(startEvent: MotionEvent)

        /**
         * Adjust for the given [startEvent] and latest [event].
         *
         * Returns `true` if adjustment was made, `false` otherwise.
         */
        fun onDragAdjust(startEvent: MotionEvent, event: MotionEvent): Boolean

        /**
         * End adjustment for given [startEvent] and last [event].
         *
         * Returns `true` if adjustment was made, `false` otherwise.
         */
        fun onDragAdjustEnd(startEvent: MotionEvent, event: MotionEvent): Boolean
    }

    private var didDrag = false
    private var didAdjust = false
    private var startEvent: MotionEvent? = null

    private val scaledTouchSlop = ViewConfiguration.get(context).scaledTouchSlop.toFloat()

    override fun onTouch(view: View, event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                startEvent?.recycle()
                startEvent = MotionEvent.obtain(event)
                didDrag = false
                didAdjust = false
                view.isPressed = true
            }
            MotionEvent.ACTION_MOVE -> {
                val startEvent = startEvent ?: return false
                val dx = event.x - startEvent.x
                val dy = event.y - startEvent.y
                if (!didDrag && (abs(dx) >= scaledTouchSlop || abs(dy) >= scaledTouchSlop)) {
                    didDrag = true
                    listener.onDragAdjustStart(startEvent)
                }
                if (didDrag) {
                    val adjusted = listener.onDragAdjust(startEvent, event)
                    if (adjusted && !didAdjust) {
                        didAdjust = true
                        view.parent?.requestDisallowInterceptTouchEvent(true)
                    }
                }
            }
            MotionEvent.ACTION_UP -> {
                val startEvent = startEvent ?: return false
                view.isPressed = false
                if (didDrag) {
                    if (listener.onDragAdjustEnd(startEvent, event)) {
                        didAdjust = true
                    }
                }
                if (!didAdjust) {
                    view.performClick()
                }
                startEvent.recycle()
                this.startEvent = null
                view.parent?.requestDisallowInterceptTouchEvent(false)
            }
            MotionEvent.ACTION_CANCEL -> {
                // if startEvent is null, we didn't receive a down event since last touch
                val startEvent = startEvent ?: return false
                view.isPressed = false
                if (didDrag) {
                    listener.onDragAdjustEnd(startEvent, event)
                }
                startEvent.recycle()
                this.startEvent = null
                view.parent?.requestDisallowInterceptTouchEvent(false)
            }
        }
        return true
    }
}
