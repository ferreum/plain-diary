package de.ferreum.pto.textadjust

import android.os.Build
import android.view.HapticFeedbackConstants
import android.view.MotionEvent
import android.widget.EditText
import de.ferreum.pto.textadjust.ButtonDragAdjustDetector.DragAdjustListener
import de.ferreum.pto.textadjust.InplaceTextAdjuster.Adjuster

class DragAdjustHandler(
    private val editText: EditText,
    private val inplaceTextAdjuster: InplaceTextAdjuster,
    private val startStopListener: (started: Boolean) -> Unit = {},
) : DragAdjustListener {
    private var adjuster: Adjuster? = null

    override fun onDragAdjustStart(startEvent: MotionEvent) {
        startStopListener(true)
        adjuster = inplaceTextAdjuster.startOnCursor(editText.text, startEvent)
    }

    override fun onDragAdjust(startEvent: MotionEvent, event: MotionEvent): Boolean {
        adjuster?.let { adjuster ->
            if (adjuster.adjustTo(editText.text, startEvent, event)) {
                editText.stopNestedScroll()
                if (Build.VERSION.SDK_INT >= 27) {
                    editText.performHapticFeedback(HapticFeedbackConstants.TEXT_HANDLE_MOVE)
                }
                return true
            }
        }
        return false
    }

    override fun onDragAdjustEnd(startEvent: MotionEvent, event: MotionEvent): Boolean {
        return onDragAdjust(startEvent, event).also {
            adjuster?.finish()
            adjuster = null
            startStopListener(false)
        }
    }
}
