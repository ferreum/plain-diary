package de.ferreum.pto.textadjust

import android.content.Context
import android.text.Selection
import android.view.MotionEvent
import de.ferreum.pto.R
import de.ferreum.pto.page.content.EventContent
import de.ferreum.pto.reminder.ReminderParser
import de.ferreum.pto.textadjust.InplaceTextAdjuster.AcceptSpec
import de.ferreum.pto.textadjust.InplaceTextAdjuster.TextGenerator
import de.ferreum.pto.textadjust.InplaceTextAdjuster.TextWithSelection
import de.ferreum.pto.util.withInsertedAt
import de.ferreum.pto.util.withReplacedAt

internal class EventReminderGenerator(context: Context) : TextGenerator {
    private val stepSize =
        context.resources.getDimensionPixelSize(R.dimen.drag_adjust_step_size).toFloat()

    override val acceptSpecs: List<AcceptSpec> get() = ACCEPT_SPECS

    override fun generateNew(
        startEvent: MotionEvent,
        event: MotionEvent,
        adjustContext: InplaceTextAdjuster.AdjustContext,
    ): TextWithSelection? {
        return null
    }

    override fun adjust(
        startText: String,
        currentText: String,
        startEvent: MotionEvent,
        prevEvent: MotionEvent,
        event: MotionEvent,
        adjustContext: InplaceTextAdjuster.AdjustContext,
    ): TextWithSelection? {
        if (event.actionMasked == MotionEvent.ACTION_CANCEL) {
            // preserve text on cancel
            return null
        }

        val prevStep = getAdjustStep(startEvent, prevEvent)
        val currentStep = getAdjustStep(startEvent, event)
        if (prevStep == currentStep) return null

        val startMatch = ReminderParser.EVENT_REGEX.matchEntire(startText)
            ?: return null
        val startTextType = ReminderParser.parseEventTextType(startMatch, 0)
            ?: return null
        val currentMatch = ReminderParser.EVENT_REGEX.matchEntire(currentText)
            ?: return null
        val selection = Selection.getSelectionStart(adjustContext.text)
        val currentTextType = ReminderParser.parseEventTextType(
            currentMatch, selection - adjustContext.matchStart)
            ?: return null

        val startReminders = startTextType.eventContent.reminders
        val currentReminders = currentTextType.eventContent.reminders

        val didInsertReminder = startReminders.size < currentReminders.size

        val doInsertNew: Boolean
        // index of modified reminder; may be out of bounds if doInsertNew is true
        val reminderSelection: Int
        if (didInsertReminder) {
            doInsertNew = false
            reminderSelection = currentTextType.reminderSelection
            if (reminderSelection !in currentReminders.indices) return null
        } else if (
            currentTextType.reminderSelection >= 0
            && startReminders.singleOrNull() != ""
            && adjustContext.text.getOrNull(selection - 1)?.isWhitespace() == true
            && adjustContext.text.getOrNull(selection)?.isWhitespace() == true
        ) {
            doInsertNew = true
            reminderSelection = (currentTextType.reminderSelection + 1)
                .coerceAtMost(currentReminders.size)
        } else if (startReminders.singleOrNull() == ""
            || (currentTextType.reminderSelection <= 0
                && startReminders.firstOrNull().isImplicitReminder())
        ) {
            // modify existing implicit reminder and do not insert new
            doInsertNew = false
            reminderSelection = 0
        } else if (currentTextType.reminderSelection !in currentReminders.indices) {
            doInsertNew = true
            reminderSelection = currentTextType.reminderSelection.coerceAtLeast(0)
        } else {
            doInsertNew = false
            reminderSelection = currentTextType.reminderSelection.coerceAtLeast(0)
        }

        check(doInsertNew || reminderSelection in currentReminders.indices) {
            "reminderSelection out of bounds: $reminderSelection not in ${currentReminders.indices}"
        }

        val startReminderSpec = if (didInsertReminder || doInsertNew) {
            "-0m"
        } else {
            val spec = startReminders[reminderSelection]
            if (spec.isEmpty() || ReminderParser.FLAG_REGEX.matches(spec)) {
                // convert empty default reminder to explicit, preserving flags
                "-0m$spec"
            } else {
                spec
            }
        }

        val currentSpecString = when {
            doInsertNew -> "-0m"
            else -> currentTextType.eventContent.reminders[reminderSelection]
        }

        val startSpecMatch = REGEX_REMINDER_SPEC.matchEntire(startReminderSpec)
            ?: return null
        val currentNumber = REGEX_REMINDER_SPEC.matchEntire(currentSpecString)
            ?.groupValues?.get(2)
            ?.toIntOrNull() ?: 0

        val prefix = startSpecMatch.groupValues[1]
        val suffix = startSpecMatch.groupValues[3]

        val startNumber = startSpecMatch.groupValues[2].toIntOrNull()
            ?: return null

        val adjNumber = if (prefix.startsWith("+")) {
            startNumber - currentStep
        } else {
            startNumber + currentStep
        }
        val newPrefix: String
        val newNumber = if (prefix == "+" || prefix == "-") {
            if (adjNumber < 0) {
                newPrefix = if (prefix == "+") "-" else "+"
                -adjNumber
            } else {
                newPrefix = prefix
                adjNumber
            }
        } else {
            // can only adjust last unit; carry not implemented
            newPrefix = prefix
            adjNumber.coerceAtLeast(0)
        }
        if (newNumber == currentNumber) return null

        val newReminder = "$newPrefix$newNumber$suffix"
        val newReminders = if (doInsertNew) {
            currentReminders.withInsertedAt(reminderSelection, newReminder)
        } else {
            currentReminders.withReplacedAt(reminderSelection, newReminder)
        }

        val (text, pos) = ReminderParser.formatReminder(
            EventContent(
                currentTextType.eventContent.time,
                newReminders,
                currentTextType.eventContent.title
            ),
            reminderSelection.coerceIn(newReminders.indices)
        )

        return TextWithSelection(text, pos ?: text.length)
    }

    private fun String?.isImplicitReminder(): Boolean =
        this != null && ReminderParser.FLAG_REGEX.matches(this)

    private fun getAdjustStep(startEvent: MotionEvent, prevEvent: MotionEvent): Int {
        return ((prevEvent.y - startEvent.y) / stepSize).toInt()
    }

    companion object {
        private val REGEX_REMINDER_SPEC = Regex("""^(.*?)(\d+)([dhms][?!]?)$""")

        private val ACCEPT_SPECS = listOf(
            AcceptSpec(ReminderParser.EVENT_REGEX, group = 0, TextGenerator.ACCEPT_WHOLE_LINE),
        )
    }

}
