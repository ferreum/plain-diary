package de.ferreum.pto.textadjust

import android.text.Editable
import android.text.Selection
import android.view.MotionEvent
import androidx.annotation.CheckResult
import de.ferreum.pto.textadjust.InplaceTextAdjuster.TextGenerator.Companion.ACCEPT_WHOLE_LINE
import de.ferreum.pto.util.fastSubstring
import de.ferreum.pto.util.lineRangeAt
import kotlin.math.max
import kotlin.math.min

class InplaceTextAdjuster(private val generator: TextGenerator) {

    /**
     * Context about the current text adjustment.
     *
     * [matchStart] and [matchEnd] represent the range of the text to be replaced, with
     * exclusive end. [text] is the whole content of the text editor.
     */
    data class AdjustContext(
        val text: CharSequence,
        val matchStart: Int,
        val matchEnd: Int,
        val acceptSpec: AcceptSpec? = null,
    )

    data class TextWithSelection(
        val text: CharSequence,
        val selection: Int = text.length,
    )

    data class AcceptSpec(
        /**
         * Regex used to find the existing text under cursor.
         *
         * May specify groups, in which case group 1 is used instead of the whole match.
         * `startText` still receives the whole match.
         */
        val regex: Regex,
        /**
         * Which group to accept from [regex].
         */
        val group: Int,
        /**
         * How many characters to look around the cursor to find a match with [length],
         * or [ACCEPT_WHOLE_LINE].
         */
        val length: Int,
    )

    interface TextGenerator {
        val acceptSpecs: List<AcceptSpec>

        /**
         * Get new text for starting adjustment from an unmatched text position.
         *
         * If cursor was a selection, the selected text is replaced.
         *
         * Returns `null` when no text should be inserted. The adjustment continues, possibly
         * inserting text in later events.
         */
        fun generateNew(
            startEvent: MotionEvent,
            event: MotionEvent,
            adjustContext: AdjustContext,
        ): TextWithSelection?

        /**
         * Get adjusted text and new cursor position for given events.
         *
         * [startEvent] is the very first event (the down event), [event] is the latest event.
         *
         * [prevEvent] is the [event] of the previous call, or, for the very first [adjust] call,
         * the [startEvent].
         *
         * Returns `null` to preserve [currentText].
         */
        fun adjust(
            startText: String,
            currentText: String,
            startEvent: MotionEvent,
            prevEvent: MotionEvent,
            event: MotionEvent,
            adjustContext: AdjustContext,
        ): TextWithSelection?

        companion object {
            const val ACCEPT_WHOLE_LINE: Int = -2
        }
    }

    /**
     * Start adjusting text on cursor position.
     *
     * Feed events to returned [Adjuster]'s [adjustTo][Adjuster.adjustTo] method.
     *
     * Does not take ownership of [startEvent].
     */
    @CheckResult
    fun startOnCursor(editable: Editable, startEvent: MotionEvent): Adjuster {
        return getTextMatch(editable)?.let { (_, range) ->
            val origText = editable.fastSubstring(range.first, range.last + 1)
            Adjuster(origText, startEvent)
        } ?: Adjuster(null, startEvent) // start for new text insertion
    }

    /**
     * Find a match under the cursor of [text].
     */
    private fun getTextMatch(text: CharSequence): Pair<AcceptSpec, IntRange>? {
        val selectionStart = Selection.getSelectionStart(text)
        val selectionEnd = Selection.getSelectionEnd(text)
        if (selectionStart < 0 || selectionEnd < 0) {
            return null
        }
        return generator.acceptSpecs.firstNotNullOfOrNull {
            applyAcceptSpec(text, it, selectionStart, selectionEnd)?.let { range -> it to range }
        }
    }

    /**
     * Try to match [acceptSpec] on [text]. Matches in range according to [AcceptSpec.length].
     */
    private fun applyAcceptSpec(
        text: CharSequence,
        acceptSpec: AcceptSpec,
        selectionStart: Int,
        selectionEnd: Int,
    ): IntRange? {
        val region = when (val acceptLength = acceptSpec.length) {
            ACCEPT_WHOLE_LINE -> {
                text.lineRangeAt(selectionStart)
                    .let { if (it.last + 1 < selectionEnd) IntRange.EMPTY else it }
            }

            else -> {
                val from = max(0, selectionStart - acceptLength)
                val to = min(text.length, selectionEnd + acceptLength)
                from..<to
            }
        }

        if (region.isEmpty()) return null

        val substring = text.fastSubstring(region.first, region.last + 1)

        val matcher = acceptSpec.regex.toPattern()
            .matcher(substring)

        // Search first match overlapping with selection
        while (matcher.find()) {
            val group = acceptSpec.group
            val start = matcher.start(group) + region.first
            val end = matcher.end(group) + region.first
            if (start > selectionEnd) {
                return null
            }
            if (selectionStart <= end) {
                return start..<end
            }
        }
        return null
    }

    inner class Adjuster(private var startText: String?, startEvent: MotionEvent) {
        private var prevEvent: MotionEvent? = MotionEvent.obtain(startEvent)

        /**
         * Update text adjusted with given [startEvent] and latest [event].
         *
         * Does not take ownership of [startEvent] or [event].
         *
         * Returns `true` if the text has been changed compared to the previous call.
         */
        @CheckResult
        fun adjustTo(editable: Editable, startEvent: MotionEvent, event: MotionEvent): Boolean {
            val prev = checkNotNull(prevEvent) { "adjustTo called after finish()" }
            val didAdjust = adjustOnCursor(editable, startEvent, prev, event)
            prevEvent?.recycle()
            prevEvent = MotionEvent.obtain(event)
            return didAdjust
        }

        private fun adjustOnCursor(
            editable: Editable,
            startEvent: MotionEvent,
            prevEvent: MotionEvent,
            newEvent: MotionEvent,
        ): Boolean {
            val insertNew = startText == null

            if (insertNew) {
                if (newEvent.actionMasked == MotionEvent.ACTION_CANCEL) {
                    return false // no insertion for cancel event
                }
                val selStart = Selection.getSelectionStart(editable)
                val selEnd = Selection.getSelectionEnd(editable)
                if (selStart < 0 || selEnd < 0) {
                    return false // no selection
                }
                val context = AdjustContext(editable, selStart, selEnd)
                val result = generator.generateNew(startEvent, newEvent, context)
                    ?: return false // no insertion
                editable.replace(selStart, selEnd, result.text)
                Selection.setSelection(editable, selStart + result.selection)
                startText = result.text.toString()
            }

            val (acceptSpec, range) = getTextMatch(editable)
                ?: return insertNew // new inserted text does not match; stop adjustment
            val matchText = editable.fastSubstring(range.first, range.last + 1)

            if (insertNew) {
                startText = matchText
            }

            val context = AdjustContext(
                editable,
                range.first,
                range.last + 1,
                acceptSpec,
            )
            val result =
                generator.adjust(startText!!, matchText, startEvent, prevEvent, newEvent, context)
                    ?: return insertNew
            editable.replace(range.first, range.last + 1, result.text)
            Selection.setSelection(editable, range.first + result.selection)
            return true
        }

        fun finish() {
            prevEvent?.recycle()
            prevEvent = null
        }
    }
}
