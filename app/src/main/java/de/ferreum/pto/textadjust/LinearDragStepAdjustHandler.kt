package de.ferreum.pto.textadjust

import android.content.Context
import android.os.Build
import android.view.HapticFeedbackConstants
import android.view.MotionEvent
import android.view.View
import de.ferreum.pto.R

class LinearDragStepAdjustHandler(
    context: Context,
    private val view: View,
    private val listener: AdjustStepListener,
) : ButtonDragAdjustDetector.DragAdjustListener {

    private val stepSize = context.resources.getDimensionPixelSize(R.dimen.drag_adjust_step_size).toFloat()

    private var prevStep = 0

    override fun onDragAdjustStart(startEvent: MotionEvent) {
        prevStep = 0
        listener.onAdjustStart()
    }

    override fun onDragAdjust(startEvent: MotionEvent, event: MotionEvent): Boolean {
        val step = getAdjustStep(startEvent, event)
        if (step == prevStep) {
            // no step taken since last event
            return false
        }
        prevStep = step
        val handled = listener.onAdjustStepChange(step)
        if (handled) {
            view.stopNestedScroll()
            if (Build.VERSION.SDK_INT >= 27) {
                view.performHapticFeedback(HapticFeedbackConstants.TEXT_HANDLE_MOVE)
            }
        }
        return true
    }

    override fun onDragAdjustEnd(startEvent: MotionEvent, event: MotionEvent): Boolean {
        val step = getAdjustStep(startEvent, event)
        return if (event.actionMasked == MotionEvent.ACTION_CANCEL) {
            listener.onAdjustEnd(0)
        } else {
            listener.onAdjustEnd(step)
        }
    }

    private fun getAdjustStep(startEvent: MotionEvent, prevEvent: MotionEvent): Int {
        return ((prevEvent.y - startEvent.y) / stepSize).toInt()
    }

    interface AdjustStepListener {
        fun onAdjustStart()

        fun onAdjustStepChange(step: Int): Boolean

        fun onAdjustEnd(step: Int): Boolean
    }

}

class InvertedAdjustStepListener(
    private val wrapped: LinearDragStepAdjustHandler.AdjustStepListener,
) : LinearDragStepAdjustHandler.AdjustStepListener {
    override fun onAdjustStart() = wrapped.onAdjustStart()
    override fun onAdjustStepChange(step: Int) = wrapped.onAdjustStepChange(-step)
    override fun onAdjustEnd(step: Int) = wrapped.onAdjustEnd(-step)
}

fun LinearDragStepAdjustHandler.AdjustStepListener.asInverted(): LinearDragStepAdjustHandler.AdjustStepListener {
    return InvertedAdjustStepListener(this)
}
