package de.ferreum.pto.textadjust

import android.content.Context
import android.view.MotionEvent
import de.ferreum.pto.R
import de.ferreum.pto.preferences.toDateTimeOnPage
import de.ferreum.pto.reminder.ReminderParser
import de.ferreum.pto.textadjust.InplaceTextAdjuster.AcceptSpec
import de.ferreum.pto.textadjust.InplaceTextAdjuster.AdjustContext
import de.ferreum.pto.textadjust.InplaceTextAdjuster.TextGenerator
import de.ferreum.pto.textadjust.InplaceTextAdjuster.TextWithSelection
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.util.Locale

internal class TimeTextGenerator(
    context: Context,
    private val pageDate: LocalDate,
    var newpageTime: LocalTime,
) : TextGenerator {

    private val stepSize =
        context.resources.getDimensionPixelSize(R.dimen.drag_adjust_step_size).toFloat()

    private val parseFormat = DateTimeFormatter.ofPattern("H:m", Locale.getDefault())
    private val formatter = DateTimeFormatter.ofPattern("H:mm", Locale.getDefault())

    override val acceptSpecs: List<AcceptSpec> get() = ACCEPT_SPECS

    override fun generateNew(
        startEvent: MotionEvent,
        event: MotionEvent,
        adjustContext: AdjustContext,
    ): TextWithSelection? {
        if (getAdjustStep(startEvent, event) == 0) {
            return null
        }
        return TextWithSelection(
            getTimeText(LocalTime.now(), adjustContext.text, adjustContext.matchStart)
        )
    }

    override fun adjust(
        startText: String,
        currentText: String,
        startEvent: MotionEvent,
        prevEvent: MotionEvent,
        event: MotionEvent,
        adjustContext: AdjustContext,
    ): TextWithSelection? {
        if (event.actionMasked == MotionEvent.ACTION_CANCEL) {
            // preserve text on cancel
            return null
        }
        val prevStep = getAdjustStep(startEvent, prevEvent)
        val step = getAdjustStep(startEvent, event)
        if (step == prevStep) {
            // no step taken since last event
            return null
        }

        val isEventMatch = adjustContext.acceptSpec == SPEC_EVENT
        val split = splitEvent(startText)
            ?: return null
        val startTimeText = split.first
        val suffix = if (isEventMatch) split.second else ""

        val atPrefix = when {
            isEventMatch -> AtPrefix.ALWAYS
            currentText.startsWith("@") -> AtPrefix.AUTO_REMOVE
            else -> AtPrefix.AUTO_INSERT
        }
        val startTime = parseTime(startTimeText.removePrefix("@"))
            ?: return null // not a valid start time
        val prevTime = startTime.minusMinutes(prevStep.toLong())
        val newTime = startTime.minusMinutes(step.toLong())
        val newTimeText =
            getTimeText(newTime, adjustContext.text, adjustContext.matchStart, atPrefix, prevTime)
        return TextWithSelection(newTimeText + suffix, newTimeText.length)
    }

    private fun splitEvent(currentText: String): Pair<String, String>? {
        val matcher = SPEC_TIME.regex.toPattern().matcher(currentText)
        if (!matcher.lookingAt()) return null

        val timeEnd = matcher.end()
        if (timeEnd >= currentText.length) return currentText to ""

        val timeText = currentText.substring(0, timeEnd)
        val suffix = currentText.substring(timeEnd, currentText.length)
        return timeText to suffix
    }

    fun getTimeText(
        newTime: LocalTime,
        text: CharSequence,
        startPos: Int,
        atPrefix: AtPrefix = AtPrefix.AUTO,
        prevTime: LocalTime? = null,
    ): String {
        val newText = newTime.format(formatter)
        val prevChar = if (startPos > 0) text[startPos - 1] else null

        return if (atPrefix == AtPrefix.ALWAYS) {
            "@$newText"
        } else if (prevChar == null || prevChar == '\n') {
            // turn into event if in future
            val newDateTime = newTime.toDateTimeOnPage(pageDate, newpageTime)
            val prevDateTime = prevTime?.toDateTimeOnPage(pageDate, newpageTime)

            val now = LocalDateTime.now()
            val threshold = if (atPrefix == AtPrefix.AUTO_INSERT) now.plusMinutes(1) else now

            val isFuture = newDateTime > threshold
            val wasFuture = prevDateTime != null && prevDateTime > threshold
            if (atPrefix == AtPrefix.AUTO || isFuture != wasFuture) {
                if (newDateTime > threshold) "@$newText" else newText
            } else {
                if (atPrefix == AtPrefix.AUTO_REMOVE) "@$newText" else newText
            }
        } else if (prevChar.isLetterOrDigit()) {
            " $newText"
        } else if (atPrefix == AtPrefix.AUTO_REMOVE) {
            "@$newText"
        } else {
            newText
        }
    }

    private fun parseTime(text: CharSequence): LocalTime? {
        return try {
            LocalTime.parse(text, parseFormat)
        } catch (e: DateTimeParseException) {
            null
        }
    }

    private fun getAdjustStep(startEvent: MotionEvent, prevEvent: MotionEvent): Int {
        return ((prevEvent.y - startEvent.y) / stepSize).toInt()
    }

    companion object {
        private val SPEC_EVENT =
            AcceptSpec(ReminderParser.EVENT_REGEX, group = 0, TextGenerator.ACCEPT_WHOLE_LINE)

        private val SPEC_TIME = AcceptSpec(Regex("@?\\d{1,2}:\\d{1,2}"), group = 0, length = 7)

        private val ACCEPT_SPECS = listOf(
            SPEC_TIME,
            SPEC_EVENT,
        )

        enum class AtPrefix {
            AUTO,
            AUTO_INSERT,
            AUTO_REMOVE,
            ALWAYS,
        }
    }
}
