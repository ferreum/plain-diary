package de.ferreum.pto.util

import android.text.GetChars

internal fun CharSequence.lineRangeAt(index: Int): IntRange {
    val lineStart = lastIndexOf('\n', index - 1).let { if (it < 0) 0 else it }
    val lineEnd = indexOf('\n', index).let { if (it < 0) length else it }
    return lineStart..<lineEnd
}

internal inline fun String.forLineRanges(block: (start: Int, end: Int) -> Unit) {
    var start = 0
    while (start < length) {
        val end = indexOf('\n', start).let { if (it < 0) length else it }
        block(start, end)
        start = end + 1
    }
}

internal fun CharSequence.isBlankAfter(index: Int): Boolean = isRegionBlank(index, length)

internal fun CharSequence.isRegionBlank(start: Int, end: Int): Boolean {
    for (i in start until end) {
        if (!this[i].isWhitespace()) {
            return false
        }
    }
    return true
}

internal fun CharSequence.fastSubstring(start: Int, end: Int): String {
    if (this is GetChars) {
        val result = CharArray(end - start)
        getChars(start, end, result, 0)
        return String(result)
    }
    // slow path because subSequence().toString() implementation copies spans first
    return substring(start, end)
}
