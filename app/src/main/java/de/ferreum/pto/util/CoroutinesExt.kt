package de.ferreum.pto.util

import de.ferreum.pto.BuildConfig
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combineTransform
import kotlinx.coroutines.flow.conflate
import kotlinx.coroutines.flow.dropWhile
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.onSubscription
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.launch
import timber.log.Timber
import kotlin.concurrent.Volatile

/**
 * Debounce without using experimental features.
 */
inline fun <T> Flow<T>.myDebounce(crossinline delayMillis: (T) -> Long): Flow<T> = channelFlow {
    collectLatest { value ->
        delay(delayMillis(value))
        send(value)
    }
    close()
}.conflate()

fun <T> Flow<T>.throttle(minDelay: Long): Flow<T> =
    conflate().transform {
        emit(it)
        delay(minDelay)
    }

/**
 * `mapLatest` without using experimental API.
 */
inline fun <T, R> Flow<T>.myMapLatest(crossinline mapper: suspend (T) -> R): Flow<R> =
    myTransformLatest { emit(mapper(it)) }

/**
 * `transformLatest` without using experimental API.
 */
inline fun <T, R> Flow<T>.myTransformLatest(
    crossinline transformer: suspend FlowCollector<R>.(T) -> Unit,
): Flow<R> = channelFlow {
    val collector = FlowCollector<R> { send(it) }
    launch {
        collectLatest {
            collector.transformer(it)
        }
        close()
    }
    awaitClose()
}.buffer(Channel.RENDEZVOUS)

/** [combineTransform] that never emits. Shortcut to avoid type inference issues. */
inline fun <T, U> Flow<T>.combineTerminal(
    other: Flow<U>,
    crossinline block: suspend (T, U) -> Unit,
): Flow<Nothing> = combineTransform(other) { a, b -> block(a, b) }

/** [combineTransform] that never emits. Shortcut to avoid type inference issues. */
@JvmName("combineFlowsTerminal")
inline fun <T, U> combineTerminal(
    first: Flow<T>,
    other: Flow<U>,
    crossinline block: suspend (T, U) -> Unit,
): Flow<Nothing> = combineTransform(first, other) { a, b -> block(a, b) }

fun <T> Flow<T>.optBuffer(capacity: Int): Flow<T> =
    if (capacity == 0) this else buffer(capacity)

fun <T> Flow<T>.timeWindow(intervalMillis: (T) -> Long): Flow<List<T>> = channelFlow {
    // explicit FlowCollector object for control over field volatility
    val collector = object : FlowCollector<T> {
        @Volatile
        var pending = mutableListOf<T>()

        var emitterJob: Job? = null

        override suspend fun emit(value: T) {
            val millis = intervalMillis(value)
            val isNewWindow = synchronized(this) {
                pending.add(value)
                pending.size == 1
            }
            if (millis == 0L) {
                emitterJob?.cancelAndJoin()
                sendCurrent()
            } else if (isNewWindow) {
                emitterJob?.cancelAndJoin()
                emitterJob = launch {
                    delay(millis)
                    sendCurrent()
                }
            }
        }

        suspend fun sendCurrent() {
            send(synchronized(this) {
                pending.also {
                    if (it.isEmpty()) {
                        // nothing in current window; no new list needed and nothing to send
                        return
                    }
                    pending = mutableListOf()
                }
            })
        }
    }
    collect(collector)
    collector.emitterJob?.cancelAndJoin()
    collector.pending.takeIf { it.isNotEmpty() }?.let { send(it) }
    close()
}.buffer(Channel.RENDEZVOUS)

fun <T> SharedFlow<T>.startWithReplayCache(followUpMapper: (Flow<T>) -> Flow<List<T>>) = channelFlow {
    var replaySize = 0
    var index = 0
    val truncatedFlow = onSubscription {
        val cache = replayCache
        replaySize = cache.size
        send(cache)
    }.dropWhile { index++ < replaySize }
    followUpMapper(truncatedFlow).collect {
        send(it)
    }
    close()
}.buffer(Channel.RENDEZVOUS)

fun <T> SharedFlow<T>.transformReplayCache(replayProducer: suspend FlowCollector<T>.(List<T>) -> Unit) = channelFlow {
    var replaySize = 0
    var index = 0
    onSubscription {
        val cache = replayCache
        replaySize = cache.size
        ChannelCollector(channel).replayProducer(cache)
    }.dropWhile { index++ < replaySize }.collect {
        send(it)
    }
    close()
}.buffer(Channel.RENDEZVOUS)

class ChannelCollector<T>(private val channel: SendChannel<T>) : FlowCollector<T> {
    override suspend fun emit(value: T) = channel.send(value)
}

/**
 * Allocates and discards flows associated with keys. Collectors with the same key collect
 * from the same upstream flow. An upstream flow is discarded when the last collector for a
 * key completes.
 *
 * [creator] should create a [SharedFlow] with
 * [SharingStarted.WhileSubscribed][kotlinx.coroutines.flow.SharingStarted.WhileSubscribed] to be
 * used as the upstream flow for the given key.
 */
class KeyedSharedFlows<K, T>(private val creator: (key: K) -> Flow<T>) {
    private val createdFlows = mutableMapOf<K, FlowRecord<T>>()

    operator fun get(key: K): Flow<T> = flow {
        val record = synchronized(createdFlows) {
            val record = createdFlows[key] ?: FlowRecord(creator(key))
            if (++record.users == 1) {
                check(createdFlows.put(key, record) == null)
            }
            record
        }
        try {
            emitAll(record.flow)
        } finally {
            synchronized(createdFlows) {
                val users = --record.users
                check(users >= 0) { "users undercounted" }
                if (users == 0) {
                    check(createdFlows.remove(key) == record)
                }
            }
        }
    }

    private data class FlowRecord<T>(val flow: Flow<T>) {
        @Volatile
        var users = 0
    }
}

private val aliveFlows = mutableListOf<String>()

internal fun logAliveFlows() {
    if (!BuildConfig.DEBUG) return
    synchronized(aliveFlows) {
        Timber.v("logAliveFlows: %s", aliveFlows.sorted().joinToString(" ") { "[$it]" })
    }
}

internal inline fun <T> Flow<T>.traceFlow(tag: () -> String): Flow<T> {
    if (!BuildConfig.DEBUG) return this
    val flowTag = tag()
    return onStart {
        Timber.d("flow: %s onStart", flowTag)
        synchronized(aliveFlows) {
            aliveFlows.add(flowTag)
        }
    }.onCompletion {
        Timber.d("flow: %s onCompletion", flowTag)
        synchronized(aliveFlows) {
            aliveFlows.remove(flowTag)
        }
    }
}
