package de.ferreum.pto.util

import java.util.concurrent.atomic.AtomicBoolean

class Event<out T> {

    private var value: T?
    private val handled: AtomicBoolean

    constructor(value: T) {
        this.value = value
        this.handled = AtomicBoolean(false)
    }

    /** For HANDLED instance creation */
    private constructor() {
        this.value = null
        this.handled = AtomicBoolean(true)
    }

    val isHandled get() = handled.get()

    val valueOnce: T? get() = once { it }

    inline fun <R> once(block: (T) -> R): R? = if (once()) block(internalTakeValue()) else null

    @PublishedApi
    @Suppress("UNCHECKED_CAST") // only called once, so value is never null if T is non-nullable
    internal fun internalTakeValue(): T = value.also { value = null } as T

    fun once() = !handled.getAndSet(true)

    override fun toString(): String {
        val value = value
        return if (isHandled) "Event.handled()" else "Event($value)"
    }

    companion object {
        private val HANDLED: Event<Nothing> = Event()

        fun <T> handled(): Event<T> = HANDLED
    }

}
