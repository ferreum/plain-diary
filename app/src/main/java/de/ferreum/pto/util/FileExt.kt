package de.ferreum.pto.util

import java.io.File
import java.io.IOException
import java.io.OutputStreamWriter
import kotlin.random.Random

internal inline fun File.writeAtomic(block: (OutputStreamWriter) -> Unit) {
    val tempFile = createTempFile()
    var exception: Throwable? = null
    try {
        tempFile.outputStream().use { out ->
            out.writer().use { writer ->
                block(writer)

                writer.flush()
                out.fd.sync()
            }
        }
        tempFile.renameTo(this) || throw IOException("file rename failed")
    } catch (t: Throwable) {
        exception = t
        throw t
    } finally {
        if (exception != null) {
            if (!tempFile.delete()) {
                exception.addSuppressed(IOException("delete tempfile failed"))
            }
        }
    }
}

private fun File.createTempFile(): File {
    var file: File
    do {
        file = File(parentFile!!, ".${name}_temp_${Random.nextInt()}")
    } while (!file.createNewFile())
    return file
}
