package de.ferreum.pto.util

import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder

val PAGE_ISO_DATE_FORMATTER = DateTimeFormatterBuilder()
    .append(DateTimeFormatter.ISO_LOCAL_DATE)
    .appendPattern(", eee")
    .toFormatter()!!
