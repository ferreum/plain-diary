package de.ferreum.pto.util

import android.content.Intent
import android.os.Parcelable
import androidx.core.content.IntentCompat
import java.io.Serializable

inline fun <reified T : Parcelable> Intent.getTypedParcelableExtra(name: String): T? =
    IntentCompat.getParcelableExtra(this, name, T::class.java)

inline fun <reified T : Parcelable?> Intent.getTypedParcelableArrayListExtra(name: String): ArrayList<T?>? =
    IntentCompat.getParcelableArrayListExtra(this, name, T::class.java)

inline fun <reified T : Serializable?> Intent.getTypedSerializableExtra(name: String): T? =
    IntentCompat.getSerializableExtra(this, name, T::class.java)
