package de.ferreum.pto.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import de.ferreum.pto.R
import timber.log.Timber
import java.time.DateTimeException
import java.time.LocalDate
import java.util.Locale

fun tryStartActivity(context: Context, intent: Intent?, errorCallback: (String) -> Unit) {
    var error: String? = null
    val opened = when (intent) {
        null -> false
        else -> try {
            context.startActivity(intent)
            true
        } catch (e: RuntimeException) {
            Timber.w(e)
            error = e.toString()
            false
        }
    }
    if (!opened) {
        errorCallback(
            error?.let { context.getString(R.string.error_cannot_open_link_with_error, it) }
                ?: context.getString(R.string.error_cannot_open_link)
        )
    }
}

private const val DATE_URI_TEMPLATE = "diary://date/%d/%d/%d"

fun createDateUri(date: LocalDate): String {
    return DATE_URI_TEMPLATE.format(Locale.ROOT, date.year, date.monthValue, date.dayOfMonth)
}

fun getDateFromUri(uri: Uri): LocalDate? {
    return when {
        "date".equals(uri.scheme, ignoreCase = true) -> matchDateUri(uri)
        "diary".equals(uri.scheme, ignoreCase = true) -> matchDiaryDateUri(uri)
        else -> null
    }
}

private val DATE_REGEX = Regex("""(\d+)[./-](\d+)[./-](\d+)""")

private fun matchDateUri(uri: Uri): LocalDate? {
    val match = DATE_REGEX.matchEntire(uri.schemeSpecificPart.orEmpty()) ?: return null
    val year = match.groupValues[1].toIntOrNull() ?: return null
    val month = match.groupValues[2].toIntOrNull() ?: return null
    val day = match.groupValues[3].toIntOrNull() ?: return null
    return try {
        LocalDate.of(year, month, day)
    } catch (e: DateTimeException) {
        null
    }
}

private fun matchDiaryDateUri(uri: Uri): LocalDate? {
    if ("date" != uri.authority) return null
    val segments = uri.pathSegments
    if (segments.size != 3) return null
    val year = segments[0].toIntOrNull() ?: return null
    val month = segments[1].toIntOrNull() ?: return null
    val day = segments[2].toIntOrNull() ?: return null
    return try {
        LocalDate.of(year, month, day)
    } catch (e: DateTimeException) {
        null
    }
}
