package de.ferreum.pto.util

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume

suspend fun Lifecycle.waitForAtMost(state: Lifecycle.State) {
    suspendCancellableCoroutine { cont ->
        val observer = object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                if (!currentState.isAtLeast(state)) {
                    cont.resume(Unit)
                    removeObserver(this)
                }
            }
        }
        addObserver(observer)
        cont.invokeOnCancellation {
            removeObserver(observer)
        }
    }
}

inline fun Lifecycle.launchCompleteOnceInState(
    state: Lifecycle.State,
    stopOnCancel: Boolean = false,
    crossinline block: suspend () -> Unit,
): Job = coroutineScope.launch {
    repeatOnLifecycle(state) {
        try {
            block()
            if (!stopOnCancel) this@launch.cancel()
        } finally {
            if (stopOnCancel) this@launch.cancel()
        }
    }
}
