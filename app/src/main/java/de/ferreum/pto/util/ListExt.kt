package de.ferreum.pto.util

internal fun <T> List<T>.withReplacedAt(index: Int, item: T): List<T> {
    if (index !in indices) throw IndexOutOfBoundsException("$index is not in $indices")
    return mapIndexed { i, orig -> if (i == index) item else orig }
}

internal fun <T> List<T>.withInsertedAt(index: Int, item: T): List<T> {
    if (index < 0 || index > size) {
        throw IndexOutOfBoundsException("cannot insert at $index in list of length $size")
    }
    val input = this
    return if (this is RandomAccess) {
        buildList(size + 1) {
            for (i in 0..<index) add(input[i])
            add(item)
            for (i in index..<input.size) add(input[i])
        }
    } else {
        val iterator = iterator()
        buildList(size + 1) {
            for (i in 0..<index) add(iterator.next())
            add(item)
            for (i in index..<input.size) add(iterator.next())
        }
    }
}

internal fun <T> List<T>.withRemovedAt(index: Int): List<T> {
    if (index !in indices) throw IndexOutOfBoundsException("$index is not in $indices")
    return filterIndexedTo(ArrayList(size - 1)) { i, _ -> i != index }
}
