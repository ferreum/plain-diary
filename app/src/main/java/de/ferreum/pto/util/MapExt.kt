package de.ferreum.pto.util

/**
 * Check whether [key] is associated with `null` and not missing.
 */
internal fun <K, V : Any?> Map<K, V>.containsNull(key: K): Boolean {
    return get(key) == null && containsKey(key)
}
