package de.ferreum.pto.util

object Markdown {
    fun formatLink(text: String, link: String): String {
        val encodedLink = link.replace(" ", "%20")
            .replace("(", "%28")
            .replace(")", "%29")
            .replace("<", "%3c")
            .replace(">", "%3e")
        return when {
            link.isBlank() -> text
            text.isBlank() -> "<$encodedLink>"
            else -> {
                val normalizedText = text.replace("\n", " ")
                "[$normalizedText]($encodedLink)"
            }
        }
    }
}
