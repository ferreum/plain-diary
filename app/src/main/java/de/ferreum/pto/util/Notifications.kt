package de.ferreum.pto.util

import android.app.Dialog
import android.app.Notification
import android.content.Context
import android.content.Intent
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.core.app.NotificationManagerCompat
import de.ferreum.pto.R
import timber.log.Timber

internal fun NotificationManagerCompat.tryNotify(id: Int, notification: Notification) {
    try {
        notify(id, notification)
    } catch (e: SecurityException) {
        // no notification permission
        Timber.w(e, "notify failed")
    }
}

internal fun Context.showNotificationsRequiredDialog(): Dialog {
    return AlertDialog.Builder(this)
        .setMessage(R.string.notification_permission_required)
        .setNegativeButton(android.R.string.cancel, null)
        .setPositiveButton(R.string.settings) { _, _ ->
            startNotificationSettings(null)
        }
        .show()
}

internal fun Context.startNotificationSettings(channel: String?) {
    // Need FLAG_ACTIVITY_NEW_TASK to prevent building stack of alternating system settings and
    // preferences activities when clicking the respective links on those screens.
    startActivity(
        if (channel != null) {
            Intent(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS)
                .putExtra(Settings.EXTRA_APP_PACKAGE, packageName)
                .putExtra(Settings.EXTRA_CHANNEL_ID, channel)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        } else {
            Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
                .putExtra(Settings.EXTRA_APP_PACKAGE, packageName)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
    )
}
