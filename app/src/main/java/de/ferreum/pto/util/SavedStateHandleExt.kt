package de.ferreum.pto.util

import androidx.lifecycle.SavedStateHandle
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

private class SavedStateHandleProperty<T>(
    private val savedStateHandle: SavedStateHandle,
    private val key: String,
    default: T,
) : ReadWriteProperty<Any, T> {
    init {
        if (!savedStateHandle.contains(key)) {
            savedStateHandle[key] = default
        }
    }

    override fun getValue(thisRef: Any, property: KProperty<*>): T {
        @Suppress("UNCHECKED_CAST")
        return savedStateHandle.get<T>(key) as T
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        savedStateHandle[key] = value
    }
}

fun <T> SavedStateHandle.property(key: String, default: T): ReadWriteProperty<Any, T> =
    SavedStateHandleProperty(this, key, default)
