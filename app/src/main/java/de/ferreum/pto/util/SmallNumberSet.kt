package de.ferreum.pto.util

/**
 * Set of numbers in range `[0, 63]`.
 */
@JvmInline
value class SmallNumberSet private constructor(private val bits: Long) {

    operator fun plus(number: Int): SmallNumberSet {
        requireInRange(number)
        return SmallNumberSet(bits or (1L shl number))
    }

    operator fun minus(number: Int): SmallNumberSet {
        if (!isInRange(number)) return this
        return SmallNumberSet(bits and (1L shl number).inv())
    }

    operator fun contains(number: Int): Boolean =
        isInRange(number) && bits and (1L shl number) != 0L

    fun isEmpty(): Boolean = bits == 0L

    private fun requireInRange(number: Int) {
        require(isInRange(number)) { "value $number is out of range (0..63)" }
    }

    private fun isInRange(number: Int) = number in 0..63

    companion object {
        fun empty() = SmallNumberSet(0L)
        fun all() = SmallNumberSet(-1L)
    }

}
