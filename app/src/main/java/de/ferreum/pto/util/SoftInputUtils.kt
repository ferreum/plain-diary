package de.ferreum.pto.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber

fun Fragment.setupSoftInputVisible(editText: EditText) =
    requireActivity().setupSoftInputVisible(editText)

fun FragmentActivity.setupSoftInputVisible(editText: EditText) {
    setSoftInputWanted(true)
    lifecycleScope.launch outer@{
        val job = launch {
            lifecycle.waitForAtMost(Lifecycle.State.CREATED)
            this@outer.cancel()
        }
        editText.showSoftInputFromOnResume()
        job.cancel()
    }
}

fun Fragment.setupSoftInputHidden(view: View = requireView()) {
    requireActivity().setupSoftInputHidden(view)
}

fun FragmentActivity.setupSoftInputHidden(view: View = findViewById(android.R.id.content)) {
    setSoftInputWanted(false)
    view.requestFocus()
    view.hideSoftInput()
}

suspend fun View.showSoftInputFromOnResume() {
    requestFocus()
    if (showSoftInput()) {
        Timber.v("showSoftInputFromOnResume: success on first try")
        return
    }
    for (i in 0 until 3) {
        delay(40)
        if (showSoftInput()) {
            Timber.v("showSoftInputFromOnResume: success at retry %s", i + 1)
            return
        }
    }
    Timber.w("showSoftInputFromOnResume: failed to show softInput")
}

fun View.showSoftInput(): Boolean {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    return imm.showSoftInput(this, 0)
}

fun View.hideSoftInput() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

/**
 * Dynamically change windowSoftInputMode. Otherwise it's not possible to reliably
 * hide the keyboard when resuming the activity for some fragments while showing it for others.
 */
fun Activity.setSoftInputWanted(showSoftInput: Boolean) {
    window.setSoftInputWanted(showSoftInput)
}

fun Window.setSoftInputWanted(showSoftInput: Boolean) {
    val orig = attributes.softInputMode
    val flag = if (showSoftInput) {
        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
    } else {
        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
    }
    val mode = flag or (orig and WindowManager.LayoutParams.SOFT_INPUT_MASK_STATE.inv())
    if (mode != orig) {
        setSoftInputMode(mode)
    }
}
