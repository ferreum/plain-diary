package de.ferreum.pto.util

import android.os.SystemClock
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

interface TimeProvider {

    fun localDateTime(): LocalDateTime = LocalDateTime.now()

    fun timeZone(): ZoneId = ZoneId.systemDefault()

    fun instant(): Instant = Instant.now()

    fun elapsedRealtime(): Long = SystemClock.elapsedRealtime()

    companion object {
        val SYSTEM: TimeProvider = object : TimeProvider {}
    }

}
