package de.ferreum.pto.util

import android.content.ContentResolver
import android.net.Uri
import android.provider.MediaStore
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

interface UriInspector {
    suspend fun getDisplayName(uri: Uri): String?
}

internal class UriInspectorImpl(
    private val contentResolver: ContentResolver,
    private val ioContext: CoroutineContext,
) : UriInspector {
    override suspend fun getDisplayName(uri: Uri): String? {
        return withContext(ioContext) {
            contentResolver.query(uri, PROJ_DISPLAY_NAME, null, null, null)?.use { cur ->
                if (cur.moveToFirst()) {
                    cur.getString(0)?.let { return@withContext it }
                }
            }
            null
        }
    }

    companion object {
        private val PROJ_DISPLAY_NAME = arrayOf(
            MediaStore.MediaColumns.DISPLAY_NAME,
        )
    }
}
