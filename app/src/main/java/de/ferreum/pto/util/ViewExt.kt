package de.ferreum.pto.util

import android.text.Editable
import android.text.InputType
import android.text.Layout
import android.text.TextWatcher
import android.view.View
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.android.awaitFrame
import kotlinx.coroutines.awaitCancellation
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.coroutines.yield
import kotlin.math.absoluteValue

internal suspend fun View.repeatOnAttach(block: suspend CoroutineScope.() -> Unit) {
    withContext(Dispatchers.Main.immediate) {
        val attachListener = object : View.OnAttachStateChangeListener {
            private var job: Job? = null

            override fun onViewAttachedToWindow(v: View) {
                job = launch(block = block)
            }

            override fun onViewDetachedFromWindow(v: View) {
                job?.cancel()
                job = null
            }
        }

        addOnAttachStateChangeListener(attachListener)
        try {
            if (isAttachedToWindow) {
                attachListener.onViewAttachedToWindow(this@repeatOnAttach)
            }
            awaitCancellation()
        } finally {
            removeOnAttachStateChangeListener(attachListener)
        }
    }
}

fun NestedScrollView.getWantedSelectionScrollPosition(textView: TextView): Int? {
    val layout = textView.layout
    val topPadding = textView.top + textView.paddingTop - layout.topPadding
    val bottomPadding = textView.paddingBottom + layout.bottomPadding
    if (layout.height <= height - topPadding - bottomPadding) {
        return 0
    }
    val selectionEnd = textView.selectionEnd.also { if (it < 0) return null }
    val selectionStart = textView.selectionStart.let { if (it > selectionEnd) selectionEnd else it }
    val (startPos, endPos) = layout.getPreferredFocusRange(
        selectionStart, selectionEnd, height, topPadding, bottomPadding)
    val endDiff = endPos + topPadding - scrollY - height
    val startDiff = startPos + topPadding - scrollY
    val posInLayout = when {
        // prefer focus on end of selection when selection is too large; could be smarter about this
        endPos - startPos > height -> endPos - height

        // already fully visible?
        startDiff > 0 && endDiff < 0 -> return null

        // scroll as little as needed
        endDiff.absoluteValue < startDiff.absoluteValue -> endPos - height
        else -> startPos
    }
    return posInLayout + topPadding
}

private fun Layout.getPreferredFocusRange(
    selectionStart: Int,
    selectionEnd: Int,
    maxHeight: Int,
    topPadding: Int,
    bottomPadding: Int,
): Pair<Int, Int> {
    val startLine = getLineForOffset(selectionStart)
    val endLine = getLineForOffset(selectionEnd)
    var startPos = getLineTop(startLine) - topPadding
    var endPos = getLineBottom(endLine) + bottomPadding
    if (endPos - startPos > maxHeight) {
        return startPos to endPos
    }
    if (endLine < lineCount - 1) {
        val marginEndPos = getLineBottom(endLine + 1) + bottomPadding
        if (marginEndPos - startPos > maxHeight) {
            return startPos to endPos
        }
        endPos = marginEndPos
    }
    if (startLine > 0) {
        val marginStartPos = getLineTop(startLine - 1) - topPadding
        if (endPos - marginStartPos > maxHeight) {
            return startPos to endPos
        }
        startPos = marginStartPos
    }
    return startPos to endPos
}

/**
 * [transform] should always return an immutable value.
 */
fun <T> TextView.textObserverFlow(transform: (CharSequence) -> T) = callbackFlow {
    val watcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        override fun afterTextChanged(s: Editable) {
            trySend(transform(s))
        }
    }
    addTextChangedListener(watcher)
    send(transform(text))
    awaitClose { removeTextChangedListener(watcher) }
}.buffer(Channel.CONFLATED)

/**
 * Set or reset [InputType] [flag], and do nothing if current inputType is in requested state.
 */
fun TextView.updateInputType(flag: Int, enable: Boolean) {
    val current = inputType
    val new = if (enable) current or flag else current and flag.inv()
    if (new != current) {
        inputType = new
    }
}

/** Resume after next frame. */
internal suspend fun awaitPostFrame() {
    awaitFrame()
    yield()
}
