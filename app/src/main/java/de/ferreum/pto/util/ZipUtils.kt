package de.ferreum.pto.util

import java.io.File
import java.io.OutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

inline fun File.writeZip(block: ZipOutputStream.() -> Unit): Unit = outputStream().use { out ->
    ZipOutputStream(out).use { zip ->
        zip.block()
    }
}

fun ZipOutputStream.directory(name: String, time: Long) {
    require(name.endsWith("/")) { "directory name must end with '/'" }
    putNextEntry(ZipEntry(name).also {
        it.method = ZipEntry.STORED
        it.time = time
        it.size = 0
        it.compressedSize = 0
        it.crc = 0
    })
    closeEntry()
}

inline fun ZipOutputStream.file(name: String, time: Long, block: OutputStream.() -> Unit) {
    require(!name.endsWith("/")) { "filename must not end with '/'" }
    putNextEntry(ZipEntry(name).also {
        it.method = ZipEntry.DEFLATED
        it.time = time
    })
    try {
        block(this)
    } finally {
        closeEntry()
    }
}
