package de.ferreum.pto.util.ui

import android.content.Context
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import de.ferreum.pto.R
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.launch
import java.io.PrintWriter
import java.io.StringWriter

internal class ErrorDialogFlowCollector(
    private val context: Context,
    lifecycleOwner: LifecycleOwner,
    private val onDialogCancel: () -> Unit,
) : FlowCollector<Throwable?> {

    private var dialog: AlertDialog? = null

    init {
        lifecycleOwner.lifecycle.addObserver(object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                if (!event.targetState.isAtLeast(Lifecycle.State.STARTED)) {
                    dialog?.dismiss()
                    dialog = null
                }
            }
        })
    }

    override suspend fun emit(value: Throwable?) {
        if (value == null) {
            dialog?.dismiss()
        } else {
            val d = dialog ?: AlertDialog.Builder(context)
                .setTitle(R.string.error)
                .setNeutralButton(android.R.string.ok) { d, _ -> d.cancel() }
                .setOnCancelListener {
                    dialog = null
                    onDialogCancel()
                }
                .create()
                .also { dialog = it }
            val sw = StringWriter()
            value.printStackTrace(PrintWriter(sw))
            d.setMessage(sw.toString())
            d.show()
        }
    }

}

internal fun Fragment.errorDialogFlowCollector(
    flow: Flow<Throwable?>,
    context: Context = requireContext(),
    lifecycleOwner: LifecycleOwner = this,
    onDialogCancel: () -> Unit,
) {
    lifecycleOwner.lifecycleScope.launch {
        lifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
            val collector = ErrorDialogFlowCollector(
                context,
                lifecycleOwner,
                onDialogCancel,
            )
            flow.collect(collector::emit)
        }
    }
}
