package de.ferreum.pto.util.ui

import androidx.core.graphics.Insets
import kotlinx.coroutines.flow.StateFlow

/**
 * Workaround to apply insets reliably inside a viewpager.
 * Pages that are not resumed do not receive window inset callbacks reliably without this.
 */
interface InsetsProvider {

    val pageInsets: StateFlow<Insets>

}
