package de.ferreum.pto.util.viewpager

import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.callbackFlow

val ViewPager2.currentItemFlow: Flow<Int>
    get() = callbackFlow {
        val callback = object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                trySend(position)
            }
        }
        registerOnPageChangeCallback(callback)
        send(currentItem)
        awaitClose {
            unregisterOnPageChangeCallback(callback)
        }
    }.buffer(Channel.CONFLATED)
