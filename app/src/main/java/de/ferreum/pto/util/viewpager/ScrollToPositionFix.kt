package de.ferreum.pto.util.viewpager

import androidx.viewpager2.widget.ViewPager2
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * Workaround for a bug in [ViewPager2]: Scroll started with [ViewPager2.setCurrentItem] sometimes
 * stops too early and ends on the wrong page.
 *
 * The workaround reinforces the scroll request if not completed after 500ms.
 */
class ScrollToPositionFix(
    private val viewPager: ViewPager2,
    private val scope: CoroutineScope,
) : ViewPager2.OnPageChangeCallback() {
    private var isReinforcing = false
    private var reinforcerJob: Job? = null
    private var selectedPosition = -1
    private var scrolledPosition = 0

    override fun onPageSelected(position: Int) {
        if (!isReinforcing) {
            selectedPosition = position
        }
    }

    override fun onPageScrollStateChanged(state: Int) {
        if (state == ViewPager2.SCROLL_STATE_SETTLING) {
            if (!isReinforcing) {
                launchReinforceJob()
            }
        } else {
            reinforcerJob?.cancel()
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        scrolledPosition = position
        if (position == selectedPosition && positionOffsetPixels == 0) {
            reinforcerJob?.cancel()
        }
    }

    private fun launchReinforceJob() {
        reinforcerJob?.cancel()
        reinforcerJob = scope.launch {
            while (true) {
                delay(500)
                Timber.tag("ScrollToPositionFix").d("reinforcerJob: reinforcing scroll to position %s from %s", selectedPosition, scrolledPosition)
                isReinforcing = true
                viewPager.currentItem = selectedPosition + if (scrolledPosition < selectedPosition) -1 else 1
                viewPager.currentItem = selectedPosition
                isReinforcing = false
            }
        }
    }

    companion object {
        fun createFor(viewPager: ViewPager2, scope: CoroutineScope): ScrollToPositionFix {
            val fix = ScrollToPositionFix(viewPager, scope)
            viewPager.registerOnPageChangeCallback(fix)
            return fix
        }
    }
}
