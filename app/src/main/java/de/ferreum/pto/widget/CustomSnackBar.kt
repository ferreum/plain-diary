package de.ferreum.pto.widget

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isInvisible
import de.ferreum.pto.R
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

class CustomSnackBar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = R.attr.customSnackBarStyle,
) : AppCompatTextView(context, attrs, defStyleAttr) {

    private var isSnackBarShowing = false

    private val hideRunnable = Runnable {
        hide()
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (isSnackBarShowing) {
                hide()
            }
        }
        return false
    }

    override fun onDetachedFromWindow() {
        animation?.cancel()
        isInvisible = !isSnackBarShowing
        super.onDetachedFromWindow()
    }

    fun showText(text: CharSequence, duration: Duration = 3.seconds) {
        this.text = text
        removeCallbacks(hideRunnable)
        postDelayed(hideRunnable, duration.inWholeMilliseconds)
        if (!isSnackBarShowing) {
            isSnackBarShowing = true
            animation?.cancel()
            if (isAttachedToWindow) {
                val anim = AnimationUtils.loadAnimation(context, R.anim.snackbar_appear)
                anim.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation?) {
                        visibility = VISIBLE
                    }

                    override fun onAnimationEnd(animation: Animation?) {
                    }

                    override fun onAnimationRepeat(animation: Animation?) {
                    }
                })
                startAnimation(anim)
            } else {
                visibility = VISIBLE
            }
        }
    }

    private fun hide() {
        if (isSnackBarShowing) {
            isSnackBarShowing = false
            animation?.cancel()
            if (isAttachedToWindow) {
                val anim = AnimationUtils.loadAnimation(context, R.anim.snackbar_disappear)
                anim.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation?) {
                    }

                    override fun onAnimationEnd(animation: Animation?) {
                        visibility = INVISIBLE
                    }

                    override fun onAnimationRepeat(animation: Animation?) {
                    }
                })
                startAnimation(anim)
            } else {
                visibility = INVISIBLE
            }
        }
    }

}
