package de.ferreum.pto.backup

import android.net.Uri
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModelStore
import de.ferreum.pto.backup.ImportZipViewModel.UiState
import de.ferreum.pto.folderImportParams
import de.ferreum.pto.preferences.FolderLocation
import de.ferreum.pto.preferences.ZipFileLocation
import de.ferreum.pto.util.UriInspector
import de.ferreum.pto.util.testCreate
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class ImportZipViewModelTest {

    private val dispatcher = UnconfinedTestDispatcher()
    private val scope = TestScope(Job() + dispatcher)

    private lateinit var viewModel: ImportZipViewModel

    private val uriInspector: UriInspector = mockk {
        coEvery { getDisplayName(any()) } coAnswers { "display name ${firstArg<Uri>()}" }
    }

    private val viewModelStore = ViewModelStore()
    private val savedStateHandle = SavedStateHandle()

    private val fakeFolderImportController = FakeFolderImportController()

    @Before
    fun setUp() {
        Dispatchers.setMain(dispatcher)
        recreateViewModel()
    }

    @After
    fun tearDown() {
        scope.cancel()
    }

    private fun recreateViewModel() {
        viewModel = viewModelStore.testCreate {
            ImportZipViewModel(
                uriInspector,
                savedStateHandle,
            )
        }
    }

    @Test
    fun `given uri was not selected, when zipDisplayName is collected, then it emits null`() = scope.runTest {
        assertNull(viewModel.zipDisplayName.first())
    }

    @Test
    fun `when setZipUri is called, then zipDisplayName emits the file name`() = scope.runTest {
        val uri: Uri = mockk()
        coEvery { uriInspector.getDisplayName(uri) } returns "my_backup.zip"

        viewModel.setZipUri(uri)

        assertEquals("my_backup.zip", viewModel.zipDisplayName.first())
    }

    @Test
    fun `given uriInspector returns null, when setZipUri is called, then zipDisplayName uses lastPathSegment`() = scope.runTest {
        val uri: Uri = mockk {
            every { lastPathSegment } returns "1234.zip"
        }
        coEvery { uriInspector.getDisplayName(uri) } returns null

        viewModel.setZipUri(uri)

        assertEquals("1234.zip", viewModel.zipDisplayName.first())
    }

    @Test
    fun `given controller is connected, when confirm is called, then it starts service with zip file`() {
        viewModel.connectController(fakeFolderImportController)
        val uri = mockk<Uri>()

        viewModel.setZipUri(uri)
        viewModel.confirm(false)

        assertEquals(
            FakeFolderImportController.StartImportCall(
                FolderImportParams(
                    ZipFileLocation(uri),
                    FolderLocation.InternalStorage,
                    overwrite = false,
                    tryMove = false,
                )
            ),
            fakeFolderImportController.startImportCalls.single()
        )
    }

    @Test
    fun `given controller is connected, when confirm is called with overwrite, then it starts service with overwrite`() {
        viewModel.connectController(fakeFolderImportController)
        val uri = mockk<Uri>()

        viewModel.setZipUri(uri)
        viewModel.confirm(true)

        assertTrue(fakeFolderImportController.startImportCalls.single().params.overwrite)
    }

    @Test
    fun `given zipUri is set, when confirm is called after recreate, then it starts service with zip file`() {
        viewModel.connectController(fakeFolderImportController)
        val uri = mockk<Uri>()

        viewModel.setZipUri(uri)
        recreateViewModel()
        viewModel.confirm(false)

        assertEquals(
            ZipFileLocation(uri),
            fakeFolderImportController.startImportCalls.single().params.zipFileLocation
        )
    }

    @Test
    fun `when controller is connected, then it inhibits error notifications`() = scope.runTest {
        viewModel.connectController(fakeFolderImportController)

        assertTrue(fakeFolderImportController.isResultNotificationsInhibited)
    }

    @Test
    fun `given controller is connected, when import fails, then state emits the error`() = scope.runTest {
        fakeFolderImportController.setImportState(ImportState.Active(
            folderImportParams(zipFileLocation = ZipFileLocation(mockk()))
        ))
        viewModel.connectController(fakeFolderImportController)
        val throwable = Throwable("testing")

        fakeFolderImportController.setImportState(ImportState.Failed(throwable))

        assertEquals(UiState.Error(throwable), viewModel.state.first())
    }

    @Test
    fun `given state is Error, when confirm is called, then state emits idle`() = scope.runTest {
        viewModel.connectController(fakeFolderImportController)
        val throwable = Throwable("testing")
        fakeFolderImportController.setImportState(ImportState.Failed(throwable))

        viewModel.confirm(false)

        assertEquals(UiState.Idle, viewModel.state.first())
    }

    @Test
    fun `given controller is connected, when disconnectController is called, then inhibitor is removed`() = scope.runTest {
        viewModel.connectController(fakeFolderImportController)

        viewModel.disconnectController()

        assertFalse(fakeFolderImportController.isResultNotificationsInhibited)
    }

    @Test
    fun `given controller was disconnected, when import fails, then it does not handle the exception`() = scope.runTest {
        fakeFolderImportController.setImportState(ImportState.Active(
            folderImportParams(ZipFileLocation(mockk()))
        ))
        viewModel.connectController(fakeFolderImportController)
        viewModel.disconnectController()

        fakeFolderImportController.setImportState(ImportState.Failed(Throwable("testing")))

        assertEquals(viewModel.state.first(), UiState.Active)
    }

    @Test
    fun `given controller is not connected, then state emits Loading`() = scope.runTest {
        assertEquals(UiState.Loading, viewModel.state.first())
    }

    @Test
    fun `given import is not active, when connectController is called, then state emits Idle`() = scope.runTest {
        fakeFolderImportController.setImportState(ImportState.None)

        viewModel.connectController(fakeFolderImportController)

        assertEquals(UiState.Idle, viewModel.state.first())
    }

    @Test
    fun `given import is completed, when confirm is called, then state emits finished`() = scope.runTest {
        fakeFolderImportController.setImportState(ImportState.Completed(ImportReport(emptyList(), 0, 1)))
        viewModel.connectController(fakeFolderImportController)

        viewModel.confirm(false)

        assertEquals(UiState.Finished, viewModel.state.first())
    }

    @Test
    fun `when showError is called, then state emits Error`() = scope.runTest {
        val throwable = Throwable("testing")
        viewModel.connectController(fakeFolderImportController)

        viewModel.showError(throwable)

        assertEquals(UiState.Error(throwable), viewModel.state.first())
    }

    @Test
    fun `given showError was called, when confirm is called, then state emits Idle`() = scope.runTest {
        val throwable = Throwable("testing")
        viewModel.connectController(fakeFolderImportController)

        viewModel.showError(throwable)
        viewModel.confirm(false)

        assertEquals(UiState.Idle, viewModel.state.first())
    }

}
