package de.ferreum.pto.files

import de.ferreum.pto.files.PageContentService.ContentSourceTag
import de.ferreum.pto.files.PageContentService.EditorContent
import de.ferreum.pto.files.PageContentService.ErrorSource
import de.ferreum.pto.files.PageContentService.FileStatus
import de.ferreum.pto.files.PageRepository.FileInfo
import de.ferreum.pto.files.PageWriteSignal.PageWrite
import de.ferreum.pto.keepalive.KeepAliveSignal
import de.ferreum.pto.page.undo.HistoryCommand
import de.ferreum.pto.page.undo.TextSnapshot
import de.ferreum.pto.page.undo.UndoHistory
import de.ferreum.pto.preferences.PtoFileHelper
import de.ferreum.pto.preferences.PtoPreferences
import de.ferreum.pto.preferences.PtoPreferencesRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import io.mockk.verify
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.cancel
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runCurrent
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.io.File
import java.io.IOException
import java.time.LocalDate
import kotlin.coroutines.EmptyCoroutineContext

@ExperimentalCoroutinesApi
class PageContentServiceTest {

    private val currentDate = LocalDate.of(2020, 2, 20)
    private val initialText = "initial text"
    private val file = File("/dev/null/test/path/unused/not/exist")

    private val mainContext = EmptyCoroutineContext
    private val ioContext = EmptyCoroutineContext

    private val fileInfoFlow = MutableSharedFlow<FileInfo>(replay = 1)

    private val preferencesFlow = MutableStateFlow(mockk<PtoPreferences>(relaxed = true))
    private val preferencesRepository: PtoPreferencesRepository = mockk {
        every { preferencesFlow } returns this@PageContentServiceTest.preferencesFlow
    }

    private val pageRepository: PageRepository = mockk {
        every { checkForChangedFile() } just runs
        every { fileInfoFlow } returns this@PageContentServiceTest.fileInfoFlow
    }

    private val undoHistory = UndoHistory()

    private val keepAliveSignal: KeepAliveSignal = mockk(relaxUnitFun = true)

    private val pageWriteSignal = PageWriteSignal()

    private lateinit var service: PageContentService
    private lateinit var serviceScope: CoroutineScope

    private val sourceTag = ContentSourceTag()

    @Before
    fun setUp() {
        coEvery { pageRepository.readFile(file) } returns initialText
    }

    private fun runTest(expectErrorFileStatus: Boolean = false, block: suspend TestContext.() -> Unit) {
        kotlinx.coroutines.test.runTest {
            serviceScope = CoroutineScope(coroutineContext + Job(coroutineContext.job))
            service = PageContentService(
                serviceScope,
                currentDate,
                pageRepository,
                preferencesRepository,
                CompletableDeferred(PtoFileHelper(File("/dev/null"))),
                undoHistory,
                keepAliveSignal,
                pageWriteSignal,
                mainContext,
                ioContext,
            )

            TestContext(this).apply {
                block()
                stop()
            }
            if (!expectErrorFileStatus) {
                (service.fileStatus.first { it != FileStatus.Loading } as? FileStatus.Error)?.let {
                    throw AssertionError("status is error: ${it.source}", it.throwable)
                }
            }

            serviceScope.cancel()
        }
    }

    @Test
    fun `it handles loading error`() = runTest {
        coEvery { pageRepository.readFile(file) } throws IOException("test exception")

        val channel = service.currentContent.asChannelIn(serviceScope)
        startFileObserver()
        fileInfoFlow.emit(FileInfo(currentDate, file, 100))
        scope.runCurrent()

        coVerify { pageRepository.readFile(file) }
        service.fileStatus.first {
            it is FileStatus.Error && it.throwable is IOException
                    && it.throwable.message == "test exception"
        }
        assertFalse(service.isEditingAllowed.value)

        coEvery { pageRepository.readFile(file) } returns initialText

        service.checkForChanges()

        verify { pageRepository.checkForChangedFile() }
        channel.assertReceive { it.text == initialText }
        assertEquals(FileStatus.Saved, service.fileStatus.first { it == FileStatus.Saved })
        assertTrue(service.isEditingAllowed.first { it })
        channel.assertNoMoreElements()
    }

    @Test
    fun `it handles error in fileInfoFlow`() {
        every { pageRepository.fileInfoFlow } returns flow<Nothing> {
            throw IllegalArgumentException("testing")
        }
        runTest(expectErrorFileStatus = true) {

            startFileObserver()
            scope.runCurrent()

            val fileStatus = service.fileStatus.first { it != FileStatus.Loading } as FileStatus.Error
            assertEquals(ErrorSource.LOADING, fileStatus.source)
            assertEquals("testing", fileStatus.throwable.message)
            assertEquals(IllegalArgumentException::class.java, fileStatus.throwable::class.java)
        }
    }

    @Test
    fun `it reloads file on external changes`() = runTest {
        val content = loadInitialContent()

        coEvery { pageRepository.readFile(file) } returns "changed text"
        fileInfoFlow.emit(FileInfo(currentDate, file, 200))

        content.assertReceive(EditorContent.fromString("changed text", 2, service.contentSourceTag))
    }

    @Test
    fun `it updates currentContent on undo after restart`() = runTest {
        startFileObserver()
        fileInfoFlow.emit(FileInfo(currentDate, file, 100))

        val job = Job()
        val content = service.currentContent.asChannelIn(serviceScope + job)
        content.assertReceive(EditorContent(
            initialText,
            -1,
            -1,
            1,
            sourceTag = service.contentSourceTag,
        ))
        service.replaceContent(TextSnapshot.fromString("new text"), sourceTag)
        content.assertReceive(EditorContent("new text", -1, -1, 2, sourceTag))
        content.assertNoMoreElements()
        job.cancel()

        stopFileObserver()
        startFileObserver()

        val content2 = service.currentContent.asChannelIn(serviceScope)
        content2.assertReceive(EditorContent("new text", -1, -1, 2, sourceTag))
        assertTrue(service.doHistoryCommand(HistoryCommand.Undo))
        content2.assertReceive(EditorContent(
            initialText,
            -1,
            -1,
            1,
            service.contentSourceTag,
        ))
        content2.assertNoMoreElements()
    }

    @Test
    fun `it updates to changed file after restart`() = runTest {
        startFileObserver()
        fileInfoFlow.emit(FileInfo(currentDate, file, 100))

        val job = Job()
        val content = service.currentContent.asChannelIn(serviceScope + job)
        content.assertReceive { it.text == initialText }
        // make a change
        service.replaceContent(TextSnapshot.fromString("new text"), sourceTag)
        content.assertReceive(EditorContent("new text", -1, -1, 2, sourceTag))
        // reload by changed file
        coEvery { pageRepository.readFile(file) } returns "changed text"
        fileInfoFlow.emit(FileInfo(currentDate, file, 200))
        content.assertReceive(EditorContent(
            "changed text",
            -1,
            -1,
            3,
            service.contentSourceTag,
        ))
        content.assertNoMoreElements()
        job.cancel()

        stopFileObserver()
        startFileObserver()

        val content2 = service.currentContent.asChannelIn(serviceScope)
        content2.assertReceive(EditorContent(
            "changed text",
            -1,
            -1,
            3,
            service.contentSourceTag,
        ))
        content2.assertNoMoreElements()
    }

    @Test
    fun `it loads initial content into editor`() = runTest {
        val content = loadInitialContent()

        assertFalse(service.isModified.first())
        assertEquals(0, undoHistory.counters.value.undoCount)
        assertEquals(0, undoHistory.counters.value.redoCount)
        assertEquals(FileStatus.Saved, service.fileStatus.first { it != FileStatus.Loading })
        content.assertNoMoreElements()
    }

    @Test
    fun `replaceContent updates currentContent`() = runTest {
        val content = loadInitialContent()
        service.replaceContent(TextSnapshot.fromString("new text"), sourceTag)

        content.assertReceive(EditorContent.fromString("new text", undoHistory.counters.value.changeId, sourceTag))
        assertTrue(service.isModified.first())
        assertEquals(1, undoHistory.counters.value.undoCount)
        assertEquals(0, undoHistory.counters.value.redoCount)
        assertEquals(FileStatus.Modified, service.fileStatus.first { it != FileStatus.Loading })
    }

    @Test
    fun `replaceContent updates cursor position without text change`() = runTest {
        val content = loadInitialContent()
        service.replaceContent(
            TextSnapshot.fromString("new text", selection = 4, selectionEnd = 6),
            sourceTag,
        )
        scope.runCurrent()
        service.replaceContent(
            TextSnapshot.fromString("new text", selection = 8, selectionEnd = 9),
            sourceTag,
        )
        scope.runCurrent()

        content.assertReceive { it.selectionStart == 4 && it.selectionEnd == 6 }
        content.assertReceive { it.selectionStart == 8 && it.selectionEnd == 9 }
        content.assertNoMoreElements()
    }

    @Test
    fun `replaceContent keeps content if only sourceTag is changed`() = runTest {
        val content = loadInitialContent()
        val sourceTag1 = ContentSourceTag()
        val sourceTag2 = ContentSourceTag()

        service.replaceContent(TextSnapshot.fromString("new text", selection = 3), sourceTag1)
        scope.runCurrent()
        service.replaceContent(TextSnapshot.fromString("new text", selection = 3), sourceTag2)
        scope.runCurrent()

        content.assertReceive { it.text == "new text" && it.selectionStart == 3 && it.sourceTag == sourceTag1 }
        content.assertNoMoreElements()
    }

    @Test
    fun `doHistoryCommand loads snapshot text into editor`() = runTest {
        val content = loadInitialContent()
        service.replaceContent(TextSnapshot.fromString("my new text"), sourceTag)
        scope.runCurrent()
        service.replaceContent(TextSnapshot.fromString("my newer text"), sourceTag)
        scope.runCurrent()
        service.doHistoryCommand(HistoryCommand.Undo)
        scope.runCurrent()

        content.assertReceive { it.text == "my new text" }
        content.assertReceive { it.text == "my newer text" }
        content.assertReceive(EditorContent.fromString("my new text", 2, service.contentSourceTag))
        content.assertNoMoreElements()
        assertEquals(1, undoHistory.counters.value.undoCount)
        assertEquals(1, undoHistory.counters.value.redoCount)
    }

    @Test
    fun `ensureSaved does nothing when not modified`() = runTest {
        val content = loadInitialContent()

        service.ensureSaved()
        service.isSaving.first { !it }

        assertFalse(service.isSaving.value)
        assertFalse(service.isModified.first())
        assertEquals(FileStatus.Saved, service.fileStatus.first { it != FileStatus.Loading })
        content.assertNoMoreElements()
    }

    @Test
    fun `triggerSave saves content`() = runTest {
        coEvery { pageRepository.saveFile("new text") } returns 200

        val channel = loadInitialContent()
        service.replaceContent(TextSnapshot.fromString("new text"), sourceTag)
        service.isModified.first { it }
        service.triggerSave()
        service.isModified.first { !it }

        assertEquals(FileStatus.Saved, service.fileStatus.first { it != FileStatus.Loading })
        channel.assertReceive { it.text == "new text" }
        channel.assertNoMoreElements()
    }

    @Test
    fun `triggerSave sends page write signal`() = runTest {
        coEvery { pageRepository.saveFile("new text") } returns 200
        val pageWriteSignals = mutableListOf<PageWrite>()
        scope.backgroundScope.launch {
            pageWriteSignal.observePageWrites().collect {
                pageWriteSignals.add(it)
            }
        }

        loadInitialContent()
        service.replaceContent(TextSnapshot.fromString("new text"), sourceTag)
        service.isModified.first { it }
        service.triggerSave()
        service.isModified.first { !it }

        assertEquals(
            listOf(PageWrite(currentDate, "new text", emptySet())),
            pageWriteSignals,
        )
    }

    @Test
    fun `ensureSaved saves current content when modified`() = runTest {
        coEvery { pageRepository.saveFile("new text") } returns 200

        val channel = loadInitialContent()
        service.replaceContent(TextSnapshot.fromString("new text"), sourceTag)
        service.ensureSaved()
        service.isSaving.first { !it }
        scope.advanceUntilIdle()

        coVerify { pageRepository.saveFile("new text") }
        coVerify { pageRepository.checkForChangedFile() }
        assertFalse(service.isModified.first())
        assertEquals(FileStatus.Saved, service.fileStatus.first { it != FileStatus.Loading })
        channel.assertReceive { it.text == "new text" }
        channel.assertNoMoreElements()
    }

    @Test
    fun `ensureSaved handles changes during save`() = runTest {
        val saveDoneSignal = MutableSharedFlow<Boolean>(replay = 1)
        coEvery { pageRepository.saveFile("new text") } coAnswers {
            saveDoneSignal.first { it }
            200
        }
        coEvery { pageRepository.checkForChangedFile() } just runs

        // load content
        val content = loadInitialContent()
        // make a change and start saving
        service.replaceContent(TextSnapshot.fromString("new text"), sourceTag)
        content.assertReceive { it.text == "new text" }
        val saver = scope.async { service.ensureSaved() }
        service.isSaving.first { it }
        assertEquals(FileStatus.Saving, service.fileStatus.first { it != FileStatus.Loading })
        // make another change and let saving complete
        service.replaceContent(TextSnapshot.fromString("newer text"), sourceTag)
        saveDoneSignal.emit(true)
        saver.await()
        scope.runCurrent()

        content.assertReceive(EditorContent.fromString("newer text", 3, sourceTag))
        assertTrue(service.isModified.first())
        assertEquals(FileStatus.Modified, service.fileStatus.first { it != FileStatus.Loading })
    }

    @Test
    fun `ensureSaved skips saving if blocking save already saved same change`() = runTest {
        val saveDoneSignal = MutableSharedFlow<Boolean>(replay = 1)
        coEvery { pageRepository.saveFile(any()) } coAnswers {
            saveDoneSignal.first { it }
            200
        }

        // load content
        val channel = loadInitialContent()
        // make a change and start saving
        service.replaceContent(TextSnapshot.fromString("new text"), sourceTag)
        channel.assertReceive { it.text == "new text" }
        val saver1 = scope.async { service.ensureSaved() }
        val saver2 = scope.async { service.ensureSaved() }
        service.isSaving.first { it }
        // unlock savers and wait for them to finish
        saveDoneSignal.emit(true)
        saver1.await()
        saver2.await()
        scope.advanceUntilIdle()

        coVerify(exactly = 1) { pageRepository.saveFile(any()) }
        assertEquals(FileStatus.Saved, service.fileStatus.first { it != FileStatus.Loading })
        channel.assertNoMoreElements()
    }

    @Test
    fun `currentSavingError emits error of last save`() = runTest {
        val exception = RuntimeException("test exception")
        coEvery { pageRepository.saveFile(any()) } throws exception

        val channel = loadInitialContent()
        service.replaceContent(TextSnapshot.fromString("new text"), sourceTag)
        service.ensureSaved()

        val status = service.fileStatus.first { it != FileStatus.Loading } as FileStatus.Error
        if (status.throwable !is RuntimeException
            || status.throwable.message != "test exception"
        ) {
            throw AssertionError("wrong error emitted: $status")
        }
        assertEquals(status.source, ErrorSource.SAVING)

        coEvery { pageRepository.saveFile(any()) } returns 200

        service.ensureSaved()
        scope.advanceUntilIdle()

        assertEquals(FileStatus.Saved, service.fileStatus.first { it != FileStatus.Loading })
        channel.assertReceive { it.text == "new text" }
        channel.assertNoMoreElements()
    }

    @Test
    fun `autosave saves after idle delay`() = runTest {
        val data = setupAutosaveTest()

        service.replaceContent(TextSnapshot.fromString("new text"), sourceTag)

        scope.advanceTimeBy(service.autosaveIdleInterval - 10L)
        assertEquals(initialText, data.savedText)
        scope.advanceTimeBy(20L)
        assertEquals("new text", data.savedText)

        service.replaceContent(TextSnapshot.fromString("newer text"), sourceTag)

        scope.advanceTimeBy(service.autosaveIdleInterval - 10L)

        service.replaceContent(TextSnapshot.fromString("newest text"), sourceTag)

        scope.advanceTimeBy(service.autosaveIdleInterval - 10L)
        assertEquals("new text", data.savedText)
        scope.advanceTimeBy(20L)
        assertEquals("newest text", data.savedText)
    }

    @Test
    fun `autosave saves during repeated edits after maximum time`() = runTest {
        val data = setupAutosaveTest()

        val timesNeeded = ((service.autosaveMaxInterval - 10L) / 500L).toInt()
        repeat(timesNeeded) { i ->
            service.replaceContent(TextSnapshot.fromString("text $i"), sourceTag)
            scope.advanceTimeBy(500L)
            assertEquals("text changed at iteration $i", initialText, data.savedText)
        }
        service.replaceContent(TextSnapshot.fromString("last text"), sourceTag)
        scope.advanceTimeBy(520L)
        assertEquals("last text", data.savedText)
    }

    @Test
    fun `autosave resets delays when undoing to saved change`() = runTest {
        val data = setupAutosaveTest()

        val timesNeeded = ((service.autosaveMaxInterval * 2.5) / service.autosaveIdleInterval).toInt()
        repeat(timesNeeded) { i ->
            service.doHistoryCommand(HistoryCommand.Undo)
            scope.advanceTimeBy(service.autosaveIdleInterval - 10L)
            service.replaceContent(TextSnapshot.fromString("text $i"), sourceTag)
            scope.advanceTimeBy(service.autosaveIdleInterval - 10L)
            assertEquals("text $i", service.currentContent.first().text)
            assertEquals("text changed at iteration $i", initialText, data.savedText)
        }
        assertEquals(initialText, data.savedText)
        service.doHistoryCommand(HistoryCommand.Undo)
        scope.advanceTimeBy(10L)
        assertEquals(initialText, service.currentContent.first().text)
        assertEquals(initialText, data.savedText)
        assertEquals(0, data.saveCount)
    }

    private fun <T> Flow<T>.asChannelIn(coroutineScope: CoroutineScope): Channel<T> {
        val channel = Channel<T>(Channel.UNLIMITED)
        onEach { channel.send(it) }.launchIn(coroutineScope)
        return channel
    }

    private suspend fun <T> ReceiveChannel<T>.assertReceive(expected: T) {
        assertEquals(expected, receive())
    }

    private suspend fun <T> ReceiveChannel<T>.assertReceive(expected: (value: T) -> Boolean) {
        val value = receive()
        if (!expected(value)) {
            throw AssertionError("unexpected value: $value")
        }
    }

    private suspend fun <T> Channel<T>.assertNoMoreElements() {
        close()
        val list = consumeAsFlow().toList()
        if (list.isNotEmpty()) {
            throw AssertionError("unexpected elements: \n${list.joinToString(separator = ",\n")}")
        }
    }

    private inner class TestContext(val scope: TestScope) {
        private var observingJob: Job? = null

        fun stop() {
            observingJob?.cancel()
            observingJob = null
        }

        fun startFileObserver() {
            observingJob?.let { throw Exception("already observing") }
            observingJob = scope.launch { service.observeFileContentIn(this) }
        }

        suspend fun stopFileObserver() {
            observingJob ?: throw Exception("observer not active")
            observingJob!!.cancelAndJoin()
            observingJob = null
        }

        suspend fun loadInitialContent(): Channel<EditorContent> {
            startFileObserver()
            fileInfoFlow.emit(FileInfo(currentDate, file, 100))
            val channel = service.currentContent.asChannelIn(serviceScope)
            channel.assertReceive(EditorContent(
                initialText,
                -1,
                -1,
                1,
                service.contentSourceTag
            ))
            return channel
        }

        suspend fun setupAutosaveTest(): AutosaveTestData {
            var testData: AutosaveTestData? = null
            coEvery { pageRepository.saveFile(any()) } answers {
                testData!!.let {
                    it.savedText = firstArg()
                    it.saveCount++
                }
                200
            }

            loadInitialContent()
            testData = AutosaveTestData(initialText, 0)
            return testData
        }
    }

    data class AutosaveTestData(var savedText: String, var saveCount: Int)

}
