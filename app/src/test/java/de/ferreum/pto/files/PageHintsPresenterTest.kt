package de.ferreum.pto.files

import android.os.SystemClock
import de.ferreum.pto.files.PageContentService.EditorContent
import de.ferreum.pto.preferences.FakePtoPreferencesRepository
import de.ferreum.pto.util.TimeProvider
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.runs
import io.mockk.unmockkStatic
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import java.time.LocalTime

@OptIn(ExperimentalCoroutinesApi::class)
class PageHintsPresenterTest {

    private val pageContent = MutableSharedFlow<EditorContent>(replay = 1)
    private val currentDate = LocalDate.of(2020, 2, 4)
    private val preferencesRepository = FakePtoPreferencesRepository()
    private val pageContentService: PageContentService = mockk {
        every { currentContent } returns pageContent
        every { observeFileContentIn(any()) } just runs
    }

    private val timeProvider = object : TimeProvider {
        var localDateTime = currentDate.atTime(12, 30)
        override fun localDateTime() = localDateTime
    }

    private lateinit var pageHintsPresenter: PageHintsPresenter

    @Before
    fun setUp() {
        mockkStatic(SystemClock::class)
        pageHintsPresenter = PageHintsPresenter(
            currentDate,
            preferencesRepository,
            pageContentService,
            timeProvider,
        )
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        unmockkStatic(SystemClock::class)
    }

    @Test
    fun `it emits note hints`() = runTest {
        pageContent.emit(contentOf("+ test1"))

        assertEquals("+", awaitCurrentHints())
    }

    @Test
    fun `it emits event hints`() = runTest {
        timeProvider.localDateTime = currentDate.atTime(12, 0)
        pageContent.emit(contentOf("@12:05"))

        assertEquals("@", awaitCurrentHints())
    }

    @Test
    fun `it handles lines with single at chars`() = runTest {
        pageContent.emit(contentOf("@\n+ test\n@\n@"))

        assertEquals("+", awaitCurrentHints())
    }

    @Test
    fun `it handles lines with single plus chars`() = runTest {
        timeProvider.localDateTime = currentDate.atTime(12, 0)
        pageContent.emit(contentOf("+\n@12:34 test\n+\n+"))

        assertEquals("@", awaitCurrentHints())
    }

    @Test
    fun `it limits number of entries`() = runTest {
        timeProvider.localDateTime = currentDate.atTime(13, 0)
        pageContent.emit(contentOf("+ test3\n@13:45 test4\n".repeat(20)))

        assertEquals("+\n@\n+\n@\n+\n\u22ee", awaitCurrentHints())
    }

    @Test
    fun `it lists multiple notes`() = runTest {
        pageContent.emit(contentOf("+ test1\n+ test2\n+ test3"))

        assertEquals("+\n+\n+", awaitCurrentHints())
    }

    @Test
    fun `it ignores past events`() = runTest {
        preferencesRepository.preferencesFlow.update { it.copy(newpageTime = LocalTime.of(4, 0)) }
        timeProvider.localDateTime = currentDate.atTime(5, 0)
        pageContent.emit(contentOf("@1:30 test1\n@4:30 test2\n@5:30 test3"))

        assertEquals("@\n@", awaitCurrentHints())
    }

    private suspend fun awaitCurrentHints() = pageHintsPresenter.hints.first { it != null }

    private fun contentOf(text: String) = EditorContent(
        text, 0, 0, 0, PageContentService.ContentSourceTag()
    )

}
