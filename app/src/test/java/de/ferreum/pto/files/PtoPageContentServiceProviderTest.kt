package de.ferreum.pto.files

import de.ferreum.pto.files.PtoPageContentServiceProvider.RetainPolicy
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.isActive
import kotlinx.coroutines.job
import kotlinx.coroutines.plus
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runCurrent
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotSame
import org.junit.Assert.assertSame
import org.junit.Test
import java.time.LocalDate

@ExperimentalCoroutinesApi
class PtoPageContentServiceProviderTest {

    private lateinit var serviceProvider: PtoPageContentServiceProvider
    private lateinit var providerJob: Job

    private val creations = mutableListOf<ServiceCreation>()

    private fun runTest(block: suspend TestScope.() -> Unit) {
        kotlinx.coroutines.test.runTest {
            providerJob = Job(coroutineContext.job)
            serviceProvider = PtoPageContentServiceProvider(
                backgroundScope + providerJob,
                ::createService,
            )
            runCurrent()

            block()

            providerJob.cancel()
        }
    }

    private fun createService(coroutineScope: CoroutineScope, currentDate: LocalDate): PageContentService {
        val retainPolicy = MutableStateFlow<RetainPolicy>(RetainPolicy.Drop)
        val service = mockk<PageContentService> {
            every { this@mockk.retainPolicy } returns retainPolicy
        }
        creations += ServiceCreation(service, coroutineScope, currentDate, retainPolicy)
        return service
    }

    @Test
    fun `obtain creates new service`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val job = Job(coroutineContext.job)

        val service = serviceProvider.obtain(date, this + job)
        runCurrent()

        val creation = creations.single()
        assertEquals(service, creation.service)
        assertEquals(true, creation.scope.isActive)
        assertEquals(date, creation.currentDate)

        job.complete()
    }

    @Test
    fun `it cancels scope when single user cancels`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val job = Job(coroutineContext.job)

        val service = serviceProvider.obtain(date, this + job)
        runCurrent()
        job.cancel()
        runCurrent()

        val creation = creations.single()
        assertEquals(service, creation.service)
        assertEquals(false, creation.scope.isActive)
        assertEquals(date, creation.currentDate)
    }

    @Test
    fun `it cancels scope after last user cancels`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val job1 = Job(coroutineContext.job)
        val job2 = Job(coroutineContext.job)

        val service1 = serviceProvider.obtain(date, this + job1)
        val service2 = serviceProvider.obtain(date, this + job2)
        runCurrent()
        val creation = creations.single()
        job1.cancel()
        runCurrent()
        assertEquals(true, creation.scope.isActive)
        job2.cancel()
        runCurrent()

        assertEquals(service1, creation.service)
        assertEquals(service2, creation.service)
        assertEquals(false, creation.scope.isActive)
        assertEquals(date, creation.currentDate)
    }

    @Test
    fun `obtain creates new service after previous cleanup`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val job1 = Job(coroutineContext.job)
        val job2 = Job(coroutineContext.job)

        val service1 = serviceProvider.obtain(date, this + job1)
        runCurrent()
        job1.cancel()
        runCurrent()
        val service2 = serviceProvider.obtain(date, this + job2)
        runCurrent()
        assertEquals(2, creations.size)
        val creation = creations[1]
        assertEquals(true, creation.scope.isActive)
        job2.cancel()
        runCurrent()

        assertSame(service1, creations[0].service)
        assertSame(service2, creations[1].service)
        assertNotSame(service1, service2)
        assertEquals(false, creation.scope.isActive)
        assertEquals(date, creation.currentDate)
        assertEquals(2, creations.size)
    }

    @Test
    fun `it preserves uncanceled service while canceling others`() = runTest {
        val date1 = LocalDate.of(2020, 2, 20)
        val date2 = LocalDate.of(2020, 2, 22)
        val job1 = Job(coroutineContext.job)
        val job2 = Job(coroutineContext.job)

        val service1 = serviceProvider.obtain(date1, this + job1)
        runCurrent()
        val service2 = serviceProvider.obtain(date2, this + job2)
        runCurrent()
        job1.cancel()
        runCurrent()

        assertEquals(service1, creations[0].service)
        assertEquals(false, creations[0].scope.isActive)
        assertEquals(date1, creations[0].currentDate)

        assertEquals(service2, creations[1].service)
        assertEquals(true, creations[1].scope.isActive)
        assertEquals(date2, creations[1].currentDate)

        job2.cancel()
    }

    @Test
    fun `it runs all services within provider scope`() = runTest {
        val date1 = LocalDate.of(2020, 2, 20)
        val date2 = LocalDate.of(2020, 2, 22)
        val job1 = Job(coroutineContext.job)
        val job2 = Job(coroutineContext.job)

        val service1 = serviceProvider.obtain(date1, this + job1)
        runCurrent()
        val service2 = serviceProvider.obtain(date2, this + job2)
        runCurrent()
        providerJob.cancel()
        runCurrent()

        assertEquals(service1, creations[0].service)
        assertEquals(false, creations[0].scope.isActive)
        assertEquals(date1, creations[0].currentDate)

        assertEquals(service2, creations[1].service)
        assertEquals(false, creations[1].scope.isActive)
        assertEquals(date2, creations[1].currentDate)

        job1.complete()
        job2.complete()
    }

    @Test
    fun `obtain handles already cancelled user scope`() = runTest {
        val date = LocalDate.of(2020, 2, 20)

        val service = serviceProvider.obtain(date, this + Job(coroutineContext.job).apply { cancel() })
        runCurrent()

        val creation = creations.single()
        assertEquals(service, creation.service)
        assertEquals(false, creation.scope.isActive)
        assertEquals(date, creation.currentDate)
    }

    @Test
    fun `obtain handles already cancelled user scope of second user`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val job = Job(coroutineContext.job)

        val service1 = serviceProvider.obtain(date, this + job)
        runCurrent()
        val service2 = serviceProvider.obtain(date, this + Job(coroutineContext.job).apply { cancel() })
        runCurrent()

        val creation = creations.single()
        assertEquals(service1, service2)
        assertEquals(service1, creation.service)
        assertEquals(true, creation.scope.isActive)
        assertEquals(date, creation.currentDate)

        job.complete()
    }

    @Test
    fun `it retains service with retainPolicy Keep`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val job = Job(coroutineContext.job)

        val service = serviceProvider.obtain(date, this + job)
        runCurrent()
        val creation = creations.single()
        creation.retainPolicy.value = RetainPolicy.Keep
        job.cancel()
        runCurrent()

        assertEquals(service, creation.service)
        assertEquals(true, creation.scope.isActive)
        assertEquals(date, creation.currentDate)
    }

    @Test
    fun `it retains service with retainPolicy Timeout until timeout expires`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val job = Job(coroutineContext.job)

        val service = serviceProvider.obtain(date, this + job)
        runCurrent()
        val creation = creations.single()
        creation.retainPolicy.value = RetainPolicy.Timeout(60000)
        job.cancel()
        advanceTimeBy(59999)
        runCurrent()
        assertEquals(true, creation.scope.isActive)
        advanceTimeBy(1)
        runCurrent()
        assertEquals(false, creation.scope.isActive)

        assertEquals(service, creation.service)
        assertEquals(date, creation.currentDate)
    }

    @Test
    fun `it clears service after timeout when changing retainPolicy from Keep to Timeout`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val job = Job(coroutineContext.job)

        val service = serviceProvider.obtain(date, this + job)
        runCurrent()
        val creation = creations.single()
        creation.retainPolicy.value = RetainPolicy.Keep
        job.cancel()
        runCurrent()
        creation.retainPolicy.value = RetainPolicy.Timeout(60000)
        advanceTimeBy(59999)
        runCurrent()
        assertEquals(true, creation.scope.isActive)
        advanceTimeBy(1)
        runCurrent()
        assertEquals(false, creation.scope.isActive)

        assertEquals(service, creation.service)
        assertEquals(date, creation.currentDate)
    }

    @Test
    fun `it clears service immediately when changing retainPolicy from Keep to Drop`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val job = Job(coroutineContext.job)

        val service = serviceProvider.obtain(date, this + job)
        runCurrent()
        val creation = creations.single()
        creation.retainPolicy.value = RetainPolicy.Keep
        runCurrent()
        job.cancel()
        runCurrent()
        assertEquals(true, creation.scope.isActive)
        creation.retainPolicy.value = RetainPolicy.Drop
        runCurrent()
        assertEquals(false, creation.scope.isActive)

        assertEquals(service, creation.service)
        assertEquals(date, creation.currentDate)
    }

    @Test
    fun `obtain uses retained service`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val job1 = Job(coroutineContext.job)
        val job2 = Job(coroutineContext.job)

        val service1 = serviceProvider.obtain(date, this + job1)
        runCurrent()
        val creation = creations.single()
        creation.retainPolicy.value = RetainPolicy.Keep
        runCurrent()
        job1.cancel()
        runCurrent()
        assertEquals(true, creation.scope.isActive)
        val service2 = serviceProvider.obtain(date, this + job2)
        runCurrent()

        assertEquals(service2, service1)
        assertEquals(service1, creation.service)
        assertEquals(true, creation.scope.isActive)
        assertEquals(date, creation.currentDate)
        assertEquals(listOf(creation), creations)

        job2.complete()
    }

    @Test
    fun `obtain cancels timeout of retained service`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val job1 = Job(coroutineContext.job)
        val job2 = Job(coroutineContext.job)

        val service1 = serviceProvider.obtain(date, this + job1)
        runCurrent()
        val creation = creations.single()
        creation.retainPolicy.value = RetainPolicy.Timeout(60000)
        runCurrent()
        job1.cancel()
        advanceTimeBy(59999)
        runCurrent()
        assertEquals(true, creation.scope.isActive)
        val service2 = serviceProvider.obtain(date, this + job2)
        runCurrent()

        assertEquals(service2, service1)
        assertEquals(service1, creation.service)
        assertEquals(true, creation.scope.isActive)
        assertEquals(date, creation.currentDate)

        job2.complete()
    }

    data class ServiceCreation(
        val service: PageContentService,
        val scope: CoroutineScope,
        val currentDate: LocalDate,
        val retainPolicy: MutableStateFlow<RetainPolicy>,
    )

}
