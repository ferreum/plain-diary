package de.ferreum.pto.page

import android.os.SystemClock
import app.cash.turbine.test
import de.ferreum.pto.files.PageContentService
import de.ferreum.pto.files.PtoPageContentServiceProvider
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.runs
import io.mockk.unmockkStatic
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.dropWhile
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.currentTime
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class FileHintsViewModelTest {

    private val prevHints = MutableStateFlow<String?>(null)
    private val prevContentService: PageContentService = mockk {
        every { hints } returns prevHints
        every { observeFileContentIn(any()) } just runs
    }
    private val nextHints = MutableStateFlow<String?>(null)
    private val nextContentService: PageContentService = mockk {
        every { hints } returns nextHints
        every { observeFileContentIn(any()) } just runs
    }
    private val pageContentServiceProvider: PtoPageContentServiceProvider = mockk()
    private val currentDate = LocalDate.of(2020, 2, 4)

    private val scope = TestScope()

    private lateinit var viewModel: FileHintsViewModel

    @Before
    fun setUp() {
        mockkStatic(SystemClock::class)
        Dispatchers.setMain(StandardTestDispatcher(scope.testScheduler))
        every { SystemClock.uptimeMillis() } answers { scope.testScheduler.currentTime }
        every { pageContentServiceProvider.obtain(any(), any()) } answers {
            when (firstArg<LocalDate>()) {
                currentDate.minusDays(1) -> prevContentService
                currentDate.plusDays(1) -> nextContentService
                else -> throw AssertionError("invalid page date: ${firstArg<LocalDate>()}")
            }
        }
        viewModel = FileHintsViewModel(
            pageContentServiceProvider,
            currentDate,
            StandardTestDispatcher(scope.testScheduler),
        )
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        unmockkStatic(SystemClock::class)
    }

    @Test
    fun `showHints shows hints`() = runTest {
        prevHints.emit("+")
        nextHints.emit("@")

        viewModel.hints.dropWhile { !it.isComplete() }.test {
            testScheduler.runCurrent()
            viewModel.showHints()

            assertEquals(Pair("+", "@"), awaitItem())
        }
    }

    @Test
    fun `showHints shows hints if called before flow starts`() = runTest {
        prevHints.emit("+")
        nextHints.emit("@")

        viewModel.showHints()
        val hints = viewModel.hints.first { it.isComplete() }

        assertEquals(Pair("+", "@"), hints)
    }

    @Test
    fun `triggerHide hides hints`() = runTest {
        prevHints.emit("+")
        nextHints.emit("@")

        viewModel.showHints()
        viewModel.hints.dropWhile { !it.isComplete() }.test {
            awaitItem()
            viewModel.triggerHide()

            assertEquals(Pair(null, null), awaitItem())
        }
    }

    @Test
    fun `triggerHide restores hints after delay`() = runTest {
        prevHints.emit("+")
        nextHints.emit("@")

        viewModel.showHints()
        viewModel.hints.dropWhile { !it.isComplete() }.test(timeout = 20.seconds) {
            awaitItem()
            viewModel.triggerHide()

            assertEquals(Pair(null, null), awaitItem())
            val hideTime = currentTime

            assertEquals(Pair("+", "@"), awaitItem())
            val diff = currentTime - hideTime
            if (diff !in 15000..15500) {
                throw AssertionError("delay not in expected range: $diff")
            }
        }
    }

    @Test
    fun `triggerHide throttles frequent requests`() = runTest {
        prevHints.emit("+")
        nextHints.emit("@")

        viewModel.showHints()
        viewModel.hints.dropWhile { !it.isComplete() }.test(timeout = 20.seconds) {
            awaitItem()
            viewModel.triggerHide()

            assertEquals(Pair(null, null), awaitItem())
            val hideTime = currentTime

            advanceTimeBy(900)
            viewModel.triggerHide()

            assertEquals(Pair("+", "@"), awaitItem())
            val diff = currentTime - hideTime
            if (diff !in 15000..15500) {
                throw AssertionError("delay not in expected range: $diff")
            }
        }
    }

    @Test
    fun `it starts while missing hints for one page`() = runTest {
        prevHints.emit("+")

        viewModel.showHints()
        val result = viewModel.hints.first { !it.isEmpty() }

        assertEquals(Pair("+", null), result)
    }

    private fun Pair<*, *>.isComplete() = first != null && second != null
    private fun Pair<*, *>.isEmpty() = first == null && second == null
}
