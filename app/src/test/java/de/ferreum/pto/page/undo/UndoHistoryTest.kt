package de.ferreum.pto.page.undo

import android.text.Selection
import de.ferreum.pto.page.undo.HistoryCommand.GoToVersion
import de.ferreum.pto.page.undo.HistoryCommand.Redo
import de.ferreum.pto.page.undo.HistoryCommand.Undo
import de.ferreum.pto.page.undo.UndoHistory.Companion.NO_TRANSACTION_TAG
import io.mockk.every
import io.mockk.mockkStatic
import io.mockk.unmockkStatic
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test

/**
 * selection after undo/redo behavior:
 *
 * change: aa bb|
 * change: aa bbb|
 * select: aa| bbb
 * change: aaa| bbb
 * undo:   aa| bbb   <- restore pre-change selection
 * undo:   aa bb|    <- restore pre-change selection
 * redo:   aa bbb|   <- restore post-change selection
 * redo:   aaa| bbb  <- restore post-change selection
 *
 * change: aa bb|
 * change: aa bbb|
 * select: |aa| bbb
 * change: c| bbb
 * undo:   |aa| bbb   <- restore pre-change selection
 * undo:   aa bb|     <- restore pre-change selection
 * redo:   aa bbb|    <- restore post-change selection
 * redo:   c| bbb     <- restore post-change selection
 */
class UndoHistoryTest {

    private lateinit var history: UndoHistory

    @Before
    fun setUp() {
        mockkStatic(Selection::class)
        every { Selection.getSelectionStart(any()) } returns -1
        every { Selection.getSelectionEnd(any()) } returns -1
        history = UndoHistory()
    }

    @After
    fun tearDown() {
        unmockkStatic(Selection::class)
    }

    private fun UndoHistory.addChange(
        text: String,
        selection: Int = -1,
        selectionEnd: Int = selection,
        preSelection: Int = -1,
        preSelectionEnd: Int = preSelection,
        transactionTag: Int = NO_TRANSACTION_TAG,
    ) = addChange(TextSnapshot(
        text,
        selectionStart = selection,
        selectionEnd = selectionEnd,
        preSelectionStart = preSelection,
        preSelectionEnd = preSelectionEnd,
        preChangeId = UndoHistory.NO_CHANGE_ID,
    ), transactionTag)

    @Test
    fun `addChange adds state to history`() {
        val snap1 = history.addChange("my text")
        val snap2 = history.addChange("my new text")

        assertEquals(1, history.counters.value.undoCount)
        assertNotEquals(snap1.changeId, snap2.changeId)
        assertEquals(snap1, TextChange(TextSnapshot.fromString("my text"), 1))
        assertEquals(snap2, TextChange(TextSnapshot.fromString("my new text"), 2))
    }

    @Test
    fun `counts are correct when history is empty`() {
        val counters = history.counters.value
        assertEquals(0, counters.undoVersion)
        assertEquals(0, counters.undoCount)
        assertEquals(0, counters.redoCount)
    }

    @Test
    fun `commands do nothing when history is empty`() {
        assertNull(history.doCommand(Undo))
        assertNull(history.doCommand(Redo))
        assertNull(history.doCommand(GoToVersion(1)))
        assertNull(history.doCommand(GoToVersion(-1)))
        assertEquals(0, history.counters.value.undoVersion)
        assertEquals(0, history.counters.value.undoCount)
        assertEquals(0, history.counters.value.redoCount)
    }

    @Test
    fun `Undo returns previous content`() {
        val snap = history.addChange("my text")
        history.addChange("my new text")

        assertEquals(snap, history.doCommand(Undo))
        assertNull(history.doCommand(Undo))
        assertEquals(0, history.counters.value.undoCount)
        assertEquals(1, history.counters.value.redoCount)
    }

    @Test
    fun `Redo returns undone content`() {
        history.addChange("my text")
        val snap = history.addChange("my new text")
        history.doCommand(Undo)

        assertEquals(1, history.counters.value.redoCount)
        assertEquals(snap, history.doCommand(Redo))
        assertNull(history.doCommand(Redo))
        assertEquals(0, history.counters.value.redoCount)
        assertEquals(1, history.counters.value.undoCount)
    }

    @Test
    fun `addChange removes redo history`() {
        history.addChange("my text")
        val snap1 = history.addChange("my new text")
        history.doCommand(Undo)
        val snap2 = history.addChange("my actual new text")

        assertEquals(1, history.counters.value.undoCount)
        assertEquals(0, history.counters.value.redoCount)
        assertNull(history.doCommand(Redo))
        assertEquals(1, history.counters.value.undoCount)
        assertEquals(0, history.counters.value.redoCount)
        assertEquals(snap2.changeId, history.counters.value.changeId)
        assertNotEquals(snap1.changeId, snap2.changeId)
    }

    @Test
    fun `addChange detects current version`() {
        history.addChange("my text", selection = 7)
        val snap1 = history.addChange("my new text", selection = 11, preSelection = 7)
        history.addChange("my actual new text", selection = 18, preSelection = 11)
        val snapUndo = history.doCommand(Undo)
        val ref = history.counters.value.undoVersion
        val snapNew = history.addChange("my new text", selection = 6, preSelection = 11)

        assertEquals(1, history.counters.value.undoCount)
        assertEquals(1, history.counters.value.redoCount)
        assertEquals(ref, history.counters.value.undoVersion)
        assertEquals(snap1, snapNew)
        assertEquals(snapUndo, snap1)
    }

    @Test
    fun `GoToVersion works back and forth`() {
        history.addChange("my text")
        val snap1 = history.addChange("my new text")
        history.addChange("my newer text")
        val snap2 = history.addChange("my newest text")
        history.addChange("my very newest text")

        val ref = history.counters.value.undoVersion
        assertEquals(5, ref)
        assertEquals(snap1, history.doCommand(GoToVersion(ref - 3)))
        assertEquals(2, history.counters.value.undoVersion)
        assertEquals(snap1.changeId, history.counters.value.changeId)
        assertEquals(snap2, history.doCommand(GoToVersion(ref - 1)))
        assertEquals(4, history.counters.value.undoVersion)
        assertEquals(snap2.changeId, history.counters.value.changeId)
    }

    @Test
    fun `GoToVersion goes to limit when exceeded`() {
        val firstSnap = history.addChange("my text")
        history.addChange("my new text")
        history.addChange("my newer text")
        history.addChange("my newest text")
        val lastSnap = history.addChange("my very newest text")

        assertEquals(5, history.counters.value.undoVersion)
        assertEquals(firstSnap, history.doCommand(GoToVersion(-23)))
        assertEquals(1, history.counters.value.undoVersion)
        assertEquals(lastSnap, history.doCommand(GoToVersion(23)))
        assertEquals(5, history.counters.value.undoVersion)
    }

    @Test
    fun `GoToVersion returns null when nothing to do`() {
        val snap1 = history.addChange("my text")
        history.addChange("my new text")
        val snap2 = history.addChange("my newer text")

        assertEquals(3, history.counters.value.undoVersion)
        assertEquals(snap1, history.doCommand(GoToVersion(1)))
        assertEquals(1, history.counters.value.undoVersion)
        assertEquals(null, history.doCommand(GoToVersion(-1)))
        assertEquals(null, history.doCommand(GoToVersion(1)))
        assertEquals(1, history.counters.value.undoVersion)
        assertEquals(snap2, history.doCommand(GoToVersion(3)))
        assertEquals(3, history.counters.value.undoVersion)
        assertEquals(null, history.doCommand(GoToVersion(3)))
        assertEquals(null, history.doCommand(GoToVersion(4)))
        assertEquals(3, history.counters.value.undoVersion)
    }

    @Test
    fun `addChange overwrites previous if transactionTag matches`() {
        val snap1 = history.addChange("my text")
        history.addChange("my new text", transactionTag = 1)
        val snap3 = history.addChange("my newer text", transactionTag = 1)
        val snap4 = history.addChange("my newest text", transactionTag = 2)

        assertEquals(NO_TRANSACTION_TAG, snap1.transactionTag)
        assertEquals(1, snap3.transactionTag)
        assertEquals(2, snap4.transactionTag)
        assertEquals(2, history.counters.value.undoCount)
        assertEquals(snap3, history.doCommand(Undo))
        assertEquals(snap1, history.doCommand(Undo))
        assertEquals(null, history.doCommand(Undo))
    }

    @Test
    fun `addChange updates transactionTag if manual redo detected`() {
        val snap1 = history.addChange("my text")
        history.addChange("my new text", transactionTag = 1)
        val snap3 = history.addChange("my newer text", transactionTag = 1)
        history.doCommand(Undo)
        val snap4 = history.addChange("my newer text", transactionTag = 2)
        val snap5 = history.addChange("my newest text", transactionTag = 2)

        assertEquals(1, snap3.transactionTag)
        assertEquals(2, snap4.transactionTag)
        assertEquals(2, snap5.transactionTag)
        assertEquals(1, history.counters.value.undoCount)
        assertEquals(snap1, history.doCommand(Undo))
        assertEquals(null, history.doCommand(Undo))
    }

    @Test
    fun `addChange updates transactionTag when text is unchanged`() {
        val snap1 = history.addChange("my text")
        history.addChange("my new text")
        val snap3 = history.addChange("my new text", transactionTag = 1)
        val snap4 = history.addChange("my newer text", transactionTag = 1)

        assertEquals(1, snap3.transactionTag)
        assertEquals(1, snap4.transactionTag)
        assertEquals(1, history.counters.value.undoCount)
        assertEquals(snap1, history.doCommand(Undo))
        assertEquals(null, history.doCommand(Undo))
    }

    @Test
    fun `trimToSizeInBytes removes oldest undo steps`() {
        history.addChange("my textmy text") // 14B
        history.addChange("my new textmy new text") // 22B
        val snap = history.addChange("my newer textmy newer text") // 26B
        history.addChange("my newest textmy newest text") // 28B

        assertEquals(3, history.counters.value.undoCount)
        assertEquals(90, history.counters.value.totalSize)
        history.trimToSizeInBytes(60)
        assertEquals(1, history.counters.value.undoCount)
        assertEquals(snap, history.doCommand(Undo))
        assertNull(history.doCommand(Undo))
        assertEquals(54, history.counters.value.totalSize)
    }

    @Test
    fun `trimToSizeInBytes preserves at least current text`() {
        history.addChange("my text")
        val snap1 = history.addChange("my new text")
        history.trimToSizeInBytes(4)
        val snap2 = history.addChange("my newer text")

        assertEquals(1, history.counters.value.undoCount)
        assertEquals(snap1, history.doCommand(Undo))
        assertNull(history.doCommand(Undo))
        assertEquals(snap2, history.doCommand(Redo))
        assertEquals(24, history.counters.value.totalSize)
    }

    @Test
    fun `trimToSizeInBytes removes redo first`() {
        // each 10B
        history.addChange("text1text1")
        history.addChange("text2text2")
        history.addChange("text3text3")
        val snap4 = history.addChange("text4text4")
        val snap5 = history.addChange("text5text5")
        history.addChange("text6text6")

        assertEquals(snap4, history.doCommand(GoToVersion(4)))
        history.trimToSizeInBytes(56)
        assertEquals(3, history.counters.value.undoCount)
        assertEquals(1, history.counters.value.redoCount)
        assertEquals(snap5, history.doCommand(GoToVersion(6)))
        assertEquals(4, history.counters.value.undoCount)
        assertEquals(0, history.counters.value.redoCount)
        assertEquals(50, history.counters.value.totalSize)
    }

    @Test
    fun `it adjusts history size within transaction`() {
        history.addChange("text1text1")
        history.addChange("text2text2", transactionTag = 2)
        history.addChange("text4", transactionTag = 2)

        assertEquals(15, history.counters.value.totalSize)
    }

}
