package de.ferreum.pto.preferences

import app.cash.turbine.test
import de.ferreum.pto.InMemorySharedPreferences
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_LOGFILE_DATE
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_LOGFILE_ENABLE
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_NEWPAGE_TIME
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_QUICKNOTES_NOTIFICATION_TITLE
import de.ferreum.pto.preferences.PtoPreferences.Companion.PREF_REMINDERS_ENABLED
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import java.time.LocalTime
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class PtoPreferencesRepositoryTest {

    private val scope = TestScope()
    private val inMemorySharedPreferences = InMemorySharedPreferences()

    private lateinit var preferencesRepository: PtoPreferencesRepositoryImpl

    private var error: Throwable? = null

    @Before
    fun setUp() {
        preferencesRepository = PtoPreferencesRepositoryImpl(
            inMemorySharedPreferences,
            scope.backgroundScope,
            errorLogger = { error = it },
        )
    }

    @Test
    fun `when preferencesFlow is used, then it emits the current preferences`() =
        scope.runTest {
            inMemorySharedPreferences.edit()
                .putString(PREF_LOGFILE_DATE, "2000-05-06")
                .putBoolean(PREF_LOGFILE_ENABLE, true)
                .putString(PREF_NEWPAGE_TIME, "05:00")
                .apply()

            val preferences = preferencesRepository.preferencesFlow.first()

            assertEquals(LocalDate.of(2000, 5, 6), preferences.logfileDate)
            assertTrue(preferences.isLogfileEnabled)
            assertEquals(LocalTime.of(5, 0), preferences.newpageTime)
            assertNull(error)
        }

    @Test
    fun `given preferencesFlow is used, when preference is changed, then it emits updated preferences`() =
        scope.runTest {
            inMemorySharedPreferences.edit()
                .putString(PREF_QUICKNOTES_NOTIFICATION_TITLE, "")
                .putBoolean(PREF_REMINDERS_ENABLED, false)
                .apply()
            preferencesRepository.preferencesFlow.distinctUntilChanged().test {
                val first = awaitItem()
                assertEquals("", first.customNotificationTitle)
                assertFalse(first.isEventRemindersEnabled)

                inMemorySharedPreferences.edit()
                    .putString(PREF_QUICKNOTES_NOTIFICATION_TITLE, "Notes")
                    .putBoolean(PREF_REMINDERS_ENABLED, true)
                    .apply()

                val result = awaitItem()
                assertEquals("Notes", result.customNotificationTitle)
                assertTrue(result.isEventRemindersEnabled)
                assertNull(error)
            }
        }

    @Test
    fun `when preferences read fails during start, then it clears preferences`() {
        scope.runTest {
            inMemorySharedPreferences.edit()
                .putInt(PREF_NEWPAGE_TIME, 123)
                .apply()
            val result = preferencesRepository.preferencesFlow.first()

            assertEquals(LocalTime.MIDNIGHT, result.newpageTime)
            assertEquals(emptyMap<String, Any?>(), inMemorySharedPreferences.all)
            assertEquals(error!!::class.java, ClassCastException::class.java)
        }
    }

    @Test
    fun `when preferences read fails during start, then it preserves logfile prefs`() {
        scope.runTest {
            inMemorySharedPreferences.edit()
                .putInt(PREF_NEWPAGE_TIME, 123)
                .putString(PREF_LOGFILE_DATE, "2020-02-20")
                .putBoolean(PREF_LOGFILE_ENABLE, true)
                .apply()
            val result = preferencesRepository.preferencesFlow.first()

            assertEquals(LocalTime.MIDNIGHT, result.newpageTime)
            assertEquals(true, result.isLogfileEnabled)
            assertEquals(LocalDate.of(2020, 2, 20), result.logfileDate)
            assertEquals(
                mapOf(PREF_LOGFILE_ENABLE to true, PREF_LOGFILE_DATE to "2020-02-20"),
                inMemorySharedPreferences.all,
            )
            assertEquals(error!!::class.java, ClassCastException::class.java)
        }
    }

}
