package de.ferreum.pto.reminder

import de.ferreum.pto.reminder.WakeupScheduler.AlarmHandle
import de.ferreum.pto.reminder.WakeupScheduler.Wakeup
import io.mockk.checkUnnecessaryStub
import io.mockk.coVerify
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import io.mockk.verify
import org.junit.Before
import org.junit.Test
import java.time.Instant

internal class CachingWakeupSchedulerTest {

    private lateinit var wakeupScheduler: WakeupScheduler

    private val wrapped: WakeupScheduler = mockk()

    @Before
    fun setUp() {
        wakeupScheduler = CachingWakeupScheduler(wrapped)
    }

    @Test
    fun `scheduleWakeup sets alarm`() {
        every { wrapped.scheduleWakeup(alarmHandle(), wakeup()) } just runs

        wakeupScheduler.scheduleWakeup(alarmHandle(), wakeup())

        checkUnnecessaryStub(wrapped)
    }

    @Test
    fun `scheduleWakeup sets changed alarm`() {
        val wakeup1 = Wakeup.exact(Instant.ofEpochMilli(1234))
        val wakeup2 = Wakeup.exact(Instant.ofEpochMilli(2345))
        every { wrapped.scheduleWakeup(alarmHandle(), wakeup1) } just runs
        every { wrapped.scheduleWakeup(alarmHandle(), wakeup2) } just runs

        wakeupScheduler.scheduleWakeup(alarmHandle(), wakeup1)
        wakeupScheduler.scheduleWakeup(alarmHandle(), wakeup2)

        checkUnnecessaryStub(wrapped)
    }

    @Test
    fun `scheduleWakeup ignores repeated call`() {
        every { wrapped.scheduleWakeup(alarmHandle(), wakeup()) } just runs

        wakeupScheduler.scheduleWakeup(alarmHandle(), wakeup())

        coVerify(exactly = 1) { wrapped.scheduleWakeup(any(), any()) }
    }

    @Test
    fun `scheduleWakeup runs for separate handles`() {
        every { wrapped.scheduleWakeup(alarmHandle().copy(requestCode = 222), wakeup()) } just runs
        every { wrapped.scheduleWakeup(alarmHandle().copy(requestCode = 333), wakeup()) } just runs

        wakeupScheduler.scheduleWakeup(alarmHandle().copy(requestCode = 222), wakeup())
        wakeupScheduler.scheduleWakeup(alarmHandle().copy(requestCode = 333), wakeup())

        checkUnnecessaryStub(wrapped)
    }

    @Test
    fun `scheduleWakeup runs after clearWakeup`() {
        every { wrapped.clearWakeup(alarmHandle()) } just runs
        every { wrapped.scheduleWakeup(alarmHandle(), wakeup()) } just runs

        wakeupScheduler.clearWakeup(alarmHandle())
        wakeupScheduler.scheduleWakeup(alarmHandle(), wakeup())

        checkUnnecessaryStub(wrapped)
    }

    @Test
    fun `clearWakeup clears alarm`() {
        every { wrapped.clearWakeup(alarmHandle()) } just runs

        wakeupScheduler.clearWakeup(alarmHandle())

        checkUnnecessaryStub(wrapped)
    }

    @Test
    fun `clearWakeup ignores repeated call`() {
        every { wrapped.clearWakeup(alarmHandle()) } just runs

        wakeupScheduler.clearWakeup(alarmHandle())
        wakeupScheduler.clearWakeup(alarmHandle())

        coVerify(exactly = 1) { wrapped.clearWakeup(any()) }
    }

    @Test
    fun `clearWakeup runs for separate handles`() {
        every { wrapped.clearWakeup(alarmHandle().copy(requestCode = 222)) } just runs
        every { wrapped.clearWakeup(alarmHandle().copy(requestCode = 333)) } just runs

        wakeupScheduler.clearWakeup(alarmHandle().copy(requestCode = 222))
        wakeupScheduler.clearWakeup(alarmHandle().copy(requestCode = 333))

        checkUnnecessaryStub(wrapped)
    }

    @Test
    fun `clearWakeup runs after scheduleWakeup`() {
        every { wrapped.scheduleWakeup(alarmHandle(), wakeup()) } just runs
        every { wrapped.clearWakeup(alarmHandle()) } just runs

        wakeupScheduler.scheduleWakeup(alarmHandle(), wakeup())
        wakeupScheduler.clearWakeup(alarmHandle())

        checkUnnecessaryStub(wrapped)
    }

    private fun alarmHandle() = AlarmHandle(777)
    private fun wakeup() = Wakeup.exact(Instant.ofEpochMilli(2345))
}
