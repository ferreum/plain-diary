package de.ferreum.pto.reminder

import de.ferreum.pto.preferences.RecordingLogger
import de.ferreum.pto.reminder.DismissedRemindersStore.ReminderRecord
import io.mockk.MockKAnnotations.init
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.io.File
import java.io.IOException
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeParseException
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

class DismissedRemindersStoreTest {

    @get:Rule
    val temporaryFolder = TemporaryFolder()

    private val logger = RecordingLogger()

    private lateinit var store: DismissedRemindersStore

    @Before
    fun setUp() {
        init()
    }

    @After
    fun tearDown() {
        logger.throwIfAny()
    }

    private fun TestScope.init(file: File) {
        store = DismissedRemindersStore(
            this,
            file,
            StandardTestDispatcher(testScheduler),
            logger
        )
    }

    @Test
    fun `it reads version 1 file`() = runTest {
        val file = temporaryFolder.newFile()
        file.writeText(
            """
                1
                2020-02-03T12:34:00|0||dismissed
                2020-02-03T12:34:00|5000||dismissed
                2020-02-03T01:23:00|5000||dismissed
            """.trimIndent()
        )

        init(file)
        val result = store.records.first()

        assertEquals(
            setOf(
                ReminderRecord(LocalDateTime.of(2020, 2, 3, 12, 34), Duration.ZERO, ""),
                ReminderRecord(LocalDateTime.of(2020, 2, 3, 12, 34), (-5).seconds, ""),
                ReminderRecord(LocalDateTime.of(2020, 2, 3, 1, 23), (-5).seconds, ""),
            ),
            result
        )
    }

    @Test
    fun `it loads missing file`() = runTest {
        val file = temporaryFolder.newNonExistingFile()

        init(file)
        val result = store.records.first()

        assertEquals(emptySet<ReminderRecord>(), result)
    }

    @Test
    fun `updateState writes state`() = runTest {
        val file = temporaryFolder.newNonExistingFile()

        init(file)
        store.updateState {
            it + ReminderRecord(LocalDateTime.of(2020, 2, 3, 12, 34), Duration.ZERO, "")
        }
        store.updateState {
            it + setOf(
                ReminderRecord(LocalDateTime.of(2020, 2, 3, 12, 34), (-5).seconds, ""),
                ReminderRecord(LocalDateTime.of(2020, 2, 3, 1, 23), (-5).seconds, ""),
            )
        }

        assertEquals(
            """
                1
                2020-02-03T12:34|0||dismissed
                2020-02-03T12:34|5000||dismissed
                2020-02-03T01:23|5000||dismissed
            """.trimIndent() + "\n",
            file.readText()
        )
    }

    @Test
    fun `updateState waits for loaded state`() = runTest {
        val file = temporaryFolder.newFile()
        file.writeText(
            """
                1
                2024-02-03T10:00:00|0||dismissed
            """.trimIndent()
        )

        init(file)
        store.updateState { it + ReminderRecord(LocalDateTime.now(), Duration.ZERO, "") }

        assertEquals(2, store.records.first().size)
        assertEquals(3, file.readLines().size)
    }

    @Test
    fun `it clears state when load fails`() = runTest {
        val file = temporaryFolder.newFile()
        file.writeText("invalid content")

        init(file)
        val result = store.records.first()

        assertEquals(emptySet<ReminderRecord>(), result)
        assertEquals("1\n", file.readText())
        assertEquals(
            listOf(NumberFormatException::class.java),
            logger.exceptions.map { it::class.java }
        )
        logger.clear()
    }

    @Test
    fun `it clears state on invalid line`() = runTest {
        val file = temporaryFolder.newFile()
        file.writeText("1\ntest|abc||dismissed\n")

        init(file)
        val result = store.records.first()

        assertEquals(emptySet<ReminderRecord>(), result)
        assertEquals("1\n", file.readText())
        assertEquals(
            listOf(DateTimeParseException::class.java),
            logger.exceptions.map { it::class.java }
        )
        logger.clear()
    }

    @Test
    fun `it clears state on incompatible version`() = runTest {
        val file = temporaryFolder.newFile()
        file.writeText("100\n2020-01-02T10:20|0||dismissed")

        init(file)
        val result = store.records.first()

        assertEquals(emptySet<ReminderRecord>(), result)
        assertEquals("1\n", file.readText())
        assertEquals(
            listOf(IOException::class.java),
            logger.exceptions.map { it::class.java }
        )
        logger.clear()
    }

    @Test
    fun `it reads state from compatible version`() = runTest {
        val file = temporaryFolder.newFile()
        file.writeText(
            """
                99
                2020-01-02T10:20|0||dismissed|more|fields
                2020-01-02T10:20|5000||newState
                2020-01-02T10:20|15000|abc!|dismissed
                2020-01-02T10:20|-2000|abc!|dismissed
            """.trimIndent()
        )

        init(file)
        val result = store.records.first()

        assertEquals(
            setOf(
                ReminderRecord(LocalDateTime.of(2020, 1, 2, 10, 20), Duration.ZERO, ""),
                ReminderRecord(LocalDateTime.of(2020, 1, 2, 10, 20), (-15).seconds, "abc!"),
                ReminderRecord(LocalDateTime.of(2020, 1, 2, 10, 20), 2.seconds, "abc!"),
            ),
            result
        )
    }

    @Test
    fun `it saves and restores many`() = runTest {
        val expected = mutableSetOf<ReminderRecord>()
        val file = temporaryFolder.newNonExistingFile()

        for (year in listOf(2020, 2030)) {
            for (month in listOf(1, 3, 5, 12)) {
                init(file)
                for (day in listOf(1, 12, 31)) {
                    val date = LocalDate.of(year, month, day)
                    for (time in listOf(LocalTime.of(4, 19), LocalTime.of(23, 45))) {
                        val set = setOf(
                            ReminderRecord(date.atTime(time), Duration.ZERO, ""),
                            ReminderRecord(date.atTime(time), (-15).minutes, "?"),
                            ReminderRecord(date.atTime(time), (-1).days, "!"),
                        )
                        expected += set
                        store.updateState { it + set }
                    }
                }
            }
        }

        assertEquals(expected, store.records.first())
        init(file)
        assertEquals(expected, store.records.first())
    }

    @Test
    fun `it throws on invalid flags`() = runTest {
        val file = temporaryFolder.newNonExistingFile()
        init(file)

        val res1 = runCatching {
            store.updateState { it + ReminderRecord(LocalDateTime.now(), Duration.ZERO, "ab|?c!") }
        }
        val res2 = runCatching {
            store.updateState { it + ReminderRecord(LocalDateTime.now(), Duration.ZERO, "ab\n?c!") }
        }

        assertEquals(IllegalArgumentException::class.java, res1.exceptionOrNull()?.javaClass)
        assertEquals(IllegalArgumentException::class.java, res2.exceptionOrNull()?.javaClass)
        assertEquals(emptySet<ReminderRecord>(), store.records.first())
        init(file)
        assertEquals(emptySet<ReminderRecord>(), store.records.first())
    }

    private fun TemporaryFolder.newNonExistingFile() = File(newFolder(), "tempfile")
}
