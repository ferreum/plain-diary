package de.ferreum.pto.reminder

import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import kotlin.time.Duration

internal fun reminder() = ReminderParser.Reminder(
    targetTime = LocalDateTime.of(2020, 1, 2, 14, 20),
    targetInstant = Instant.ofEpochMilli(10_000),
    reminderOffset = Duration.ZERO,
    flags = "",
    title = "test reminder",
    pageDate = LocalDate.of(2020, 1, 1),
    source = "test source",
)
