package de.ferreum.pto.reminder

import de.ferreum.pto.preferences.FakePtoPreferencesRepository
import de.ferreum.pto.reminder.ReminderParser.Reminder
import de.ferreum.pto.util.TimeProvider
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

internal class ReminderParserTest {

    private lateinit var parser: ReminderParser

    private val prefRepository = FakePtoPreferencesRepository()
    private val timeProvider = object : TimeProvider {
        override fun timeZone() = ZoneId.of("Europe/Berlin")
    }

    @Before
    fun setUp() {
        parser = ReminderParser(
            timeProvider,
        )
    }

    @Test
    fun `it parses event line`() = runTest {
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 1, 1),
            "@12:34 test"
        )

        assertEquals(
            listOf(
                Reminder(
                    targetTime = LocalDateTime.of(2020, 1, 1, 12, 34),
                    targetInstant = LocalDateTime.of(2020, 1, 1, 12, 34)
                        .atZone(timeProvider.timeZone())
                        .toInstant(),
                    reminderOffset = Duration.ZERO,
                    flags = "",
                    title = "test",
                    pageDate = LocalDate.of(2020, 1, 1),
                    source = "@12:34 test",
                )
            ),
            result
        )
    }

    @Test
    fun `it respects newpage time preference`() = runTest {
        prefRepository.preferencesFlow.update {
            it.copy(newpageTime = LocalTime.of(6, 0))
        }
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 2, 2),
            """
                @4:34 -6h -2h test
            """.trimIndent(),
        )

        assertEquals(
            listOf(
                Reminder(
                    targetTime = LocalDateTime.of(2020, 2, 3, 4, 34),
                    targetInstant = LocalDateTime.of(2020, 2, 3, 4, 34)
                        .atZone(timeProvider.timeZone())
                        .toInstant(),
                    reminderOffset = (-6).hours,
                    flags = "",
                    title = "test",
                    pageDate = LocalDate.of(2020, 2, 2),
                    source = "@4:34 -6h -2h test",
                ),
                Reminder(
                    targetTime = LocalDateTime.of(2020, 2, 3, 4, 34),
                    targetInstant = LocalDateTime.of(2020, 2, 3, 4, 34)
                        .atZone(timeProvider.timeZone())
                        .toInstant(),
                    reminderOffset = (-2).hours,
                    flags = "",
                    title = "test",
                    pageDate = LocalDate.of(2020, 2, 2),
                    source = "@4:34 -6h -2h test",
                ),
            ),
            result
        )
    }

    @Test
    fun `it parses short time`() = runTest {
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 1, 1),
            "@2:34 test"
        )

        assertEquals(
            listOf(
                Reminder(
                    targetTime = LocalDateTime.of(2020, 1, 1, 2, 34),
                    targetInstant = LocalDateTime.of(2020, 1, 1, 2, 34)
                        .atZone(timeProvider.timeZone())
                        .toInstant(),
                    reminderOffset = Duration.ZERO,
                    flags = "",
                    title = "test",
                    pageDate = LocalDate.of(2020, 1, 1),
                    source = "@2:34 test",
                )
            ),
            result
        )
    }

    @Test
    fun `it parses event with colon`() = runTest {
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 1, 1),
            "@:12:34-1h9m test"
        )

        assertEquals(
            listOf(
                Reminder(
                    targetTime = LocalDateTime.of(2020, 1, 1, 12, 34),
                    targetInstant = LocalDateTime.of(2020, 1, 1, 12, 34)
                        .atZone(timeProvider.timeZone())
                        .toInstant(),
                    reminderOffset = (-69).minutes,
                    flags = "",
                    title = "test",
                    pageDate = LocalDate.of(2020, 1, 1),
                    source = "@:12:34-1h9m test",
                )
            ),
            result
        )
    }

    @Test
    fun `it parses negative duration`() = runTest {
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 1, 1),
            "@12:34-1h9m test"
        )

        assertEquals(
            listOf(
                Reminder(
                    targetTime = LocalDateTime.of(2020, 1, 1, 12, 34),
                    targetInstant = LocalDateTime.of(2020, 1, 1, 12, 34)
                        .atZone(timeProvider.timeZone())
                        .toInstant(),
                    reminderOffset = (-69).minutes,
                    flags = "",
                    title = "test",
                    pageDate = LocalDate.of(2020, 1, 1),
                    source = "@12:34-1h9m test",
                )
            ),
            result
        )
    }

    @Test
    fun `it parses positive duration`() = runTest {
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 1, 1),
            "@12:34+1h9m test"
        )

        assertEquals(
            listOf(
                Reminder(
                    targetTime = LocalDateTime.of(2020, 1, 1, 12, 34),
                    targetInstant = LocalDateTime.of(2020, 1, 1, 12, 34)
                        .atZone(timeProvider.timeZone())
                        .toInstant(),
                    reminderOffset = 69.minutes,
                    flags = "",
                    title = "test",
                    pageDate = LocalDate.of(2020, 1, 1),
                    source = "@12:34+1h9m test",
                )
            ),
            result
        )
    }

    @Test
    fun `it parses multiple durations`() = runTest {
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 1, 1),
            "@12:34 -1h -15m -2m +5m test"
        )

        assertEquals(
            listOf(
                (-1).hours to "test",
                (-15).minutes to "test",
                (-2).minutes to "test",
                5.minutes to "test",
            ),
            result.map { it.reminderOffset to it.title }
        )
    }

    @Test
    fun `it parses different duration formats`() = runTest {
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 1, 1),
            """
                @12:34-1s second
                @12:34-1m minute
                @12:34-1h hour
                @12:34-1d day
                @12:34+1h plus hour
                @12:34-1d2h30m40s mixed
                @12:34 -30m spaced
                @12:34 +1h plus hour spaced
                @12:34   -1d2h30m40s spaced mixed
            """.trimIndent()
        )

        assertEquals(
            listOf(
                "second" to (-1).seconds,
                "minute" to (-1).minutes,
                "hour" to (-1).hours,
                "day" to (-1).days,
                "plus hour" to 1.hours,
                "mixed" to -(1.days + 2.hours + 30.minutes + 40.seconds),
                "spaced" to (-30).minutes,
                "plus hour spaced" to 1.hours,
                "spaced mixed" to -(1.days + 2.hours + 30.minutes + 40.seconds),
            ),
            result.map { it.title to it.reminderOffset }
        )
    }

    @Test
    fun `it ignores invalid duration`() = runTest {
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 1, 1),
            """
                @12:34-1x one
                @12:34-1m3x two
                @12:34-h three
                @12:34-dhms four
                @12:34-1dhms five
                @12:34+1x six
                @12:34+1m3x seven
            """.trimIndent()
        )

        assertEquals(
            listOf(
                "-1x one" to Duration.ZERO,
                "-1m3x two" to Duration.ZERO,
                "-h three" to Duration.ZERO,
                "-dhms four" to Duration.ZERO,
                "-1dhms five" to Duration.ZERO,
                "+1x six" to Duration.ZERO,
                "+1m3x seven" to Duration.ZERO,
            ),
            result.map { it.title to it.reminderOffset }
        )
    }

    @Test
    fun `it handles empty file`() = runTest {
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 1, 1),
            ""
        )

        assertEquals(emptyList<Reminder>(), result)
    }

    @Test
    fun `it ignores other lines`() = runTest {
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 1, 1),
            """
                @ 12:34 aaa1
                23:20 aaa2
                @012:34 aaa3
                 @10:00 aaa4
                aaa5 @13:00 aaa6
                + 14:30 aaa6

                aaa9

            """.trimIndent()
        )

        assertEquals(emptyList<Reminder>(), result)
    }

    @Test
    fun `it matches in file with other lines`() = runTest {
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 1, 1),
            """
                @ 12:34 aaa1
                23:20 aaa2
                @10:00 test1
                aaa @11:30 aaa3
                @34:56 aaa4
                @11:23 test2

                aaa9
            """.trimIndent()
        )

        assertEquals(
            listOf(
                Reminder(
                    targetTime = LocalDateTime.of(2020, 1, 1, 10, 0),
                    targetInstant = LocalDateTime.of(2020, 1, 1, 10, 0)
                        .atZone(timeProvider.timeZone())
                        .toInstant(),
                    reminderOffset = Duration.ZERO,
                    flags = "",
                    title = "test1",
                    pageDate = LocalDate.of(2020, 1, 1),
                    source = "@10:00 test1",
                ),
                Reminder(
                    targetTime = LocalDateTime.of(2020, 1, 1, 11, 23),
                    targetInstant = LocalDateTime.of(2020, 1, 1, 11, 23)
                        .atZone(timeProvider.timeZone())
                        .toInstant(),
                    reminderOffset = Duration.ZERO,
                    flags = "",
                    title = "test2",
                    pageDate = LocalDate.of(2020, 1, 1),
                    source = "@11:23 test2",
                ),
            ),
            result
        )
    }

    @Test
    fun `it parses reminder flags`() = runTest {
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 1, 1),
            "@10:20 -1h? -15m -5m! test"
        )

        assertEquals(
            listOf(
                (-1).hours to "?",
                (-15).minutes to "",
                (-5).minutes to "!",
            ),
            result.map { it.reminderOffset to it.flags }
        )
    }

    @Test
    fun `it parses reminder flags of time`() = runTest {
        val result = parser.parsePageReminders(
            prefRepository.preferences.newpageTime,
            LocalDate.of(2020, 1, 1),
            """
                @10:20! test1
                @10:30? test2
            """.trimIndent()
        )

        assertEquals(
            listOf(
                "test1" to "!",
                "test2" to "?",
            ),
            result.map { it.title to it.flags }
        )
    }

    @Test
    fun `findEventByTime returns offset`() {
        val offset = ReminderParser.indexOfEventByTime(
            """
                aaaa
                @12:34 test
            """.trimIndent(),
            LocalTime.of(12, 34),
        )

        assertEquals(5, offset)
    }

    @Test
    fun `findEventByTime ignores other events`() {
        val offset = ReminderParser.indexOfEventByTime(
            """
                aaaa
                @10:20 test1
                @12:34 test2
            """.trimIndent(),
            LocalTime.of(12, 34),
        )

        assertEquals(18, offset)
    }

    @Test
    fun `findEventByTime returns null if not found`() {
        val offset = ReminderParser.indexOfEventByTime(
            """
                aaaa
                @10:20 test1
                @12:34 test2
            """.trimIndent(),
            LocalTime.of(23, 30),
        )

        assertEquals(null, offset)
    }
}
