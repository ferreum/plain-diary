package de.ferreum.pto.reminder

import de.ferreum.pto.InMemorySharedPreferences
import de.ferreum.pto.files.PageWriteSignal.PageWriteFlag
import de.ferreum.pto.preferences.FakePtoPreferencesRepository
import de.ferreum.pto.preferences.RecordingLogger
import de.ferreum.pto.reminder.DismissedRemindersStore.Companion.toRecord
import de.ferreum.pto.reminder.DismissedRemindersStore.ReminderRecord
import de.ferreum.pto.reminder.ReminderService.Companion.UPCOMING_MILLIS
import de.ferreum.pto.reminder.ReminderService.Companion.notifyAlarm
import de.ferreum.pto.reminder.ReminderService.Companion.softAlarm
import de.ferreum.pto.reminder.ReminderService.ReminderState
import de.ferreum.pto.reminder.UpcomingPageWatcher.PageUpdate
import de.ferreum.pto.reminder.WakeupScheduler.Wakeup
import de.ferreum.pto.util.TimeProvider
import io.mockk.called
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import io.mockk.verify
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

class ReminderServiceTest {

    private val nowDateTime = LocalDateTime.of(2020, 1, 2, 12, 30)
    private val today = nowDateTime.toLocalDate()
    private val tomorrow = nowDateTime.toLocalDate().plusDays(1)
    private val dayafter = nowDateTime.toLocalDate().plusDays(2)

    private val scope = TestScope()
    private val upcomingPageWatcher: UpcomingPageWatcher = mockk {
        every { observeContent(any()) } returns flowOf(
            PageUpdate(today, "content"),
            PageUpdate(tomorrow, "content"),
            PageUpdate(dayafter, "content"),
        )
        every { triggerCurrentDayCheck() } just runs
    }
    private val sharedPreferences = InMemorySharedPreferences()
    private val prefRepository = FakePtoPreferencesRepository().apply {
        preferencesFlow.update { it.copy(isEventRemindersEnabled = true) }
    }
    private val reminderParser: ReminderParser = mockk {
        coEvery { parsePageReminders(any(), any(), any()) } returns emptyList()
    }
    private val wakeupScheduler: WakeupScheduler = mockk {
        every { scheduleWakeup(notifyAlarm, any()) } just runs
        every { scheduleWakeup(softAlarm, any()) } just runs
        every { clearWakeup(any()) } just runs
    }
    private val alarmSignal = AlarmSignal()
    private val reminderNotifier: ReminderNotifier = mockk {
        every { notify(any()) } just runs
        every { setUpcoming(any()) } just runs
    }
    private val reminderStateRepository: ReminderStateRepository = mockk {
        every { dismissedReminders } returns flowOf(emptySet())
    }
    private val logger = RecordingLogger()

    private val timeProvider = object : TimeProvider {
        var localDateTime = nowDateTime
        var instant = Instant.ofEpochMilli(999_000)
        var elapsedRealtime = 1000L
        override fun localDateTime() = localDateTime
        override fun instant() = instant
        override fun elapsedRealtime() = elapsedRealtime
    }

    private lateinit var reminderService: ReminderService

    @Before
    fun setUp() {
        reminderService = ReminderService(
            scope.backgroundScope,
            upcomingPageWatcher,
            sharedPreferences,
            prefRepository,
            reminderParser,
            wakeupScheduler,
            alarmSignal,
            reminderNotifier,
            timeProvider,
            reminderStateRepository,
            logger,
        )
    }

    @After
    fun tearDown() {
        logger.throwIfAny()
    }

    @Test
    fun `it shows notification for past reminder`() = scope.runTest {
        every { upcomingPageWatcher.observeContent(setOf(PageWriteFlag.NO_EVENTS)) } returns flowOf(
            PageUpdate(today, "content page1"),
            PageUpdate(tomorrow, "content page2"),
            PageUpdate(dayafter, "content page3"),
        )
        val reminder = ReminderParser.Reminder(
            targetTime = LocalDateTime.of(2020, 1, 2, 10, 20),
            targetInstant = Instant.ofEpochMilli(600_000),
            reminderOffset = (-2).minutes,
            flags = "",
            title = "test reminder",
            pageDate = LocalDate.of(2020, 1, 2),
            source = "test source",
        )
        coEvery {
            reminderParser.parsePageReminders(
                prefRepository.preferences.newpageTime, today, "content page1")
        } returns listOf(reminder)
        timeProvider.instant = Instant.ofEpochMilli(500_000)
        timeProvider.elapsedRealtime = 1337

        reminderService.run()
        testScheduler.runCurrent()

        verify(exactly = 1) { reminderNotifier.notify(reminder) }
        assertEquals(1337L, reminderService.latestNotificationMillis.first())
        assertEquals(ReminderState.LOADED, reminderService.reminderState.first())
    }

    @Test
    fun `it shows notification only once`() = scope.runTest {
        val contentFlow = MutableSharedFlow<PageUpdate>()
        every { upcomingPageWatcher.observeContent(any()) } returns contentFlow
        coEvery {
            reminderParser.parsePageReminders(any(), today, any())
        } answers {
            listOf(
                reminder().copy(
                    targetInstant = Instant.ofEpochMilli(600_000),
                    reminderOffset = (-2).minutes,
                    title = thirdArg<String>(),
                )
            )
        }
        timeProvider.instant = Instant.ofEpochMilli(500_000)

        reminderService.run()
        testScheduler.runCurrent()
        contentFlow.emit(PageUpdate(today, "content1 page1"))
        contentFlow.emit(PageUpdate(tomorrow, "content1 page2"))
        contentFlow.emit(PageUpdate(dayafter, "content1 page3"))
        testScheduler.runCurrent()
        contentFlow.emit(PageUpdate(today, "content2 page1"))
        alarmSignal.notifyAlarm(notifyAlarm)
        testScheduler.advanceTimeBy(10.seconds)

        verify(exactly = 1) { reminderNotifier.notify(any()) }
        assertEquals(ReminderState.LOADED, reminderService.reminderState.first())
    }

    @Test
    fun `it clears alarm if no reminders in future`() = scope.runTest {
        coEvery {
            reminderParser.parsePageReminders(any(), today, any())
        } returns listOf(
            reminder().copy(targetInstant = Instant.ofEpochMilli(20_000))
        )
        timeProvider.instant = Instant.ofEpochMilli(100_000)

        reminderService.run()
        testScheduler.runCurrent()

        verify { wakeupScheduler.clearWakeup(notifyAlarm) }
        verify(exactly = 0) { wakeupScheduler.scheduleWakeup(any(), any()) }
        assertEquals(ReminderState.LOADED, reminderService.reminderState.first())
    }

    @Test
    fun `it restarts if flow fails`() = scope.runTest {
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } throws IllegalStateException("testing")
        timeProvider.instant = Instant.ofEpochMilli(2000)

        reminderService.run()
        testScheduler.runCurrent()
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } returns
            listOf(reminder().copy(targetInstant = Instant.ofEpochMilli(1000)))
        testScheduler.advanceTimeBy(31.seconds)

        assertEquals(IllegalStateException::class, logger.exceptions.single()::class)
        coVerify { reminderNotifier.notify(reminder().copy(targetInstant = Instant.ofEpochMilli(1000))) }
        logger.clear()
    }

    @Test
    fun `it schedules wakeup for next future reminder`() = scope.runTest {
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } returns listOf(
            reminder().copy(
                targetInstant = Instant.ofEpochMilli(900_000),
                reminderOffset = (-2).minutes,
            ),
            reminder().copy(
                targetInstant = Instant.ofEpochMilli(9900_000),
                reminderOffset = (-2).minutes,
            ),
        )
        timeProvider.instant = Instant.ofEpochMilli(1000)

        reminderService.run()
        testScheduler.runCurrent()

        verify {
            wakeupScheduler.scheduleWakeup(
                notifyAlarm,
                Wakeup.exact(Instant.ofEpochMilli(780_000))
            )
        }
        assertEquals(null, reminderService.latestNotificationMillis.first())
        assertEquals(ReminderState.LOADED, reminderService.reminderState.first())
    }

    @Test
    fun `it schedules wakeup for 2 days future reminder`() = scope.runTest {
        coEvery { reminderParser.parsePageReminders(any(), dayafter, any()) } returns listOf(
            reminder().copy(
                targetInstant = Instant.ofEpochMilli(86400_000L * 2 + 300_000L),
                reminderOffset = (-2).minutes,
            ),
        )
        timeProvider.instant = Instant.ofEpochMilli(1000)

        reminderService.run()
        testScheduler.runCurrent()

        verify {
            wakeupScheduler.scheduleWakeup(
                notifyAlarm,
                Wakeup.exact(Instant.ofEpochMilli(86400_000L * 2 + 300_000L - 120_000L))
            )
        }
        assertEquals(null, reminderService.latestNotificationMillis.first())
        assertEquals(ReminderState.LOADED, reminderService.reminderState.first())
    }

    @Test
    fun `it ignores past reminder`() = scope.runTest {
        val contentFlow = MutableSharedFlow<PageUpdate>()
        every { upcomingPageWatcher.observeContent(any()) } returns contentFlow
        coEvery { reminderParser.parsePageReminders(any(), today, "content1") } returns emptyList()
        coEvery { reminderParser.parsePageReminders(any(), today, "content2") } returns listOf(
            reminder().copy(targetInstant = Instant.ofEpochMilli(10_000))
        )
        timeProvider.instant = Instant.ofEpochMilli(20_000)

        reminderService.run()
        testScheduler.runCurrent()
        contentFlow.emit(PageUpdate(today, "content1"))
        contentFlow.emit(PageUpdate(tomorrow, "content page2"))
        contentFlow.emit(PageUpdate(dayafter, "content page3"))
        testScheduler.runCurrent()
        contentFlow.emit(PageUpdate(today, "content2"))
        testScheduler.runCurrent()

        verify(exactly = 0) { wakeupScheduler.scheduleWakeup(any(), any()) }
        verify(exactly = 0) { reminderNotifier.notify(any()) }
        assertEquals(ReminderState.LOADED, reminderService.reminderState.first())
    }

    @Test
    fun `it sends upcoming reminder before foreground notification`() = scope.runTest {
        val reminder = reminder().copy(
            targetInstant = Instant.ofEpochMilli(200_000),
            flags = "!",
        )
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } returns listOf(reminder)
        timeProvider.instant = Instant.ofEpochMilli(5_000)

        reminderService.run()
        testScheduler.runCurrent()

        verify { reminderNotifier.setUpcoming(reminder) }
        verify(exactly = 0) { reminderNotifier.notify(any()) }
    }

    @Test
    fun `it sets wakeup for upcoming notification`() = scope.runTest {
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } returns listOf(
            reminder().copy(
                targetInstant = Instant.ofEpochMilli(UPCOMING_MILLIS + 15_000L),
                flags = "!",
            )
        )
        timeProvider.instant = Instant.ofEpochMilli(5_000)

        reminderService.run()
        testScheduler.runCurrent()

        verify(exactly = 1) {
            wakeupScheduler.scheduleWakeup(softAlarm,
                Wakeup.inexact(Instant.ofEpochMilli(15_000), wakeDevice = false))
        }
        verify(exactly = 1) {
            wakeupScheduler.scheduleWakeup(notifyAlarm,
                Wakeup.exact(Instant.ofEpochMilli(UPCOMING_MILLIS + 15_000)))
        }
        verify { reminderNotifier.setUpcoming(null) }
        verify(exactly = 0) { reminderNotifier.notify(any()) }
    }

    @Test
    fun `it processes reminders on softAlarm`() = scope.runTest {
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } returns listOf(
            reminder().copy(
                targetInstant = Instant.ofEpochMilli(UPCOMING_MILLIS + 15_000L),
                flags = "!",
            )
        )
        timeProvider.instant = Instant.ofEpochMilli(5_000)

        reminderService.run()
        testScheduler.runCurrent()
        alarmSignal.notifyAlarm(softAlarm)
        testScheduler.runCurrent()

        verify(exactly = 2) {
            wakeupScheduler.scheduleWakeup(softAlarm,
                Wakeup.inexact(Instant.ofEpochMilli(15_000), wakeDevice = false))
        }
    }

    @Test
    fun `it clears upcoming notification for foreground alarm`() = scope.runTest {
        val reminder = reminder().copy(
            targetInstant = Instant.ofEpochMilli(30_000),
            flags = "!",
        )
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } returns listOf(reminder)
        timeProvider.instant = Instant.ofEpochMilli(35_000)

        reminderService.run()
        testScheduler.runCurrent()

        verify { wakeupScheduler.clearWakeup(notifyAlarm) }
        verify { wakeupScheduler.clearWakeup(softAlarm) }
        verify { reminderNotifier.setUpcoming(null) }
        verify { reminderNotifier.notify(reminder) }
    }

    @Test
    fun `it clears upcoming notification for dismissed foreground alarm`() = scope.runTest {
        every { reminderStateRepository.dismissedReminders } returns flowOf(
            setOf(ReminderRecord(LocalDateTime.of(2020, 1, 2, 10, 20), (-5).minutes, "!"))
        )
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } returns listOf(
            reminder().copy(
                targetTime = LocalDateTime.of(2020, 1, 2, 10, 20),
                reminderOffset = (-5).minutes,
                targetInstant = Instant.ofEpochMilli(30_000),
                flags = "!",
            )
        )
        timeProvider.instant = Instant.ofEpochMilli(5_000)

        reminderService.run()
        testScheduler.runCurrent()

        verify { wakeupScheduler.clearWakeup(notifyAlarm) }
        verify { reminderNotifier.setUpcoming(null) }
        verify(exactly = 0) { reminderNotifier.notify(any()) }
    }

    @Test
    fun `it skips missed alarm with long offset`() = scope.runTest {
        coEvery { reminderNotifier.notifyMissed(any(), any()) } just runs
        val reminder = reminder().copy(
            targetInstant = dayafter.atTime(12, 0).atZone(timeProvider.timeZone()).toInstant(),
            reminderOffset = (-2).days,
        )
        coEvery { reminderParser.parsePageReminders(any(), dayafter, any()) } returns listOf(reminder)
        timeProvider.instant = today.atTime(13, 0).atZone(timeProvider.timeZone()).toInstant()

        reminderService.run()
        testScheduler.runCurrent()

        verify { reminderNotifier.notifyMissed(reminder, silent = true) }
        verify(exactly = 0) { reminderNotifier.notify(any()) }
    }

    @Test
    fun `it deletes dismiss records for deleted reminders`() = scope.runTest {
        val records = MutableStateFlow(
            setOf(
                ReminderRecord(LocalDateTime.of(2020, 1, 2, 10, 20), Duration.ZERO, ""),
                ReminderRecord(LocalDateTime.of(2020, 1, 2, 10, 20), (-5).minutes, ""),
                ReminderRecord(LocalDateTime.of(2020, 1, 3, 10, 20), Duration.ZERO, ""),
                ReminderRecord(LocalDateTime.of(2020, 1, 3, 10, 20), (-5).minutes, ""),
            )
        )
        every { reminderStateRepository.dismissedReminders } returns records
        coEvery { reminderStateRepository.forgetDismissedReminders(any()) } answers {
            records.update { it - firstArg<Set<ReminderRecord>>() }
        }
        val reminder = reminder().copy(
            targetTime = LocalDateTime.of(2020, 1, 2, 10, 20),
            reminderOffset = (-5).minutes,
        )
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } returns listOf(reminder)
        timeProvider.instant = Instant.ofEpochMilli(5_000)

        reminderService.run()
        testScheduler.runCurrent()

        assertEquals(setOf(reminder.toRecord()), records.value)
    }

    @Test
    fun `it does nothing when reminders preference is disabled`() = scope.runTest {
        prefRepository.preferencesFlow.update { it.copy(isEventRemindersEnabled = false) }

        reminderService.run()
        testScheduler.runCurrent()

        verify { upcomingPageWatcher wasNot called }
        verify(exactly = 0) { reminderNotifier.notify(any()) }
        verify(exactly = 0) { wakeupScheduler.scheduleWakeup(any(), any()) }
        verify { wakeupScheduler.clearWakeup(notifyAlarm) }
        assertEquals(ReminderState.IDLE, reminderService.reminderState.first())
        assertEquals(null, reminderService.nextReminder.first())
    }

    @Test
    fun `it stops work when reminder preference gets disabled`() = scope.runTest {
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } returns listOf(
            reminder().copy(
                targetInstant = Instant.ofEpochMilli(200_000),
                reminderOffset = (-2).minutes,
            )
        )
        timeProvider.instant = Instant.ofEpochMilli(100_000)

        reminderService.run()
        testScheduler.advanceTimeBy(4.seconds)
        clearMocks(reminderNotifier, wakeupScheduler, answers = false)
        prefRepository.preferencesFlow.update { it.copy(isEventRemindersEnabled = false) }
        testScheduler.runCurrent()

        verify(exactly = 0) { reminderNotifier.notify(any()) }
        verify { reminderNotifier.setUpcoming(null) }
        verify { wakeupScheduler.clearWakeup(notifyAlarm) }
        verify { wakeupScheduler.clearWakeup(softAlarm) }
        verify(exactly = 0) { wakeupScheduler.scheduleWakeup(any(), any()) }
        assertEquals(ReminderState.IDLE, reminderService.reminderState.first())
    }

    @Test
    fun `it starts work when reminder preference gets enabled`() = scope.runTest {
        prefRepository.preferencesFlow.update { it.copy(isEventRemindersEnabled = false) }
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } returns listOf(
            reminder().copy(
                targetInstant = Instant.ofEpochMilli(200_000),
                reminderOffset = (-2).minutes,
            )
        )
        timeProvider.instant = Instant.ofEpochMilli(5_000)

        reminderService.run()
        testScheduler.advanceTimeBy(4.seconds)
        clearMocks(reminderNotifier, wakeupScheduler, answers = false)
        prefRepository.preferencesFlow.update { it.copy(isEventRemindersEnabled = true) }
        testScheduler.runCurrent()

        verify(exactly = 0) { reminderNotifier.notify(any()) }
        verify { wakeupScheduler.scheduleWakeup(notifyAlarm, Wakeup.exact(Instant.ofEpochMilli(80_000))) }
        assertEquals(ReminderState.LOADED, reminderService.reminderState.first())
    }

    @Test
    fun `nextReminder publishes next reminder`() = scope.runTest {
        val reminder = reminder().copy(
            targetInstant = timeProvider.instant.plusSeconds(3600),
            reminderOffset = (-1).minutes,
        )
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } returns listOf(reminder)

        reminderService.run()
        testScheduler.runCurrent()

        assertEquals(reminder, reminderService.nextReminder.first())
    }

    @Test
    fun `nextReminder ignores silent reminder`() = scope.runTest {
        val reminder = reminder().copy(
            targetInstant = timeProvider.instant.plusSeconds(3600),
            reminderOffset = (-1).minutes,
        )
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } returns listOf(
            reminder().copy(
                targetInstant = timeProvider.instant.plusSeconds(1800),
                flags = "?",
            ),
            reminder,
        )

        reminderService.run()
        testScheduler.runCurrent()

        assertEquals(reminder, reminderService.nextReminder.first())
    }

    @Test
    fun `it clears nextReminder`() = scope.runTest {
        val reminder = reminder().copy(targetInstant = timeProvider.instant.minusSeconds(10))
        coEvery { reminderParser.parsePageReminders(any(), today, any()) } returns listOf(reminder)

        reminderService.run()
        testScheduler.runCurrent()

        assertEquals(null, reminderService.nextReminder.first())
    }

    @Test
    fun `it publishes reminderState`() = scope.runTest {
        assertEquals(ReminderState.INIT, reminderService.reminderState.first())
        reminderService.run()
        testScheduler.advanceTimeBy(2.seconds)
        assertEquals(ReminderState.LOADED, reminderService.reminderState.first())
        prefRepository.preferencesFlow.update { it.copy(isEventRemindersEnabled = false) }
        testScheduler.advanceTimeBy(2.seconds)
        assertEquals(ReminderState.IDLE, reminderService.reminderState.first())
        prefRepository.preferencesFlow.update { it.copy(isEventRemindersEnabled = true) }
        testScheduler.runCurrent()
        assertEquals(ReminderState.LOADED, reminderService.reminderState.first())
    }

}
