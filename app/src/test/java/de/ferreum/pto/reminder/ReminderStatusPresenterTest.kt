package de.ferreum.pto.reminder

import app.cash.turbine.test
import de.ferreum.pto.preferences.MinuteTicker
import de.ferreum.pto.reminder.AlarmServiceStateProvider.AlarmState
import de.ferreum.pto.reminder.ReminderStatusPresenter.ReminderStatus
import de.ferreum.pto.util.TimeProvider
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test
import java.time.Instant
import kotlin.time.Duration.Companion.hours

internal class ReminderStatusPresenterTest {

    private val scope = TestScope()
    private val reminderService: ReminderService = mockk()
    private val minuteTickerFlow = MutableSharedFlow<Unit>(replay = 1)
        .apply { tryEmit(Unit) }
    private val minuteTicker: MinuteTicker = mockk {
        every { ticker } returns minuteTickerFlow
    }
    private val timeProvider = object : TimeProvider {
        var currentInstant = Instant.ofEpochSecond(1000)
        override fun instant() = currentInstant
    }
    private val alarmServiceState = MutableStateFlow<AlarmState>(AlarmState.Stopped)
    private val alarmServiceStateProvider: AlarmServiceStateProvider = mockk {
        every { state } returns alarmServiceState
    }

    @Test
    fun `it emits None if no reminder present`() = scope.runTest {
        every { reminderService.nextReminder } returns flowOf(null)
        val presenter = createPresenter()

        val result = presenter.status.awaitCurrentItem()

        assertEquals(ReminderStatus.None, result)
    }

    @Test
    fun `it emits None if more than 12 hours in future`() = scope.runTest {
        every { reminderService.nextReminder } returns flowOf(
            reminder().copy(targetInstant = Instant.ofEpochSecond(12.hours.inWholeSeconds + 100))
        )
        val presenter = createPresenter()
        timeProvider.currentInstant = Instant.ofEpochSecond(10)

        val result = presenter.status.awaitCurrentItem()

        assertEquals(ReminderStatus.None, result)
    }

    @Test
    fun `it emits Upcoming if less than 12 hours in future`() = scope.runTest {
        val reminder = reminder().copy(
            targetInstant = Instant.ofEpochSecond(12.hours.inWholeSeconds)
        )
        every { reminderService.nextReminder } returns flowOf(reminder)
        val presenter = createPresenter()
        timeProvider.currentInstant = Instant.ofEpochSecond(10)

        val result = presenter.status.awaitCurrentItem()

        assertEquals(ReminderStatus.Upcoming(reminder), result)
    }

    @Test
    fun `it emits Upcoming if less than 12 hours in future with offset`() = scope.runTest {
        val reminder = reminder().copy(
            targetInstant = Instant.ofEpochSecond(18.hours.inWholeSeconds),
            reminderOffset = (-6).hours,
        )
        every { reminderService.nextReminder } returns flowOf(reminder)
        val presenter = createPresenter()
        timeProvider.currentInstant = Instant.ofEpochSecond(10)

        val result = presenter.status.awaitCurrentItem()

        assertEquals(ReminderStatus.Upcoming(reminder), result)
    }

    @Test
    fun `it emits foreground alarm`() = scope.runTest {
        every { reminderService.nextReminder } returns flowOf(reminder())
        val alarmState = AlarmState.Started(reminder(), false)
        alarmServiceState.value = alarmState
        val presenter = createPresenter()

        val result = presenter.status.awaitCurrentItem()

        assertEquals(ReminderStatus.OngoingForeground, result)
    }

    private fun createPresenter() = ReminderStatusPresenter(
        scope.backgroundScope,
        reminderService,
        minuteTicker,
        timeProvider,
        alarmServiceStateProvider,
    )

    private suspend fun Flow<ReminderStatus>.awaitCurrentItem(): ReminderStatus {
        lateinit var result: ReminderStatus
        test {
            scope.testScheduler.runCurrent()

            result = expectMostRecentItem()
            ensureAllEventsConsumed()
        }
        return result
    }
}
