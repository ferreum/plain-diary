package de.ferreum.pto.reminder

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import app.cash.turbine.test
import de.ferreum.pto.files.PageWriteSignal
import de.ferreum.pto.files.PageWriteSignal.PageWrite
import de.ferreum.pto.files.PageWriteSignal.PageWriteFlag
import de.ferreum.pto.preferences.FakePtoPreferencesRepository
import de.ferreum.pto.preferences.RecordingLogger
import de.ferreum.pto.reminder.UpcomingPageWatcher.Companion.daychangeAlarm
import de.ferreum.pto.reminder.UpcomingPageWatcher.PageUpdate
import de.ferreum.pto.util.TimeProvider
import io.mockk.coEvery
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import io.mockk.verify
import io.mockk.verifySequence
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

@ExperimentalCoroutinesApi
class UpcomingPageWatcherTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val scope = TestScope()
    private var pageContent = mapOf(
        LocalDate.of(2020, 1, 2) to "yesterday",
        LocalDate.of(2020, 1, 3) to "today",
        LocalDate.of(2020, 1, 4) to "tomorrow",
        LocalDate.of(2020, 1, 5) to "day after",
    )
    private val preferencesRepository = FakePtoPreferencesRepository().apply {
        preferencesFlow.update {
            it.copy(
                newpageTime = LocalTime.of(4, 0),
                isEventRemindersEnabled = true,
            )
        }
    }
    private val pageWriteSignal = PageWriteSignal()
    private val wakeupScheduler: WakeupScheduler = mockk {
        every { scheduleWakeup(any(), any()) } just runs
        every { clearWakeup(any()) } just runs
    }
    private val alarmSignal = AlarmSignal()
    private val timeProvider = object : TimeProvider {
        var localDateTime = LocalDateTime.of(2020, 1, 3, 12, 0)
        var timeZone = ZoneId.of("Europe/Berlin")
        override fun localDateTime(): LocalDateTime = localDateTime
        override fun timeZone(): ZoneId = timeZone
    }
    private val logger = RecordingLogger()

    private lateinit var pageWatcher: UpcomingPageWatcher

    private val lifecycleOwner = object : LifecycleOwner {
        override val lifecycle: Lifecycle
            get() = this@UpcomingPageWatcherTest.lifecycle
    }
    private val lifecycle: LifecycleRegistry = LifecycleRegistry(lifecycleOwner)

    @Before
    fun setUp() {
        Dispatchers.setMain(StandardTestDispatcher(scope.testScheduler))
    }

    @After
    fun tearDown() {
        logger.throwIfAny()
        Dispatchers.resetMain()
    }

    private fun createWatcher() {
        pageWatcher = UpcomingPageWatcher(
            scope.backgroundScope,
            { pageContent.getValue(it) },
            preferencesRepository,
            pageWriteSignal,
            wakeupScheduler,
            alarmSignal,
            timeProvider,
            logger,
            lifecycleOwner,
        )
    }

    @Test
    fun `it emits current and next two days on start`() = scope.runTest {
        preferencesRepository.preferencesFlow.update { it.copy(newpageTime = LocalTime.of(4, 0)) }
        timeProvider.localDateTime = LocalDateTime.of(2020, 1, 3, 10, 0)

        createWatcher()
        val results = pageWatcher.observeContent().take(3).toList()

        assertEquals(
            mapOf(
                LocalDate.of(2020, 1, 3) to "today",
                LocalDate.of(2020, 1, 4) to "tomorrow",
                LocalDate.of(2020, 1, 5) to "day after",
            ),
            results.associate { it.date to it.content }
        )
    }

    @Test
    fun `it emits current and next day on start while other flow is active`() = scope.runTest {
        preferencesRepository.preferencesFlow.update { it.copy(newpageTime = LocalTime.of(4, 0)) }
        timeProvider.localDateTime = LocalDateTime.of(2020, 1, 3, 10, 0)

        createWatcher()
        val job = launch { pageWatcher.observeContent().collect() }
        testScheduler.runCurrent()
        val results = pageWatcher.observeContent().take(3).toList()

        assertEquals(
            mapOf(
                LocalDate.of(2020, 1, 3) to "today",
                LocalDate.of(2020, 1, 4) to "tomorrow",
                LocalDate.of(2020, 1, 5) to "day after",
            ),
            results.associate { it.date to it.content }
        )
        job.cancel()
    }

    @Test
    fun `it emits previous and two days before newpageTime`() = scope.runTest {
        preferencesRepository.preferencesFlow.update { it.copy(newpageTime = LocalTime.of(4, 0)) }
        timeProvider.localDateTime = LocalDateTime.of(2020, 1, 3, 3, 0)

        createWatcher()
        val results = pageWatcher.observeContent().take(3).toList()

        assertEquals(
            mapOf(
                LocalDate.of(2020, 1, 2) to "yesterday",
                LocalDate.of(2020, 1, 3) to "today",
                LocalDate.of(2020, 1, 4) to "tomorrow",
            ),
            results.associate { it.date to it.content }
        )
    }

    @Test
    fun `it emits PageWrite after initial update`() = scope.runTest {
        createWatcher()
        pageWatcher.observeContent().test {
            assertEquals(
                mapOf(
                    LocalDate.of(2020, 1, 3) to "today",
                    LocalDate.of(2020, 1, 4) to "tomorrow",
                    LocalDate.of(2020, 1, 5) to "day after",
                ),
                listOf(awaitItem(), awaitItem(), awaitItem()).associate { it.date to it.content }
            )

            testScheduler.advanceTimeBy(200.milliseconds)
            pageWriteSignal.notifyPageWrite(PageWrite(LocalDate.of(2020, 1, 3), "new text"))

            testScheduler.runCurrent()
            assertEquals(
                mapOf(LocalDate.of(2020, 1, 3) to "new text"),
                listOf(awaitItem()).associate { it.date to it.content }
            )
        }
    }

    @Test
    fun `it emits updated page on WholePage update`() = scope.runTest {
        createWatcher()
        val result = async { pageWatcher.observeContent().drop(3).first() }
        testScheduler.advanceTimeBy(2.seconds)
        pageWriteSignal.notifyPageWrite(PageWrite(LocalDate.of(2020, 1, 3), "new text"))

        assertEquals(PageUpdate(LocalDate.of(2020, 1, 3), "new text"), result.await())
    }

    @Test
    fun `it schedules daychange wakeup`() = scope.runTest {
        preferencesRepository.preferencesFlow.update { it.copy(newpageTime = LocalTime.of(6, 0)) }
        timeProvider.localDateTime = LocalDateTime.of(2020, 1, 2, 10, 20)
        timeProvider.timeZone = ZoneId.of("Pacific/Easter")

        createWatcher()
        val job = launch { pageWatcher.observeContent().collect() }
        testScheduler.advanceTimeBy(10.seconds)

        verify {
            wakeupScheduler.scheduleWakeup(
                daychangeAlarm,
                WakeupScheduler.Wakeup(
                    LocalDateTime.of(2020, 1, 3, 10, 0)
                        .atZone(timeProvider.timeZone)
                        .toInstant(),
                    16.hours
                ),
            )
        }
        verify(exactly = 0) { wakeupScheduler.clearWakeup(any()) }
        job.cancel()
    }

    @Test
    fun `it keeps wakeup while any flow is active`() = scope.runTest {
        preferencesRepository.preferencesFlow.update { it.copy(newpageTime = LocalTime.of(6, 0)) }
        timeProvider.localDateTime = LocalDateTime.of(2020, 1, 2, 10, 20)
        timeProvider.timeZone = ZoneId.of("Pacific/Easter")

        createWatcher()
        val job1 = launch { pageWatcher.observeContent().collect() }
        val job2 = launch { pageWatcher.observeContent().collect() }
        testScheduler.runCurrent()
        job2.cancel()
        testScheduler.advanceTimeBy(10.seconds)

        verify(exactly = 1) { wakeupScheduler.scheduleWakeup(daychangeAlarm, any()) }
        verify(exactly = 0) { wakeupScheduler.clearWakeup(any()) }
        job1.cancel()
    }

    @Test
    fun `it clears daychange wakeup on startup if reminders disabled`() = scope.runTest {
        preferencesRepository.preferencesFlow.update { it.copy(isEventRemindersEnabled = false) }

        createWatcher()
        testScheduler.runCurrent()

        verify(exactly = 1) { wakeupScheduler.clearWakeup(daychangeAlarm) }
        confirmVerified(wakeupScheduler)
    }

    @Test
    fun `it clears daychange wakeup when reminders are disabled`() = scope.runTest {
        createWatcher()
        testScheduler.runCurrent()
        preferencesRepository.preferencesFlow.update { it.copy(isEventRemindersEnabled = false) }
        testScheduler.advanceTimeBy(2.seconds)

        verifySequence {
            wakeupScheduler.scheduleWakeup(daychangeAlarm, any())
            wakeupScheduler.clearWakeup(daychangeAlarm)
        }
        confirmVerified(wakeupScheduler)
    }

    @Test
    fun `it schedules daychange wakeup on daychange alarm`() = scope.runTest {
        preferencesRepository.preferencesFlow.update { it.copy(newpageTime = LocalTime.of(6, 0)) }
        timeProvider.localDateTime = LocalDateTime.of(2020, 1, 2, 10, 20)
        timeProvider.timeZone = ZoneId.of("Pacific/Easter")

        createWatcher()
        backgroundScope.launch { pageWatcher.observeContent().collect() }
        testScheduler.runCurrent()
        timeProvider.localDateTime = LocalDateTime.of(2020, 1, 3, 10, 20)
        alarmSignal.notifyAlarm(daychangeAlarm)
        testScheduler.runCurrent()

        verifySequence {
            wakeupScheduler.scheduleWakeup(
                daychangeAlarm,
                WakeupScheduler.Wakeup(
                    LocalDateTime.of(2020, 1, 3, 10, 0)
                        .atZone(timeProvider.timeZone)
                        .toInstant(),
                    16.hours
                )
            )
            wakeupScheduler.scheduleWakeup(
                daychangeAlarm,
                WakeupScheduler.Wakeup(
                    LocalDateTime.of(2020, 1, 4, 10, 0)
                        .atZone(timeProvider.timeZone)
                        .toInstant(),
                    16.hours
                )
            )
        }
    }

    @Test
    fun `it emits current day content on daychange alarm`() = scope.runTest {
        createWatcher()
        val results = async { pageWatcher.observeContent().drop(3).take(3).toList() }
        testScheduler.advanceTimeBy(2.seconds)
        timeProvider.localDateTime = LocalDateTime.of(2020, 1, 4, 10, 20)
        pageContent += LocalDate.of(2020, 1, 4) to "new day"
        pageContent += LocalDate.of(2020, 1, 5) to "new next day"
        pageContent += LocalDate.of(2020, 1, 6) to "new day after"

        alarmSignal.notifyAlarm(daychangeAlarm)

        assertEquals(
            mapOf(
                LocalDate.of(2020, 1, 4) to "new day",
                LocalDate.of(2020, 1, 5) to "new next day",
                LocalDate.of(2020, 1, 6) to "new day after",
            ),
            results.await().associate { it.date to it.content }
        )
    }

    @Test
    fun `triggerCurrentDayCheck updates on new day`() = scope.runTest {
        createWatcher()
        val results = async { pageWatcher.observeContent().drop(3).take(3).toList() }
        testScheduler.advanceTimeBy(2.seconds)
        timeProvider.localDateTime = LocalDateTime.of(2020, 1, 4, 10, 20)
        pageContent += LocalDate.of(2020, 1, 4) to "new day"
        pageContent += LocalDate.of(2020, 1, 5) to "new next day"
        pageContent += LocalDate.of(2020, 1, 6) to "new day after"

        pageWatcher.triggerCurrentDayCheck()

        assertEquals(
            mapOf(
                LocalDate.of(2020, 1, 4) to "new day",
                LocalDate.of(2020, 1, 5) to "new next day",
                LocalDate.of(2020, 1, 6) to "new day after",
            ),
            results.await().associate { it.date to it.content }
        )
    }

    @Test
    fun `triggerCurrentDayCheck does nothing on same day`() = scope.runTest {
        createWatcher()
        pageWatcher.observeContent().test {
            testScheduler.advanceTimeBy(2.seconds)
            awaitItem()
            awaitItem()
            awaitItem()

            pageWatcher.triggerCurrentDayCheck()
            testScheduler.runCurrent()

            expectNoEvents()
        }
    }

    @Test
    fun `processLifecycle resume updates on new day`() = scope.runTest {
        createWatcher()
        val results = async { pageWatcher.observeContent().drop(3).take(3).toList() }
        testScheduler.advanceTimeBy(2.seconds)
        timeProvider.localDateTime = LocalDateTime.of(2020, 1, 4, 10, 20)
        pageContent += LocalDate.of(2020, 1, 4) to "new day"
        pageContent += LocalDate.of(2020, 1, 5) to "new next day"
        pageContent += LocalDate.of(2020, 1, 6) to "new day after"

        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)

        assertEquals(
            mapOf(
                LocalDate.of(2020, 1, 4) to "new day",
                LocalDate.of(2020, 1, 5) to "new next day",
                LocalDate.of(2020, 1, 6) to "new day after",
            ),
            results.await().associate { it.date to it.content }
        )
    }

    @Test
    fun `it ignores write with filtered flag`() = scope.runTest {
        createWatcher()
        val results = async {
            pageWatcher.observeContent(setOf(PageWriteFlag.NO_EVENTS))
                .drop(3).take(2).toList()
        }
        testScheduler.advanceTimeBy(2.seconds)

        pageWriteSignal.notifyPageWrite(PageWrite(LocalDate.of(2020, 1, 3), "content", setOf(PageWriteFlag.NO_EVENTS)))
        testScheduler.advanceTimeBy(2.seconds)
        pageWriteSignal.notifyPageWrite(PageWrite(LocalDate.of(2020, 1, 4), "content"))
        testScheduler.advanceTimeBy(2.seconds)
        pageWriteSignal.notifyPageWrite(PageWrite(LocalDate.of(2020, 1, 4), "content"))
        testScheduler.advanceTimeBy(2.seconds)

        assertEquals(
            listOf(LocalDate.of(2020, 1, 4), LocalDate.of(2020, 1, 4)),
            results.await().map { it.date }
        )
    }

    @Test
    fun `it restarts on internal exception`() = scope.runTest {
        coEvery { wakeupScheduler.scheduleWakeup(any(), any()) } throws IllegalStateException("testing")
        timeProvider.localDateTime = LocalDateTime.of(2020, 1, 3, 10, 0)

        createWatcher()
        val deferred = async { pageWatcher.observeContent().drop(3).first() }
        testScheduler.advanceTimeBy(2.seconds)
        coEvery { wakeupScheduler.scheduleWakeup(any(), any()) } just runs
        testScheduler.advanceTimeBy(31.seconds)
        pageWriteSignal.notifyPageWrite(PageWrite(LocalDate.of(2020, 1, 3), "content"))
        val result = deferred.await()

        assertEquals(IllegalStateException::class, logger.exceptions.single()::class)
        assertEquals(LocalDate.of(2020, 1, 3) to "content", result.date to result.content)
        logger.clear()
    }
}
