
package de.ferreum.pto.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import de.ferreum.pto.preferences.PtoFileHelper
import de.ferreum.pto.search.SearchToken.RegexToken
import io.mockk.mockk
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.takeWhile
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runCurrent
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.io.File
import java.time.DayOfWeek
import java.time.LocalDate
import java.util.Locale
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.time.Duration

@ExperimentalCoroutinesApi
class SearchViewModelTest {

    @get:Rule
    val temporaryFolder = TemporaryFolder()

    private val viewModelStore = ViewModelStore()

    private val mainDispatcher = UnconfinedTestDispatcher()

    private lateinit var fileHelper: PtoFileHelper

    private lateinit var vm: SearchViewModel

    @Before
    fun setUp() {
        Dispatchers.setMain(mainDispatcher)
        fileHelper = PtoFileHelper(temporaryFolder.root)
    }

    @After
    fun mainTearDown() {
        Dispatchers.resetMain()
    }

    private fun runTest(block: suspend TestScope.() -> Unit) {
        runTest(mainDispatcher, timeout = Duration.INFINITE) {
            vm = ViewModelProvider(viewModelStore, object : ViewModelProvider.Factory {
                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return SearchViewModel(
                        EmptyCoroutineContext,
                        EmptyCoroutineContext,
                        CompletableDeferred(fileHelper),
                        mockk(relaxUnitFun = true),
                    ) as T
                }
            })[SearchViewModel::class.java]

            block()

            viewModelStore.clear()
        }
    }


    @Test
    fun `search finds single file`() = runTest {
        createFile("2020/02/20.txt").writeText("test")

        vm.setSearchTokens(listOf(searchToken("test")))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(listOf(FileSearchResult(LocalDate.of(2020, 2, 20), "test", listOf(SearchMatch(0, 0, 4)))), list)
    }

    @Test
    fun `search finds many files`() = runTest {
        val expected = dateRange(LocalDate.of(2019, 2, 20), LocalDate.of(2020, 2, 20)).map {
            createFile(it).writeText("test")
            FileSearchResult(it, "test", listOf(SearchMatch(0, 0, 4)))
        }.toList()

        vm.setSearchTokens(listOf(searchToken("test")))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(366, list.size)
        assertEquals(expected.reversed(), list)
    }

    @Test
    fun `it updates search when query changes`() = runTest {
        dateRange().writeWeekdays()
        val expected1 = dateRange().filter { it.dayOfWeek == DayOfWeek.MONDAY }
            .map { FileSearchResult(it, DayOfWeek.MONDAY.toString(), listOf(SearchMatch(0, 0, 6))) }
            .toList()
        val expected2 = dateRange().filter { it.dayOfWeek == DayOfWeek.SATURDAY }
            .map { FileSearchResult(it, DayOfWeek.SATURDAY.toString(), listOf(SearchMatch(0, 0, 8))) }
            .toList()
        assertEquals(4, expected1.size)
        assertEquals(5, expected2.size)

        val channel = Channel<Flow<SearchResultEvent>>()
        val job = launch {
            vm.searchResults.collect { channel.send(it) }
            channel.close()
        }

        vm.setSearchTokens(listOf(searchToken("MONDAY")))
        assertEquals(expected1.reversed(), channel.receive().toResultsList())

        vm.setSearchTokens(listOf(searchToken("SATURDAY")))
        assertEquals(expected2.reversed(), channel.receive().toResultsList())

        job.cancel()
    }

    @Test
    fun `it groups fast results`() = runTest {
        dateRange().writeWeekdays()
        val expected = dateRange().filter { it.dayOfWeek == DayOfWeek.MONDAY }
            .map { FileSearchResult(it, DayOfWeek.MONDAY.toString(), listOf(SearchMatch(0, 0, 6))) }
            .toList()

        vm.fileReadThrottleMillis = 0
        vm.searchParallelism = 0
        vm.setSearchTokens(listOf(searchToken("MONDAY")))
        val list = vm.searchResults
            .map { it.toUnflattenedResultsList() }
            .first { it.isNotEmpty() }

        assertEquals(listOf(expected.reversed()), list)
    }

    @Test
    fun `it emits slow results without grouping`() = runTest {
        dateRange().writeWeekdays()
        val expected = dateRange().filter { it.dayOfWeek == DayOfWeek.MONDAY }.map {
            listOf(FileSearchResult(it, DayOfWeek.MONDAY.toString(), listOf(SearchMatch(0, 0, 6))))
        }.toList()

        vm.fileReadThrottleMillis = 100
        vm.searchParallelism = 0
        vm.setSearchTokens(listOf(searchToken("MONDAY")))
        val list = vm.searchResults
            .map { it.toUnflattenedResultsList() }
            .first { it.isNotEmpty() }

        assertEquals(expected.reversed(), list)
    }

    @Test
    fun `it finds substring matches`() = runTest {
        dateRange().writeWeekdays()
        val expected = dateRange().transform {
            val text = it.dayOfWeek.toString()
            val offset = text.indexOf("DAY")
            emit(FileSearchResult(it, text, listOf(SearchMatch(offset, offset, 3))))
        }.toList()

        vm.setSearchTokens(listOf(searchToken("DAY")))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(expected.reversed(), list)
    }

    @Test
    fun `it skips overlapping matches`() = runTest {
        val date = RANDOM_DATE
        createFile(date).writeText("aaaaaaaabaaaa")

        vm.setSearchTokens(listOf(searchToken("aaa")))
        val list = vm.searchResults.firstNonEmptyToList()

        verifyResults(list) {
            val result = list.single()
            assertEquals("aaaaaaaabaaaa", result.text)
            assertEquals(listOf(
                SearchMatch(0, 0, 6),
                SearchMatch(9, 9, 3),
            ), result.matches)
        }
    }

    @Test
    fun `it adds context string`() = runTest {
        val date = RANDOM_DATE
        createFile(date).writeText(LOREM_IPSUM)

        vm.setSearchTokens(listOf(searchToken("veniam")))
        val list = vm.searchResults.firstNonEmptyToList()

        verifyResults(list) {
            val result = list.single()
            val match = result.matches.single()
            assertEquals(result.date, date)
            result.text.assertSubstringAt(match.textOffset, "veniam")
            LOREM_IPSUM.assertSubstringAt(match.fileOffset, "veniam")
        }
    }

    @Test
    fun `it provides text up to start of line`() = runTest {
        val date = RANDOM_DATE
        createFile(date).writeText(LOREM_IPSUM)

        vm.setSearchTokens(listOf(searchToken("aute")))
        val list = vm.searchResults.firstNonEmptyToList()

        verifyResults(list) {
            val result = list.single()
            val match = result.matches.single()
            assertEquals(result.date, date)
            result.text.assertSubstringAt(match.textOffset, "aute")
            LOREM_IPSUM.assertSubstringAt(match.fileOffset, "aute")
        }
    }

    @Test
    fun `it provides text up to end of line`() = runTest {
        val date = RANDOM_DATE
        createFile(date).writeText(LOREM_IPSUM)

        vm.setSearchTokens(listOf(searchToken("magna")))
        val list = vm.searchResults.firstNonEmptyToList()

        verifyResults(list) {
            val result = list.single()
            val match = result.matches.single()
            assertEquals(result.date, date)
            result.text.assertSubstringAt(match.textOffset, "magna")
            LOREM_IPSUM.assertSubstringAt(match.fileOffset, "magna")
        }
    }

    @Test
    fun `it only matches files containing all query tokens`() = runTest {
        val date = LocalDate.of(2020, 1, 20)
        dateRange().writeWeekdays()
        getFile(date).appendText(" this one")

        vm.setSearchTokens(listOf(searchToken("this"), searchToken("day")))
        val list = vm.searchResults.firstNonEmptyToList()

        val fileText = "MONDAY this one"
        verifyResults(list) {
            val result = list.single()
            val matches = result.matches
            assertEquals(result.date, date)
            assertEquals(2, matches.size)
            assertEquals(fileText, result.text)
            result.text.assertSubstringAt(matches[0].textOffset, "DAY")
            fileText.assertSubstringAt(matches[0].fileOffset, "DAY")
            result.text.assertSubstringAt(matches[1].textOffset, "this")
            fileText.assertSubstringAt(matches[1].fileOffset, "this")
        }
    }

    @Test
    fun `it preserves results when resuming after search done`() = runTest {
        // this test doesn't seem to work how I need it to--it doesn't expose the duplication
        // through re-subscribe after replay behavior observed in the app after resume
        dateRange().writeWeekdays()

        vm.setSearchTokens(listOf(searchToken("DAY")))
        val list1 = vm.searchResults.firstNonEmptyToList()
        getFile(dateRange().first()).writeText("")
        runCurrent()
        val list2 = vm.searchResults.first().toResultsList()

        assertEquals(list1, list2)
    }

    @Test
    fun `it matches newline`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        createFile(date).writeText("text with\nnewline")

        vm.setSearchTokens(listOf(RegexToken(Regex("""\n"""))))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(listOf(FileSearchResult(date, "text with", listOf(SearchMatch(9, 9, 1)))), list)
    }

    @Test
    fun `it only adds newline after other lines`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        createFile(date).writeText("text with\n\nnewlines")

        vm.setSearchTokens(listOf(RegexToken(Regex("""lines"""))))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(listOf(FileSearchResult(date, "newlines", listOf(SearchMatch(14, 3, 5)))), list)
    }

    @Test
    fun `it matches newline at end of file`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        createFile(date).writeText("${"z".repeat(22)}aaaabbbbccccddddeeee\n")

        vm.setSearchTokens(listOf(RegexToken(Regex("""\n"""))))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(listOf(FileSearchResult(date, "…bbbbccccddddeeee", listOf(SearchMatch(42, 17, 1)))), list)
    }

    @Test
    fun `it matches end of file`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        createFile(date).writeText("text")

        vm.setSearchTokens(listOf(RegexToken(Regex("""$"""))))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(listOf(FileSearchResult(date, "text", listOf(SearchMatch(4, 4, 0)))), list)
    }

    @Test
    fun `it matches text starting with newline`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        createFile(date).writeText("text with\nnewline")

        vm.setSearchTokens(listOf(RegexToken(Regex("""\nnewline"""))))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(listOf(FileSearchResult(date, "text with\nnewline", listOf(SearchMatch(9, 9, 8)))), list)
    }

    @Test
    fun `it matches consecutive newlines`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val zmargin = "z".repeat(22)
        createFile(date).writeText("${zmargin}${"a".repeat(18)}\n\n${zmargin}${"b".repeat(18)}\n\n")

        vm.setSearchTokens(listOf(RegexToken(Regex("""\n"""))))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(listOf(
            FileSearchResult(date, "…${"a".repeat(16)}\n\n…${"b".repeat(16)}\n", listOf(
                SearchMatch(40, 17, 2),
                SearchMatch(82, 36, 2),
            ))
        ), list)
    }

    @Test
    fun `it adds newline when omitting newline between matches`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        createFile(date).writeText("aax\nbbx\nccc\nddx\neee")

        vm.setSearchTokens(listOf(RegexToken(Regex("""x"""))))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(listOf(
            FileSearchResult(date, "aax\nbbx\nddx", listOf(
                SearchMatch(2, 2, 1),
                SearchMatch(6, 6, 1),
                SearchMatch(14, 10, 1),
            ))
        ), list)
    }

    @Test
    fun `it adds ellipsis for truncated lines`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val zmargin = "z".repeat(22)
        createFile(date).writeText("${zmargin}${"a".repeat(18)}x${"b".repeat(18)}${zmargin}\n${zmargin}${"c".repeat(18)}x${"d".repeat(18)}${zmargin}")

        vm.setSearchTokens(listOf(RegexToken(Regex("""x"""))))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(listOf(
            FileSearchResult(date, "…${"a".repeat(16)}x${"b".repeat(16)}…\n…${"c".repeat(16)}x${"d".repeat(16)}…", listOf(
                SearchMatch(40, 17, 1),
                SearchMatch(122, 53, 1),
            ))
        ), list)
    }

    @Test
    fun `it extends to close line start`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val zmargin = "z".repeat(22)
        createFile(date).writeText("wwww${"a".repeat(18)}x${"b".repeat(18)}${zmargin}\nwwww${"c".repeat(18)}x${"d".repeat(18)}${zmargin}")

        vm.setSearchTokens(listOf(RegexToken(Regex("""x"""))))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(listOf(
            FileSearchResult(date, "wwww${"a".repeat(18)}x${"b".repeat(16)}…\nwwww${"c".repeat(18)}x${"d".repeat(16)}…", listOf(
                SearchMatch(22, 22, 1),
                SearchMatch(86, 63, 1),
            ))
        ), list)
    }

    @Test
    fun `it extends to close line end`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        val zmargin = "z".repeat(22)
        createFile(date).writeText("${zmargin}${"a".repeat(18)}x${"b".repeat(18)}wwww\n${zmargin}${"c".repeat(18)}x${"d".repeat(18)}wwww")

        vm.setSearchTokens(listOf(RegexToken(Regex("""x"""))))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(listOf(
            FileSearchResult(date, "…${"a".repeat(16)}x${"b".repeat(18)}wwww\n…${"c".repeat(16)}x${"d".repeat(18)}wwww", listOf(
                SearchMatch(40, 17, 1),
                SearchMatch(104, 58, 1),
            ))
        ), list)
    }

    @Test
    fun `it matches text including newline`() = runTest {
        val date = LocalDate.of(2020, 2, 20)
        createFile(date).writeText("text with\nnewline")

        vm.setSearchTokens(listOf(RegexToken(Regex("""with\nnewline"""))))
        val list = vm.searchResults.firstNonEmptyToList()

        assertEquals(listOf(FileSearchResult(date, "text with\nnewline", listOf(SearchMatch(5, 5, 12)))), list)
    }

    private suspend fun Flow<LocalDate>.writeWeekdays() {
        collect {
            createFile(it).writeText(it.dayOfWeek.toString())
        }
    }

    private fun dateRange() = dateRange(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 2, 2))

    private fun dateRange(firstDate: LocalDate, lastDate: LocalDate) = flow {
        var date = firstDate
        while (date <= lastDate) {
            emit(date)
            date = date.plusDays(1)
        }
    }

    private fun createFile(date: LocalDate): File {
        return createFile(getFile(date))
    }

    private fun createFile(path: String): File {
        return createFile(File(fileHelper.pagesDir, path))
    }

    private fun createFile(file: File): File {
        file.parentFile!!.mkdirs()
        file.createNewFile() || throw Exception("createNewFile failed")
        return file
    }

    private fun getFile(date: LocalDate) = File(fileHelper.pagesDir,
        "%d/%02d/%02d.txt".format(Locale.ROOT, date.year, date.monthValue, date.dayOfMonth))

    private suspend fun Flow<Flow<SearchResultEvent>>.firstNonEmptyToList() =
        map { it.toResultsList() }.first { it.isNotEmpty() }

    private suspend fun Flow<SearchResultEvent>.toResultsList() =
        toUnflattenedResultsList().flatten()

    private suspend fun Flow<SearchResultEvent>.toUnflattenedResultsList(): List<List<FileSearchResult>> {
        return takeWhile { it is SearchResults }
            .map { (it as SearchResults).results }
            .toList()
    }

    private fun searchToken(text: String) = SearchToken.PlainToken(text)

    private fun verifyResults(results: List<FileSearchResult>, block: (List<FileSearchResult>) -> Unit) {
        try {
            block(results)
        } catch (e: AssertionError) {
            throw Exception("Assertion failed during verifyResults. Actual: $results", e)
        } catch (e: Throwable) {
            throw Exception("Error during verifyResults. Actual: $results", e)
        }
    }

    companion object {
        @Suppress("SpellCheckingInspection")
        private const val LOREM_IPSUM = ("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n"
            + "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n"
            + "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n"
            + "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n")

        /** generated by fair dice roll */
        private val RANDOM_DATE = LocalDate.of(2020, 2, 20)
    }

}

private fun CharSequence.assertSubstringAt(offset: Int, string: String) {
    assertTrue("offset $offset is not in range $indices", offset in indices)
    val endIndex = offset + string.length
    assertTrue("offset end $endIndex is not in range $indices", endIndex in indices)
    assertEquals(string, substring(offset, endIndex))
}
