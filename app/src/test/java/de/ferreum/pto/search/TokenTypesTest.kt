package de.ferreum.pto.search

import de.ferreum.pto.R
import org.junit.Assert.assertEquals
import org.junit.Test

class TokenTypesTest {

    @Test
    fun `wordBoundToken matches word`() {
        val token = WordBoundTokenCreator("aaa", 1) as SearchToken.RegexToken
        val matches = token.regex.findAll("aaa baaab aaa aaab baaa aaa").map { it.range }.toList()

        assertEquals(listOf(0..2, 10..12, 24..26), matches)
    }

    @Test
    fun `wordBoundToken matches special with word`() {
        val token = WordBoundTokenCreator("aaa!", 1) as SearchToken.RegexToken
        val matches = token.regex.findAll("aaa! aaa !; !aaa aaa.! aaa !").map { it.range }.toList()

        assertEquals(listOf(0..3, 5..9, 23..27), matches)
    }

    @Test
    fun `wordBoundToken drops starting and ending blanks and matches any number of blank chars`() {
        val token = WordBoundTokenCreator(" aa bb ", 1) as SearchToken.RegexToken
        val matches = token.regex.findAll("  aa  bb  aa   bb aabb aa ; bb;aabb aa  \tbb  ").map { it.range }.toList()

        assertEquals(token.regex.pattern, listOf(2..7, 10..16, 36..42), matches)
    }

    @Test
    fun `wordBoundToken errors when input is blank`() {
        try {
            WordBoundTokenCreator("  \t \n  ", 1)
        } catch (e: TextMessageException) {
            assertEquals(R.string.search_token_error_blank_wordbound_token, e.messageRes)
        }
    }
}
