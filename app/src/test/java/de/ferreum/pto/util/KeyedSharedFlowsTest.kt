package de.ferreum.pto.util

import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test

class KeyedSharedFlowsTest {

    @Test
    fun `it creates new flow`() = runTest {
        val flows = KeyedSharedFlows { n: Int ->
            flowOf(n * 4)
        }

        val result = flows[3].toList()

        assertEquals(listOf(12), result)
    }

    @Test
    fun `it shares flow with same key`() = runTest {
        var creations = emptyList<Int>()
        val flows = KeyedSharedFlows { n: Int ->
            creations = creations + n
            flow {
                delay(100)
                emit(n * 4)
            }
        }

        val result1 = async { flows[3].toList() }
        val result2 = async { flows[3].toList() }

        assertEquals(listOf(12), result1.await())
        assertEquals(listOf(12), result2.await())
        assertEquals(listOf(3), creations)
    }

    @Test
    fun `it shares flow for multiple collectors`() = runTest {
        var creations = emptyList<Int>()
        val flows = KeyedSharedFlows { n: Int ->
            creations = creations + n
            flow {
                delay(100)
                emit(n * 4)
            }
        }

        val flow = flows[3]
        val result1 = async { flow.toList() }
        val result2 = async { flow.toList() }

        assertEquals(listOf(12), result1.await())
        assertEquals(listOf(12), result2.await())
        assertEquals(listOf(3), creations)
    }

    @Test
    fun `it creates flows with separate keys`() = runTest {
        var creations = emptyList<Int>()
        val flows = KeyedSharedFlows { n: Int ->
            creations = creations + n
            flowOf(n * 4)
        }

        val result1 = async { flows[3].toList() }
        val result2 = async { flows[4].toList() }

        assertEquals(listOf(12), result1.await())
        assertEquals(listOf(16), result2.await())
        assertEquals(listOf(3, 4), creations)
    }

    @Test
    fun `it creates new flow after discard`() = runTest {
        var creations = emptyList<Int>()
        val flows = KeyedSharedFlows { n: Int ->
            creations = creations + n
            flowOf(n * 4)
        }

        val result1 = flows[3].toList()
        val result2 = flows[3].toList()

        assertEquals(listOf(12), result1)
        assertEquals(listOf(12), result2)
        assertEquals(listOf(3, 3), creations)
    }

    @Test
    fun `it recreates flow for multiple collect calls`() = runTest {
        var creations = emptyList<Int>()
        val flows = KeyedSharedFlows { n: Int ->
            creations = creations + n
            flowOf(n * 4)
        }

        val flow = flows[3]
        val result1 = flow.toList()
        val result2 = flow.toList()

        assertEquals(listOf(12), result1)
        assertEquals(listOf(12), result2)
        assertEquals(listOf(3, 3), creations)
    }

}
