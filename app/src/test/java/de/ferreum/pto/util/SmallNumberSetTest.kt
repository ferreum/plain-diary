package de.ferreum.pto.util

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class SmallNumberSetTest {

    @Test
    fun `contains works for empty`() {
        for (i in -5..65) {
            assertEquals("i=$i", false, i in SmallNumberSet.empty())
        }
    }

    @Test
    fun `contains works for all`() {
        for (i in -5..65) {
            assertEquals("i=$i", i in 0..63, i in SmallNumberSet.all())
        }
    }

    @Test
    fun `plus adds element`() {
        for (i in 0..63) {
            val result = SmallNumberSet.empty() + i

            for (j in 0..63) {
                assertEquals("i=$i j=$j", j == i, j in result)
            }
        }
    }

    @Test
    fun `plus adds second element`() {
        for (i in 0..63) {
            val outer = SmallNumberSet.empty() + i

            for (j in 0..63) {
                val result = outer + j

                for (k in -5..65) {
                    assertEquals("i=$i j=$j", k == i || k == j, k in result)
                }
            }
        }
    }

    @Test
    fun `plus throws if out of range`() {
        assertEquals(
            IllegalArgumentException::class,
            kotlin.runCatching { SmallNumberSet.empty() + 64 }.exceptionOrNull()!!::class
        )
        assertEquals(
            IllegalArgumentException::class,
            kotlin.runCatching { SmallNumberSet.empty() + 65 }.exceptionOrNull()!!::class
        )
        assertEquals(
            IllegalArgumentException::class,
            kotlin.runCatching { SmallNumberSet.empty() + -1 }.exceptionOrNull()!!::class
        )
        assertEquals(
            IllegalArgumentException::class,
            kotlin.runCatching { SmallNumberSet.empty() + -2 }.exceptionOrNull()!!::class
        )
    }

    @Test
    fun `minus removes element`() {
        for (i in -5..65) {
            val result = SmallNumberSet.all() - i

            for (j in 0..63) {
                assertEquals("i=$i j=$j", j != i, j in result)
            }
        }
    }

    @Test
    fun `isEmpty works`() {
        assertTrue(SmallNumberSet.empty().isEmpty())
        assertFalse(SmallNumberSet.all().isEmpty())
        assertFalse((SmallNumberSet.empty() + 12).isEmpty())
        assertTrue((SmallNumberSet.empty() + 12 - 12).isEmpty())
    }

}
