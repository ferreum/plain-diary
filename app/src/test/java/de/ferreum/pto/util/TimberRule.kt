package de.ferreum.pto.util

import org.junit.rules.MethodRule
import org.junit.runners.model.FrameworkMethod
import org.junit.runners.model.Statement
import timber.log.Timber
import java.io.PrintWriter
import java.io.StringWriter

class TimberPrintRule : MethodRule {
    override fun apply(base: Statement, method: FrameworkMethod?, target: Any?): Statement {
        return object : Statement() {
            override fun evaluate() {
                Timber.plant(PrintingTree)
                try {
                    base.evaluate()
                } finally {
                    Timber.uproot(PrintingTree)
                }
            }
        }
    }
}

object PrintingTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        println("$priority $tag: $message")
        t?.let {
            val sw = StringWriter()
            t.printStackTrace(PrintWriter(sw))
            sw.toString().lines().forEach { println("$priority $tag:   $it") }
        }
    }
}
