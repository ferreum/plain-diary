package de.ferreum.pto.util

import android.text.Layout
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Test

class ViewExtTest {

    @Test
    fun `given text fits the scroll view, then it scrolls to top`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 5,
                lineLength = 10,
            )
            layout.withTextViewSelection(30, 30) { textView ->

                assertEquals(0, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    @Test
    fun `given second to last line is selected, then it scrolls to bottom`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 20,
                lineLength = 10,
            )
            layout.withTextViewSelection(190, 190) { textView ->

                assertEquals(99, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    @Test
    fun `given fifth line is selected while scrolled down, then it scrolls to selection start`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 20,
                lineLength = 10,
            )
            scrollTo(0, 99)
            layout.withTextViewSelection(50, 50) { textView ->

                assertEquals(30, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    @Test
    fun `given first line is selected, then it scrolls to very top`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 20,
                lineLength = 10,
            )
            layout.withTextViewSelection(2, 5) { textView ->

                assertEquals(0, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    @Test
    fun `given last line is selected, then it scrolls to very bottom`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 20,
                lineLength = 10,
            )
            layout.withTextViewSelection(195, 198) { textView ->

                assertEquals(99, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    @Test
    fun `given additional line fits, then it scrolls to line below end`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 20,
                lineLength = 10,
            )
            scrollTo(0, 0)
            layout.withTextViewSelection(100, 180) { textView ->

                assertEquals(89, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    @Test
    fun `given two additional lines fit, then it scrolls to line above start`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 20,
                lineLength = 10,
            )
            scrollTo(0, 99)
            layout.withTextViewSelection(110, 180) { textView ->

                assertEquals(90, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    @Test
    fun `given line below and additional margin does not fit, then it scrolls to end without margin`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 20,
                lineLength = 10,
                bottomPadding = 2,
            )
            scrollTo(0, 0)
            layout.withTextViewSelection(100, 180, paddingBottom = 3) { textView ->

                assertEquals(84, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    @Test
    fun `given line below and additional margin can be included, then it scrolls to end with margin`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 20,
                lineLength = 10,
                bottomPadding = 2,
            )
            scrollTo(0, 0)
            layout.withTextViewSelection(120, 180, paddingBottom = 3) { textView ->

                assertEquals(94, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    @Test
    fun `given line above start and additional margin can be included, then it scrolls to top with margin`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 20,
                lineLength = 10,
                topPadding = -2,
                bottomPadding = 8,
            )
            scrollTo(0, 99)
            layout.withTextViewSelection(80, 80, paddingTop = 3, paddingBottom = 8) { textView ->

                // Note: top padding negates itself when scrolling up
                assertEquals(60, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    @Test
    fun `given selection is near bottom and view is large enough, then it scrolls to bottom with margin`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 20,
                lineLength = 10,
                topPadding = -4,
                bottomPadding = 2
            )
            scrollTo(0, 0)
            layout.withTextViewSelection(190, 190, paddingTop = 4, paddingBottom = 3) { textView ->

                assertEquals(112, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    @Test
    fun `given selection is larger than view, then it scrolls to end of selection`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 20,
                lineLength = 10,
            )
            layout.withTextViewSelection(11, 150) { textView ->

                assertEquals(49, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    @Test
    fun `given selection is already visible and in middle of text, then it does not scroll`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 80,
                lineLength = 10,
            )
            scrollTo(0, 89)
            layout.withTextViewSelection(170, 170) { textView ->

                assertEquals(null, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    @Test
    fun `given selection is visible but not bottom margin, then it scrolls to bottom margin`() {
        withMockScrollView(height = 100) {
            val layout = createMockLayout(
                lineHeight = 9,
                lineSpace = 1,
                lineCount = 80,
                lineLength = 10,
                topPadding = -4,
                bottomPadding = 2,
            )
            scrollTo(0, 97)
            layout.withTextViewSelection(180, 180, paddingTop = 4, paddingBottom = 3) { textView ->

                assertEquals(102, getWantedSelectionScrollPosition(textView))
            }
        }
    }

    private inline fun withMockScrollView(
        height: Int,
        block: NestedScrollView.() -> Unit,
    ) {
        var scrollPosition = 0 to 0
        mockk<NestedScrollView> {
            every { this@mockk.height } returns height
            every { scrollTo(any(), any()) } answers {
                scrollPosition = firstArg<Int>() to secondArg()
            }
            every { scrollX } answers { scrollPosition.first }
            every { scrollY } answers { scrollPosition.second }
        }.block()
    }

    private fun createMockLayout(
        lineHeight: Int,
        lineSpace: Int,
        lineCount: Int,
        lineLength: Int,
        /** Note: [Layout.getTopPadding] returns a negative value to make things confusing. */
        topPadding: Int = 0,
        bottomPadding: Int = 0,
    ): Layout {
        fun checkBounds(line: Int) {
            if (line < 0 || line >= lineCount) {
                throw ArrayIndexOutOfBoundsException(line)
            }
        }
        return mockk {
            every { this@mockk.lineCount } returns lineCount
            every { getLineForOffset(any()) } answers {
                val line = (firstArg<Int>() - 1) / lineLength
                line.coerceIn(0, lineCount)
            }
            every { getLineTop(any()) } answers {
                val line = firstArg<Int>()
                checkBounds(line)
                (lineHeight + lineSpace) * line
            }
            every { getLineBottom(any()) } answers {
                val line = firstArg<Int>()
                checkBounds(line)
                (lineHeight + lineSpace) * line + lineHeight
            }
            every { height } returns (lineHeight + lineSpace) * lineCount - lineSpace
            every { this@mockk.topPadding } returns topPadding
            every { this@mockk.bottomPadding } returns bottomPadding
        }
    }

    private inline fun Layout.withTextViewSelection(
        selectionStart: Int,
        selectionEnd: Int,
        paddingTop: Int = 0,
        paddingBottom: Int = 0,
        block: (TextView) -> Unit,
    ) {
        val textView = mockk<TextView> {
            every { this@mockk.layout } returns this@withTextViewSelection
            every { this@mockk.selectionStart } returns selectionStart
            every { this@mockk.selectionEnd } returns selectionEnd
            every { top } returns 0
            every { this@mockk.paddingTop } returns paddingTop
            every { this@mockk.paddingBottom } returns paddingBottom
        }
        block(textView)
    }

}
