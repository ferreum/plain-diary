package de.ferreum.pto.util

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore

inline fun <reified VM : ViewModel> ViewModelStore.testCreate(
    crossinline block: () -> VM
) = ViewModelProvider(this, object : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        require(modelClass == VM::class.java) { "invalid viewModel class: $modelClass" }
        return modelClass.cast(block())!!
    }
})[VM::class.java]
