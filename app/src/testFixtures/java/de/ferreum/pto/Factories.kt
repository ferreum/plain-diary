package de.ferreum.pto

import android.net.Uri
import de.ferreum.pto.backup.FolderImportParams
import de.ferreum.pto.preferences.FolderLocation
import de.ferreum.pto.preferences.ZipFileLocation
import java.io.File

fun folderImportParams(
    zipFileLocation: ZipFileLocation = ZipFileLocation(Uri.parse("content://backup56.zip")),
    destFolder: FolderLocation = FolderLocation.ExternalStorage(File("/default/dest/folder")),
) = FolderImportParams(
    zipFileLocation = zipFileLocation,
    destFolder = destFolder,
    overwrite = true,
    tryMove = true,
)
