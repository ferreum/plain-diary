package de.ferreum.pto

import android.content.SharedPreferences

class InMemorySharedPreferences : SharedPreferences {
    private val values = mutableMapOf<String?, Any?>()

    private val listeners = mutableSetOf<SharedPreferences.OnSharedPreferenceChangeListener>()

    private inline fun <reified T> getOrDefault(key: String?, defaultValue: T): T {
        return if (key in values) {
            values[key] as T
        } else {
            defaultValue
        }
    }

    override fun getAll(): MutableMap<String?, *> {
        return values.toMutableMap()
    }

    override fun getString(key: String?, defValue: String?): String? = getOrDefault(key, defValue)
    override fun getStringSet(key: String?, defValues: MutableSet<String>?): MutableSet<String>? =
        getOrDefault(key, defValues)
    override fun getInt(key: String?, defValue: Int): Int = getOrDefault(key, defValue)
    override fun getLong(key: String?, defValue: Long): Long = getOrDefault(key, defValue)
    override fun getFloat(key: String?, defValue: Float): Float = getOrDefault(key, defValue)
    override fun getBoolean(key: String?, defValue: Boolean): Boolean = getOrDefault(key, defValue)
    override fun contains(key: String?): Boolean = key in values
    override fun edit(): SharedPreferences.Editor = InMemoryEditor()

    override fun registerOnSharedPreferenceChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener?) {
        listener?.let { listeners.add(it) }
    }

    override fun unregisterOnSharedPreferenceChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener?) {
        listener?.let { listeners.remove(it) }
    }

    inner class InMemoryEditor : SharedPreferences.Editor {
        private val modified = mutableMapOf<String?, Any?>()
        private var doClear = false

        override fun putString(key: String?, value: String?): SharedPreferences.Editor {
            modified[key] = value
            return this
        }

        override fun putStringSet(
            key: String?,
            values: MutableSet<String>?
        ): SharedPreferences.Editor {
            modified[key] = values
            return this
        }

        override fun putInt(key: String?, value: Int): SharedPreferences.Editor {
            modified[key] = value
            return this
        }

        override fun putLong(key: String?, value: Long): SharedPreferences.Editor {
            modified[key] = value
            return this
        }

        override fun putFloat(key: String?, value: Float): SharedPreferences.Editor {
            modified[key] = value
            return this
        }

        override fun putBoolean(key: String?, value: Boolean): SharedPreferences.Editor {
            modified[key] = value
            return this
        }

        override fun remove(key: String?): SharedPreferences.Editor {
            modified[key] = REMOVED
            return this
        }

        override fun clear(): SharedPreferences.Editor {
            doClear = true
            return this
        }

        override fun commit(): Boolean {
            val changed = modified.keys.toMutableSet()
            if (doClear) {
                changed.addAll(values.keys)
                values.clear()
            }
            modified.entries.forEach { (key, value) ->
                if (key == REMOVED) {
                    values.remove(key)
                } else {
                    values[key] = value
                }
            }
            changed.forEach { key ->
                listeners.forEach {
                    it.onSharedPreferenceChanged(this@InMemorySharedPreferences, key)
                }
            }
            return true
        }

        override fun apply() {
            commit()
        }
    }

    companion object {
        private val REMOVED = Any()
    }
}
