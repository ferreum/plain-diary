package de.ferreum.pto.preferences

import kotlinx.coroutines.flow.MutableStateFlow
import java.time.LocalDate
import java.time.LocalTime

class FakePtoPreferencesRepository : PtoPreferencesRepository {
    override val preferencesFlow = MutableStateFlow(
        PtoPreferences(
            isIndexPageEnabled = false,
            isTemplateEnabled = false,
            isAutostartQuickNotes = false,
            isQuickNotesExpansionEnabled = true,
            isTextClassifierEnabled = true,
            isSupportEmojiEnabled = true,
            isSuggestionsEnabled = true,
            editorFontSize = 18,
            isIsoDateFormatEnabled = false,
            indexPageDate = LocalDate.of(2000, 1, 2),
            templatePageDate = LocalDate.of(2000, 1, 1),
            customNotificationTitle = null,
            useInvisibleNotificationIcon = false,
            darkmode = "system",
            newpageTime = LocalTime.MIDNIGHT,
            isEventRemindersEnabled = false,
            isLogfileEnabled = false,
            logfileDate = LocalDate.of(2000, 1, 3),
        )
    )

    val preferences by preferencesFlow::value
}
