package de.ferreum.pto.util

import java.io.File

fun File.getFileNameContentMap(): Map<String, String> {
    return walk().asSequence()
        .filter { it.isFile }
        .associate { file -> file.toRelativeString(this) to file.readText() }
}
