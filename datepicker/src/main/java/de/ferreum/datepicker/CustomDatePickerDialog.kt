////////////////////////////////////////////////////////////////////////////////
//
//  CustomCalendarDialog - Custom calendar dialog for Android
//
//  Copyright © 2017  Bill Farmer
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////
package de.ferreum.datepicker

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.YearMonth
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class CustomDatePickerDialog @JvmOverloads constructor(
        context: Context,
        themeResId: Int = 0,
        private val listener: OnDateSetListener?,
        private val startDate: LocalDate,
        dayDecorators: List<DayDecorator> = emptyList(),
        monthDecorators: List<MonthDecorator> = emptyList(),
        dateFormatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL),
) : AlertDialog(context, themeResId), DatePickerListener, DialogInterface.OnClickListener {

    val datePickerView: DatePickerView

    var dateFormatter = dateFormatter
        set(value) {
            if (value !== field) {
                field = value
                updateTitle(datePickerView.selectedDate ?: startDate)
            }
        }

    override fun onDateSelected(date: LocalDate) {
        updateTitle(date)
    }

    override fun onMonthChanged(month: YearMonth) {
    }

    override fun onSelectedDateClicked(date: LocalDate) {
        listener?.onDateSet(date)
        dismiss()
    }

    override fun onClick(dialog: DialogInterface, which: Int) {
        when (which) {
            BUTTON_POSITIVE -> listener?.onDateSet(datePickerView.selectedDate ?: startDate)
        }
    }

    init {
        updateTitle(startDate)
        setButton(BUTTON_POSITIVE, getContext().getString(android.R.string.ok), this)
        setButton(BUTTON_NEGATIVE, getContext().getString(android.R.string.cancel), this)
        val view = LayoutInflater.from(getContext()).inflate(R.layout.datepicker_dialog, null)
        setView(view)
        datePickerView = view.findViewById(R.id.calendar)
        datePickerView.dayDecorators = dayDecorators
        datePickerView.monthDecorators = monthDecorators
        datePickerView.listener = this
        datePickerView.firstDayOfWeek = DayOfWeek.MONDAY
        datePickerView.goToMonth(YearMonth.from(startDate))
    }

    private fun updateTitle(date: LocalDate) {
        setTitle(dateFormatter.format(date))
    }

    fun interface OnDateSetListener {
        fun onDateSet(date: LocalDate)
    }

}
