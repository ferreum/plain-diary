/*
 * Copyright (c) 2016 Stacktips {link: http://stacktips.com}.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.ferreum.datepicker

import android.content.Context
import android.graphics.Rect
import android.os.Build
import android.os.Parcelable
import android.util.AttributeSet
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.widget.ListPopupWindow
import androidx.core.content.withStyledAttributes
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.isActive
import kotlinx.coroutines.job
import kotlinx.coroutines.plus
import kotlinx.parcelize.Parcelize
import java.text.DateFormatSymbols
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.YearMonth

class DatePickerView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = R.attr.datepickerStyle,
) : LinearLayout(context, attrs, defStyleAttr) {

    var firstDayOfWeek = DayOfWeek.MONDAY
        set(value) {
            if (value != field) {
                field = value
                updateDayOfWeekLabels()
            }
        }

    var isLastWeekRowAlwaysVisible = false
        set(value) {
            if (value != field) {
                field = value
                updateDaysInMonth()
            }
        }

    var isOverflowVisible = true
        set(value) {
            if (value != field) {
                field = value
                updateDaysInMonth()
            }
        }

    var dayDecorators: List<DayDecorator> = emptyList()
        set(value) {
            field = value
            updateDaysInMonth()
        }

    var monthDecorators: List<MonthDecorator>
        get() = yearMonthAdapter.decorators
        set(value) {
            yearMonthAdapter.decorators = value
        }

    var listener: DatePickerListener? = null

    private lateinit var currentMonth: YearMonth
    var selectedDate: LocalDate? = null
        private set

    private lateinit var weekRows: MutableList<ViewGroup>
    private lateinit var dayViews: MutableList<DayView>
    private val yearMonthText: TextView
    private val yearMonthAdapter = YearMonthAdapter(context)
    private var yearMonthDropDown: YearMonthDropDownWindow

    private var attachedScope: CoroutineScope? = null
    private var currentMonthScope: CoroutineScope? = null

    private val rect = Rect()
    private val coordinates = IntArray(2)

    private val onDayOfMonthClickListener = OnClickListener { view ->
        view as DayView
        if (view.date == selectedDate) {
            listener?.onSelectedDateClicked(view.date)
        } else {
            setSelectedDate(view.date)
        }
    }

    init {
        orientation = VERTICAL
        inflateView(context)

        var dropDownThemeResId = 0
        context.withStyledAttributes(attrs, R.styleable.DatePicker_DatePickerView, defStyleAttr) {
            isLastWeekRowAlwaysVisible = getBoolean(R.styleable.DatePicker_DatePickerView_datepickerAlwaysShowLastRow, false)
            isOverflowVisible = getBoolean(R.styleable.DatePicker_DatePickerView_datepickerShowOverflowDays, true)
            dropDownThemeResId = getResourceId(R.styleable.DatePicker_DatePickerView_datepickerYearMonthDropDownTheme, 0)
        }

        val dropDownContext = if (dropDownThemeResId != 0) {
            ContextThemeWrapper(context, dropDownThemeResId)
        } else {
            context
        }

        yearMonthText = findViewById(R.id.yearMonthText)

        yearMonthDropDown = YearMonthDropDownWindow(dropDownContext, attrs, defStyleAttr)
        yearMonthDropDown.anchorView = yearMonthText
        yearMonthDropDown.setAdapter(yearMonthAdapter)
        yearMonthDropDown.setOnItemClickListener { _, _, position, _ ->
            goToMonth(yearMonthAdapter.getItem(position))
            yearMonthDropDown.dismiss()
        }
        yearMonthText.setOnClickListener {
            if (!yearMonthDropDown.isShowing) {
                showYearMonthDropDown()
            }
        }

        val previous: ImageButton = findViewById(R.id.previousMonth)!!
        val next: ImageButton = findViewById(R.id.nextMonth)!!
        previous.setOnClickListener {
            goToMonth(currentMonth.minusMonths(1))
        }
        next.setOnClickListener {
            goToMonth(currentMonth.plusMonths(1))
        }

        updateDayOfWeekLabels()
        goToMonth(YearMonth.now())
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        attachedScope = CoroutineScope(Dispatchers.Main.immediate + Job())
        updateDaysInMonth()
    }

    override fun onDetachedFromWindow() {
        attachedScope?.cancel()
        super.onDetachedFromWindow()
    }

    override fun onSaveInstanceState(): Parcelable {
        val superState = super.onSaveInstanceState()
        return SavedState(superState, currentMonth, selectedDate, yearMonthDropDown.isShowing)
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        state as SavedState
        super.onRestoreInstanceState(state.superState)

        selectedDate = state.selectedDate
        goToMonth(state.currentMonth)

        if (state.isYearMonthDropDownShown) {
            viewTreeObserver?.let { vto ->
                val listener = object : OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        if (!yearMonthDropDown.isShowing) {
                            showYearMonthDropDown()
                        }
                        viewTreeObserver?.let { vto ->
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                vto.removeOnGlobalLayoutListener(this)
                            } else {
                                @Suppress("DEPRECATION")
                                vto.removeGlobalOnLayoutListener(this)
                            }
                        }
                    }
                }
                vto.addOnGlobalLayoutListener(listener)
            }
        }
    }

    private fun inflateView(context: Context) {
        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.datepicker_view_main, this)
        val weekRowsContainer: ViewGroup = findViewById(R.id.week_rows_container)
        val weekRows = mutableListOf<ViewGroup>()
        val dayViews = mutableListOf<DayView>()
        repeat(NUM_ROWS) {
            val row = inflater.inflate(R.layout.datepicker_view_week_row_container,
                    weekRowsContainer, false) as ViewGroup
            weekRows.add(row)
            repeat(NUM_WEEK_DAYS) {
                val dayView = inflater.inflate(R.layout.datepicker_view_day, row, false) as DayView
                dayView.setOnClickListener(onDayOfMonthClickListener)
                row.addView(dayView)
                dayViews.add(dayView)
            }
            weekRowsContainer.addView(row)
        }
        this.weekRows = weekRows
        this.dayViews = dayViews
    }

    fun goToMonth(currentMonth: YearMonth) {
        this.currentMonth = currentMonth
        yearMonthText.text = yearMonthAdapter.getMonthText(currentMonth)

        updateDaysInMonth()

        listener?.onMonthChanged(currentMonth)
    }

    private fun showYearMonthDropDown() {
        getWindowVisibleDisplayFrame(rect)
        yearMonthText.getLocationOnScreen(coordinates)
        val yOffset = coordinates[1] - rect.top
        yearMonthDropDown.verticalOffset = -yOffset
        yearMonthDropDown.show()
        val position = yearMonthAdapter.getPositionForMonth(currentMonth)
        yearMonthDropDown.setSelection(position)
        yearMonthDropDown.listView!!.setSelectionFromTop(position, yOffset)
    }

    private fun updateDayOfWeekLabels() {
        val weekDaysArray = DateFormatSymbols.getInstance().shortWeekdays
        for (dayOfWeek in DayOfWeek.values()) {
            val index = getDayOfWeekIndex(dayOfWeek)
            val view: TextView = findViewById(weekdays[index])
            view.text = weekDaysArray[dayOfWeek.toJavaCalendarDayOfWeek()]
        }
    }

    private fun updateDaysInMonth() {
        val attachedScope = attachedScope?.takeIf { it.isActive } ?: return

        val firstDay = currentMonth.atDay(1)

        currentMonthScope?.cancel()
        val currentMonthScope = attachedScope + Job(attachedScope.coroutineContext.job)
        this.currentMonthScope = currentMonthScope

        val dayOfMonthIndex = getDayOfWeekIndex(firstDay.dayOfWeek)
        val lastDayOfMonth = currentMonth.lengthOfMonth()
        val firstVisibleDay = firstDay.minusDays(dayOfMonthIndex.toLong())
        val showLastRow = isLastWeekRowAlwaysVisible || lastDayOfMonth + dayOfMonthIndex > TOTAL_CELLS - 7
        var day = firstVisibleDay
        for ((i, dayView) in dayViews.withIndex()) {
            dayView.bind(currentMonthScope, day, dayDecorators)
            val isCurrentMonth = day.isInMonth(currentMonth)
            dayView.isActivated = isCurrentMonth
            dayView.isSelected = day == selectedDate
            if (isCurrentMonth) {
                dayView.isVisible = true
            } else {
                dayView.isInvisible = !isOverflowVisible || (i >= 35 && !showLastRow)
            }
            day = day.plusDays(1)
        }

        weekRows[5].isVisible = showLastRow
    }

    private fun setSelectedDate(date: LocalDate) {
        if (date == selectedDate) return

        selectedDate?.let {
            getDayOfMonthView(it)?.isSelected = false
        }
        selectedDate = date
        getDayOfMonthView(date)?.isSelected = true

        listener?.onDateSelected(date)
    }

    private fun getDayOfMonthView(date: LocalDate): DayView? {
        val firstMonthDay = currentMonth.atDay(1)
        val offset = date.toEpochDay() - firstMonthDay.toEpochDay()
        val index = offset + getDayOfWeekIndex(firstMonthDay.dayOfWeek)
        if (index !in dayViews.indices) return null
        return dayViews[index.toInt()]
    }

    private fun getDayOfWeekIndex(dayOfWeek: DayOfWeek) =
            (dayOfWeek.value - firstDayOfWeek.value + 7) % 7

    private class YearMonthDropDownWindow(
            context: Context,
            attrs: AttributeSet? = null,
            defStyleAttr: Int = 0,
    ) : ListPopupWindow(context, attrs, defStyleAttr) {
        init {
            isModal = true
            inputMethodMode = INPUT_METHOD_NOT_NEEDED
        }

        override fun show() {
            super.show()
            listView!!.choiceMode = ListView.CHOICE_MODE_SINGLE
        }
    }

    @Parcelize
    private data class SavedState(
            val superState: Parcelable?,
            val currentMonth: YearMonth,
            val selectedDate: LocalDate?,
            val isYearMonthDropDownShown: Boolean,
    ) : Parcelable

    companion object {
        private const val NUM_ROWS = 6
        private const val NUM_WEEK_DAYS = 7
        private const val TOTAL_CELLS = NUM_ROWS * NUM_WEEK_DAYS

        private val weekdays = intArrayOf(
                R.id.weekday_1, R.id.weekday_2, R.id.weekday_3,
                R.id.weekday_4, R.id.weekday_5, R.id.weekday_6,
                R.id.weekday_7
        )
    }
}
