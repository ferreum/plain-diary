/*
 * Copyright (c) 2016 Stacktips {link: http://stacktips.com}.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.ferreum.datepicker

import android.content.Context
import android.util.AttributeSet
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.time.LocalDate

internal class DayView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = R.attr.datepickerDayOfMonthStyle,
) : DecorableTextView(context, attrs, defStyleAttr) {

    lateinit var date: LocalDate
        private set

    internal fun bind(scope: CoroutineScope, date: LocalDate, decorators: List<DayDecorator>) {
        this.date = date
        text = date.dayOfMonth.toString()
        clearDecoration()
        for (decorator in decorators) {
            scope.launch {
                applyDecoration(decorator.decorate(date))
            }
        }
    }

}
