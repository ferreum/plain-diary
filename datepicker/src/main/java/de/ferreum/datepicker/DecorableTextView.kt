package de.ferreum.datepicker

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.annotation.StyleRes
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.withStyledAttributes
import androidx.appcompat.R as AppCompatR

open class DecorableTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = android.R.attr.textViewStyle,
) : AppCompatTextView(context, attrs, defStyleAttr) {

    private val normalBackground: Drawable?
    @StyleRes
    private var normalTextAppearance: Int = 0

    init {
        context.withStyledAttributes(attrs, R.styleable.DatePicker_DecorableTextView, defStyleAttr) {
            normalTextAppearance = getResourceId(
                R.styleable.DatePicker_DecorableTextView_android_textAppearance,
                AppCompatR.style.TextAppearance_AppCompat,
            )
        }
        normalBackground = background
    }

    fun clearDecoration() {
        setTextAppearance(normalTextAppearance)
        background = normalBackground
    }

    fun applyDecoration(decoration: Decoration?) {
        when (decoration) {
            null -> {}
            is Decoration.TextAppearance -> setTextAppearance(decoration.styleId)
            is Decoration.Background -> background = decoration.drawable
        }
    }

}
