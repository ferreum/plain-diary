package de.ferreum.datepicker

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import java.text.DateFormatSymbols
import java.time.YearMonth

internal class YearMonthAdapter(val context: Context) : BaseAdapter() {

    var decorators: List<MonthDecorator> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private val monthTextArray = DateFormatSymbols.getInstance().shortMonths

    private val monthFormat = context.getString(R.string.datepicker_title_format)

    override fun getCount() = monthCount

    override fun hasStableIds() = true

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): YearMonth {
        return minMonth.plusMonths(position.toLong())
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val holder = getHolder(convertView, parent)
        holder.bind(getItem(position))
        return holder.view
    }

    private fun getHolder(convertView: View?, parent: ViewGroup): ViewHolder {
        convertView?.let { return it.tag as ViewHolder }
        val view =  LayoutInflater.from(context).inflate(R.layout.datepicker_row_year_month, parent, false)
        val holder = ViewHolder(view)
        view.tag = holder
        return holder
    }

    fun getPositionForMonth(yearMonth: YearMonth): Int {
        return yearMonth.getIndex() - minMonthIndex
    }

    fun getMonthText(yearMonth: YearMonth): CharSequence {
        val monthText = monthTextArray[yearMonth.monthValue - 1]
        return monthFormat.format(monthText, yearMonth.year)
    }

    private inner class ViewHolder(val view: View) {
        private val textView: DecorableTextView = view.findViewById(R.id.textView)

        private var currentMonth: YearMonth? = null
        private var attachedScope: CoroutineScope? = null
        private var decorateJob: Job? = null

        init {
            view.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
                override fun onViewAttachedToWindow(v: View) {
                    attachedScope = CoroutineScope(Dispatchers.Main + Job())
                    currentMonth?.let { bind(it) }
                }

                override fun onViewDetachedFromWindow(v: View) {
                    attachedScope?.cancel()
                }
            })
        }

        fun bind(yearMonth: YearMonth) {
            currentMonth = yearMonth
            textView.text = getMonthText(yearMonth)
            attachedScope?.run {
                textView.clearDecoration()
                decorateJob?.cancel()
                decorateJob = launch {
                    for (decorator in decorators) {
                        launch {
                            textView.applyDecoration(decorator.decorate(yearMonth))
                        }
                    }
                }
            }
        }
    }

    companion object {
        private val minMonth = YearMonth.of(0, 1)
        private val maxMonth = YearMonth.of(9999, 12)
        private val minMonthIndex = minMonth.getIndex()
        private val maxMonthIndex = maxMonth.getIndex()
        private val monthCount = maxMonthIndex - minMonthIndex + 1
    }

}

private fun YearMonth.getIndex() = year * 12 + monthValue
