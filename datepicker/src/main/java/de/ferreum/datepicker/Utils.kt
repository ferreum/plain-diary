package de.ferreum.datepicker

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.YearMonth

internal fun LocalDate.isInMonth(month: YearMonth) = month.year == year && month.month == this.month

internal fun DayOfWeek.toJavaCalendarDayOfWeek() = value % 7 + 1
