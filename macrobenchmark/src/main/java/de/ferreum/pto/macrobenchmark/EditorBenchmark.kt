package de.ferreum.pto.macrobenchmark

import android.os.Build
import android.os.SystemClock
import android.widget.EditText
import android.widget.ImageButton
import androidx.annotation.RequiresApi
import androidx.benchmark.macro.ExperimentalMetricApi
import androidx.benchmark.macro.MacrobenchmarkScope
import androidx.benchmark.macro.PowerCategory
import androidx.benchmark.macro.PowerCategoryDisplayLevel
import androidx.benchmark.macro.PowerMetric
import androidx.benchmark.macro.StartupMode
import androidx.benchmark.macro.junit4.MacrobenchmarkRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import de.ferreum.pto.macrobenchmark.util.dragDown
import de.ferreum.pto.macrobenchmark.util.dragGesture
import de.ferreum.pto.macrobenchmark.util.dragUp
import de.ferreum.pto.macrobenchmark.util.dragUpDown
import de.ferreum.pto.macrobenchmark.util.typeText
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.math.PI
import kotlin.math.sin

@OptIn(ExperimentalMetricApi::class)
@RunWith(AndroidJUnit4::class)
@RequiresApi(Build.VERSION_CODES.Q)
class EditorBenchmark {
    @get:Rule
    val benchmarkRule = MacrobenchmarkRule()

    @Test
    fun typing() = measureRepeated(
        iterations = 5,
        setupBlock = {
            startupEditor()
            setupText("$LOREM_IPSUM\n$LOREM_IPSUM\n$LOREM_IPSUM")
        }
    ) {
        val ui = InstrumentationRegistry.getInstrumentation().uiAutomation
        ui.typeText("\n\n" + LOREM_IPSUM.substring(0, 335))
        pressHome()
    }

    @Test
    fun typingPaged() = measureRepeated(
        iterations = 5,
        setupBlock = {
            startupEditor()
            setupText(LOREM_IPSUM)
            peekPages()
        }
    ) {
        val ui = InstrumentationRegistry.getInstrumentation().uiAutomation
        ui.typeText("\n\n" + LOREM_IPSUM.substring(0, 335))
        pressHome()
    }

    @Test
    fun undoRedo() = measureRepeated(
        iterations = 5,
        setupBlock = {
            startupEditor()
            setupText(LOREM_IPSUM)
            val ui = InstrumentationRegistry.getInstrumentation().uiAutomation
            ui.typeText("\n\n" + LOREM_IPSUM.substring(0, 124))
            pressHome()
            startActivityAndWait()
        }
    ) {
        device.dragDown(device.findObject(By.clazz(ImageButton::class.java).desc("Undo")))
        device.dragUpDown(device.findObject(By.clazz(ImageButton::class.java).desc("Undo")))

        pressHome()
    }

    @Test
    fun timeAdjust() = measureRepeated(
        iterations = 5,
        setupBlock = {
            startupEditor()
            setupText("$LOREM_IPSUM\n$LOREM_IPSUM\n\n")
            pressHome()
            startActivityAndWait()
        }
    ) {
        device.dragUpDown(device.findObject(By.clazz(ImageButton::class.java).desc("Add time")))

        pressHome()
    }

    @Test
    fun reminderProcessing() = measureRepeated(
        iterations = 5,
        setupBlock = {
            startupEditor()
            setupText("$LOREM_IPSUM\n$LOREM_IPSUM\n\n@12:34 aaa\n@12:45 bbb\n@13:34 ccc\n@13:45 ddd\n")
            pressHome()
            startActivityAndWait()
        }
    ) {
        val timeButton = device.findObject(By.clazz(ImageButton::class.java).desc("Add time"))
        repeat(10) {
            device.dragUp(timeButton, 32)
            clickSaveButton()
        }

        pressHome()
    }

    @Test
    fun swipePages() = measureRepeated(
        iterations = 5,
        setupBlock = {
            startupEditor()
        }
    ) {
        repeat(8) {
            swipePrevPage()
        }
        pressHome()
    }

    @Test
    fun idle() = measureRepeated(
        iterations = 5,
        setupBlock = {
            startupEditor()
            setupText(LOREM_IPSUM)
        }
    ) {
        SystemClock.sleep(12000)
        pressHome()
    }

    private fun measureRepeated(
        iterations: Int,
        startupMode: StartupMode? = StartupMode.WARM,
        setupBlock: MacrobenchmarkScope.() -> Unit,
        measureBlock: MacrobenchmarkScope.() -> Unit,
    ) {
        benchmarkRule.measureRepeated(
            packageName = "de.ferreum.pto.profileable",
            metrics = powerMetrics(),
            iterations = iterations,
            startupMode = startupMode,
            setupBlock = setupBlock,
            measureBlock = measureBlock,
        )
    }

    private fun powerMetrics() = listOf(
        PowerMetric(
            PowerMetric.Power(
                mapOf(
                    PowerCategory.CPU to PowerCategoryDisplayLevel.TOTAL,
                    PowerCategory.GPU to PowerCategoryDisplayLevel.TOTAL,
                    PowerCategory.MEMORY to PowerCategoryDisplayLevel.TOTAL,
                )
            )
        )
    )

    private fun MacrobenchmarkScope.clickSaveButton() {
        device.findObject(By.res("de.ferreum.pto.profileable", "saveStatusImage"))
            .click()
    }

    private fun MacrobenchmarkScope.swipePrevPage() {
        val obj = device.findObject(By.clazz(ImageButton::class.java).desc("Add time"))
        device.dragGesture(obj.visibleCenter, 200) {
            xOffset = it * xRange
        }
    }

    private fun MacrobenchmarkScope.peekPages() {
        val obj = device.findObject(By.clazz(ImageButton::class.java).desc("Add time"))
        device.dragGesture(obj.visibleCenter, 1000) {
            xOffset = (sin(it * 2 * PI) * 100f).toFloat() * (1f - it)
        }
    }

    private fun MacrobenchmarkScope.startupEditor() {
        killProcess()
        pressHome()
        startActivityAndWait()
    }

    private fun MacrobenchmarkScope.setupText(text: String) {
        val editText = device.findObject(By.clazz(EditText::class.java).focused(true))
        editText.text = text
    }

    companion object {
        const val LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    }
}
