package de.ferreum.pto.macrobenchmark.util

import android.app.UiAutomation
import android.graphics.Point
import android.os.SystemClock
import android.view.InputDevice
import android.view.KeyCharacterMap
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.MotionEvent.ACTION_DOWN
import android.view.MotionEvent.ACTION_MOVE
import android.view.MotionEvent.ACTION_UP
import android.view.MotionEvent.PointerCoords
import android.view.MotionEvent.PointerProperties
import android.view.MotionEvent.TOOL_TYPE_FINGER
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiObject2
import kotlin.math.PI
import kotlin.math.min
import kotlin.math.sin

fun UiAutomation.typeText(text: String) {
    text.forEach { chr ->
        val key = charToKeycode(chr)
        pressKeyCode(key, if (chr.isUpperCase()) KeyEvent.META_SHIFT_ON else 0)
    }
}

fun UiAutomation.pressKeyCode(keyCode: Int, metaState: Int) {
    val eventTime = SystemClock.uptimeMillis()
    val downEvent = KeyEvent(
        eventTime, eventTime, KeyEvent.ACTION_DOWN,
        keyCode, 0, metaState, KeyCharacterMap.VIRTUAL_KEYBOARD, 0, 0,
        InputDevice.SOURCE_KEYBOARD
    )
    val upEvent = KeyEvent(
        eventTime, eventTime, KeyEvent.ACTION_UP,
        keyCode, 0, metaState, KeyCharacterMap.VIRTUAL_KEYBOARD, 0, 0,
        InputDevice.SOURCE_KEYBOARD
    )
    injectInputEvent(downEvent, false)
    injectInputEvent(upEvent, false)
}

private fun charToKeycode(chr: Char): Int {
    val key = when (chr) {
        ' ' -> KeyEvent.KEYCODE_SPACE
        '\n' -> KeyEvent.KEYCODE_ENTER
        ',' -> KeyEvent.KEYCODE_COMMA
        '.' -> KeyEvent.KEYCODE_PERIOD
        else -> KeyEvent.keyCodeFromString(chr.uppercase())
    }
    return key
}

fun UiDevice.dragUp(uiObject: UiObject2, lengthDp: Int, duration: Long = 200) {
    val density = InstrumentationRegistry.getInstrumentation().targetContext.resources.displayMetrics.density
    dragGesture(uiObject.visibleCenter, duration) {
        yOffset = density * -lengthDp * it
    }
}

fun UiDevice.dragDown(uiObject: UiObject2) {
    dragGesture(uiObject.visibleCenter, 500) {
        yOffset = yRange * it
    }
}

fun UiDevice.dragUpDown(uiObject: UiObject2, duration: Long = 5000L) {
    dragGesture(uiObject.visibleCenter, duration) {
        yOffset = yRange * sin(it * 10 * PI).toFloat()
    }
}

fun UiDevice.dragGesture(origin: Point, duration: Long, offsetFunc: GestureScope.(Float) -> Unit) {
    val ui = InstrumentationRegistry.getInstrumentation().uiAutomation

    val scope = GestureScope(
        dispWidth = displayWidth,
        dispHeight = displayHeight,
        xRange = min(displayWidth - origin.x, origin.x) * .9f,
        yRange = min(displayHeight - origin.y, origin.y) * .9f,
        ui = ui,
    )

    val start = SystemClock.uptimeMillis()

    val coords = listOf(PointerCoords())
    coords[0].apply {
        x = origin.x.toFloat()
        y = origin.y.toFloat()
    }

    ui.injectInputEvent(getMotionEvent(start, start, ACTION_DOWN, coords), false)

    var now: Long
    do {
        now = SystemClock.uptimeMillis()

        scope.offsetFunc((now - start) / duration.toFloat())

        coords[0].apply {
            x = origin.x + scope.xOffset
            y = origin.y + scope.yOffset
        }

        ui.injectInputEvent(getMotionEvent(start, now, ACTION_MOVE, coords), false)
        SystemClock.sleep(16)
    } while (now - start < duration)

    ui.injectInputEvent(getMotionEvent(start, now, ACTION_UP, coords), false)
}

data class GestureScope(
    val dispWidth: Int,
    val dispHeight: Int,
    val xRange: Float,
    val yRange: Float,
    val ui: UiAutomation,
    var xOffset: Float = 0f,
    var yOffset: Float = 0f,
)

/** Helper function to obtain a MotionEvent. */
private fun getMotionEvent(
    downTime: Long, eventTime: Long, action: Int,
    coordinates: List<PointerCoords>,
): MotionEvent {
    val props = coordinates.map { PointerProperties() }.toTypedArray()
    props[0].id = 1
    props[0].toolType = TOOL_TYPE_FINGER
    val coords = coordinates.toTypedArray()
    val ev = MotionEvent.obtain(
        downTime, eventTime, action, props.size, props, coords,
        0, 0, 1f, 1f, 0, 0, InputDevice.SOURCE_TOUCHSCREEN, 0
    )
    return ev
}
