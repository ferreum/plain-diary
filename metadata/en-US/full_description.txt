To-do list, notes, appointment book, diary, journal, and… alarm clock? All possible with one plaintext file per day. PTO brings a convenient editor and important additional functions.

<b>Features</b>

• Powerful text editor with undo/redo, autosave, and more.
• Quickly take notes on the lockscreen, without unlocking your device.
• Reminders for upcoming events (format: <tt>@12:34 -15m foobar</tt>)
• Fast search through all pages.
• Save and open links to other apps and webpages.
• Uses no network connection. All content is local on the device.
• Optional template for new pages.
• Backup/import pages to/from ZIP file.
• And more, see the help: https://gitlab.com/ferreum/pto/-/blob/master/app/src/main/assets/help.md

<b>Origin</b>

This is a (now distant) fork of billthefarmer's Diary: https://github.com/billthefarmer/diary

PTO has a lot of internal and UI changes. It focuses on usage as a general organizer instead of a Diary, though it can still be used as such, if you don't need the removed features. Most notably the markdown view has been removed, so no inline media can be displayed.

See the help file for more differences: https://gitlab.com/ferreum/pto/-/blob/master/app/src/main/assets/help.md#origin
