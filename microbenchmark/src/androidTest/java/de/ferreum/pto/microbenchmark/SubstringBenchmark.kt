package de.ferreum.pto.microbenchmark

import android.text.SpannableStringBuilder
import androidx.benchmark.junit4.BenchmarkRule
import androidx.benchmark.junit4.measureRepeated
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.random.Random

@RunWith(AndroidJUnit4::class)
class SubstringBenchmark {

    @get:Rule
    val benchmarkRule = BenchmarkRule()

    private var startIndex = 500 // randomized
    private var endIndex = 560 // randomized

    private val random = Random(1337)

    private val spannableBuilder = SpannableStringBuilder()
    init {
        spannableBuilder.append(LOREM_IPSUM)
        spannableBuilder.append(LOREM_IPSUM)
        spannableBuilder.append(LOREM_IPSUM)
        spannableBuilder.append(LOREM_IPSUM)
        spannableBuilder.append(LOREM_IPSUM)
        spannableBuilder.append(LOREM_IPSUM)
    }

    private fun randomize() {
        val length = random.nextInt(20, 200)
        val index = random.nextInt(0, spannableBuilder.length - length)
        startIndex = index
        endIndex = index + length
    }

    @Test
    fun fullToString() {
        var result = ""
        benchmarkRule.measureRepeated {
            result = spannableBuilder.toString()
        }
        assertEquals(spannableBuilder.toString(), result)
    }

    @Test
    fun fullGetChars() {
        var result = ""
        benchmarkRule.measureRepeated {
            val chars = CharArray(spannableBuilder.length)
            spannableBuilder.getChars(0, spannableBuilder.length, chars, 0)
            result = String(chars)
        }
        assertEquals(spannableBuilder.toString(), result)
    }

    @Test
    fun substring() {
        var result = ""
        benchmarkRule.measureRepeated {
            runWithTimingDisabled { randomize() }
            result = spannableBuilder.substring(startIndex, endIndex)
        }
        assertEquals(spannableBuilder.substring(startIndex, endIndex), result)
    }

    @Test
    fun stringBuilder() {
        var result = ""
        benchmarkRule.measureRepeated {
            runWithTimingDisabled { randomize() }
            result = buildString { append(spannableBuilder, startIndex, endIndex) }
        }
        assertEquals(spannableBuilder.substring(startIndex, endIndex), result)
    }

    @Test
    fun getChars() {
        var result = ""
        benchmarkRule.measureRepeated {
            runWithTimingDisabled { randomize() }
            val chars = CharArray(endIndex - startIndex)
            spannableBuilder.getChars(startIndex, endIndex, chars, 0)
            result = String(chars)
        }
        assertEquals(spannableBuilder.substring(startIndex, endIndex), result)
    }

    companion object {
        const val LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    }
}
